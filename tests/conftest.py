# pylint: disable=missing-module-docstring
import ska_csp_lmc_common  # pylint: disable=unused-import # noqa F401

pytest_plugins = [
    "ska_csp_lmc_common.testing.plugins.device_plugin",
    "ska_csp_lmc_common.testing.plugins.common_plugin",
    "ska_csp_lmc_common.testing.test_bdd_utils",
]
