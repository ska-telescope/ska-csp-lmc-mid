# ------------- Test rejection of commands with wrong inputs -------------------

    @pipeline
    Background:
        Given All subsystems are fresh initialized
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
    
    @pipeline @XTP-32720
    Scenario:  AssignResources rejected on Subarray01 without PST beams

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 state is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY

        When AssignResources is issued on CspSubarray01 without PST (invalid input)
        Then CspSubarray01 reject the command and raises an exception

    @pipeline @XTP-32721
    Scenario:  Configure rejected on IDLE Subarray01 without PST beams

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 state is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE

        When Configure is issued on CspSubarray01 without PST (invalid input)
        Then CspSubarray01 reject the command and raises an exception

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('releaseallresources', '0')
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        #And CspSubarray01 pssSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 assignedReceptors is ()
        #And CspSubarray01 assignedResources is ()

        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')        
        And CspController CspCbfState is OFF
        And CspController state is OFF
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 state is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY
        And CspController commandResult is ('off', '0')

    @pipeline @XTP-32722
    Scenario:  Configure rejected on READY Subarray01 without PST beams

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 state is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_ver3.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('configure', '0')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspSubarray01 configurationID is "sbi-mvp01-20200325-00001-science_A"
        
        When Configure is issued on CspSubarray01 without PST (invalid input)
        Then CspSubarray01 reject the command and raises an exception
    
        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('releaseallresources', '0')
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        #And CspSubarray01 pssSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 assignedReceptors is ()
        #And CspSubarray01 assignedResources is ()

        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')        
        And CspController CspCbfState is OFF
        And CspController state is OFF
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 state is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY
        And CspController commandResult is ('off', '0')    