Feature: Csp-Lmc Mid Subarray On and Off
    As the Csp-Lmc Subarray for Mid telescope
    I want to turn On and Off Cbf Subarray
    and change my state accordingly

    
    # Scenario: Csp Subarray is powered ON
    #     Given Subarray01 is fresh initialized

    #     When On Command is issued on CspSubarray01 (no argument)
    #     Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
    #     And CspSubarray01 commandResult is ('on', '0')
    #     And CbfSubarray01 state is ON
    #     And CspSubarray01 state is ON
    #     And CspSubarray01 cbfSubarrayState is ON

    
    Scenario: Csp Subarray is powered OFF if ON/EMPTY
        Given Subarray01 is fresh initialized
        
        When On Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('on', '0')

        When Off Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('off', '0')
        And CbfSubarray01 state is OFF
        And CspSubarray01 state is OFF
        And CspSubarray01 cbfSubarrayState is OFF

    
    Scenario: Csp Subarray is powered OFF if only CBF is IDLE
        Given Subarray01 is fresh initialized
        
        When On Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('on', '0')
        And AddReceptors Command is issued on CbfSubarray01 with argument ["SKA001","SKA022"]
        And CbfSubarray01 ObsState is IDLE
        And CspSubarray01 ObsState is IDLE

        When Off Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('off', '0')
        And CbfSubarray01 state is OFF
        And CspSubarray01 state is OFF
        And CbfSubarray01 ObsState is EMPTY
        And CspSubarray01 ObsState is EMPTY
        And CspSubarray01 cbfSubarrayState is OFF
        And CspSubarray01 cbfSubarrayObsState is EMPTY


    Scenario: Csp Subarray OFF if IDLE
        All subsystems are fresh initialized
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('on', '0')
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 pssSubarrayState is ON
        And CbfSubarray01 ObsState are sent straight to IDLE
        And PssSubarray01 ObsState are sent straight to IDLE        
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE

        When Off Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('off', '0')
        And CbfSubarray01 state is OFF
        And CspSubarray01 state is OFF
        And CbfSubarray01 ObsState is EMPTY
        And CspSubarray01 ObsState is EMPTY
        And CspSubarray01 cbfSubarrayState is OFF
        And CspSubarray01 cbfSubarrayObsState is EMPTY

    
    Scenario: Csp Subarray OFF if READY
        All subsystems are fresh initialized
        And Vcc001 is initialized
        And Vcc002 is initialized
        
        When On Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('on', '0')
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 pssSubarrayState is ON
        And CbfSubarray01 ObsState are sent straight to READY
        And PssSubarray01 ObsState are sent straight to READY        
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 pssSubarrayObsState is READY
        And CspSubarray01 obsState is READY

        When Off Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('off', '0')
        And CbfSubarray01 state is OFF
        And CspSubarray01 state is OFF
        And CbfSubarray01 ObsState is EMPTY
        And CspSubarray01 ObsState is EMPTY
        And CspSubarray01 cbfSubarrayState is OFF
        And CspSubarray01 cbfSubarrayObsState is EMPTY

    
    Scenario: Csp Subarray OFF if SCANNING
        All subsystems are fresh initialized
        And Vcc001 is initialized
        And Vcc002 is initialized
        #And Vcc are properly registered
        
        When On Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('on', '0')
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 pssSubarrayState is ON
        And CbfSubarray01 ObsState are sent straight to SCANNING
        And PssSubarray01 ObsState are sent straight to SCANNING        
        And CspSubarray01 cbfSubarrayObsState is SCANNING
        And CspSubarray01 pssSubarrayObsState is SCANNING
        And CspSubarray01 obsState is SCANNING

        When Off Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('off', '0')
        And CbfSubarray01 state is OFF
        And CspSubarray01 state is OFF
        And CbfSubarray01 ObsState is EMPTY
        And CspSubarray01 ObsState is EMPTY
        And CspSubarray01 cbfSubarrayState is OFF
        And CspSubarray01 cbfSubarrayObsState is EMPTY

    # The subarray On/OFF command has been removed
    # @pipeline @XTP-35559
    # Scenario: Csp Subarray OFF if ABORTED
    #     All subsystems are fresh initialized
    #     And Vcc001 is initialized
    #     And Vcc002 is initialized
    #     And On Command is issued on CspSubarray01 (no argument)
    #     And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
    #     And CspSubarray01 commandResult is ('on', '0')
    #     And CspSubarray01 cbfSubarrayState is ON
    #     And CspSubarray01 pssSubarrayState is ON        
    #     And CbfSubarray01 ObsState are sent straight to ABORTED
    #     And PssSubarray01 ObsState are sent straight to ABORTED        
    #     And CspSubarray01 cbfSubarrayObsState is ABORTED
    #     And CspSubarray01 pssSubarrayObsState is ABORTED
    #     And CspSubarray01 obsState is ABORTED

    #     When Off Command is issued on CspSubarray01 (no argument)
    #     Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
    #     And CspSubarray01 commandResult is ('off', '0')
    #     And CbfSubarray01 state is OFF
    #     And CspSubarray01 state is OFF
    #     And CbfSubarray01 ObsState is EMPTY
    #     And CspSubarray01 ObsState is EMPTY
    #     And CspSubarray01 cbfSubarrayState is OFF
    #     And CspSubarray01 cbfSubarrayObsState is EMPTY
