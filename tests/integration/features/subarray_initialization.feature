Feature: Csp-Lmc Mid Subarray initialization
    As the Csp-Lmc Subarray for Mid telescope
    1) I want to connect to Cbf Subarray
    and report its states and modes
    2) I want to subscribe the changes in states and mode of Cbf Subarray
    and change my state accordingly

    @pipeline @XTP-33525
    Scenario: Csp Subarray is initialized with success
        Given Subarray01 is fresh initialized

        Then CspSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CspSubarray01 AdminMode is ONLINE
        And CspSubarray01 HealthState is UNKNOWN

        And CbfSubarray01 state is ON
        And CbfSubarray01 ObsState is EMPTY
        And CbfSubarray01 AdminMode is ONLINE
        And CbfSubarray01 HealthState is UNKNOWN

        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 cbfSubarrayAdminMode is ONLINE
        And CspSubarray01 cbfSubarrayHealthState is UNKNOWN

    
    # Scenario: Cbf Subarray is turned On
    #     Given Subarray01 is fresh initialized

    #     When On Command is issued on CbfSubarray01 (no argument)
    #     Then CbfSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
    #     And CbfSubarray01 state is ON
    #     And CspSubarray01 state is ON
    #     And CspSubarray01 cbfSubarrayState is ON

    
    Scenario: Cbf Subarray changes ObsState to IDLE
        Given Subarray01 is fresh initialized
        
        When On Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('on', '0')

        When AddReceptors Command is issued on CbfSubarray01 with argument ["SKA001","SKA022"]
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CbfSubarray01 ObsState is IDLE
        And CspSubarray01 ObsState is IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
