Feature: Csp-Lmc Mid Controller commands
    As the Csp-Lmc controller for Mid telescope
    I want to send the basic commands to sub-elements
    
    @pipeline @XTP-35914
    Scenario: Csp is powered On with success
        Given All subsystems are fresh initialized
        And CspController isCommunicating is True
        And CspController set cbfSimulationMode to TRUE
        And CspController SimulationMode is FALSE
        And CspController state is OFF
        And CbfController state is OFF
        And CbfSubarray01 state is ON
        And CspSubarray01 state is ON
        And CspSubarray02 state is ON

        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController alarmDishIdCfg is 0

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController commandResult is ('on', '0')
        And CspController state is ON
        And CbfController state is ON


    @pipeline @XTP-29916
    Scenario: Csp is powered On with success after dish id vcc map fails and then it is loaded properly
        Given All subsystems are fresh initialized
        And CspController isCommunicating is True
        And CspController state is OFF
        And CbfController state is OFF
        And CbfSubarray01 state is ON
        And CspSubarray01 state is ON
        And CspSubarray02 state is ON

        And CspController set cbfSimulationMode to TRUE
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg_invalid.json)
        And CspController longRunningCommandAttributes is (3, 'FAILED')
        And CspController alarmDishIdCfg is 1
        And CspSubarray01 healthstate is FAILED
        And CspSubarray02 healthstate is FAILED

        When LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController alarmDishIdCfg is 0
        And CspSubarray01 healthstate is UNKNOWN
        And CspSubarray02 healthstate is UNKNOWN

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController commandResult is ('on', '0')
        And CspController state is ON
        And CbfController state is ON
 

    @pipeline @XTP-29707
    Scenario: Csp is powered On with success after dish id vcc map is loaded
        Given All subsystems are fresh initialized
        And CspController isCommunicating is True
        And CspController set cbfSimulationMode to TRUE
        And CspController state is OFF
        And CbfController state is OFF
        And CbfSubarray01 state is ON
        And CspSubarray01 state is ON
        And CspSubarray02 state is ON
  
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController dishVccConfig is {"interface":"https://schema.skao.int/ska-mid-cbf-initsysparam/1.0","dish_parameters":{"SKA001":{"vcc":1,"k":11},"SKA036":{"vcc":2,"k":101},"SKA063":{"vcc":3,"k":1127},"SKA100":{"vcc":4,"k":620}}}

        
        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController commandResult is ('on', '0')
        And CspController state is ON
        And CbfController state is ON
   

    @pipeline @XTP-29706
    Scenario: Csp is powered On with success after dish id vcc map is loaded from car
        Given All subsystems are fresh initialized
        And CspController isCommunicating is True
        And CspController set cbfSimulationMode to TRUE

        And CspController state is OFF
        And CbfController state is OFF
        And CbfSubarray01 state is ON
        And CspSubarray01 state is ON
        And CspSubarray02 state is ON

        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg_car.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController sourceDishVccConfig is '{"interface":"https://schema.skao.int/ska-mid-cbf-initsysparam/1.0","tm_data_sources":["car://gitlab.com/ska-telescope/ska-telmodel-data?ska-sdp-tmlite-repository-1.0.0#tmdata"],"tm_data_filepath":"instrument/ska1_mid_psi/ska-mid-cbf-system-parameters.json"}'
        When On Command is issued on CspController with argument []

        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController commandResult is ('on', '0')
        And CspController state is ON
        And CbfController state is ON

    
    @pipeline @XTP-28269
    Scenario: Csp On fails in simulationMode False without hw
        Given All subsystems are fresh initialized
        And CspController isCommunicating is True
        And CspController state is OFF
        And CbfController state is OFF
        And CbfSubarray01 state is ON
        And CspSubarray01 state is ON
        And CspSubarray02 state is ON
 
        When CspController set cbfSimulationMode to FALSE
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController alarmDishIdCfg is 0
        And On Command is issued on CspController with argument []

        Then CspController longRunningCommandAttributes is (3, 'FAILED')
        And CspController commandResult is ('on', '3')
        And CspController state is OFF
        And CbfController state is OFF
        And CspSubarray01 state is ON
        And CspSubarray02 state is ON
        When CspController set cbfSimulationMode to TRUE
        
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON
        And CbfController state is ON

    # The CSP is not STANDBY at the initialisation and it cannot move to it
    # Scenario: Csp is powered Off with success, when it is in STANDBY state
    #     Given All subsystems are fresh initialized
    #     And CspController isCommunicating is True
    #     And CspController state is OFF
    #     And CbfController state is OFF
    #     And CspSubarray01 state is ON
    #     And CspSubarray02 state is ON

    #     When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']

    #     Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
    #     And CspController commandResult is ('off', '0')
    #     And CspController state is OFF
    #     And CbfController state is OFF
    #     # subarrays stay always ON
    #     And CspSubarray01 state is ON
    #     And CspSubarray02 state is ON

    @pipeline
    Scenario: Csp controller is powered STANDBY but command is REJECTED
        Given All subsystems are fresh initialized
        And CspController isCommunicating is True
        And CspController state is OFF
        And CbfController state is OFF
        And CspSubarray01 state is ON
        And CspSubarray02 state is ON

        And CspController set cbfSimulationMode to TRUE
        And CspController SimulationMode is FALSE
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController alarmDishIdCfg is 0
        When On Command is issued on CspController with argument []

        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController commandResult is ('on', '0')
        And CspController state is ON
        And CbfController state is ON
        And CspSubarray01 state is ON
        And CspSubarray02 state is ON

       When Standby Command is issued on CspController with argument []
       #    Then CspController longRunningCommandAttributes is (5, 'COMPLETED')
       #    Then CspController commandResult is ('standby', '5')
        # the standby command is rejected, so the device attributes do not change
       Then CspController state is ON
       And CbfController state is ON
       And CspSubarray01 state is ON
       And CspSubarray02 state is ON

    @pipeline
    Scenario: Csp is powered Standby when it is in OFF state, but command is REJECTED
        Given All subsystems are fresh initialized
        And CspController isCommunicating is True
        And CspController set cbfSimulationMode to TRUE
        And CspController SimulationMode is FALSE
        And CspController state is OFF
        And CbfController state is OFF
        And CspSubarray01 state is ON
        And CspSubarray02 state is ON
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController alarmDishIdCfg is 0
        When Standby Command is issued on CspController with argument []

        And CspController state is OFF
        And CbfController state is OFF
        And CspSubarray01 state is ON
        And CspSubarray02 state is ON
