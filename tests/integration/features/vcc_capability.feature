Feature: Csp-Lmc Mid Vcc Capability reports information about Cbf's VCC

    @pipeline
    Scenario: Mid Vcc Capability is properly initialized
        Given CspCapabilityVcc is initialized
        And All subsystems are fresh initialized
        Then CspCapabilityVcc state is ON
        And CspCapabilityVcc healthState is OK
        And CspCapabilityVcc AdminMode is ONLINE
        And CspCapabilityVcc vccsDeployed is 4
        And CspCapabilityVcc VccFqdn is "mid_csp_cbf/vcc/001", "mid_csp_cbf/vcc/002", "mid_csp_cbf/vcc/003", "mid_csp_cbf/vcc/004"     
        And CspCapabilityVcc vccState is "DISABLE", "DISABLE", "DISABLE", "DISABLE",
        And CspCapabilityVcc vccHealthState is "UNKNOWN", "UNKNOWN", "UNKNOWN", "UNKNOWN"
        And CspCapabilityVcc vccAdminMode is "OFFLINE", "OFFLINE", "OFFLINE", "OFFLINE",
        And CspCapabilityVcc vccsFqdnAssigned is '{}'

    @pipeline
    Scenario: Mid Vcc Capability vccSubarray01 is properly assigned and released

        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        And CspSubarray01 obsState is EMPTY
        And CspCapabilityVcc state is ON
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')        
        And On Command is issued on CspController with argument []
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_1_2_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 assignedReceptors is ("SKA001", "SKA036")
        And CspSubarray01 obsState is IDLE
        And CspCapabilityVcc vccState is "ON", "ON","DISABLE", "DISABLE",
        And CspCapabilityVcc vccHealthState is "UNKNOWN", "UNKNOWN", "UNKNOWN", "UNKNOWN", 
        And CspCapabilityVcc vccAdminMode is "ONLINE", "ONLINE", "OFFLINE", "OFFLINE",
        # randomly ordered, sometimes fails, check needs to be robust
        #And CspCapabilityVcc vccsFqdnAssigned is '{"1": ["mid_csp_cbf/vcc/002", "mid_csp_cbf/vcc/001"]}'

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspCapabilityVcc vccsFqdnAssigned is '{"1": []}'

        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')        
        And CspController state is OFF
        And CspCapabilityVcc vccstate is "DISABLE", "DISABLE", "DISABLE", "DISABLE"
