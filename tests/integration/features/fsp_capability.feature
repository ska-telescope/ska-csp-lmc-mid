Feature: Csp-Lmc Mid Fsp Capability reports information about Cbf's FSP

    @pipeline
    Scenario: Mid Fsp Capability is properly initialized
        Given CspCapabilityFsp is initialized
        And All subsystems are fresh initialized
        Then CspCapabilityFsp state is ON
        And CspCapabilityFsp healthState is OK
        And CspCapabilityFsp AdminMode is ONLINE
        And CspCapabilityFsp fspsDeployed is 4
        And CspCapabilityFsp fspFqdn is "mid_csp_cbf/fsp/01", "mid_csp_cbf/fsp/02", "mid_csp_cbf/fsp/03", "mid_csp_cbf/fsp/04"
        And CspCapabilityFsp fspFunctionMode is "IDLE", "IDLE", "IDLE", "IDLE"
        And CspCapabilityFsp fspState is "DISABLE", "DISABLE", "DISABLE", "DISABLE"
        And CspCapabilityFsp fspHealthState is "UNKNOWN", "UNKNOWN", "UNKNOWN", "UNKNOWN"
        And CspCapabilityFsp fspAdminMode is "OFFLINE", "OFFLINE", "OFFLINE", "OFFLINE"
        #And CspCapabilityFsp fspAvailable is [1, 2, 3, 4]
        And CspCapabilityFsp fspPss is []
        And CspCapabilityFsp fspPst is []
        And CspCapabilityFsp fspCorrelation is []
        #And CspCapabilityFsp fspUnavailable is [1, 2, 3, 4]
        And CspCapabilityFsp fspsSubarray01 is '{}'
        And CspCapabilityFsp fspsSubarray02 is '{}'

    @pipeline
    Scenario: Mid Fsp Capability maps properly the FSP attributes
        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And On Command is issued on CspController with argument []
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON
        And CspSubarray01 obsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_1_2_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is IDLE

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_ver3.json)
        #Then CspSubarray01 longRunningCommandStatus is 'COMPLETED'
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspCapabilityFsp fspFunctionMode is "CORR", "CORR", "IDLE", "IDLE"
        And CspCapabilityFsp fspState is "ON", "ON", "DISABLE", "DISABLE"
        And CspCapabilityFsp fspCorrelation is [1, 2]
        #And CspCapabilityFsp fspAvailable is []
        And CspCapabilityFsp fspsSubarrayMembership is "1", "1", "", ""
        And CspCapabilityFsp fspAdminMode is "ONLINE", "ONLINE", "OFFLINE", "OFFLINE"

        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 
        
        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')        
        And CspController state is OFF

    @pipeline 
    Scenario: Mid Fsp Capability is properly assigned and released

        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')        
        And On Command is issued on CspController with argument []
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON
        And CspSubarray01 obsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_1_2_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is IDLE

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_ver3.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspCapabilityFsp fspFunctionMode is ("CORR", "CORR", "IDLE", "IDLE")
        #And CspCapabilityFsp fspAvailable is []

        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspCapabilityFsp fspPss is []
        And CspCapabilityFsp fspPst is []
        And CspCapabilityFsp fspCorrelation is []
        #And CspCapabilityFsp fspUnavailable is [1, 2, 3, 4]
        #And CspCapabilityFsp fspAvailable is [1, 2, 3, 4]
        And CspCapabilityFsp fspsSubarrayMembership is "", "", "", ""
        And CspCapabilityFsp fspsSubarray01 is '{}'
        And CspCapabilityFsp fspsSubarray02 is '{}'

        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')        
        And CspController state is OFF
        And CspCapabilityFsp fspstate is "DISABLE", "DISABLE", "DISABLE", "DISABLE"
