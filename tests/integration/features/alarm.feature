Feature: Csp-Lmc Mid alarm test
           
# ------------- Test all commands -------------------
    @pipeline @XTP-30075 
    Scenario: Test CSP.LMC AlarmHandler
        Given The alarm handler configured with rules in alarmcsp.txt
        When CspController set testAlarm to True
        And CspSubarray01 set testAlarm to True
        Then Alarms are raised
        When CspController set testAlarm to False
        And CspSubarray01 set testAlarm to False
        Then CspController testAlarm is False
        And CspSubarray01 testAlarm is False
        When All alarms are acknoledged
        Then The alarm handler list of nack alarms is empty