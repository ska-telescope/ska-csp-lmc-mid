Feature: Csp-Lmc Mid Subarray reconfigure resources
      As the Csp-Lmc Subarray for Mid telescope
      I want to reconfigure resources on Cbf Subarray
      and verify the obsMode

    @pipeline @XTP-58526
    Scenario: Csp Subarray obsMode when configuring resources and scanning
        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        And CspCapabilityFsp state is ON
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')        
        And On Command is issued on CspController with argument []
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON
        And CbfController state is ON

        # CBF accept only MKT dish id. So the test is performed with them
        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_1_2_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is IDLE

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_ver3.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspSubarray01 obsMode is (ObsMode.IMAGING,) 

        When Scan Command is issued on CspSubarray01 (argument from file: Scan.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is SCANNING
        And CspSubarray01 obsState is SCANNING
        # And CspSubarray01 commandResult is ('scan', '0')
        And CspSubarray01 obsMode is (ObsMode.IMAGING,) 
        And Scan lasts for 2 seconds

        When EndScan Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspSubarray01 obsMode is (ObsMode.IMAGING,) 
        
        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 
        
        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')        
        And CspController state is OFF

