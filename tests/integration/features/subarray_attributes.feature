Feature: Csp-Lmc Mid Subarray attributes
    As the Csp-Lmc Subarray for Mid telescope
    I want to read basic attributes

    @XTP-36047
    Scenario: Csp subarray reports HealthState
        Given All subsystems are fresh initialized
        And CspSubarray01 isCommunicating is True
        And CspController set adminmode to ONLINE
        And CspSubarray01 healthstate is UNKNOWN


    @pipeline @obsmode @XTP-57446
    Scenario: Csp subarray reports ObsMode
        Given All subsystems are fresh initialized
        And CspSubarray01 isCommunicating is True
        And CspController set adminmode to ONLINE
        And CspSubarray01 obsMode is (ObsMode.IDLE,)  
