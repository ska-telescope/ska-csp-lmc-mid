Feature: Csp-Lmc Mid Subarray Assign and Release resources
      As the Csp-Lmc Subarray for Mid telescope
      I want to Assign and Release resources on Cbf Subarray
      and change my state accordingly

    Scenario: Csp Subarray assign MORE resources
        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController commandResult is ('on', '0')
        And Vcc are properly registered     
        
        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_3_4.json)
        # Then CspSubarray01 longRunningCommandResult is '0'
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CbfSubarray01 ObsState is IDLE
        And CspSubarray01 ObsState is IDLE
        And CspSubarray01 AssignedReceptors is ["SKA001", "SKA022", "SKA103", "SKA104"]

        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is OFF
        And CbfController state is OFF
    
    Scenario: Csp Subarray release SOME resources
        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController commandResult is ('on', '0')
        And Vcc are properly registered

        
        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 AssignedReceptors is ["SKA001", "SKA022", "SKA103", "SKA104"]
        
        When ReleaseResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_3_4)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('releaseresources', '0')
        And CbfSubarray01 ObsState is IDLE
        And CspSubarray01 ObsState is IDLE
        And CspSubarray01 AssignedReceptors is ["SKA001", "SKA022"]

        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is OFF
        And CbfController state is OFF

    @pipeline @XTP-35727
    Scenario: Csp Subarray release ALL resources
        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        And CspSubarray01 obsState is EMPTY

        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        
        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController CspCbfState is ON
        And CspController state is ON
        #And CspSubarray01 pssSubarrayState is ON
        And CspSubarray01 cbfSubarrayState is ON

        And CspSubarray01 cbfSubarrayObsState is EMPTY
        #And CspSubarray01 pssSubarrayObsState is EMPTY
        
        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_1_2_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 assignedReceptors is ("SKA001", "SKA036")
        And CspSubarray01 assignedResources is ('mid_csp_cbf/vcc/001', 'mid_csp_cbf/vcc/002')
        And CspCapabilityVcc state is ON

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('releaseallresources', '0')
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 assignedReceptors is ()

        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is OFF
        And CbfController state is OFF
    
    Scenario: Csp Subarray assign resources with success if EMPTY
        Given Subarray01 is fresh initialized
        And Controller is fresh initialized
        And CspSubarray01 state is ON

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON
        And CspController commandResult is ('on', '0')
        And Vcc are properly registered
        
        # When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_1_2.json)
        # Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        # And CspSubarray01 commandResult is ('assignresources', '0')
        # And CbfSubarray01 ObsState is IDLE
        # And CspSubarray01 ObsState is IDLE
        # And CspSubarray01 AssignedReceptors is ["SKA001", "SKA022"]

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (3, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '3')
        And CbfSubarray01 ObsState is EMPTY
        And CspSubarray01 ObsState is EMPTY
        And CspSubarray01 AssignedReceptors is ["", ""]

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_1_2_MIX.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CbfSubarray01 ObsState is IDLE
        And CspSubarray01 ObsState is IDLE
        And CspSubarray01 AssignedReceptors is ["", "MKT001"]

        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')        
        And CspController state is OFF