Feature: Csp-Lmc Mid test subscription to archive
           
# ------------- Test archive subscription -------------------
    @pipeline @XTP-30072 @xfail
    Scenario: Test state subscription on Controller and Subarray01
        Given All subsystems are fresh initialized
        And CspController state is subscribed for archiving
        And CspSubarray01 state is subscribed for archiving
        
        When LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 state is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY
        And CspController pushed 2 archive events on state
        And CspSubarray01 pushed 1 archive events on state
        
    @pipeline @XTP-30073 @xfail 
    Scenario: Test healthstate subscription on Controller and Subarray01
        Given All subsystems are fresh initialized
        And CspController healthstate is subscribed for archiving
        And CspSubarray01 healthstate is subscribed for archiving
        
        When CspController set adminmode to OFFLINE
        Then CspSubarray01 adminmode is OFFLINE
        And CspSubarray01 isCommunicating is False
        And CspController adminmode is OFFLINE
        And CspController isCommunicating is False
        And CspController healthState is UNKNOWN 
        And CspSubarray01 healthState is UNKNOWN 
        And CspController pushed 3 archive events on healthstate
        And CspSubarray01 pushed 3 archive events on healthstate

    @pipeline @XTP-30074 @xfail
    Scenario: Test obsState subscription on Subarray01
        Given All subsystems are fresh initialized
        And CspSubarray01 obsstate is subscribed for archiving
        
        When LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON
        And CspSubarray01 state is ON
        And CspSubarray01 obsState is EMPTY
        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_1_2_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is IDLE
        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is EMPTY
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        And CspController state is OFF
        And CspSubarray01 state is ON
        Then CspSubarray01 pushed 5 archive events on obsstate
