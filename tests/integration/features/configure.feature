# ------------- Scan -------------------
    @pipeline
    Scenario: Reconfigure on Subarray01 without PST beams
        Given All subsystems are fresh initialized

        When LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController cspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 state is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY 
        And CspSubarray01 obsState is EMPTY
        
        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_1FSP_ver3.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('configure', '0')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspSubarray01 assignedResources is ('mid_csp_cbf/fsp/01', 'mid_csp_cbf/vcc/001', 'mid_csp_cbf/vcc/002')

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_2ndFSP_ver3.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 commandResult is ('configure', '0')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspSubarray01 assignedResources is ('mid_csp_cbf/fsp/02', 'mid_csp_cbf/vcc/001', 'mid_csp_cbf/vcc/002')
        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY
        
        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')        
        And CspController state is OFF
#        When Abort Command is issued on CspSubarray01 (no argument)
#        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
#        And CspSubarray01 cbfSubarrayObsState is ABORTED
#        And CspSubarray01 obsState is ABORTED
#        When Restart Command is issued on CspSubarray01 (no argument)
#        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
#        And CspSubarray01 cbfSubarrayObsState is EMPTY
#        And CspSubarray01 obsState is EMPTY
#        And CspSubarray01 assignedResources is ()
#        And CspSubarray01 assignedVcc is []
#        And CspSubarray01 assignedFsp is []


    @oipeline
    Scenario: Test obsMode value after abort restart and configuring

        Given All subsystems are fresh initialized

        When LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController cspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 state is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY 
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_2ndFSP_ver3.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspSubarray01 obsMode is (ObsMode.IMAGING,) 

        When Scan Command is issued on CspSubarray01 (argument from file: Scan.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is SCANNING
        And CspSubarray01 obsState is SCANNING
        And PstBeam01 obsState is SCANNING
        And CspSubarray01 obsMode is ( ObsMode.IMAGING,)  

        When Abort Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is ([0, 'Abort command completed with success'], 'COMPLETED')
        And CspSubarray01 obsState is ABORTED
        And CspSubarray01 obsMode is (ObsMode.IDLE,)

        
        When Restart Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CbfSubarray01 obsState is EMPTY
        And CspSubarray01 obsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_1FSP_ver3.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspSubarray01 obsMode is (ObsMode.IMAGING,)
        
        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And PstBeam01 obsState is IDLE

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED') 
        And CspSubarray01 obsState is EMPTY
