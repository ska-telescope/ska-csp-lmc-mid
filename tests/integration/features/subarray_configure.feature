Feature: Csp-Lmc Mid Subarray configure and Release resources
      As the Csp-Lmc Subarray for Mid telescope
      I want to configure resources on Cbf Subarray
      and verify the fsp parameters

    @pipeline
    Scenario: Csp Subarray VCC information assigning and releasing resources
        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        And CspSubarray01 obsState is EMPTY
        And CspCapabilityFsp state is ON
        And CspSubarray01 assignedFspHealthState is ()
        And CspSubarray01 assignedFspState is ()
        And CspSubarray01 assignedFsp is []
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON
        And CbfController state is ON

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_1_2_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 assignedResources is ('mid_csp_cbf/vcc/001', 'mid_csp_cbf/vcc/002')

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_ver3.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        # And CspCapabilityFsp fspCorrelation is [1, 2]
        # And CspCapabilityFsp fspAvailable is [3, 4]
        And CspSubarray01 assignedResources is ('mid_csp_cbf/fsp/01', 'mid_csp_cbf/fsp/02','mid_csp_cbf/vcc/001', 'mid_csp_cbf/vcc/002')
        And CspSubarray01 assignedFsp is [1, 2]
        And CspSubarray01 assignedFspState is ("ON", "ON")
        And CspSubarray01 assignedFspHealthState is ("UNKNOWN", "UNKNOWN")
        #And CspSubarray01 assignedFspFunctionMode is ("CORR", "CORR")
        #And CspSubarray01 obsMode is (ObsMode.IMAGING,) 
        
        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspCapabilityFsp fspCorrelation is []
        # And CspCapabilityFsp fspAvailable is [1, 2, 3, 4]
        And CspSubarray01 assignedFsp is []
        And CspSubarray01 assignedFspState is ()
        And CspSubarray01 assignedFspHealthState is ()

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY
        #And CspSubarray01 obsMode is (ObsMode.IDLE,) 
        
        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')        
        And CspController state is OFF

