Feature: Csp-Lmc Mid Controller attributes
    As the Csp-Lmc controller for Mid telescope
    I want to read basic attributes

    @pipeline @XTP-35143
    Scenario: Csp controller reports SimulationMode
        Given All subsystems are fresh initialized
        And CspController isCommunicating is True
        And CspController SimulationMode is FALSE

        When CspController set SimulationMode to TRUE 
        Then CspController SimulationMode is TRUE  

    @XTP-35549
    Scenario: Csp controller reports HealthState
        Given All subsystems are fresh initialized
        And CspController isCommunicating is True
        And CspController set adminmode to ONLINE 
        Then CspController healthstate is UNKNOWN