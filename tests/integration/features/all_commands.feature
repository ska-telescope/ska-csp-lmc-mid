
Feature: Csp-Lmc Mid test all commands
           
# ------------- Test all commands -------------------
    @pipeline @stress @debug @XTP-33487
    Scenario: Test all commands on Subarray01
        Given All subsystems are fresh initialized
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is subscribed for archiving
        And CspSubarray01 state is subscribed for archiving
        And CspSubarray01 state is ON

        And On Command is issued on CspController with argument []
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController CspCbfState is ON
        And CspController state is ON
        #And CspSubarray01 pssSubarrayState is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        #And CspSubarray01 pssSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_1_2_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 assignedReceptors is ("SKA001", "SKA036")
        And CspSubarray01 assignedResources is ('mid_csp_cbf/vcc/001', 'mid_csp_cbf/vcc/002')
        And CspCapabilityVcc state is ON

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF_ver3.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspSubarray01 configurationID is 'sbi-mvp01-20200325-00001-science_A'
        And CspCapabilityFsp fspCorrelation is [1, 2]
        # fspAvailable and fspUnavailable attributes need to be reconsidered
        #And CspCapabilityFsp fspAvailable is []
        #And CspCapabilityFsp fspUnvailable is [3, 4]
        And CspSubarray01 assignedReceptors is ("SKA001", "SKA036")
        And CspSubarray01 assignedResources is ('mid_csp_cbf/fsp/01', 'mid_csp_cbf/fsp/02', 'mid_csp_cbf/vcc/001', 'mid_csp_cbf/vcc/002')
        And CspCapabilityFsp fspAdminMode is "ONLINE", "ONLINE", "OFFLINE", "OFFLINE"
        And CspCapabilityFsp fspState is "ON", "ON", "DISABLE", "DISABLE"

        When Scan Command is issued on CspSubarray01 (argument from file: Scan.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is SCANNING
        And CspSubarray01 obsState is SCANNING

        When Endscan Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 obsState is READY
        And CspSubarray01 assignedResources is ('mid_csp_cbf/fsp/01', 'mid_csp_cbf/fsp/02', 'mid_csp_cbf/vcc/001', 'mid_csp_cbf/vcc/002')

        When GoToIdle Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspCapabilityFsp fspCorrelation is []
        #And CspCapabilityFsp fspAvailable is [1, 2, 3, 4]
        And CspSubarray01 assignedResources is ('mid_csp_cbf/vcc/001', 'mid_csp_cbf/vcc/002')

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('releaseallresources', '0')
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        #And CspSubarray01 pssSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 assignedReceptors is ()
        #And CspSubarray01 assignedResources is ()

        When Off Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController CspCbfState is OFF
        And CspController state is OFF
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 state is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY
        And CspController commandResult is ('off', '0')
        
