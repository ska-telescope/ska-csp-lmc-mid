# ------------- Test all commands -------------------
Feature: Csp-Lmc Mid Controller commands
    As the Csp-Lmc controller for Mid telescope
    I want to send the basic commands to sub-elements

    @pipeline
    Background:
        Given All subsystems are fresh initialized


    @pipeline  @thisone
    Scenario: Test On/Off commands only on PST beam
        And CspSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CspSubarray01 assignedTimingBeamIDS is []
        And PstBeam01 state is OFF
        
        When On Command is issued on CspController with argument ["mid-pst/beam/01"]
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And PstBeam01 state is ON
        And PstBeam01 ObsState is IDLE

        When Off Command is issued on CspController with argument ["mid-pst/beam/01"]
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And PstBeam01 state is OFF
        And PstBeam01 ObsState is IDLE


    @pipeline @thisone
    Scenario: Test On commands on CspController with PST beam

        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        
        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 state is ON
        And CbfSubarray01 state is ON
        And CspSubarray01 ObsState is EMPTY
        And CbfSubarray01 ObsState is EMPTY
        And PstBeam01 state is ON
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsMode is (ObsMode.IDLE,) 
