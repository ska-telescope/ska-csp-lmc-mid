Feature: Csp-Lmc Mid Subarray Assign and Release resources
      As the Csp-Lmc Subarray for Mid telescope
      I want to Assign and Release resources on Cbf Subarray
      and change my state accordingly using also SKA dish

    @pipeline @assign @release @stress
    Scenario: Csp Subarray re-assign receptors and then release all
        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_3_4_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CbfSubarray01 ObsState is IDLE
        And CspSubarray01 ObsState is IDLE
        And CspSubarray01 assignedReceptors is ("SKA063", "SKA100")
        And CspSubarray01 assignedResources is ("mid_csp_cbf/vcc/003", "mid_csp_cbf/vcc/004")

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_1_2_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 assignedReceptors is ("SKA001", "SKA036", "SKA063", "SKA100")
        And CspSubarray01 assignedResources is ("mid_csp_cbf/vcc/001", "mid_csp_cbf/vcc/002", "mid_csp_cbf/vcc/003", "mid_csp_cbf/vcc/004")
        And CspSubarray01 obsState is IDLE

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 assignedReceptors is ()

        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is OFF
        And CbfController state is OFF

    @pipeline @assign @release @XTP-33489 @skip
    Scenario: Csp Subarray assign all SKA receptors and release SOME receptors
        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')            
        And On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON
        
        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 ObsState is IDLE
        And CspSubarray01 assignedReceptors is ("SKA001", "SKA036", "SKA063", "SKA100")
        
        When ReleaseResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_3_4_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('releaseresources', '0')
        And CbfSubarray01 ObsState is IDLE
        And CspSubarray01 ObsState is IDLE
        And CspSubarray01 assignedReceptors is ("SKA001", "SKA036")
        And CspSubarray01 assignedVcc is [1,2]
 
        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 assignedReceptors is ()
        And CspSubarray01 assignedVcc is []
        
        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is OFF
        And CbfController state is OFF
        And CspSubarray01 state is ON

    @pipeline @assign @release
    Scenario: Csp Subarray assign all SKA receptors and release ALL receptors
        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON
        
        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 assignedReceptors is ("SKA001", "SKA036", "SKA063", "SKA100")

        When ReleaseAllResources command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('releaseallresources', '0')
        And CbfSubarray01 ObsState is EMPTY
        And CspSubarray01 ObsState is EMPTY
        
        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is OFF
        And CbfController state is OFF

    @assign @release
    Scenario: Csp Subarray assign resources with success if EMPTY using SKA and MIX dished
        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON
        
        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CbfSubarray01 ObsState is IDLE
        And CspSubarray01 ObsState is IDLE
        And CspSubarray01 assignedReceptors is ("SKA001", "SKA036", "SKA063", "SKA100")
        
        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_1_2_MIX.json)
        Then CbfSubarray01 ObsState is IDLE
        And CspSubarray01 ObsState is IDLE
        And CspSubarray01 assignedReceptors is ("SKA001", "SKA036", "SKA063", "SKA100")

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 assignedReceptors is ()
        And CspSubarray01 assignedVcc is []
        And CspSubarray01 ObsState is EMPTY

        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is OFF
        And CbfController state is OFF

    @pipeline @assign 
    Scenario: Csp Subarray assign resources with failure using SKA and MKT dishes
        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        
        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_1_2_MIX.json)
        Then CbfSubarray01 ObsState is EMPTY
        And CspSubarray01 ObsState is EMPTY
        And CspSubarray01 assignedReceptors is ()

        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is OFF
        And CbfController state is OFF

    @pipeline @assign  
    Scenario: Csp Subarray assign resources with failure using valid and invalid SKA dish
        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')            

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_valid_invalid_SKA.json)
        Then CbfSubarray01 ObsState is EMPTY
        And CspSubarray01 ObsState is EMPTY
        And CspSubarray01 assignedReceptors is ()

        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is OFF
        And CbfController state is OFF

    @pipeline @assign 
    Scenario: Csp Subarray assign resources with failure using only invalid SKA dish
        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')            

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_invalid_SKA.json)
        Then CbfSubarray01 ObsState is EMPTY
        And CspSubarray01 ObsState is EMPTY

        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is OFF
        And CbfController state is OFF

    @pipeline @assign 
    Scenario: Csp Subarray assign resources with failure using only not deployed SKA dishes
        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')            

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_notdeployed_SKA.json)
        Then CbfSubarray01 ObsState is EMPTY
        And CspSubarray01 ObsState is EMPTY
        And CspSubarray01 assignedReceptors is ()

        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is OFF
        And CbfController state is OFF
    
    @pipeline
    Scenario: Csp Subarray assign resources with failure using deployed and not deployed SKA dishes
        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')            

        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_deployed_notdeployed_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (3, 'FAILED')
        And CspSubarray01 commandResult is ('assignresources', '3')
        # If validation fails on CBF no dishes are assigned, we ramain in EMPTY obsstate
        #And CbfSubarray01 ObsState is IDLE
        #And CspSubarray01 ObsState is IDLE
        #And CspSubarray01 assignedReceptors is ("SKA001", "SKA100")
        #When ReleaseAllResources command is issued on CspSubarray01 (no argument)
        #Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        #And CspSubarray01 commandResult is ('releaseallresources', '0')
        And CbfSubarray01 ObsState is EMPTY
        And CspSubarray01 ObsState is EMPTY

        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is OFF
        And CbfController state is OFF

    @pipeline 
    Scenario: assignedResources attribute is filled when Csp Subarray assign and release ALL resources
        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')        
        And On Command is issued on CspController with argument []
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON
        
        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('assignresources', '0')
        And CspSubarray01 assignedReceptors is ("SKA001", "SKA036", "SKA063", "SKA100")
        And CspSubarray01 assignedResources is ("mid_csp_cbf/vcc/001", "mid_csp_cbf/vcc/002", "mid_csp_cbf/vcc/003", "mid_csp_cbf/vcc/004")
        And CspSubarray01 assignedVcc is [1,2,3,4]
        And CspSubarray01 obsState is IDLE

        When ReleaseAllResources command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 commandResult is ('releaseallresources', '0')
        And CbfSubarray01 ObsState is EMPTY
        And CspSubarray01 ObsState is EMPTY
        And CspSubarray01 assignedReceptors is ()

        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is OFF
        And CbfController state is OFF
        And CbfSubarray01 state is ON

    @pipeline 
    Scenario: Csp Subarray VCC information assigning and releasing resources
        Given All subsystems are fresh initialized
        And CspSubarray01 state is ON
        And CspCapabilityVcc state is ON
        And CspSubarray01 obsState is EMPTY

        And LoadDishCfg Command is issued on CspController (argument from file: load_dish_cfg.json)
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')        
        And On Command is issued on CspController with argument []
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is ON
        And CspSubarray01 assignedVccHealthState is ()
        And CspSubarray01 assignedVccState is ()

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF_1_2_SKA.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 assignedReceptors is ("SKA001", "SKA036")
        And CspSubarray01 assignedResources is ('mid_csp_cbf/vcc/001', 'mid_csp_cbf/vcc/002')
        And CspSubarray01 assignedVccState is ("ON", "ON")
        And CspSubarray01 assignedVccHealthState is ("UNKNOWN", "UNKNOWN")
        And CspSubarray01 assignedVcc is [1,2]

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 assignedReceptors is ()
        And CspSubarray01 assignedResources is ()
        And CspSubarray01 assignedVccHealthState is ()
        And CspSubarray01 assignedVccState is ()

        # switch off all the FSPs and VCCs for next tests
        When Off Command is issued on CspController with argument ['mid_csp_cbf/sub_elt/controller']
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController state is OFF
        And CbfController state is OFF