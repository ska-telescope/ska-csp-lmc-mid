import logging
import time

import pytest
import tango
from load_alarms import alarm_ack, check_alarms, load_alarm_rules_from_file
from mid_test_device import MidTestDevice
from pytest_bdd import given, parsers, scenarios, then, when
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.testing.test_bdd_utils import (
    command_invalid_input,
    fresh_new_subsystems,
)
from tango import DevState

module_logger = logging.getLogger(__name__)


#  -------------- SCENARIOS --------------
scenarios("../features/controller_basic_commands.feature")
scenarios("../features/controller_attributes.feature")
scenarios("../features/subarray_initialization.feature")
scenarios("../features/subarray_attributes.feature")
# The commands On/Off have been removed from subarrays (they are always On)
# scenarios("../features/subarray_on_off.feature")
scenarios("../features/subarray_assign_resources.feature")
scenarios("../features/subarray_assign_resources_SKA.feature")
scenarios("../features/subarray_configure.feature")
scenarios("../features/subarray_reconfigure.feature")
scenarios("../features/configure.feature")
scenarios("../features/subarray_scan.feature")
scenarios("../features/all_commands.feature")  # delete dummy test
scenarios("../features/fsp_capability.feature")
scenarios("../features/vcc_capability.feature")
scenarios("../features/archive_subscription.feature")
scenarios("../features/alarm.feature")
scenarios("../features/reject_wrong_inputs.feature")
scenarios("../features/pst_beams.feature")
# -------------- MARKERS --------------


def pytest_configure(config):
    config.addinivalue_line("markers", "csp_k8s")
    config.addinivalue_line("markers", "bdd")
    config.addinivalue_line("markers", "controller")
    config.addinivalue_line("markers", "thisone")
    config.addinivalue_line("markers", "subarray")
    config.addinivalue_line("markers", "pipeline")
    config.addinivalue_line("markers", "all")
    config.addinivalue_line("markers", "nightly")
    config.addinivalue_line("markers", "stress")


# -------------- FIXTURES --------------

temp_dev_info = None

repository = "mid"
test_context = "real"

list_of_controller_names = [
    "CbfController",
    "MidCspController",
]

list_of_subarray_names = [
    "CbfSubarray01",
    "CbfSubarray02",
    "CbfSubarray03",
    "MidCspSubarray01",
    "MidCspSubarray03",
    "MidCspSubarray02",
]
list_of_capabilities_names = [
    "MidCspCapabilityFsp",
    "MidCspCapabilityVcc",
]

list_of_beam_names = [
    "MidPstBeam01",
]

list_of_vcc_names = [
    "Vcc001",
    "Vcc002",
    "Vcc003",
    "Vcc004",
]

list_of_fsp_names = ["Fsp001", "Fsp002", "Fsp003", "Fsp004"]


@pytest.fixture(scope="session")
def all_controllers():
    controllers = {}
    for name in list_of_controller_names:
        idx = name
        if name[0:3] in ["Sim", "Mid", "Low"]:
            idx = name[3:]
        controllers[idx] = MidTestDevice(name, repository, test_context)
    return controllers


@pytest.fixture(scope="session")
def all_subarrays():
    subarrays = {}
    for name in list_of_subarray_names:
        idx = name
        if name[:3] in ["Sim", "Mid", "Low"]:
            idx = name[3:]
        subarrays[idx] = MidTestDevice(name, repository, test_context)
    return subarrays


@pytest.fixture(scope="session")
def all_beams():
    beams = {}
    for name in list_of_beam_names:
        idx = name
        if name[:3] in ["Sim", "Mid", "Low"]:
            idx = name[3:]
        beams[idx] = MidTestDevice(name, repository, test_context)
    return beams


@pytest.fixture(scope="session")
def all_vccs():
    vccs = {}
    for name in list_of_vcc_names:
        idx = name
        if name[:3] in ["Sim", "Mid", "Low"]:
            idx = name[3:]
        vccs[idx] = MidTestDevice(name, repository, test_context)
    return vccs


@pytest.fixture(scope="session")
def all_capabilities():
    capabilities = {}
    for name in list_of_capabilities_names:
        idx = name
        if name[:3] in ["Sim", "Mid", "Low"]:
            idx = name[3:]
        capabilities[idx] = MidTestDevice(name, repository, test_context)
    return capabilities


# Steps for AlarmHandler
@given(parsers.parse("The alarm handler configured with rules in {filename}"))
def alarm_handler_configured(filename):
    try:
        file_path = f"tests/test_data/{filename}"
        module_logger.info(f"Reading file {file_path}")
        result = load_alarm_rules_from_file(file_path, "mid-csp/alarm/0")
        module_logger.info(f"Result: {result}")
    except tango.DevFailed:
        module_logger.error("Failure in opening a proxy to the alarm device")


@then("Alarms are raised")
def alarm_handler_detected_alarms():
    assert check_alarms("mid-csp/alarm/0")


@then("The alarm handler list of nack alarms is empty")
def alarm_handler_nack_list_empty():
    assert not check_alarms("mid-csp/alarm/0")


@when("All alarms are acknoledged")
def alarm_handler_ack_all_alarms():
    alarm_ack("mid-csp/alarm/0")


@given("All subsystems are fresh initialized")
def mid_fresh_new_subsystems(
    all_capabilities, all_controllers, all_subarrays, all_beams
):
    for device in all_capabilities.values():
        # TODO: reinizialize capabilities
        module_logger.info(f"{device} is not reinitialized")

    fresh_new_subsystems(all_controllers, all_subarrays, all_beams)


@given(parsers.parse("{device_name} is initialized"))
def mid_dev_init(
    device_name,
    all_capabilities,
    all_controllers,
    all_subarrays,
    all_beams,
    all_vccs,
):
    all_subsystems = {
        **all_capabilities,
        **all_controllers,
        **all_subarrays,
        **all_beams,
        **all_vccs,
    }
    device = all_subsystems[device_name]
    device.initialize_device()
    device.ping_device()
    # Check that initialization has finished, comparing state to NOT INIT
    timeout = 20
    total = 0
    step = 1
    while True:
        if device.state() != DevState.INIT:
            break
        if total > timeout:
            assert (
                False
            ), f"Waiting for initialization for {device.dev_name()} timedout"
        time.sleep(step)
        total += step


@given(parsers.parse("Vcc are properly registered"))
def vcc_ok(all_controllers, all_capabilities):
    module_logger.info("Override vcc_ok")
    all_subsystems = {**all_controllers, **all_capabilities}
    cbf_ctrl = all_subsystems["CbfController"]
    vcc_cap = all_subsystems["CspCapabilityVcc"]

    probe_poller(
        vcc_cap.proxy,
        "vccstate",
        [DevState.ON, DevState.ON, DevState.ON, DevState.ON],
        time=10,
    )
    if list(vcc_cap.proxy.vccstate) == [
        DevState.OFF,
        DevState.OFF,
        DevState.OFF,
        DevState.OFF,
    ]:
        module_logger.info("Restarting Vcc")
        cbf_ctrl.send_command("On")
        probe_poller(
            vcc_cap.proxy,
            "vccstate",
            [DevState.ON, DevState.ON, DevState.ON, DevState.ON],
            time=10,
        )


@given("Csp Subarray setup as IDLE with random resources")
def subarray_setup_IDLE(all_controllers, all_subarrays, all_capabilities):
    module_logger.info("Override subarray sertup!!")
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    device = all_subsystems["CspSubarray01"]
    if device.proxy.state() != DevState.ON:
        device.send_command("On")
        device.get_attribute(
            "longrunningcommandattributes", "(0, 'COMPLETED')"
        )

    vcc_ok(all_controllers, all_capabilities)
    device.get_attribute("state", "ON")
    device.send_command_with_argument_from_file(
        "AssignResources",
        "test_AssignResources_random.json",
    )

    all_subarrays["CbfSubarray01"].get_attribute("ObsState", "IDLE")
    device.get_attribute("ObsState", "IDLE")
    device.get_attribute("longrunningcommandattributes", "(0, 'COMPLETED')")


key_to_remove = {
    "AssignResources": {"without PST": "dish"},
    "Configure": {
        "without PST": ['["cbf"]', "fsp"],
    },
}


@when(
    parsers.parse(
        "{command_name} is issued on CspSubarray01 "
        "{is_pst_present} (invalid input)"
    )
)
def mid_command_invalid_input(command_name, is_pst_present):
    """
    Test that command is rejected after telmodel validation
    if input is invalid
    It uses a common function, while the key to remove
    to make invalid input are specialized.
    """
    command_invalid_input(
        command_name, is_pst_present, key_to_remove, telescope="mid"
    )
