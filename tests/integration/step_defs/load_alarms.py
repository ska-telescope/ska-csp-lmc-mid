import logging
import pathlib

from tango import DevFailed, DeviceProxy

module_logger = logging.getLogger(__name__)


def load_alarm_rules_from_file(file, fqdn):

    try:
        _file = pathlib.Path(file)
        file_extension = pathlib.Path(_file).suffix
        if file_extension == ".txt":
            f = open(_file, "r")
            response = f.read()
            validate_attribute_properties(response)
            module_logger.info(f"validated response: {response}")

            validate_attribute_properties(response)
            result = load_alarms(response, fqdn)
            return result
        else:
            module_logger.error("Provided file is not .txt file")
            result = {"error": "Provided file is not .txt file"}
            return result
    except Exception as e:
        module_logger.error(f"Exception: {e}")


def get_device_proxy(fqdn: str) -> DeviceProxy:
    """
    Method to get device proxy of Alarm Handler tango device

    Arg:
        fqdn(str): FQDN instance for alarm handler
        ex - "alarm/handler/01"

    Return:
        alarm_handler_proxy: device proxy object of alarm handler device

    """
    alarm_handler_proxy = DeviceProxy(fqdn)
    return alarm_handler_proxy


def validate_attribute_properties(response: str):
    """
    A method to validate the required attribute properties in alarm rule

    Arg:
        response(str): alarm rules to configure
    Raises:
        AlarmRuleValidationError: if mamndatory property fiels are
        missing in alarm rule
    """
    required_properties = [
        "tag=",
        "formula=",
        "priority=",
        "group=",
        "message=",
    ]
    alarm_rules_list = response.split("\n")
    module_logger.debug(f"alarm rule list:{alarm_rules_list}")
    for rule in alarm_rules_list:
        for property in required_properties:
            if rule and property not in rule:
                module_logger.warning(
                    f"Missing {property[:-1]} property in alarm rule {rule}"
                )
            else:
                module_logger.debug(
                    f"Alarm rule {rule} validation is successful"
                )


def load_alarms(response: str, fqdn: str) -> dict:
    """
    Method to load alarms using alarmhandler Load API

    Args:
       response (str): alarm rules to load
       fqdn (str): alarm handler device instance fqdn
       ex - "alarm/handler/01"

    Return:
       json: returns json containing alarm summary and configured
       alarms details with failed
       and successful configured alarms count.

    """
    alarm_handler_proxy = get_device_proxy(fqdn)
    alarm_configure_dict = {}
    configure_errors = []
    alarm_configure_dict["configure_success_count"] = 0
    alarm_configure_dict["configure_fail_count"] = 0
    alarm_rules_list = response.split("\n")
    for rule in alarm_rules_list:
        try:
            module_logger.debug(f"Loading rule {rule}")
            if rule:
                alarm_handler_proxy.command_inout("Load", rule)
                alarm_configure_dict["configure_success_count"] += 1
        except DevFailed as ex:
            module_logger.error(f"Exception occured: {ex.args[0].desc}")
            if "already exist" not in ex.args[0].desc:
                alarm_configure_dict["configure_fail_count"] += 1
                configure_errors.append(ex.args[0].desc)

    if configure_errors:
        return {
            "error": configure_errors,
            "configured_alarms": alarm_configure_dict,
        }
    elif len(configure_errors) > 0:
        return {"error": configure_errors}
    else:
        return {"configured_alarms": alarm_configure_dict}


def check_alarms(fqdn: str):
    """
    Wether all configured alarms are present in the list
    of not ack alarms.
    This means that all the configured alarms are raised
    by the AlarmHandler but not yet ack.
    """
    import time

    count = 0
    ret_code = False
    time_start = time.time()
    try:
        alarm_handler_proxy = get_device_proxy(fqdn)
        list_of_alarms = alarm_handler_proxy.alarmList
        module_logger.info(f"alarm list: {list_of_alarms}")
        while (time.time() - time_start) < 1.0:
            list_of_nack_alarms = alarm_handler_proxy.alarmUnacknowledged
            if not list_of_nack_alarms:
                time.sleep(0.2)
        module_logger.info(f"alarm nacklist: {list_of_nack_alarms}")

        for alarm in list_of_alarms:
            if alarm in list_of_nack_alarms:
                count += 1
        if count == len(list_of_alarms):
            ret_code = True
    except DevFailed as ex:
        module_logger.error(f"Exception occured: {ex.args[0].desc}")
    return ret_code


def alarm_ack(fqdn: str):
    alarm_handler_proxy = get_device_proxy(fqdn)

    try:
        list_of_nack_alarms = alarm_handler_proxy.alarmUnacknowledged
        for alarm in list_of_nack_alarms:
            alarm_handler_proxy.ack(alarm)
    except DevFailed as ex:
        module_logger.error(f"Exception occured: {ex.args[0].desc}")


# response = load_alarm_rules_from_file('alarmcsp.txt',  'mid-csp/alarm/0')
