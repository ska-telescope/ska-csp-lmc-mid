import logging

from ska_csp_lmc_common.testing.poller import Poller, Probe, probe_poller
from ska_csp_lmc_common.testing.test_bdd_utils import TestDevice

module_logger = logging.getLogger(__name__)


class MidTestDevice(TestDevice):
    def get_attribute(self, attribute, value, timeout=15):
        prober = None
        attribute_lowercase = attribute.lower()
        if (
            attribute_lowercase.find("vcc") > -1
            or attribute_lowercase.find("fsp") > -1
        ):
            value = eval(value)
            return probe_poller(
                self.proxy,
                attribute,
                value,
                timeout,
            )
        if (
            isinstance(value, str)
            and ", " in value
            and "longrunning" not in attribute.lower()
        ):

            command_id = None
            prober = Probe(
                self.proxy,
                attribute,
                eval(value),
                f"{attribute} is not {value}."
                f"It is {self.proxy.read_attribute(attribute).value}",
                command_id=command_id,
            )
            Poller(timeout, 0.1).check(prober)
        else:
            super().get_attribute(attribute, value, timeout=timeout)

    def check_subarray_init_state(self):
        module_logger.info(f"Checking status for {self.name}")
        self.get_attribute("State", "ON")
        self.get_attribute("HealthState", "UNKNOWN", timeout=15)
        self.get_attribute("ObsState", "EMPTY")
        # Mid Cbf Controller set the adminmode=online of all cbf devices
        # (including subarrays) at the end of the initialization.
        # To avoid conflicts in test, we are setting online as well.
        # The possibility to set in ENGINEERING for Low is mantained.
        self.get_attribute("AdminMode", "ONLINE")
        if "CspSubarray" in self.return_device_name():
            self.get_attribute("capabilityConnected", "True")

    def check_controller_init_state(self):
        module_logger.info(f"Checking status for {self.name}")
        self.get_attribute("State", "OFF")
        self.get_attribute("HealthState", "UNKNOWN", timeout=15)
        self.get_attribute("AdminMode", "ONLINE")
