import logging
from typing import Callable, Optional
from unittest.mock import ANY

import mock
import pytest
from mid_mocked_connector import MidMockedConnector
from mid_properties import test_properties_mid_subarray
from ska_control_model import ResultCode, TaskStatus
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.testing.poller import probe_poller

from ska_csp_lmc_mid.manager.mid_capability_connection import (
    MidCapabilityConnectionManager,
)
from ska_csp_lmc_mid.manager.mid_subarray_component_manager import (
    MidCspSubarrayComponentManager,
)

module_logger = logging.getLogger(__name__)


@pytest.fixture
def mock_completed_callback():
    """Fixture for a mock completed callback"""
    return mock.MagicMock()


class MockMidCspSubarrayComponentManager(MidCspSubarrayComponentManager):
    def __init__(self, mock_completed_callback, *args, **kwargs):
        self.callback = mock_completed_callback
        super().__init__(*args, **kwargs)

    def _start_communicating(
        self,
        task_callback: Optional[Callable] = None,
    ) -> None:

        super()._start_communicating(self.callback)


@pytest.fixture
def mock_subarray_component_manager(mock_completed_callback):
    with mock.patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = test_properties_mid_subarray
        cm_conf = ComponentManagerConfiguration("", module_logger)
        cm_conf.add_attributes()
        subarray_component_manager = MockMidCspSubarrayComponentManager(
            mock_completed_callback,
            mock.MagicMock(),
            mock.MagicMock(),
            mock.MagicMock(),
            cm_conf,
            logger=module_logger,
        )
        subarray_component_manager.init()
        yield subarray_component_manager


@mock.patch("ska_csp_lmc_common.component.Connector", new=MidMockedConnector)
def test_subarray_cm_connection_with_capababilities(
    mock_subarray_component_manager, mock_completed_callback
):
    """
    Test the connection to the capability devices when the
    _retrieve_capability_fqdn method raises an exception
    Args:
        mock_subarray_component_manager (_type_): _description_
        mock_completed_callback (_type_): _description_
    """
    with mock.patch.object(
        MidCapabilityConnectionManager,
        "_retrieve_capability_fqdn",
    ) as mocked_retrieve:
        mocked_retrieve.side_effect = ValueError(
            "Error in retrieving VCC/FSP capability fqdn"
        )
        mock_subarray_component_manager.start_communicating()
        probe_poller(
            mock_subarray_component_manager, "is_communicating", True, time=5
        )

        mock_completed_callback.assert_called_with(
            result=(ResultCode.FAILED, ANY),
            status=TaskStatus.COMPLETED,
        )
