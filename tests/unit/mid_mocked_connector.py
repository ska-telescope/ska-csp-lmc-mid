import json
import logging
import random
import threading
import time

import mock
from mid_attribute_factory import MidAttributeDataFactory
from ska_control_model import ResultCode, SimulationMode
from ska_csp_lmc_common.testing import mock_config
from ska_csp_lmc_common.testing.mocked_connector import MockedConnector
from ska_csp_lmc_common.testing.test_utils import create_dummy_event

module_logger = logging.getLogger(__name__)

test_fsp_property_devices = [
    "mid_csp_cbf/fsp/01",
    "mid_csp_cbf/fsp/02",
    "mid_csp_cbf/fsp/03",
    "mid_csp_cbf/fsp/04",
]

test_vcc_property_devices = [
    "mid_csp_cbf/vcc/001",
    "mid_csp_cbf/vcc/002",
    "mid_csp_cbf/vcc/003",
    "mid_csp_cbf/vcc/004",
]


class MidMockedConnector(MockedConnector):
    """Class to mock the connection with a sub-system controller.

    Inherits from the base class MockedConnector and overrides the
    fake_get_Attribute method to specialize for Mid instance.
    """

    attr_factory_class = MidAttributeDataFactory

    @classmethod
    def fake_command(cls, fqdn, command_name, argument):
        """Mock the command_inout TANGO call.
        Rely on a global configuration for the MockedConnector class to
        generate different behavior during the execution of a command.

        If no failures are injected, the execution of this method raises
        the appropriate event for the required attribute (State, obsState,
        etc).
        """

        if command_name == "initsysparam":
            # build a different command_id depending on the FQDN
            # fqdn expected format domain/faimily/device_XX
            command_id = f"12345678_{fqdn[-2:]}_Inisysparam"

            def throw_event():
                # thow an event on the LRC status attribute
                # after 0.5 sec

                time_to_sleep = random.uniform(0.3, 1.5)
                if (
                    mock_config.mock_timeout
                    and mock_config.mock_fail_device
                    and mock_config.mock_fail_device in fqdn
                ):
                    time_to_sleep = 6
                module_logger.info(
                    f"run InitsysParam command on {fqdn} "
                    f"time_to_sleep: {time_to_sleep}"
                )
                result = (
                    command_id,
                    json.dumps([0, f"InitsysParam completed on {fqdn}"]),
                )
                _event = create_dummy_event(
                    fqdn, "longrunningcommandresult", result
                )
                callback = cls.event_subscription_map[fqdn][
                    "longrunningcommandresult"
                ]
                _event1 = create_dummy_event(
                    fqdn,
                    "longrunningcommandstatus",
                    (command_id, "COMPLETED"),
                )
                callback1 = cls.event_subscription_map[fqdn][
                    "longrunningcommandstatus"
                ]

                time.sleep(time_to_sleep)
                if (
                    mock_config.mock_fail_device
                    and mock_config.mock_fail_device in fqdn
                    and mock_config.mock_event_failure
                ):
                    return
                callback(_event)
                callback1(_event1)

            threading.Thread(target=throw_event).start()
            return [[ResultCode.QUEUED], [command_id]]
        else:
            return super().fake_command(fqdn, command_name, argument)


class MidMockedConnectorException(MockedConnector):
    """Class to mock the connection with a sub-system controller.

    Inherits from the base class MockedConnector and overrides the
    fake_get_attribute method to specialize for Mid instance.
    """

    @classmethod
    def fake_get_attribute(cls, fqdn, attr_name):
        raise ValueError("Failure in reading attribute")


class CapabilityMockedConnector(MidMockedConnector):

    component = mock.MagicMock()
    receptorid = mock.PropertyMock(
        side_effect=["SKA001", "SKA022", "SKA103", "SKA104"]
    )
    simulationmode = mock.PropertyMock(
        side_effect=[
            SimulationMode.TRUE,
            SimulationMode.TRUE,
            SimulationMode.TRUE,
            SimulationMode.TRUE,
        ]
    )
    type(component).receptorid = receptorid
    type(component).simulationmode = simulationmode
    # type(component).receptorID = receptorid
    # type(component).simulationMode = simulationmode
    deviceid = mock.PropertyMock(side_effect=[1, 2, 3, 4])
    type(component).deviceid = deviceid
    mock_attrs = {
        "deviceproxy": component,
        "deviceproxy.get_property.return_value": {
            "FSP": test_fsp_property_devices,
            "VCC": test_vcc_property_devices,
        },
        "deviceproxy.poll_attribute": mock.MagicMock(),
        # "deviceproxy.deviceid.side_effect": [1,2,3,4],
        # "deviceproxy.deviceid": 2,
        # "deviceproxy.deviceid": random.randint(1,4),
        # "deviceproxy.receptorid": random.randint(5,8),
    }
