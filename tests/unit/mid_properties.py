test_properties_mid_ctrl = {
    "ConnectionTimeout": "3",
    "PingConnectionTime": "1",
    "DefaultCommandTimeout": "5",
    "CspCbf": "mid_csp_cbf/sub_elt/controller",
    "CspPss": "mid-pss/control/0",
    "CspPstBeams": [
        "mid-pst/beam/01",
        "mid-pst/beam/02",
    ],
    "CspSubarrays": [
        "mid-csp/subarray/01",
        "mid-csp/subarray/02",
        "mid-csp/subarray/03",
    ],
}

test_properties_mid_subarray = {
    "ConnectionTimeout": "3",
    "PingConnectionTime": "1",
    "DefaultCommandTimeout": "5",
    "SubID": "1",
    "CspController": "mid-csp/control/0",
    "CbfSubarray": "mid_csp_cbf/sub_elt/subarray_01",
    "PssSubarray": "mid-pss/subarray/01",
    "PstBeams": [
        "mid-pst/beams/01",
        "mid-pst/beams/02",
    ],
}

test_properties_capability = {
    "CspCbf": "mid_csp_cbf/sub_elt/controller",
    "ConnectionTimeout": 3,
    "PingConnectionTime": 1,
    "DefaultCommandTimeout": 5,
}
