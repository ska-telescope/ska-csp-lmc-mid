import logging

import mock
import pytest
from mid_mocked_connector import (
    MidMockedConnector,
    MidMockedConnectorException,
)

from ska_csp_lmc_mid.controller import MidCbfControllerComponent
from ska_csp_lmc_mid.mid_receptor_util import index_to_receptor_id

module_logger = logging.getLogger(__name__)


@pytest.fixture
@mock.patch("ska_csp_lmc_common.component.Connector", new=MidMockedConnector)
def ctrl_component():
    ctrl = MidCbfControllerComponent(fqdn="mid-cbf/controller/0")
    ctrl.connect()
    return ctrl


@pytest.fixture
@mock.patch(
    "ska_csp_lmc_common.component.Connector", new=MidMockedConnectorException
)
def ctrl_component_fail():
    ctrl = MidCbfControllerComponent(fqdn="mid-cbf/controller/0")
    ctrl.connect()
    return ctrl


class TestMidCbfControllerComponent:
    def test_list_of_receptors(self, ctrl_component):
        """Test the."""
        assert set(ctrl_component.list_of_receptors) == {
            "SKA001",
            "SKA022",
            "SKA103",
            "SKA104",
        }

    def test_unassigned_receptors(self, ctrl_component):
        assert ctrl_component.unassigned_receptors == [
            "SKA001",
        ]

    def test_receptors_affiliation(self, ctrl_component):
        assert ctrl_component.receptors_affiliation == [0, 1, 0, 2]

    def test_list_of_receptors_with_read_failure(self, ctrl_component_fail):
        assert ctrl_component_fail.list_of_receptors == []

    def test_unassigned_receptors_with_read_failure(self, ctrl_component_fail):
        assert not ctrl_component_fail.unassigned_receptors

    def test_receptors_affiliation_with_read_failure(
        self, ctrl_component_fail
    ):
        assert not ctrl_component_fail.receptors_affiliation

    def test_linked_receptors(self, ctrl_component):
        assert set(ctrl_component.linked_receptors.values()) == {
            "SKA001",
            "SKA022",
            "SKA104",
        }

    def test_linked_receptors_with_read_failure(
        self, ctrl_component_fail, caplog
    ):
        # set log level to debug because the message had serverity reduced
        caplog.set_level(logging.DEBUG)
        assert not ctrl_component_fail.linked_receptors
        failure_message = (
            "Failure in accessing CBF " + "proxy to get VCC-Receptor map"
        )
        msg_found = any(
            message == failure_message for message in caplog.messages
        )
        assert msg_found

    def test_receptors_id_CBF_mapping(self, ctrl_component):
        """
        Validate the mapping between CBF receptor index to receptor id
        """
        test_list = ["0", "1", "64", "65", "70", "197", "199", "-1"]
        result_list = [
            "DIDINV",
            "MKT000",
            "MKT063",
            "SKA001",
            "SKA006",
            "SKA133",
            "DIDINV",
            "DIDINV",
        ]
        for index, item in enumerate(test_list):
            result = index_to_receptor_id(item)
            assert result_list[index] == result
