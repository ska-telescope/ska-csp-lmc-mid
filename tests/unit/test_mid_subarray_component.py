import logging
import os

import mock
from mid_attribute_factory import MidAttributeDataFactory
from mid_mocked_connector import MidMockedConnector
from ska_csp_lmc_common.testing.test_classes import TestBaseSubarray

from ska_csp_lmc_mid.subarray import MidCbfSubarrayComponent

file_path = os.getcwd()
file_config = "AssignResources_CBF_PSS_PST.json"

fqdn = "mid_csp_cbf/sub_elt/subarray_01"

module_logger = logging.getLogger(__name__)


# TODO: insert test description


@mock.patch("ska_csp_lmc_common.component.Connector", new=MidMockedConnector)
@mock.patch(
    "ska_csp_lmc_mid.subarray.mid_cbf_subarray_component.Connector",
    new=MidMockedConnector,
)
class TestMidCbfSubarrayComponent:
    @classmethod
    def set_up(cls):
        component = MidCbfSubarrayComponent(fqdn)
        component.connect()
        component.csp_ctrl_fqdn = "mid-csp/control/0"
        input_argument = TestBaseSubarray.assignresources_input(
            filename=file_config
        )
        callback = mock.MagicMock()
        return component, input_argument["dish"], callback

    @mock.patch(
        "ska_csp_lmc_common.component.Connector", new=MidMockedConnector
    )
    def test_assigned_receptors(self):
        component, input_argument, callback = self.set_up()
        assert component.assigned_receptors == [
            "SKA001",
            "SKA022",
            "SKA103",
            "SKA104",
        ]

    def test_some_receptors_are_already_assigned(self):

        component, input_argument, callback = self.set_up()
        input_argument["receptor_ids"] = [
            "SKA001",
            "SKA022",
            "SKA103",
            "SKA104",
        ]
        MidAttributeDataFactory.attributes_ctrl_default[
            "unassignedReceptorIDs"
        ] = ["SKA103", "SKA104"]
        MidAttributeDataFactory.attributes_ctrl_default[
            "receptorMembership"
        ] = [2, 2, 0, 0]
        output_list = component._validated_receptors(
            input_argument, component._assign_receptors_validator
        )
        assert output_list == ["SKA103", "SKA104"]

    def test_remove_duplicated_receptors(self):
        """Test duplicated input resources"""

        MidAttributeDataFactory.attributes_ctrl_default[
            "unassignedReceptorIDs"
        ] = ["SKA001", "SKA022", "SKA103", "SKA104"]
        MidAttributeDataFactory.attributes_ctrl_default[
            "receptorMembership"
        ] = [0, 0, 0, 0]
        component, input_argument, callback = self.set_up()
        input_argument = {
            "receptor_ids": [
                "SKA001",
                "SKA001",
                "SKA022",
                "SKA103",
                "SKA103",
                "SKA104",
            ]
        }
        receptors_to_assign = component._validated_receptors(
            input_argument, component._assign_receptors_validator
        )
        assert receptors_to_assign == ["SKA001", "SKA022", "SKA103", "SKA104"]

    def test_validate_receptors_list(self):
        """
        Validate the assign resurces receptor id validator
        """
        component = MidCbfSubarrayComponent(fqdn)
        test_list = [
            "DIDINV",
            "MKT000",
            "MKT063",
            "SKA000",
            "SKA001",
            "SKA134",
            "SKA13",
            "MKN001",
            "MKT0001",
            "0001",
            35,
        ]

        result_list = [
            False,
            True,
            True,
            False,
            True,
            False,
            False,
            False,
            False,
            False,
            False,
        ]

        for index, item in enumerate(test_list):
            result = component._receptor_id_validation(item)
            assert result_list[index] == result
