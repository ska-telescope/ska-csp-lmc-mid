import functools
import logging
import os

import mock
import pytest
from mid_mocked_connector import MidMockedConnector
from mid_properties import test_properties_mid_ctrl
from mock import MagicMock, patch
from ska_control_model import HealthState, SimulationMode, TaskStatus
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.testing import mock_config
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.testing.test_classes import TestBaseController
from ska_tango_base.utils import generate_command_id
from ska_tango_testing.mock import MockCallableGroup
from tango import DevState

from ska_csp_lmc_mid.manager import MidCspControllerComponentManager

file_path = os.getcwd()
logger = logging.getLogger(__name__)


@pytest.fixture()
def callbacks() -> MockCallableGroup:
    """
    Return a dictionary of callbacks with asynchrony support.

    :return: a collections.defaultdict that returns callbacks by name.
    """
    return MockCallableGroup(
        "loaddishcfg_task",
        timeout=5.0,
    )


class NewMockedConnector(MidMockedConnector):
    """Class to mock the connection with a sub-system controller.

    Inherits from the base class MockedConnector and overrides the
    fake_get_Attribute method to specialize for Mid instance.
    """

    @classmethod
    def fake_set_attribute(self, attr_name):
        return mock.MagicMock()


def test_ctrl_cm_when_wrong_sub_systems_fqdn_specified():
    """Test Subarray ctrl_component_manager initialization when CSP
    Subarray device properties are wrong.

    Expected result: CSP Subarray State FAULT CSP Subarray HealthState FAILED
    """
    wrong_properties = {
        "ConnectionTimeout": "1",
        "PingConnectionTime": "1",
        "DefaultCommandTimeout": "5",
        "CspCbf": "mid-cbx/control/0",
        "PstBeams": ["mid-pst/beam/01", "mid-pst/beam/02"],
    }
    with patch(
        "ska_csp_lmc_common.manager"
        ".manager_configuration"
        ".ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = wrong_properties
        wrong_ctrl_component_manager_conf = ComponentManagerConfiguration(
            "", logger
        )
        wrong_ctrl_component_manager_conf.add_attributes()
        ctrl_component_manager = MidCspControllerComponentManager(
            MagicMock(),
            MagicMock(),
            MagicMock(),
            wrong_ctrl_component_manager_conf,
            logger=logger,
        )
        ctrl_component_manager.init()
        ctrl_component_manager.op_state_model.op_state == DevState.FAULT
        ctrl_component_manager.health_model.health_state == HealthState.FAILED  # noqa:E501


def test_ctrl_cm_connection_failed(ctrl_component_manager):
    """Test the communicating flag after communication failure."""
    import time

    ctrl_component_manager.start_communicating()
    time.sleep(2)
    assert not ctrl_component_manager.is_communicating
    assert not len(ctrl_component_manager.online_components)


def print_value(value):
    print(f"New values is: {value}")


class TestMidControllerManager(TestBaseController):
    devices_list = {
        "cbf-ctrl": test_properties_mid_ctrl["CspCbf"],
        "pss-ctrl": test_properties_mid_ctrl["CspPss"],
        "pst-beam-01": test_properties_mid_ctrl["CspPstBeams"][0],
        "pst-beam-02": test_properties_mid_ctrl["CspPstBeams"][1],
        "subarray_01": test_properties_mid_ctrl["CspSubarrays"][0],
        "subarray_02": test_properties_mid_ctrl["CspSubarrays"][1],
        "subarray_03": test_properties_mid_ctrl["CspSubarrays"][2],
    }

    @pytest.fixture
    @patch("ska_csp_lmc_common.component.Connector", new=MidMockedConnector)
    def ctrl_component_manager_communicating(self, ctrl_component_manager):
        ctrl_component_manager._admin_mode = 2
        ctrl_component_manager.start_communicating()
        # wait to establish communication
        probe_poller(ctrl_component_manager, "is_communicating", True, time=3)

    def test_ctrl_cm_components_created(self, ctrl_component_manager):
        assert len(ctrl_component_manager.components) == 7
        assert (
            ctrl_component_manager.components[
                test_properties_mid_ctrl["CspCbf"]
            ].name
            == "cbf-controller"
        )
        assert (
            ctrl_component_manager.components[
                test_properties_mid_ctrl["CspPss"]
            ].name
            == "pss-controller"
        )
        assert (
            ctrl_component_manager.components[
                test_properties_mid_ctrl["CspPstBeams"][0]
            ].name
            == "pst-beam-01"
        )
        assert (
            ctrl_component_manager.components[
                test_properties_mid_ctrl["CspPstBeams"][1]
            ].name
            == "pst-beam-02"
        )

    def test_ctrl_cm_online_components(
        self, ctrl_component_manager, ctrl_cm_connected
    ):
        """Verify that callbacks are created in initilization's succeed
        method."""
        assert len(ctrl_component_manager.online_components) == 7

    def test_ctrl_cm_stop_communicating(
        self, ctrl_component_manager, ctrl_cm_connected
    ):
        assert ctrl_component_manager.is_communicating
        ctrl_component_manager.stop_communicating()
        probe_poller(ctrl_component_manager, "is_communicating", False, time=4)

    def test_ctrl_cm_simulation_mode(
        self, ctrl_component_manager, ctrl_cm_connected
    ):
        assert (
            ctrl_component_manager._programmed_cbf_simulation_mode
            == SimulationMode.TRUE
        )
        ctrl_component_manager.set_cbf_simulation_mode(SimulationMode.FALSE)
        assert (
            ctrl_component_manager._programmed_cbf_simulation_mode
            == SimulationMode.FALSE
        )

    def test_ctrl_cm_simulation_mode_with_exception(
        self, ctrl_component_manager, ctrl_cm_connected
    ):
        assert (
            ctrl_component_manager._programmed_cbf_simulation_mode
            == SimulationMode.TRUE
        )

        with patch(
            "ska_csp_lmc_common.component.Component.write_attr"
        ) as mock_write:
            mock_write.side_effect = ValueError("Error in writing")
            ctrl_component_manager.set_cbf_simulation_mode(
                SimulationMode.FALSE
            )
            assert (
                ctrl_component_manager._programmed_cbf_simulation_mode
                == SimulationMode.TRUE
            )

    def test_loaddishcfg(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
    ):
        """
        Test the behavior of the loaddischcfg command.

        Expected behavior: the command is reported as COMPLETED, OK
        """

        dish_cfg = """{
            "interface": "https://schema.skao.int/"
                        "ska-mid-cbf-initsysparam/1.0",
            "dish_parameters": {
                "SKA001": {"vcc": 1, "k": 11},
                "SKA036": {"vcc": 2, "k": 101},
                "SKA063": {"vcc": 3, "k": 1127},
                "SKA100": {"vcc": 4, "k": 620},
            },
        }"""
        TestMidControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.OFF
        )
        ctrl_component_manager.loaddishcfg(
            dish_cfg,
            task_callback=functools.partial(
                callbacks["loaddishcfg_task"],
                generate_command_id("loaddishcfg"),
            ),
        )

        callbacks["loaddishcfg_task"].assert_against_call(
            status=TaskStatus.QUEUED, lookahead=3
        )

        callbacks["loaddishcfg_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS, lookahead=3
        )

        callbacks["loaddishcfg_task"].assert_against_call(
            status=TaskStatus.COMPLETED, lookahead=3
        )

    def test_loaddishcfg_failed_with_timeout(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
        mock_config_init,
    ):
        """
        Expected behavior: the command is reported as FAILED, FAILED
        and the alarm flag is set.
        """
        mock_config.mock_timeout = True
        mock_config.mock_fail_device = "mid_csp_cbf/sub_elt/controller"
        dish_cfg = """{
            "interface": "https://schema.skao.int/"
                        "ska-mid-cbf-initsysparam/1.0",
            "dish_parameters": {
                "SKA001": {"vcc": 1, "k": 11},
                "SKA036": {"vcc": 2, "k": 101},
                "SKA063": {"vcc": 3, "k": 1127},
                "SKA100": {"vcc": 4, "k": 620},
            },
        }"""
        TestMidControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.OFF
        )
        ctrl_component_manager.loaddishcfg(
            dish_cfg,
            task_callback=functools.partial(
                callbacks["loaddishcfg_task"],
                generate_command_id("loaddishcfg"),
            ),
        )

        callbacks["loaddishcfg_task"].assert_against_call(
            status=TaskStatus.QUEUED, lookahead=3
        )

        callbacks["loaddishcfg_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS, lookahead=3
        )
        # sleep for the timeout
        import time

        time.sleep(6)
        callbacks["loaddishcfg_task"].assert_against_call(
            status=TaskStatus.FAILED, lookahead=3
        )
        assert ctrl_component_manager.alarm_dish_id_cfg

    def test_loaddishcfg_failed(
        self,
        ctrl_component_manager,
        ctrl_component_manager_communicating,
        callbacks,
    ):
        """
        Test the behavior of the loaddischcfg command when it
        fails on Mid CBF Controller

        Expected behavior: the command is reported as FAILED, FAILED
        and the alarm flag is set.
        """

        dish_cfg = """{
            "interface": "https://schema.skao.int/"
                        "ska-mid-cbf-initsysparam/1.0",
            "dish_parameters": {
                "SKA001": {"vcc": 1, "k": 11},
                "SKA036": {"vcc": 2, "k": 101},
                "SKA063": {"vcc": 3, "k": 1127},
                "SKA100": {"vcc": 4, "k": 620},
            },
        }"""
        TestMidControllerManager.set_subsystems_and_go_to_state(
            ctrl_component_manager, DevState.OFF
        )
        with patch(
            "ska_csp_lmc_mid.controller."
            "mid_ctrl_component.MidCbfControllerComponent.run"
        ) as mock_run:
            mock_run.side_effect = ValueError("Error in initysyparam")
            ctrl_component_manager.loaddishcfg(
                dish_cfg,
                task_callback=functools.partial(
                    callbacks["loaddishcfg_task"],
                    generate_command_id("loaddishcfg"),
                ),
            )

            callbacks["loaddishcfg_task"].assert_against_call(
                status=TaskStatus.QUEUED, lookahead=3
            )

            callbacks["loaddishcfg_task"].assert_against_call(
                status=TaskStatus.IN_PROGRESS, lookahead=3
            )

            callbacks["loaddishcfg_task"].assert_against_call(
                status=TaskStatus.FAILED, lookahead=3
            )
            assert ctrl_component_manager.alarm_dish_id_cfg
