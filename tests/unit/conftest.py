import logging
import time

import mock
import pytest
from mid_mocked_connector import MidMockedConnector
from mid_properties import (
    test_properties_mid_ctrl,
    test_properties_mid_subarray,
)
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.testing import mock_config
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_csp_lmc_mid.manager import (
    MidCspControllerComponentManager,
    MidCspSubarrayComponentManager,
)

module_logger = logging.getLogger(__name__)


@pytest.fixture(scope="session")
def ctrl_component_manager(ctrl_op_state_model, ctrl_health_state_model):
    with mock.patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = test_properties_mid_ctrl
        cm_conf = ComponentManagerConfiguration("", module_logger)
        cm_conf.add_attributes()
        ctrl_component_manager = MidCspControllerComponentManager(
            ctrl_op_state_model,
            ctrl_health_state_model,
            cm_conf,
            logger=module_logger,
        )
        ctrl_component_manager.init()
        yield ctrl_component_manager


@pytest.fixture(scope="session")
def subarray_component_manager(
    subarray_health_state_model,
    subarray_op_state_model,
    subarray_obs_state_model,
):
    with mock.patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = test_properties_mid_subarray
        cm_conf = ComponentManagerConfiguration("", module_logger)
        cm_conf.add_attributes()
        subarray_component_manager = MidCspSubarrayComponentManager(
            subarray_health_state_model,
            subarray_op_state_model,
            subarray_obs_state_model,
            cm_conf,
            logger=module_logger,
        )
        subarray_component_manager.init()
        yield subarray_component_manager


@pytest.fixture()
@mock.patch(
    "ska_csp_lmc_mid.manager.mid_capability_connection.Connector",
    new=MidMockedConnector,
)
@mock.patch("ska_csp_lmc_common.component.Connector", new=MidMockedConnector)
def ctrl_cm_connected(ctrl_component_manager):
    """Perform the Mid CSP controller initialization.

    The Connector class is mocked through the MockedConnector class.
    """
    ctrl_component_manager._store_admin_mode = (
        ctrl_component_manager._admin_mode
    )
    ctrl_component_manager._admin_mode = 0  # AdminMode.ONLINE
    ctrl_component_manager.start_communicating()
    probe_poller(ctrl_component_manager, "is_communicating", True, time=3)
    time.sleep(2)


@pytest.fixture()
@mock.patch(
    "ska_csp_lmc_mid.manager.mid_capability_connection.Connector",
    new=MidMockedConnector,
)
@mock.patch("ska_csp_lmc_common.component.Connector", new=MidMockedConnector)
def subarray_cm_connected(subarray_component_manager):
    """Perform the CSP subarray initialization with PST beams that can already
    belong to the subarray (re-initialization).

    The Connector class is mocked through the MockedConnector class.
    """

    subarray_component_manager.start_communicating()
    probe_poller(subarray_component_manager, "is_communicating", True, time=3)
    probe_poller(
        subarray_component_manager, "capability_connected", True, time=3
    )
    # time.sleep(2)


@pytest.fixture()
def mock_config_init():
    mock_config.mock_timeout = False
    mock_config.mock_exception = False
    mock_config.mock_fail_device = None
    mock_config.mock_event_failure = False
    mock_config.mock_event_value = None
    mock_config.mock_event = False
    mock_config.mock_cbk_cmd_result = None
    mock_config.mock_cbk_cmd = False


@pytest.fixture
def change_event_callbacks() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of Tango device change event callbacks with
    asynchrony support.

    :return: a collections.defaultdict that returns change event
        callbacks by name.
    """
    return MockTangoEventCallbackGroup(
        "adminMode",
        "obsState",
        "state",
        "healthState",
        "isCommunicating",
        "longRunningCommandProgress",
        "longRunningCommandResult",
        "longRunningCommandStatus",
        timeout=5,
    )
