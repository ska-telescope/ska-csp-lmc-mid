from ska_csp_lmc_common.testing.test_classes import TestBase


class TestMidFspCapability(TestBase):
    @classmethod
    def raise_event_on_fsp(
        cls, id, attr_name, value, argument=None, callback=None
    ):
        """Helper function to raise a change event on a PST Controller
        attribute."""
        fsp_id = f"fsp-0{id}"
        cls.raise_event_on_sub_system(
            attr_name, value, cls.devices_list[fsp_id], argument, callback
        )


class TestMidVccCapability(TestBase):
    @classmethod
    def raise_event_on_vcc(
        cls, id, attr_name, value, argument=None, callback=None
    ):
        """Helper function to raise a change event on a PST Controller
        attribute."""
        # vcc_id = f"vcc-{id}"
        vcc_id = f"vcc-0{id}"
        cls.raise_event_on_sub_system(
            attr_name, value, cls.devices_list[vcc_id], argument, callback
        )
