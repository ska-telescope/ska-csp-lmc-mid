import logging

import numpy
from ska_control_model import AdminMode, HealthState, SimulationMode
from ska_csp_lmc_common.testing.mock_attribute import (
    AttributeData,
    AttributeDataFactory,
)
from tango import DevState

from ska_csp_lmc_mid.constant import FspModes

module_logger = logging.getLogger(__name__)


class MidAttributeDataFactory(AttributeDataFactory):
    attributes_ctrl_default = dict(
        AttributeDataFactory.attributes_ctrl_default
    )
    attributes_subarray_default = dict(
        AttributeDataFactory.attributes_subarray_default
    )
    attributes_ctrl_default.update(
        {"cspVccCapabilityAddress": "mid-csp/capability-vcc/0"}
    )
    attributes_ctrl_default.update(
        {"cspFspCapabilityAddress": "mid-csp/capability-fsp/0"}
    )
    attributes_ctrl_default.update(
        {"unassignedReceptorIDs": ["SKA022", "SKA103", "SKA104"]}
    )
    attributes_ctrl_default.update({"receptorMembership": [2, 1, 0, 0]})
    attributes_ctrl_default.update(
        {"receptorsList": ["SKA001", "SKA022", "SKA103", "SKA104"]}
    )
    attributes_ctrl_default.update(
        {"vcctoreceptor": ["1:86", "2:168", "3:65", "4:167"]}
    )
    attributes_ctrl_default.update(
        {"receptortovcc": ["65:3", "86:1", "167:4", "168:2"]}
    )
    attributes_ctrl_default.update(
        {"reportvccsubarraymembership": [1, 2, 0, 0]}
    )

    attributes_ctrl_default.update(
        {
            "reportvccstate": [
                DevState.ON,
                DevState.ON,
                DevState.ON,
                DevState.FAULT,
            ]
        }
    )
    attributes_ctrl_default.update(
        {
            "reportvcchealthstate": [
                HealthState.OK,
                HealthState.OK,
                HealthState.OK,
                HealthState.DEGRADED,
            ]
        }
    )

    attributes_subarray_default.update(
        {"receptors": ["SKA001", "SKA022", "SKA103", "SKA104"]}
    )
    attributes_fsp_default = {
        "state": DevState.OFF,
        "healthstate": HealthState.UNKNOWN,
        "functionmode": FspModes.IDLE,
        # "subarraymembership": [1, 2],
        "subarraymembership": numpy.array([1, 2], numpy.uint16),
        "adminmode": AdminMode.ONLINE,
        "versionid": "0.11.6",
        "hwversion": "NA",
        "fwversion": "NA",
        "fspssubarray01": "",
        "fspsfqdnassigned": [
            ["", "", "", ""],
            ["", "", "", ""],
            ["", "", "", ""],
        ],
    }

    attributes_vcc_default = {
        "state": DevState.OFF,
        "healthstate": HealthState.UNKNOWN,
        "subarraymembership": 1,
        "adminmode": AdminMode.ONLINE,
        "simulationmode": SimulationMode.TRUE,
        "dishid": "SKA001",
        "bandavailable": [
            2,
        ],
        "frequencyband": 2,
        "deviceid": 1,
        "versionid": "0.11.6",
        "hwversion": "NA",
        "fwversion": "NA",
        "vccssubarray01": "",
        "vccsfqdnassigned": """{
            "1": ["mid-cbf/vcc/001", "mid-cbf/vcc/002"]
            }
            """,
        "vccJson": """{
                    "vccs_deployed": 3,
                    "vcc_fqdn_member_order": [1,2, 3],
                    "vcc": [
                            {
                            "vcc_id": 1,
                            "receptor_id": "SKA001",
                            "fqdn": "mid-cbf/vcc/001",
                            "band_available": [2],
                            "band_active": "2",
                            "subarray_membership": 1
                            },
                            {
                            "vcc_id": 2,
                            "receptor_id": "SKA002",
                            "fqdn": "mid-cbf/vcc/002",
                            "band_available": [2],
                            "band_active": "2",
                            "subarray_membership": 1
                            },
                            {
                            "vcc_id": 3,
                            "receptor_id": "SKA003",
                            "fqdn": "mid-cbf/vcc/003",
                            "band_available": [2],
                            "band_active": "2",
                            "subarray_membership": 0
                            },
                            {
                            "vcc_id": 4,
                            "receptor_id": "SKA004",
                            "fqdn": "mid-cbf/vcc/004",
                            "band_available": [1],
                            "band_active": "1",
                            "subarray_membership": 0
                            }
                            ]}
                            """,
    }

    def create_attribute(self, attr_name):
        if "fsp" in self._device:

            return AttributeData(
                attr_name, self.__class__.attributes_fsp_default
            )
        elif "vcc" in self._device:
            return AttributeData(
                attr_name, self.__class__.attributes_vcc_default
            )
        return super().create_attribute(attr_name)
