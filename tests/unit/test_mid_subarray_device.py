import json
import logging
import time

import mock
import pytest
from mid_attribute_factory import MidAttributeDataFactory

# Local imports
from mid_mocked_connector import MidMockedConnector
from mid_properties import test_properties_mid_subarray

# SKA imports
from ska_control_model import AdminMode, HealthState, ObsMode, ObsState
from ska_csp_lmc_common.subarray import PstBeamComponent
from ska_csp_lmc_common.testing import mock_config
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.testing.test_classes import TestBaseSubarray
from ska_csp_lmc_common.testing.test_utils import mock_cbk_with_cmd_result
from ska_csp_lmc_common.utils.cspcommons import read_release_version
from ska_tango_base.commands import ResultCode
from tango import DevFailed, DevState

from ska_csp_lmc_mid import release

module_logger = logging.getLogger(__name__)

# Device test case
device_to_load = {
    "path": "tests/unit/csplmc_dsconfig.json",
    "package": "ska_csp_lmc_mid",
    "device": "subarray1",
}


@pytest.fixture
@mock.patch("ska_csp_lmc_common.component.Connector", new=MidMockedConnector)
@mock.patch(
    "ska_csp_lmc_mid.manager.mid_capability_connection.Connector",
    new=MidMockedConnector,
)
def mid_subarray_init(device_under_test):
    """Perform the CSP subarray initialization belong to the subarray (re-
    initialization).

    The Connector class is mocked through the MockedConnector class.
    """
    with mock.patch.object(
        PstBeamComponent, "subarray_id", new_callable=mock.PropertyMock
    ) as mocked_subarray_id:
        mocked_subarray_id.return_value = 0
        # at the end of the device intialization, the device is
        # OFFLINE in DISABLE state and UNKNOWN healthState
        probe_poller(device_under_test, "state", DevState.DISABLE, time=5)
        assert device_under_test.adminmode == AdminMode.OFFLINE
        probe_poller(device_under_test, "state", DevState.DISABLE, time=5)
        assert device_under_test.healthstate == HealthState.UNKNOWN
        assert device_under_test.obsstate == ObsState.EMPTY
        device_under_test.adminMode = AdminMode.ENGINEERING
        probe_poller(
            device_under_test, "adminMode", AdminMode.ENGINEERING, time=5
        )
        # wait to let the component manager subscribe for events
        probe_poller(device_under_test, "isCommunicating", True, time=5)
        probe_poller(device_under_test, "State", DevState.OFF, time=5)
        probe_poller(device_under_test, "healthState", HealthState.OK, time=5)
        probe_poller(device_under_test, "capabilityConnected", True, time=5)


@pytest.fixture
@mock.patch("ska_csp_lmc_common.component.Connector", new=MidMockedConnector)
@mock.patch(
    "ska_csp_lmc_mid.manager.mid_capability_connection.Connector",
    new=MidMockedConnector,
)
def mid_subarray_init_with_all_subsystems(device_under_test):
    """Perform the CSP subarray initialization with all PST beams already
    belonging to the subarray (re-initialization).

    The Connector class is mocked through the MockedConnector class.
    """
    with mock.patch.object(
        PstBeamComponent, "subarray_id", new_callable=mock.PropertyMock
    ) as mocked_subarray_id:
        mocked_subarray_id.return_value = 1

        # at the end of the device intialization, the device is
        # OFFLINE in DISABLE state and UNKNOWN healthState
        probe_poller(device_under_test, "adminMode", AdminMode.OFFLINE, time=1)
        probe_poller(device_under_test, "state", DevState.DISABLE, time=1)
        assert device_under_test.healthstate == HealthState.UNKNOWN
        assert device_under_test.obsstate == ObsState.EMPTY
        # enable the device starting the connection with the sub-systems
        device_under_test.adminMode = AdminMode.ENGINEERING
        probe_poller(
            device_under_test, "adminMode", AdminMode.ENGINEERING, time=2
        )
        # wait to let the component manager subscribe for events
        probe_poller(device_under_test, "isCommunicating", True, time=20)
        probe_poller(device_under_test, "State", DevState.OFF, time=3)
        probe_poller(device_under_test, "healthState", HealthState.OK, time=3)


@mock.patch("ska_csp_lmc_common.component.Connector", new=MidMockedConnector)
class TestMidCspSubarray(TestBaseSubarray):
    devices_list = {
        "cbf-subarray": test_properties_mid_subarray["CbfSubarray"],
        "pss-subarray": test_properties_mid_subarray["PssSubarray"],
        "pst-beam-01": test_properties_mid_subarray["PstBeams"][0],
        "pst-beam-02": test_properties_mid_subarray["PstBeams"][1],
    }

    def test_mid_subarray_first_initialization(
        self, device_under_test, mid_subarray_init
    ):
        """Perform the CSP first initialization, with PST Beams not assigned:
        they return 0 for the subarray_id.
        """
        assert device_under_test.State() == DevState.OFF
        pst_beams_state = device_under_test.pstBeamsState
        pst_state_dict = json.loads(pst_beams_state)
        if "mid-pst/beams/01" in pst_state_dict:
            assert pst_state_dict["mid-pst/beams/01"] == "UNKNOWN"
        if "mid-pst/beams/01" in pst_state_dict:
            assert pst_state_dict["mid-pst/beams/02"] == "UNKNOWN"

    def test_mid_subarray_init_with_all_sub_systems(
        self, device_under_test, mid_subarray_init_with_all_subsystems
    ):
        """Perform the CSP first initialization, with PST Beams already
        belonging to the Mid.CSP subarray."""
        # add sleep to be sure that capability connection is performed
        time.sleep(2)
        assert device_under_test.State() == DevState.OFF
        pst_beams_state = device_under_test.pstBeamsState
        pst_state_dict = json.loads(pst_beams_state)
        if "mid-pst/beams/01" in pst_state_dict:
            assert pst_state_dict["mid-pst/beams/01"] == "OFF"
        if "mid-pst/beams/02" in pst_state_dict:
            assert pst_state_dict["mid-pst/beams/02"] == "OFF"
        pst_beams_health = device_under_test.pstBeamsHealthState
        pst_health_dict = json.loads(pst_beams_health)
        if "mid-pst/beams/01" in pst_state_dict:
            assert pst_health_dict["mid-pst/beams/01"] == "OK"
        if "mid-pst/beams/02" in pst_state_dict:
            assert pst_health_dict["mid-pst/beams/02"] == "OK"

    def test_version_id(self, device_under_test):

        version_id, build_state = read_release_version(release)
        assert device_under_test.versionId == version_id
        assert device_under_test.buildState == build_state

    @mock.patch(
        "ska_csp_lmc_mid.subarray.mid_cbf_subarray_component.Connector",
        new=MidMockedConnector,
    )
    @pytest.mark.skip(
        reason="TelModel in Mid has no schema"
        " to assign PSS and PST resources"
    )
    def test_mid_subarray_assign_resources_cbf_pss_pst(
        self, device_under_test, mid_subarray_init
    ):

        # force the subarray to ON/EMPTY
        TestMidCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestMidCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.EMPTY
        )

        # load the file with resources
        config_dict = self.assignresources_input(
            filename="AssignResources_CBF_PSS_PST.json"
        )
        # build the attribute for the test

        config_string = json.dumps(config_dict)
        device_under_test.AssignResources(config_string)

        probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=5)
        # assignedReceptors is a numpy.array
        assert (
            list(device_under_test.assignedReceptors)
            == MidAttributeDataFactory.attributes_subarray_default["receptors"]
        )

        # NOTE: it is not possible to set probe_poller for a list
        # This can be solved checking the LRCStatus COMPLETED.
        # Still can't be done with probe_poller
        timeout = 5
        t_0 = time.time()
        time_elaps = 0
        while time_elaps < timeout:
            if time_elaps > timeout:
                assert False, "AssignedTimingBeamsIDs is "
                f"{list(device_under_test.assignedTimingBeamIDs)}"
            time_elaps = time.time() - t_0
            assert list(device_under_test.assignedTimingBeamIDs) == [1, 2]
            time.sleep(0.1)

        assert len(device_under_test.assignedResources) == 4
        assert device_under_test.assignedResources == (
            "mid-cbf/vcc/001",
            "mid-cbf/vcc/002",
            "mid-pst/beams/01",
            "mid-pst/beams/02",
        )
        assert device_under_test.assignedVcc.tolist() == [1, 2]

    @mock.patch(
        "ska_csp_lmc_mid.subarray.mid_cbf_subarray_component.Connector",
        new=MidMockedConnector,
    )
    def test_mid_subarray_assign_resources(
        self, device_under_test, mid_subarray_init
    ):

        # force the subarray to ON/EMPTY
        TestMidCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestMidCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.EMPTY
        )

        # load the file with resources
        config_dict = self.assignresources_input(
            filename="AssignResources_CBF.json"
        )
        # build the attribute for the test

        config_string = json.dumps(config_dict)
        device_under_test.AssignResources(config_string)

        probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=5)
        # assignedReceptors is a numpy.array
        assert (
            list(device_under_test.assignedReceptors)
            == MidAttributeDataFactory.attributes_subarray_default["receptors"]
        )

        assert len(device_under_test.assignedResources) == 2
        assert device_under_test.assignedResources == (
            "mid-cbf/vcc/001",
            "mid-cbf/vcc/002",
        )
        assert device_under_test.assignedVcc.tolist() == [1, 2]

    @mock.patch(
        "ska_csp_lmc_mid.subarray.mid_cbf_subarray_component.Connector",
        new=MidMockedConnector,
    )
    def test_mid_subarray_re_assign_resources(
        self, device_under_test, mid_subarray_init, mock_config_init
    ):
        # configure the system to raise this CmdDone event
        mock_config.mock_cbk_cmd_result = mock_cbk_with_cmd_result(
            "assignresources",
            "mid-cbf/subarray/01",
            tuple([ResultCode.OK, "Command OK"]),
        )
        # configure the value returned for the Mid attributes
        MidAttributeDataFactory.attributes_subarray_default[
            "unassignedReceptorIDs"
        ] = ["SKA001", "SKA022", "SKA103", "SKA104"]
        MidAttributeDataFactory.attributes_subarray_default[
            "receptorMembership"
        ] = [0, 0, 0, 0]
        # force the device to ON/IDLE
        TestMidCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestMidCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )
        assert device_under_test.obsstate == ObsState.IDLE
        # exercise the device
        config_dict = TestMidCspSubarray.assignresources_input(
            # filename="AssignResources_CBF_PSS_PST.json"
            filename="AssignResources_CBF.json"
        )
        config_string = json.dumps(config_dict)
        _, command_id = device_under_test.AssignResources(config_string)

        probe_poller(
            device_under_test,
            "obsstate",
            ObsState.IDLE,
            time=5,
            command_id=command_id,
        )

        # assignedReceptors is a numpy.array
        assert (
            list(device_under_test.assignedReceptors)
            == MidAttributeDataFactory.attributes_subarray_default["receptors"]
        )

    @mock.patch(
        "ska_csp_lmc_common.component.Connector", new=MidMockedConnector
    )
    def test_mid_subarray_idle_with_no_resources_to_assign(
        self, device_under_test, mid_subarray_init
    ):
        """Test that the CSP Subarray maintains its obsState to EMPTY if the
        input json has no valid resources to allocate to the subarray,"""
        TestMidCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestMidCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.EMPTY
        )
        assert device_under_test.obsstate == ObsState.EMPTY
        config_string = '{"subarray_id": 1, "dish": {}}'
        _, command_id = device_under_test.AssignResources(config_string)
        probe_poller(
            device_under_test,
            "longRunningCommandStatus",
            "COMPLETED",
            time=10,
            command_id=command_id,
        )
        time.sleep(5)
        assert device_under_test.commandResult == (
            "assignresources",
            str(ResultCode.FAILED.value),
        )

        # probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=10)

    @mock.patch(
        "ska_csp_lmc_mid.subarray.mid_cbf_subarray_component.Connector",
        new=MidMockedConnector,
    )
    def test_mid_subarray_removeall_resources(
        self, device_under_test, mid_subarray_init
    ):
        with mock.patch(
            "ska_csp_lmc_mid.subarray"
            ".MidCbfSubarrayComponent.assigned_receptors",
            new_callable=mock.PropertyMock,
        ) as mock_read:
            mock_read.return_value = (
                MidAttributeDataFactory.attributes_subarray_default[
                    "receptors"
                ]
            )
            TestMidCspSubarray.set_subsystems_and_go_to_state(
                device_under_test, DevState.ON
            )
            TestMidCspSubarray.set_subsystems_and_go_to_obsstate(
                device_under_test, ObsState.IDLE
            )
            assert device_under_test.obsstate == ObsState.IDLE
            device_under_test.ReleaseAllResources()
            probe_poller(device_under_test, "obsstate", ObsState.EMPTY, time=7)

    def test_subarray_cm_configure_no_sub_id(
        self,
        device_under_test,
        mid_subarray_init,
    ):
        """
        Test CSP Subarray Configure when the configure script does not specify
        sub_id.

        Expected behavior:
        - the CSP Subarray observing state won't be updated, since the command
          is not executed at all.
        - the command raises an Exception

        """
        TestMidCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestMidCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )
        config_input = TestMidCspSubarray.configuration_input(
            filename="Configure_CBF.json"
        )
        config_input["common"].pop("subarray_id")
        with pytest.raises(
            DevFailed,
        ) as df:
            device_under_test.Configure(json.dumps(config_input))
            assert "ValueError: Validation 'CSP config 2.0' Key " in str(df)
        assert device_under_test.obsstate == ObsState.IDLE

    def test_subarray_cm_configure_wrong_sub_id(
        self,
        device_under_test,
        mid_subarray_init,
    ):
        """
        Test CSP Subarray Configure when the configure script does not specify
        sub_id.

        Expected behavior:
        - the CSP Subarray observing state won't be updated, since the command
          is not executed at all.
        - the command raises an exception
        """
        TestMidCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestMidCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )
        config_input = TestMidCspSubarray.configuration_input(
            filename="Configure_CBF.json"
        )
        config_input["common"]["subarray_id"] = 4
        with pytest.raises(DevFailed) as df:
            device_under_test.Configure(json.dumps(config_input))
        assert "ValueError: Wrong value for the subarray ID: 4" in str(df)

    def test_subarray_cm_configure(
        self,
        device_under_test,
        mid_subarray_init,
    ):
        """
        Test CSP Subarray Configure for interface ver 3.0 and 4.0

        Expected behavior:
        - the script is validated
        - the CspSubarray device moves to READY
        - for ver 3.0 the obsMode is IMAGING
        """
        TestMidCspSubarray.set_subsystems_and_go_to_state(
            device_under_test, DevState.ON
        )
        TestMidCspSubarray.set_subsystems_and_go_to_obsstate(
            device_under_test, ObsState.IDLE
        )
        config_input = TestMidCspSubarray.configuration_input(
            filename="Configure_CBF_ver3.json"
        )
        device_under_test.Configure(json.dumps(config_input))
        probe_poller(device_under_test, "obsstate", ObsState.READY, time=7)
        val = (ObsMode.IMAGING,)
        probe_poller(device_under_test, "obsMode", val, time=3)
        device_under_test.GoToIdle()
        val = (ObsMode.IDLE,)
        probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=7)
        probe_poller(device_under_test, "obsMode", val, time=3)
        config_input = TestMidCspSubarray.configuration_input(
            filename="Configure_CBF_ver4.json"
        )
        device_under_test.Configure(json.dumps(config_input))
        probe_poller(device_under_test, "obsstate", ObsState.READY, time=7)
        val = (ObsMode.IMAGING,)
        probe_poller(device_under_test, "obsMode", val, time=10)
