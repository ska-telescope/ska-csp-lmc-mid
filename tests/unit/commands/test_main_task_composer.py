import logging

# import pytest
from mock import MagicMock
from ska_control_model import ObsState
from ska_csp_lmc_common.commands.main_task_composer import MainTaskComposer
from ska_csp_lmc_common.commands.task import LeafTask, Task, TaskExecutionType
from tango import DevState

from ska_csp_lmc_mid.commands.mid_command_map import MidCommandMap

module_logger = logging.getLogger(__name__)

dummy_cm = MagicMock()
dummy_cm.online_components = {
    "sim_csp_cbf/subarray/01": MagicMock(),
    "sim-pss/subarray/01": MagicMock(),
    "sim-pst/beam/01": MagicMock(),
    "sim-pst/beam/02": MagicMock(),
}


def test_compose_assign_resources():
    """test that the MainTask composer compose assigneresources
    as expected from command map"""
    # All subsystems are in the right state
    for _, component in dummy_cm.online_components.items():
        component.obs_state = ObsState.EMPTY

    dummy_cm.resources = {
        "sim_csp_cbf/subarray/01": {"dummy_resources_cbf": "ok"},
        "sim-pss/subarray/01": {"dummy_resources_pss": "ok"},
        "sim-pst/beam/02": {"subarray_id": 1},
        "sim-pst/beam/01": {"subarray_id": 1},
    }

    dummy_callback = MagicMock()

    composed = Task(
        "assign",
        task_type=TaskExecutionType.PARALLEL,
        subtasks=[
            LeafTask(
                name="internal_connect_to_pst",
                target=dummy_cm,
                command_name="connect_to_pst",
                task_type=TaskExecutionType.INTERNAL,
            ),
            LeafTask(
                name="assignresources_pss",
                target=dummy_cm.online_components["sim-pss/subarray/01"],
                command_name="assignresources",
                task_type=TaskExecutionType.DEVICE,
                argin={"dummy_resources_pss": "ok"},
            ),
            LeafTask(
                "addreceptors_cbf",
                dummy_cm.online_components[
                    "sim_csp_cbf/subarray/01"
                ],  # Fixed here
                "addreceptors",
                task_type=TaskExecutionType.DEVICE,
                argin={"dummy_resources_cbf": "ok"},
            ),
        ],
        completed_callback=dummy_callback,
    )

    command_map = MidCommandMap()
    subarray_command_map = command_map.get_subarray_command_map()
    assert (
        MainTaskComposer(
            "assign", subarray_command_map, dummy_cm, dummy_callback
        ).compose()
        == composed
    )


def test_compose_configure_all():
    """test that the MainTask composer compose configure command
    as expected from command map"""
    # All subsystems are in the right state
    for _, component in dummy_cm.online_components.items():
        component.obs_state = ObsState.READY
    dummy_cm.resources = {
        "sim_csp_cbf/subarray/01": {"dummy_configure_cbf": "ok"},
        "sim-pst/beam/01": {"dummy_configure_pst1": "ok"},
        "sim-pst/beam/02": {"dummy_configure_pst2": "ok"},
    }
    dummy_callback = MagicMock()

    composed = Task(
        "configure",
        task_type=TaskExecutionType.SEQUENTIAL,
        subtasks=[
            Task(
                "configure_pst_pss",
                task_type=TaskExecutionType.PARALLEL,
                subtasks=[
                    Task(
                        "configurescan_pst",
                        task_type=TaskExecutionType.PARALLEL,
                        subtasks=[
                            LeafTask(
                                "configurescan_pst01",
                                dummy_cm.online_components["sim-pst/beam/01"],
                                "configurescan",
                                task_type=TaskExecutionType.DEVICE,
                                argin={"dummy_configure_pst1": "ok"},
                            ),
                            LeafTask(
                                "configurescan_pst02",
                                dummy_cm.online_components["sim-pst/beam/02"],
                                "configurescan",
                                task_type=TaskExecutionType.DEVICE,
                                argin={"dummy_configure_pst2": "ok"},
                            ),
                        ],
                    ),
                ],
            ),
            LeafTask(
                name="internal_update_json",
                target=dummy_cm,
                command_name="update_json",
                task_type=TaskExecutionType.INTERNAL,
            ),
            LeafTask(
                name="configurescan_cbf",
                target=dummy_cm.online_components["sim_csp_cbf/subarray/01"],
                command_name="configurescan",
                task_type=TaskExecutionType.DEVICE,
                argin={"dummy_configure_cbf": "ok"},
            ),
        ],
    )
    command_map = MidCommandMap()
    subarray_command_map = command_map.get_subarray_command_map()
    assert (
        MainTaskComposer(
            "configure", subarray_command_map, dummy_cm, dummy_callback
        ).compose()
        == composed
    )


def test_compose_configure_only_cbf():
    """test that the MainTask composer compose assigneresources
    as expected from command map"""
    # Send command only to CBF
    for _, component in dummy_cm.online_components.items():
        component.obs_state = ObsState.READY
    dummy_cm.resources = {
        "sim_csp_cbf/subarray/01": {"dummy_configure_cbf": "ok"},
    }
    dummy_callback = MagicMock()

    composed = Task(
        "configure",
        task_type=TaskExecutionType.SEQUENTIAL,
        subtasks=[
            LeafTask(
                name="internal_update_json",
                target=dummy_cm,
                command_name="update_json",
                task_type=TaskExecutionType.INTERNAL,
            ),
            LeafTask(
                name="configurescan_cbf",
                target=dummy_cm.online_components["sim_csp_cbf/subarray/01"],
                command_name="configurescan",
                task_type=TaskExecutionType.DEVICE,
                argin={"dummy_configure_cbf": "ok"},
            ),
        ],
    )
    command_map = MidCommandMap()
    subarray_command_map = command_map.get_subarray_command_map()
    assert (
        MainTaskComposer(
            "configure", subarray_command_map, dummy_cm, dummy_callback
        ).compose()
        == composed
    )


def test_compose_scan_cbf_pst01():
    """test that the MainTask composer compose scan
    as expected from command map"""
    # test that the command map is not modified from common command map
    # All subsystems are in the right state
    for _, component in dummy_cm.online_components.items():
        component.obs_state = ObsState.READY
    dummy_cm.resources = {
        "sim_csp_cbf/subarray/01": {"dummy_scan_cbf": "ok"},
        "sim-pst/beam/01": {"dummy_scan_pst1": "ok"},
    }
    dummy_callback = MagicMock()

    composed = Task(
        "scan",
        task_type=TaskExecutionType.PARALLEL,
        subtasks=[
            Task(
                "scan_pst",
                task_type=TaskExecutionType.PARALLEL,
                subtasks=[
                    LeafTask(
                        "scan_pst01",
                        dummy_cm.online_components["sim-pst/beam/01"],
                        "scan",
                        task_type=TaskExecutionType.DEVICE,
                        argin={"dummy_scan_pst1": "ok"},
                    ),
                ],
            ),
            LeafTask(
                name="scan_cbf",
                target=dummy_cm.online_components["sim_csp_cbf/subarray/01"],
                command_name="scan",
                task_type=TaskExecutionType.DEVICE,
                argin={"dummy_scan_cbf": "ok"},
            ),
        ],
    )
    command_map = MidCommandMap()
    subarray_command_map = command_map.get_subarray_command_map()
    assert (
        MainTaskComposer(
            "scan", subarray_command_map, dummy_cm, dummy_callback
        ).compose()
        == composed
    )


def test_compose_off_controller():
    """test that the MainTask composer compose off command from controller
    as expected from command map"""
    # test that the command map is not modified from common command map
    dummy_cm.online_components = {
        "sim_csp_cbf/sub_elt/controller": MagicMock(),
        "sim-pst/beam/01": MagicMock(),
        "sim-pst/beam/02": MagicMock(),
        "sim-csp/subarray/01": MagicMock(),
        "sim-csp/subarray/02": MagicMock(),
        "sim-csp/subarray/03": MagicMock(),
    }
    dummy_cm.online_fqdns = list(dummy_cm.online_components.keys())

    dummy_cm.resources = {}
    for fqdn, component in dummy_cm.online_components.items():
        dummy_cm.resources[fqdn] = None
        component.state = DevState.ON

    dummy_callback = MagicMock()

    composed = Task(
        "off",
        task_type=TaskExecutionType.PARALLEL,
        subtasks=[
            Task(
                "off_pst",
                task_type=TaskExecutionType.PARALLEL,
                subtasks=[
                    LeafTask(
                        "off_pst01",
                        target=dummy_cm.online_components["sim-pst/beam/01"],
                        command_name="off",
                        task_type=TaskExecutionType.DEVICE,
                    ),
                    LeafTask(
                        "off_pst02",
                        target=dummy_cm.online_components["sim-pst/beam/02"],
                        command_name="off",
                        task_type=TaskExecutionType.DEVICE,
                    ),
                ],
            ),
            LeafTask(
                "off_cbf",
                target=dummy_cm.online_components[
                    "sim_csp_cbf/sub_elt/controller"
                ],
                command_name="off",
                task_type=TaskExecutionType.DEVICE,
            ),
        ],
        completed_callback=dummy_callback,
    )
    command_map = MidCommandMap()
    controller_command_map = command_map.get_controller_command_map()
    assert (
        MainTaskComposer(
            "off", controller_command_map, dummy_cm, dummy_callback
        ).compose()
        == composed
    )


def test_compose_loaddishcfg_controller():
    """test that the MainTask composer compose loadDishCfg command
    from controller as expected from command map"""

    dummy_cm.online_components = {
        "sim_csp_cbf/sub_elt/controller": MagicMock(),
        "sim-pst/beam/01": MagicMock(),
        "sim-pst/beam/02": MagicMock(),
        "sim-csp/subarray/01": MagicMock(),
        "sim-csp/subarray/02": MagicMock(),
        "sim-csp/subarray/03": MagicMock(),
    }
    dummy_cm.online_fqdns = list(dummy_cm.online_components.keys())

    dish_config = {"interface": {"input": 3}}
    dummy_cm.resources = {}
    for fqdn, component in dummy_cm.online_components.items():
        if fqdn == "sim_csp_cbf/sub_elt/controller":
            dummy_cm.resources[fqdn] = dish_config
            component.state = DevState.OFF

    dummy_callback = MagicMock()

    composed = Task(
        "loaddishcfg",
        task_type=TaskExecutionType.SEQUENTIAL,
        subtasks=[
            LeafTask(
                "initsysparam_cbf",
                target=dummy_cm.online_components[
                    "sim_csp_cbf/sub_elt/controller"
                ],
                command_name="initsysparam",
                task_type=TaskExecutionType.DEVICE,
                argin={"interface": {"input": 3}},
            ),
            LeafTask(
                name="internal_update_loaddishcfg",
                target=dummy_cm,
                command_name="update_loaddishcfg",
                task_type=TaskExecutionType.INTERNAL,
            ),
        ],
        completed_callback=dummy_callback,
    )
    command_map = MidCommandMap()
    controller_command_map = command_map.get_controller_command_map()
    assert (
        MainTaskComposer(
            "loaddishcfg", controller_command_map, dummy_cm, dummy_callback
        ).compose()
        == composed
    )
