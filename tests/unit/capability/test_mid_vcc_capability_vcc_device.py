import json
import logging

import mock
import pytest
from mid_mocked_connector import CapabilityMockedConnector
from mid_test_classes import TestMidVccCapability
from ska_control_model import HealthState
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.utils.cspcommons import read_release_version
from tango import DevState

from ska_csp_lmc_mid import release

device_to_load = {
    "path": "charts/ska-csp-lmc-mid/data/midcspconfig.json",
    "package": "ska_csp_lmc_mid",
    "device": "capabilityvcc",
}

module_logger = logging.getLogger(__name__)


class TestMidVccCapabilityDevice(TestMidVccCapability):
    @mock.patch(
        "ska_csp_lmc_common.component.Connector", new=CapabilityMockedConnector
    )
    def test_vcc_device_initialization(self, device_under_test):
        probe_poller(device_under_test, "state", DevState.ON, time=10)

        assert device_under_test.healthstate == HealthState.OK
        assert device_under_test.vccsDeployed == 4
        assert device_under_test.vccFqdn == (
            "mid_csp_cbf/vcc/001",
            "mid_csp_cbf/vcc/002",
            "mid_csp_cbf/vcc/003",
            "mid_csp_cbf/vcc/004",
        )
        assert list(device_under_test.vccState) == [
            "OFF",
            "OFF",
            "OFF",
            "OFF",
        ]
        assert device_under_test.vccHealthState == (
            "UNKNOWN",
            "UNKNOWN",
            "UNKNOWN",
            "UNKNOWN",
        )
        assert device_under_test.vccAdminMode == (
            "ONLINE",
            "ONLINE",
            "ONLINE",
            "ONLINE",
        )

        probe_poller(device_under_test, "isCommunicating", True, time=5)

        fqdn_member = []
        for fqdn in device_under_test.vccFqdn:
            fqdn_member.append(fqdn[-3:])

        assert device_under_test.isCommunicating
        vcc_json_dict = json.loads(device_under_test.vccJson)
        assert vcc_json_dict["vccs_deployed"] == device_under_test.vccsDeployed
        for i in range(4):
            assert (
                vcc_json_dict["vcc"][i]["fqdn"]
                == f"mid_csp_cbf/vcc/{fqdn_member[i]}"
            )
            assert vcc_json_dict["vcc"][i]["health_state"] == "UNKNOWN"
            assert vcc_json_dict["vcc"][i]["admin_mode"] == "ONLINE"
            assert vcc_json_dict["vcc"][i]["subarray_membership"] == 1
            assert vcc_json_dict["vcc"][i]["vcc_id"] == i + 1

    @mock.patch(
        "ska_csp_lmc_common.component.Connector", new=CapabilityMockedConnector
    )
    def test_vcc_device_is_initialized_version(self, device_under_test):

        # reach VCC states on
        probe_poller(device_under_test, "state", DevState.ON, time=10)
        version_id, build_state = read_release_version(release)

        assert device_under_test.versionId == version_id
        assert device_under_test.buildState == build_state

        assert device_under_test.vccsSwVersion == (
            "0.11.6",
            "0.11.6",
            "0.11.6",
            "0.11.6",
        )
        assert device_under_test.vccsHwVersion == ("NA", "NA", "NA", "NA")
        assert device_under_test.vccsFwVersion == ("NA", "NA", "NA", "NA")

    @pytest.mark.skip(reason="The test is not stable")
    @mock.patch(
        "ska_csp_lmc_common.component.Connector", new=CapabilityMockedConnector
    )
    def test_vcc_fqdn_assigned(self, device_under_test):
        probe_poller(device_under_test, "isCommunicating", True, time=5)
        vcc_fqdn_assigned = device_under_test.vccsFqdnAssigned
        assert len(vcc_fqdn_assigned[1]) == 1
