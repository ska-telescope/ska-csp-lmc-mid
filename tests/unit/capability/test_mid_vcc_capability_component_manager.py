import logging
from time import sleep

import mock
import pytest
from mid_mocked_connector import CapabilityMockedConnector
from mid_properties import test_properties_capability
from mid_test_classes import TestMidVccCapability
from ska_control_model import HealthState
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.testing.poller import probe_poller
from tango import DevState

from ska_csp_lmc_mid.manager import MidVccCapabilityComponentManager

module_logger = logging.getLogger(__name__)


def callback(attr_name, attr_value):
    module_logger.debug(f"attr_name: {attr_name} value; {attr_value}")


@pytest.fixture(scope="function")
def capability_component_manager_disconnected(
    capability_op_state_model=None, capability_health_state_model=None
):
    with mock.patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = test_properties_capability
        cm_conf = ComponentManagerConfiguration("", module_logger)
        cm_conf.add_attributes()
        capability_component_manager = MidVccCapabilityComponentManager(
            properties=cm_conf,
            update_device_property_cbk=callback,
            logger=module_logger,
        )
        capability_component_manager.init()
        return capability_component_manager


@pytest.fixture(scope="function")
@mock.patch(
    "ska_csp_lmc_common.component.Connector", new=CapabilityMockedConnector
)
def capability_component_manager(
    capability_op_state_model=None, capability_health_state_model=None
):
    with mock.patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = test_properties_capability
        cm_conf = ComponentManagerConfiguration("", module_logger)
        cm_conf.add_attributes()
        capability_component_manager = MidVccCapabilityComponentManager(
            properties=cm_conf,
            update_device_property_cbk=callback,
            logger=module_logger,
        )
        capability_component_manager.init()
        capability_component_manager.start_communicating()
        probe_poller(
            capability_component_manager, "is_communicating", True, time=3
        )
        vcc_fqdn_list = capability_component_manager.devices_fqdn
        devices_dict = dict()
        for fqdn in vcc_fqdn_list:
            vcc_id = f"vcc-0{vcc_fqdn_list.index(fqdn)+1}"

            devices_dict.update({vcc_id: fqdn})
        TestMidVccCapabilityComponentManager.devices_list = devices_dict
        return capability_component_manager


class TestMidVccCapabilityComponentManager(TestMidVccCapability):
    def test_vcc_capability_cm_not_connected(
        self, capability_component_manager_disconnected
    ):
        import time

        # wait for the timeout (configured to 3 sec)
        time.sleep(4)
        probe_poller(
            capability_component_manager_disconnected,
            "is_communicating",
            False,
            time=3,
        )
        assert not capability_component_manager_disconnected.devices_state

    def test_vcc_capability_cm_connected(self, capability_component_manager):
        """
        Test the connection with the VCC devices and the initial values for the
        main attributes.
        """

        probe_poller(
            capability_component_manager,
            "devices_state",
            ["OFF", "OFF", "OFF", "OFF"],
            time=10,
        )
        probe_poller(
            capability_component_manager,
            "devices_admin_mode",
            [
                "ONLINE",
                "ONLINE",
                "ONLINE",
                "ONLINE",
            ],
            time=4,
        )
        probe_poller(
            capability_component_manager,
            "devices_health_state",
            [
                "UNKNOWN",
                "UNKNOWN",
                "UNKNOWN",
                "UNKNOWN",
            ],
            time=4,
        )
        assert capability_component_manager.devices_deployed == len(
            capability_component_manager.devices_fqdn
        )

    def test_subaray_map_with_vcc_sub_membership_change(
        self, capability_component_manager
    ):
        """
        Test the vcc assigned to the subarray after an events that modify the
        VCC subarray membership
        default subarray_membership config:
            vcc_1: [1]
            vcc_2: [1]
            vcc_3: [1]
            vcc_4: [1]
        deafult config: "state": DevState.OFF,
                        "health_state": 'UNKNOWN',
        """
        subarray_map = {}
        subarray_map = capability_component_manager.subarray_vcc_map.copy()

        for sub in subarray_map.keys():
            assert capability_component_manager.subarray_vcc_map[sub] == (
                subarray_map[sub]
            )
        modified_device_num = 1
        # id = str(modified_device_num).zfill(3)
        # vcc_id = f"vcc_{id}"
        modified_membership = 2
        probe_poller(
            capability_component_manager, "is_communicating", True, time=3
        )
        probe_poller(
            capability_component_manager,
            "devices_subarray_membership",
            [1, 1, 1, 1],
            time=5,
        )
        probe_poller(
            capability_component_manager,
            "devices_health_state",
            ["UNKNOWN", "UNKNOWN", "UNKNOWN", "UNKNOWN"],
            time=5,
        )
        # time.sleep(2)
        # wait for the cahange propagation
        TestMidVccCapabilityComponentManager.raise_event_on_vcc(
            modified_device_num,
            "subarraymembership",
            modified_membership,
        )
        # sleep(0.5)
        # check the FSPs assigned to the subarray at after the
        # configuration event
        probe_poller(
            capability_component_manager,
            "devices_subarray_membership",
            [2, 1, 1, 1],
            time=5,
        )
        probe_poller(
            capability_component_manager,
            "devices_state",
            ["OFF", "OFF", "OFF", "OFF"],
            time=5,
        )
        probe_poller(
            capability_component_manager,
            "devices_health_state",
            ["UNKNOWN", "UNKNOWN", "UNKNOWN", "UNKNOWN"],
            time=5,
        )

    def test_subaray_map_with_vcc_attributes_change(
        self, capability_component_manager
    ):
        """
        Test the vcc assigned to the subarray after some events that modify the
        one vcc attribute
        """
        probe_poller(
            capability_component_manager, "is_communicating", True, time=3
        )
        # time.sleep(2)
        subarray_map = {}
        subarray_map = capability_component_manager.subarray_vcc_map.copy()

        # check the FSPs assigned to the subarray at the initial configuration
        for sub in subarray_map.keys():
            assert capability_component_manager.subarray_vcc_map[sub] == (
                subarray_map[sub]
            )
        modified_attributes = ["state", "healthstate"]
        attr_name = ["state", "health_state"]
        input_value = [DevState.UNKNOWN, HealthState.UNKNOWN]
        return_value = ["UNKNOWN", "UNKNOWN"]
        modified_device_num = 1
        id = str(modified_device_num).zfill(3)
        vcc_id = f"vcc_{id}"

        for pos, attr in enumerate(modified_attributes):
            value = input_value[pos]
            ret_val = return_value[pos]
            TestMidVccCapabilityComponentManager.raise_event_on_vcc(
                modified_device_num, attr, value
            )
            # wait for the cahange propagation
            sleep(0.5)
            mod_subarray_map = (
                capability_component_manager.subarray_vcc_map.copy()
            )
            for sub_vcc_dict in mod_subarray_map.values():
                if vcc_id in sub_vcc_dict.keys():
                    assert ret_val == sub_vcc_dict[vcc_id][attr_name[pos]]
