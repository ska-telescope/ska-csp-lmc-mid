import logging
import time

import mock
import numpy
import pytest
from mid_mocked_connector import CapabilityMockedConnector
from mid_properties import test_properties_capability
from mid_test_classes import TestMidFspCapability
from ska_control_model import HealthState
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.testing.poller import probe_poller
from tango import DevState

from ska_csp_lmc_mid.constant import FspModes
from ska_csp_lmc_mid.manager import MidFspCapabilityComponentManager

module_logger = logging.getLogger(__name__)


def callback(attr_name, attr_value):
    module_logger.debug(f"attr_name: {attr_name} value; {attr_value}")


@pytest.fixture(scope="function")
def capability_component_manager_disconnected():
    with mock.patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = test_properties_capability
        cm_conf = ComponentManagerConfiguration("", module_logger)
        cm_conf.add_attributes()
        capability_component_manager = MidFspCapabilityComponentManager(
            properties=cm_conf,
            update_device_property_cbk=callback,
            logger=module_logger,
        )
        capability_component_manager.init()
        return capability_component_manager


@pytest.fixture(scope="function")
@mock.patch(
    "ska_csp_lmc_common.component.Connector", new=CapabilityMockedConnector
)
def capability_component_manager(
    capability_op_state_model=None, capability_health_state_model=None
):
    with mock.patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = test_properties_capability
        cm_conf = ComponentManagerConfiguration("", module_logger)
        cm_conf.add_attributes()
        capability_component_manager = MidFspCapabilityComponentManager(
            properties=cm_conf,
            update_device_property_cbk=callback,
            logger=module_logger,
        )
        capability_component_manager.init()
        capability_component_manager.start_communicating()
        probe_poller(
            capability_component_manager, "is_communicating", True, time=3
        )
        fsp_fqdn_list = capability_component_manager.devices_fqdn
        devices_dict = {}
        for fqdn in fsp_fqdn_list:
            fsp_id = f"fsp-0{fsp_fqdn_list.index(fqdn)+1}"

            devices_dict[fsp_id] = fqdn
        TestMidFspCapabilityComponentManager.devices_list = devices_dict
        return capability_component_manager


class TestMidFspCapabilityComponentManager(TestMidFspCapability):
    def test_fsp_capability_cm_not_connected(
        self, capability_component_manager_disconnected
    ):
        probe_poller(
            capability_component_manager_disconnected,
            "is_communicating",
            False,
            time=3,
        )
        assert not capability_component_manager_disconnected.devices_state

    def test_fsp_capability_cm_connected(self, capability_component_manager):
        """
        Test the connection with the FSP devices and the initial values for the
        main attributes.
        """

        probe_poller(
            capability_component_manager,
            "devices_state",
            ["OFF", "OFF", "OFF", "OFF"],
            time=4,
        )
        probe_poller(
            capability_component_manager,
            "devices_function_mode",
            ["IDLE", "IDLE", "IDLE", "IDLE"],
            time=4,
        )
        probe_poller(
            capability_component_manager,
            "devices_admin_mode",
            [
                "ONLINE",
                "ONLINE",
                "ONLINE",
                "ONLINE",
            ],
            time=4,
        )
        probe_poller(
            capability_component_manager,
            "devices_health_state",
            [
                "UNKNOWN",
                "UNKNOWN",
                "UNKNOWN",
                "UNKNOWN",
            ],
            time=4,
        )
        assert capability_component_manager.devices_deployed == len(
            capability_component_manager.devices_fqdn
        )

    def test_fsp_capability_available(self, capability_component_manager):
        """
        Test the list of available and not available FSPs.

        An FSP is available if its state is in [ON, OFF] and if its
        function mode is IDLE.
        It is unvailable if its state is not in [ON, OFF].
        """
        probe_poller(
            capability_component_manager, "is_communicating", True, time=3
        )
        probe_poller(
            capability_component_manager,
            "devices_available",
            [1, 2, 3, 4],
            time=3,
        )
        probe_poller(
            capability_component_manager, "devices_unavailable", [], time=3
        )

    def test_fsp_capability_partially_available(
        self, capability_component_manager
    ):
        """
        Test the FSPs' availability.

        The test force the state of the FSPs 1 and 3 to UNKNOWN and FAULT.
        FPFs 2 and 4 are in OFF state.
        """
        TestMidFspCapabilityComponentManager.raise_event_on_fsp(
            1, "state", DevState.UNKNOWN
        )
        TestMidFspCapabilityComponentManager.raise_event_on_fsp(
            3, "state", DevState.FAULT
        )

        probe_poller(
            capability_component_manager, "devices_available", [2, 4], time=5
        )
        probe_poller(
            capability_component_manager, "devices_unavailable", [1, 3], time=3
        )

    def test_subaray_map_with_fsp_sub_membership_change(
        self, capability_component_manager
    ):
        """
        Test the fsp assigned to the subarray after an events that modify the
        FSP subarray membership
        default subarray_membership config:
            fsp_1: [1, 2]
            fsp_2: [1, 2]
            fsp_3: [1, 2]
            fsp_4: [1, 2]
        deafult config: "state": DevState.OFF,
                        "health_state": 'UNKNOWN',
                        "function_mode": 'IDLE'
        """
        subarray_map = {}
        subarray_map = capability_component_manager.subarray_fsp_map.copy()

        for sub in subarray_map.keys():
            assert capability_component_manager.subarray_fsp_map[sub] == (
                subarray_map[sub]
            )
        probe_poller(
            capability_component_manager,
            "devices_subarray_membership",
            ["1,2", "1,2", "1,2", "1,2"],
            time=5,
        )
        probe_poller(
            capability_component_manager,
            "devices_state",
            ["OFF", "OFF", "OFF", "OFF"],
            time=5,
        )
        probe_poller(
            capability_component_manager,
            "devices_health_state",
            ["UNKNOWN", "UNKNOWN", "UNKNOWN", "UNKNOWN"],
            time=5,
        )
        modified_device_num = 1
        # id = str(modified_device_num).zfill(2)
        # fsp_id = f"fsp_{id}"
        modified_subarray = [2, 3]
        probe_poller(
            capability_component_manager, "is_communicating", True, time=3
        )
        # time.sleep(2)
        # wait for the cahange propagation
        TestMidFspCapabilityComponentManager.raise_event_on_fsp(
            modified_device_num,
            "subarraymembership",
            numpy.array(modified_subarray, numpy.uint16),
        )
        time.sleep(0.5)
        # check the FSPs assigned to the subarray at after the
        # configuration event
        probe_poller(
            capability_component_manager,
            "devices_subarray_membership",
            ["2,3", "1,2", "1,2", "1,2"],
            time=5,
        )
        probe_poller(
            capability_component_manager,
            "devices_state",
            ["OFF", "OFF", "OFF", "OFF"],
            time=5,
        )
        probe_poller(
            capability_component_manager,
            "devices_health_state",
            ["UNKNOWN", "UNKNOWN", "UNKNOWN", "UNKNOWN"],
            time=5,
        )

        # sub_map = capability_component_manager.subarray_fsp_map.copy()
        # for sub_id, sub_fsp_dict in sub_map.items():
        #     if sub_id in modified_subarray:
        #         assert fsp_id in sub_fsp_dict.keys()
        #         # default values
        #         assert sub_fsp_dict[fsp_id]["state"] == DevState.OFF
        #         assert sub_fsp_dict[fsp_id]["health_state"] == "UNKNOWN"
        #         assert sub_fsp_dict[fsp_id]["function_mode"] == "IDLE"
        #     else:
        #         assert not (fsp_id in sub_fsp_dict.keys())

    def test_subaray_map_fsp_with_attributes_change(
        self, capability_component_manager
    ):
        """
        Test the fsp assigned to the subarray after some events that modify the
        one FSP attribute
        """
        probe_poller(
            capability_component_manager, "is_communicating", True, time=3
        )
        # time.sleep(2)
        subarray_map = {}
        subarray_map = capability_component_manager.subarray_fsp_map.copy()

        # check the FSPs assigned to the subarray at the initial configuration
        for sub in subarray_map.keys():
            assert capability_component_manager.subarray_fsp_map[sub] == (
                subarray_map[sub]
            )
        modified_attributes = ["state", "healthstate", "functionmode"]
        attr_name = ["state", "health_state", "function_mode"]
        input_value = [DevState.UNKNOWN, HealthState.UNKNOWN, FspModes.CORR]
        # return_value = [DevState.UNKNOWN, "UNKNOWN", "CORR"]
        return_value = ["UNKNOWN", "UNKNOWN", "CORR"]
        modified_device_num = 1
        id = str(modified_device_num).zfill(2)
        fsp_id = f"fsp_{id}"

        for pos, attr in enumerate(modified_attributes):
            value = input_value[pos]
            ret_val = return_value[pos]
            TestMidFspCapabilityComponentManager.raise_event_on_fsp(
                modified_device_num, attr, value
            )
            # wait for the propagation of the modification
            time.sleep(0.5)
            mod_subarray_map = (
                capability_component_manager.subarray_fsp_map.copy()
            )
            for sub_fsp_dict in mod_subarray_map.values():
                if fsp_id in sub_fsp_dict.keys():
                    assert ret_val == sub_fsp_dict[fsp_id][attr_name[pos]]
