import json

import mock
from mid_mocked_connector import CapabilityMockedConnector
from mid_test_classes import TestMidFspCapability
from ska_control_model import HealthState
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.utils.cspcommons import read_release_version
from tango import DevState

from ska_csp_lmc_mid import release

device_to_load = {
    "path": "charts/ska-csp-lmc-mid/data/midcspconfig.json",
    "package": "ska_csp_lmc_mid",
    "device": "capabilityfsp",
}


class TestMidFspCapabilityDevice(TestMidFspCapability):
    @mock.patch(
        "ska_csp_lmc_common.component.Connector", new=CapabilityMockedConnector
    )
    def test_fsp_device_initialization(self, device_under_test):
        probe_poller(device_under_test, "state", DevState.ON, time=10)

        assert device_under_test.healthstate == HealthState.OK
        assert device_under_test.fspsDeployed == 4
        assert device_under_test.fspFqdn == (
            "mid_csp_cbf/fsp/01",
            "mid_csp_cbf/fsp/02",
            "mid_csp_cbf/fsp/03",
            "mid_csp_cbf/fsp/04",
        )
        assert device_under_test.fspfunctionmode == (
            "IDLE",
            "IDLE",
            "IDLE",
            "IDLE",
        )
        assert list(device_under_test.fspState) == [
            "OFF",
            "OFF",
            "OFF",
            "OFF",
        ]
        assert device_under_test.fspHealthState == (
            "UNKNOWN",
            "UNKNOWN",
            "UNKNOWN",
            "UNKNOWN",
        )
        assert device_under_test.fspAdminMode == (
            "ONLINE",
            "ONLINE",
            "ONLINE",
            "ONLINE",
        )
        probe_poller(device_under_test, "isCommunicating", True, time=5)
        assert not device_under_test.fspUnavailable
        assert list(device_under_test.fspAvailable) == [1, 2, 3, 4]
        assert not device_under_test.fspPss
        assert not device_under_test.fspPst
        assert not device_under_test.fspCorrelation
        # assert device_under_test.isCommunicating
        probe_poller(device_under_test, "isCommunicating", True, time=3)
        probe_poller(
            device_under_test,
            "fspsSubarrayMembership",
            ("1,2", "1,2", "1,2", "1,2"),
            time=5,
        )
        json_dict = json.loads(device_under_test.fspJson)
        assert json_dict["fsps_deployed"] == device_under_test.fspsDeployed
        expected_dict = {
            "fsp_01": {
                "state": "OFF",
                "admin_mode": "ONLINE",
                "health_state": "UNKNOWN",
                "function_mode": "IDLE",
            },
            "fsp_02": {
                "state": "OFF",
                "admin_mode": "ONLINE",
                "health_state": "UNKNOWN",
                "function_mode": "IDLE",
            },
            "fsp_03": {
                "state": "OFF",
                "admin_mode": "ONLINE",
                "health_state": "UNKNOWN",
                "function_mode": "IDLE",
            },
            "fsp_04": {
                "state": "OFF",
                "admin_mode": "ONLINE",
                "health_state": "UNKNOWN",
                "function_mode": "IDLE",
            },
        }
        expected_string = json.dumps(expected_dict)
        assert device_under_test.fspsSubarray01 == expected_string
        assert device_under_test.fspsSubarray02 == expected_string
        assert device_under_test.fspsSubarray03 == "{}"
        assert device_under_test.fspsSubarray04 == "{}"
        for i in range(4):
            assert json_dict["fsp"][i]["fsp_id"] == i + 1
            assert json_dict["fsp"][i]["fqdn"] == f"mid_csp_cbf/fsp/0{i+1}"
            assert json_dict["fsp"][i]["function_mode"] == "IDLE"
            assert json_dict["fsp"][i]["health_state"] == "UNKNOWN"
            assert json_dict["fsp"][i]["admin_mode"] == "ONLINE"
            assert json_dict["fsp"][i]["subarray_membership"] == "1,2"

    @mock.patch(
        "ska_csp_lmc_common.component.Connector", new=CapabilityMockedConnector
    )
    def test_fsp_device_is_initialized_version(self, device_under_test):
        """Test the FSP version HW, SW, FW."""

        probe_poller(device_under_test, "state", DevState.ON, time=10)

        version_id, build_state = read_release_version(release)

        assert device_under_test.versionId == version_id
        assert device_under_test.buildState == build_state
        assert device_under_test.fspsSwVersion == (
            "0.11.6",
            "0.11.6",
            "0.11.6",
            "0.11.6",
        )
        assert device_under_test.fspsHwVersion == ("NA", "NA", "NA", "NA")
        assert device_under_test.fspsFwVersion == ("NA", "NA", "NA", "NA")
