import json

import pytest
from ska_csp_lmc_common.testing.test_classes import load_json_file

from ska_csp_lmc_mid.subarray.mid_json_schema_validator import (
    CspMidJsonValidator,
)


# pylint: disable-next=missing-class-docstring
class TestMidJsonSchemaValidator:
    def test_validate_assignresources_config_json_no_dish(self):
        """Test the config json script is not loaded if the
        requested section ('dish') is missing inside the json script."""
        assign_input = load_json_file(
            filename="AssignResources_CBF_PSS_PST.json"
        )
        assign_input.pop("dish")
        json_input = json.dumps(assign_input)
        with pytest.raises(ValueError):
            CspMidJsonValidator("Assignresources").validate(json_input)

    def test_validate_configure_config_json_no_cbf(self):
        """Test the config json script is not loaded if the
        requested section ('cbf') is missing inside the json script."""
        assign_input = load_json_file(filename="Configure_CBF.json")
        assign_input.pop("cbf")
        json_input = json.dumps(assign_input)
        with pytest.raises(ValueError):
            CspMidJsonValidator("Configure").validate(json_input)
