import logging

import mock
import pytest
from mid_mocked_connector import MidMockedConnector
from mid_properties import test_properties_mid_subarray
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.testing import mock_config

from ska_csp_lmc_mid.manager.mid_capability_connection import (
    MidCapabilityConnectionManager,
)

module_logger = logging.getLogger(__name__)


@pytest.fixture(scope="function")
def capability_manager():
    with mock.patch(
        "ska_csp_lmc_common.manager.manager_configuration."
        "ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = test_properties_mid_subarray
        cm_conf = ComponentManagerConfiguration("", module_logger)
        cm_conf.add_attributes()
        return MidCapabilityConnectionManager(
            cm_conf, mock.Mock(), module_logger
        )


class TestMiCapabilityConnectionManager:
    @mock.patch(
        "ska_csp_lmc_common.component.Connector", new=MidMockedConnector
    )
    @mock.patch(
        "ska_csp_lmc_mid.manager.mid_capability_connection.Connector",
        new=MidMockedConnector,
    )
    def test_capability_init(self, capability_manager):
        """
        Test that the task to establish connection with capability
        devices has two subtasks.
        """
        task = capability_manager.create_capability_task(mock.MagicMock)
        assert len(task.subtasks) == 2

    @mock.patch(
        "ska_csp_lmc_common.component.Connector", new=MidMockedConnector
    )
    @mock.patch(
        "ska_csp_lmc_mid.manager.mid_capability_connection.Connector",
        new=MidMockedConnector,
    )
    def test_capability_read_ctrl_error(
        self, capability_manager, mock_config_init
    ):
        """
        Test the connection to the Mid CSP Capability devices when
        the reading of Mid CSP Controller attributes fail.
        """
        # simulate a failure in reading on Mid CSP.LMC  Controller
        mock_config.mock_exception = True
        mock_config.mock_fail_device = "mid-csp/control/0"

        with pytest.raises(ValueError):
            capability_manager.create_capability_task(mock.MagicMock)
        assert capability_manager.fsp_capability_fqdn == ""
        assert capability_manager.vcc_capability_fqdn == ""
        assert not capability_manager.is_communicating

    def test_capability_with_failure_in_retrieve_fqdn(
        self, capability_manager
    ):
        with mock.patch.object(
            MidCapabilityConnectionManager,
            "_retrieve_capability_fqdn",
        ) as mocked_retrieve:
            mocked_retrieve.side_effect = ValueError(
                "Error in retrieving VCC/FSP capability fqdn"
            )
            assert capability_manager.vcc_capability_fqdn == ""
            assert capability_manager.fsp_capability_fqdn == ""
