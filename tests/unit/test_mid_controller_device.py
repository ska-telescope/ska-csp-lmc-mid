from __future__ import annotations

import mock
import pytest
from mid_mocked_connector import MidMockedConnector
from mid_properties import test_properties_mid_ctrl
from ska_control_model import AdminMode, HealthState
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.testing.test_classes import TestBaseController
from ska_csp_lmc_common.utils.cspcommons import read_release_version
from tango import DevState

from ska_csp_lmc_mid import release

device_to_load = {
    "path": "tests/unit/csplmc_dsconfig.json",
    "package": "ska_csp_lmc_mid",
    "device": "controller",
}


class TestMidCspController(TestBaseController):
    devices_list = {
        "cbf-ctrl": test_properties_mid_ctrl["CspCbf"],
        "pss-ctrl": test_properties_mid_ctrl["CspPss"],
        "subarray_01": test_properties_mid_ctrl["CspSubarrays"][0],
        "subarray_02": test_properties_mid_ctrl["CspSubarrays"][1],
        "subarray_03": test_properties_mid_ctrl["CspSubarrays"][2],
    }

    @pytest.fixture
    @mock.patch(
        "ska_csp_lmc_common.component.Connector", new=MidMockedConnector
    )
    def init_controller(self, device_under_test):
        self.init_process(device_under_test)

    def init_process(self, device_under_test):
        """Method called at the start of each test. It initialize the
        CspController.

        NOTE: this test is a copy of a method present in
        test_controller_device.py present in common repository. Consider to
        move to the class TestBaseController
        """

        probe_poller(device_under_test, "state", DevState.DISABLE, time=1)
        assert device_under_test.adminmode == AdminMode.OFFLINE
        assert device_under_test.healthstate == HealthState.UNKNOWN
        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(device_under_test, "isCommunicating", True, time=5)
        probe_poller(device_under_test, "state", DevState.STANDBY, time=5)
        probe_poller(
            device_under_test, "cspCbfAdminMode", AdminMode.ONLINE, time=5
        )
        probe_poller(
            device_under_test, "cspPssAdminMode", AdminMode.ONLINE, time=5
        )

        assert device_under_test.healthstate == HealthState.OK

    def set_initial_state(self, device_under_test, state: DevState):
        """Set the initial state to the value specified in input."""
        TestMidCspController.create_events("state", state)
        TestMidCspController.raise_event("state")
        probe_poller(device_under_test, "state", state, time=10)

    def test_csp_vcc_capability_address(self, device_under_test):
        assert (
            device_under_test.cspVccCapabilityAddress
            == "mid-csp/capability-vcc/0"
        )

    def test_csp_fsp_capability_address(self, device_under_test):
        assert (
            device_under_test.cspFspCapabilityAddress
            == "mid-csp/capability-fsp/0"
        )

    def test_receptors_list(self, init_controller, device_under_test):
        """
        The test relies on the mocked value of the attribute
        *receptorsList* defined in the module
        *mocked_attr_factory*.
        """
        assert device_under_test.receptorsList == (
            "SKA001",
            "SKA022",
            "SKA103",
            "SKA104",
        )

    def test_available_capabilities(self, init_controller, device_under_test):
        """
        Check the value of the availableCapabilities attribute.

        The test relies on the mocked value of the attribute
        *reportVccSubarrayMemebership* defined in the module
        *mocked_attr_factory*.
        """
        assert device_under_test.availableCapabilities == (
            "SearchBeams:1500",
            "TimingBeams:16",
            "VCC:2",
            "VlbiBeams:20",
        )

    def test_read_receptor_membership(
        self, init_controller, device_under_test
    ):
        """
        Check the Controller receptorMembership attribute value.

        The test relies on the mocked values of the attributes
        *reportVccSubarrayMemebership*, *receptorsList and *vccToReceptor*
        defined in the module *mocked_attr_factory*.
        vccToReceptor = ["1:86", "2:168", "3:65", "4:167"]
        The code evaluates the correspondence between the VCCs and the
        receptor id (ADR-32 format):

        vcc_to_receptor_map: {'1': 'SKA022', '2': 'SKA104',
                              '3': 'SKA001', '4': 'SKA103'}

        reportVccSubarrayMembership = [1,2,0,0]
        receptorsList = ["SKA001", "SKA022", "SKA103", "SKA104"]
        it follows that:
        receptorMembership is [0,1,0,2]
        """
        assert device_under_test.receptorMembership[0] == 0
        assert device_under_test.receptorMembership[1] == 1
        assert device_under_test.receptorMembership[2] == 0
        assert device_under_test.receptorMembership[3] == 2

    def test_read_receptor_membership_no_cbf(self, device_under_test):
        assert not device_under_test.receptorMembership

    def test_read_unassigned_receptor_ids(
        self, init_controller, device_under_test
    ):
        assert device_under_test.unassignedReceptorIDs == ("SKA001",)

    def test_read_unassigned_receptor_ids_no_cbf(self, device_under_test):
        assert not device_under_test.unassignedReceptorIDs

    def test_version_id(self, device_under_test):

        version_id, build_state = read_release_version(release)
        assert device_under_test.versionId == version_id
        assert device_under_test.buildState == build_state

    def test_loaddishcfg_on_device(self, init_controller, device_under_test):
        """
        Test the value of dihVccConfig and sourceDishVccConfig
        """

        self.set_initial_state(device_under_test, DevState.OFF)
        dish_cfg = """{
            "interface": "https://schema.skao.int/"
                        "ska-mid-cbf-initsysparam/1.0",
            "dish_parameters": {
                "SKA001": {"vcc": 1, "k": 11},
                "SKA036": {"vcc": 2, "k": 101},
                "SKA063": {"vcc": 3, "k": 1127},
                "SKA100": {"vcc": 4, "k": 620},
            },
        }"""

        def dish_vcc_config(param):
            if param == "sysParam":
                return dish_cfg
            else:
                return ""

        with mock.patch(
            "ska_csp_lmc_mid.controller.mid_ctrl_component."
            "MidCbfControllerComponent.read_attr"
        ) as mock_read_attr:
            mock_read_attr.side_effect = dish_vcc_config
            [[result], [command_id]] = device_under_test.LoadDishCfg(dish_cfg)
            probe_poller(
                device_under_test,
                "longRunningCommandStatus",
                "COMPLETED",
                command_id=command_id,
                time=5,
            )
            assert device_under_test.dishVccConfig == dish_cfg
            assert device_under_test.sourcedishVccConfig == ""
