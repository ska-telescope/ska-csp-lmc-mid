import functools
import logging
import os
from unittest.mock import ANY

import pytest
from mid_properties import test_properties_mid_subarray
from mock import MagicMock, patch
from ska_control_model import (
    HealthState,
    ObsMode,
    ObsState,
    ResultCode,
    TaskStatus,
)
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.testing.poller import probe_poller
from ska_csp_lmc_common.testing.test_classes import TestBaseSubarray
from ska_tango_base.utils import generate_command_id
from ska_tango_testing.mock import MockCallableGroup
from tango import DevState

from ska_csp_lmc_mid.manager import MidCspSubarrayComponentManager

file_path = os.getcwd()
logger = logging.getLogger(__name__)


@pytest.fixture()
def callbacks() -> MockCallableGroup:
    """
    Return a dictionary of callbacks with asynchrony support.

    :return: a collections.defaultdict that returns callbacks by name.
    """
    return MockCallableGroup(
        "configure_task",
        "gotoidle_task",
        "obsreset_task",
        "reset_task",
        timeout=5.0,
    )


def test_subarray_cm_when_wrong_sub_systems_fqdn_specified():
    """Test Subarray subarray_component_manager initialization when CSP
    Subarray device properties are wrong.

    Expected result: CSP Subarray State FAULT CSP Subarray HealthState FAILED
    """
    wrong_properties = {
        "ConnectionTimeout": "1",
        "PingConnectionTime": "1",
        "DefaultCommandTimeout": "5",
        "SubID": "1",
        "CspController": "mid-csp/control/0",
        "CbfSubarray": "mid-cbx/subarray/01",
        "PssSubarray": "mid-pss/subarray/01",
        "PstBeams": ["mid-pst/beam/01", "mid-pst/beam/02"],
    }
    with patch(
        "ska_csp_lmc_common.manager"
        ".manager_configuration"
        ".ComponentManagerConfiguration.get_device_properties"
    ) as mock_prop:
        mock_prop.return_value = wrong_properties
        wrong_subarray_component_manager_conf = ComponentManagerConfiguration(
            "", logger
        )
        wrong_subarray_component_manager_conf.add_attributes()
        subarray_component_manager = MidCspSubarrayComponentManager(
            MagicMock(),
            MagicMock(),
            MagicMock(),
            wrong_subarray_component_manager_conf,
            logger=logger,
        )
        subarray_component_manager.init()
        subarray_component_manager.op_state_model.op_state == DevState.FAULT
        subarray_component_manager.health_model.health_state == HealthState.FAILED  # noqa:E501


def test_subarray_cm_connection_failed(subarray_component_manager):
    """Test the communicating flag after communication failure."""
    import time

    subarray_component_manager.start_communicating()
    time.sleep(2)
    # wait to establish communication
    assert not subarray_component_manager.is_communicating
    assert not len(subarray_component_manager.online_components)


def print_value(value):
    print(f"New values is: {value}")


class TestMidSubarrayManager(TestBaseSubarray):
    devices_list = {
        "cbf-subarray": test_properties_mid_subarray["CbfSubarray"],
        "pss-subarray": test_properties_mid_subarray["PssSubarray"],
        "pst-beam-01": test_properties_mid_subarray["PstBeams"][0],
        "pst-beam-02": test_properties_mid_subarray["PstBeams"][1],
    }

    def test_subarray_cm_components_created(self, subarray_component_manager):
        assert subarray_component_manager.sub_id == 1
        assert len(subarray_component_manager.components) == 4
        assert (
            subarray_component_manager.components[
                test_properties_mid_subarray["CbfSubarray"]
            ].name
            == "cbf-subarray-01"
        )
        assert (
            subarray_component_manager.components[
                test_properties_mid_subarray["PssSubarray"]
            ].name
            == "pss-subarray-01"
        )
        assert (
            subarray_component_manager.components[
                test_properties_mid_subarray["PstBeams"][0]
            ].name
            == "pst-beam-01"
        )
        assert (
            subarray_component_manager.components[
                test_properties_mid_subarray["PstBeams"][1]
            ].name
            == "pst-beam-02"
        )

    def test_subarray_cm_online_components(
        self, subarray_component_manager, subarray_cm_connected
    ):
        """Verify that callbacks are created in initilization's succeed
        method."""
        assert len(subarray_component_manager.online_components) == 2

    def test_subarray_cm_stop_communicating(
        self, subarray_component_manager, subarray_cm_connected
    ):
        assert subarray_component_manager.is_communicating
        subarray_component_manager.stop_communicating()
        probe_poller(
            subarray_component_manager, "is_communicating", False, time=4
        )

    def test_subarray_cm_gotoidle_from_ready(
        self, subarray_component_manager, subarray_cm_connected, callbacks
    ):
        """
        Test that observation state transitions to IDLE when in READY
        when commanded by GoToIdle
        """
        # create op_state events
        TestMidSubarrayManager.raise_event_on_pss_subarray(
            "state", DevState.ON
        )
        TestMidSubarrayManager.raise_event_on_cbf_subarray(
            "state", DevState.ON
        )
        # create CBF events
        TestMidSubarrayManager.raise_event_on_pss_subarray(
            "obsstate", ObsState.READY
        )
        TestMidSubarrayManager.raise_event_on_cbf_subarray(
            "obsstate", ObsState.READY
        )

        probe_poller(
            subarray_component_manager.obs_state_model,
            "obs_state",
            ObsState.READY,
            time=1,
        )
        probe_poller(
            subarray_component_manager.op_state_model,
            "op_state",
            DevState.ON,
            time=1,
        )

        subarray_component_manager.deconfigure(
            functools.partial(
                callbacks["gotoidle_task"], generate_command_id("GoToIdle")
            ),
        )
        callbacks["gotoidle_task"].assert_against_call(
            status=TaskStatus.QUEUED,
        )
        callbacks["gotoidle_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            lookahead=5,
        )
        probe_poller(
            subarray_component_manager.obs_state_model,
            "obs_state",
            ObsState.IDLE,
            time=1,
        )

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_subarray_cm_configure_failed(
        self,
        initial_obsstate,
        subarray_component_manager,
        subarray_cm_connected,
        callbacks,
    ):
        """Test a failure in the ConfigureCommand, not mocking the event for
        subsystems at command run.

        The ObsState of CSP stays in inital_obsstate, since the configuration
        is failing The command resultCode is FAILED
        """
        assert subarray_component_manager.is_communicating
        TestMidSubarrayManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestMidSubarrayManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, initial_obsstate
        )
        argin = {"test": "dummy"}
        subarray_component_manager.configure(
            functools.partial(
                callbacks["configure_task"], generate_command_id("Configure")
            ),
            **argin,
        )
        probe_poller(
            subarray_component_manager.obs_state_model,
            "obs_state",
            initial_obsstate,
            time=1,
        )

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_subarray_cm_configure_cbf_and_pss(
        self,
        initial_obsstate,
        subarray_component_manager,
        subarray_cm_connected,
        callbacks,
    ):
        """
        Test CSP Subarray Configure command when the script specifies
        the configuration both CSP and CBF.

        Expected behavior:
        - the CSP Subarray observing state moves to READY
        - The task status is COMPLETED
        """
        assert subarray_component_manager.is_communicating
        TestMidSubarrayManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestMidSubarrayManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, initial_obsstate
        )

        config_input = TestMidSubarrayManager.configuration_input(
            filename="Configure_CBF.json"
        )
        subarray_component_manager.configure(
            functools.partial(
                callbacks["configure_task"], generate_command_id("Configure")
            ),
            **config_input,
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.QUEUED
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.COMPLETED, lookahead=2
        )
        probe_poller(
            subarray_component_manager.obs_state_model,
            "obs_state",
            ObsState.READY,
            time=3,
        )

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_subarray_cm_configure_only_cbf(
        self,
        initial_obsstate,
        subarray_component_manager,
        subarray_cm_connected,
        callbacks,
    ):
        """
        Test CSP Subarray Configure when the configuration script
        specifies only the CBF configuration.

        Expected beahvior:
        - the CSP subarray observing state moves to READY
        - the PSS Subarray observing state maintains its value
        - the command task status is COMPLETED
        """
        assert subarray_component_manager.is_communicating
        TestMidSubarrayManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestMidSubarrayManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, initial_obsstate
        )
        config_input = TestMidSubarrayManager.configuration_input(
            filename="Configure_CBF.json"
        )
        config_input.pop("pss")

        subarray_component_manager.configure(
            functools.partial(
                callbacks["configure_task"], generate_command_id("Configure")
            ),
            **config_input,
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.QUEUED
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.COMPLETED, lookahead=2
        )
        probe_poller(
            subarray_component_manager.obs_state_model,
            "obs_state",
            ObsState.READY,
            time=2,
        )
        pss_sub1 = subarray_component_manager.online_components[
            "mid-pss/subarray/01"
        ]

        probe_poller(
            pss_sub1,
            "obs_state",
            initial_obsstate,
            time=2,
        )

    @pytest.mark.parametrize(
        "initial_obsstate", [ObsState.IDLE, ObsState.READY]
    )
    def test_subarray_cm_configure_only_pss(
        self,
        initial_obsstate,
        subarray_component_manager,
        subarray_cm_connected,
        callbacks,
    ):
        """
        Test configuration when the script contains only PSS subarray
        configuration.

        Expected behavior:
        - the CSP Subarray ovserving state won't be updated.
        - the PSS Subarray observing state is READY
        """
        assert subarray_component_manager.is_communicating
        TestMidSubarrayManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestMidSubarrayManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, initial_obsstate
        )
        config_input = TestMidSubarrayManager.configuration_input(
            filename="Configure_CBF_PSS.json"
        )
        config_input.pop("cbf")

        subarray_component_manager.configure(
            functools.partial(
                callbacks["configure_task"], generate_command_id("Configure")
            ),
            **config_input,
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.QUEUED
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.COMPLETED, lookahead=2
        )
        probe_poller(
            subarray_component_manager.obs_state_model,
            "obs_state",
            initial_obsstate,
            time=1,
        )

        pss_sub1 = subarray_component_manager.online_components[
            "mid-pss/subarray/01"
        ]
        probe_poller(pss_sub1, "obs_state", ObsState.READY, time=2)
        assert (
            subarray_component_manager.obs_state_model.obs_state
            == initial_obsstate
        )

    def test_subarray_cm_invoke_reset_with_cbf_in_fault(
        self, subarray_component_manager, subarray_cm_connected, callbacks
    ):
        """
        Test Subarray Reset command when only CBF sub-system is in FAULT
        state.

        Note: the mocked sub-system device moves to state OFF when Reset
              is invoked. The real  behavior is to re-establish the original
              value for the State.

        Expected behavior:
        - the State of the CSP subarray moves from FAULT to OFF
        - the observing state maintains its value (IDLE)
        """
        TestMidSubarrayManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestMidSubarrayManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, ObsState.IDLE
        )
        TestMidSubarrayManager.raise_event_on_cbf_subarray(
            "state", DevState.FAULT
        )

        subarray_component_manager.reset(
            functools.partial(
                callbacks["reset_task"], generate_command_id("Reset")
            )
        )
        callbacks["reset_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["reset_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            lookahead=5,
        )

        probe_poller(
            subarray_component_manager.op_state_model,
            "op_state",
            DevState.OFF,
            time=1,
        )

        probe_poller(
            subarray_component_manager.obs_state_model,
            "obs_state",
            ObsState.IDLE,
            time=1,
        )

    def test_subarray_cm_invoke_reset_with_no_subsystem_in_fault(
        self, subarray_component_manager, subarray_cm_connected, callbacks
    ):
        """
        Test CSP Subarray Reset command when no sub-system is in
        FAULT.

        The Reset command is accepted in any state.

        Expected behavior:
        - the State of the CSP subarray maintains its value.
        - the observing state maintains its value (IDLE)
        - the task status is COMPLETED
        """
        TestMidSubarrayManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestMidSubarrayManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, ObsState.IDLE
        )

        subarray_component_manager.reset(
            functools.partial(
                callbacks["reset_task"], generate_command_id("Reset")
            )
        )
        callbacks["reset_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["reset_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            result=[ResultCode.OK, ANY],
            lookahead=5,
        )

        probe_poller(
            subarray_component_manager.op_state_model,
            "op_state",
            DevState.ON,
            time=1,
        )

    def test_subarray_cm_invoke_reset_with_only_subarray_in_fault(
        self, subarray_component_manager, subarray_cm_connected, callbacks
    ):
        """
        Test Controller Reset command when only the CSP Subarray is in FAULT.

        Expected behavior:
        - the State of the CSP subarray maintains its value.
        - the observing state maintains its value (IDLE)
        - the task status is COMPLETED
        """
        TestMidSubarrayManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestMidSubarrayManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, ObsState.IDLE
        )
        subarray_component_manager.op_state_model.component_fault(True)
        assert subarray_component_manager.op_state_model.faulty

        subarray_component_manager.reset(
            functools.partial(
                callbacks["reset_task"], generate_command_id("Reset")
            )
        )
        callbacks["reset_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["reset_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            result=[ResultCode.OK, ANY],
            lookahead=5,
        )
        probe_poller(
            subarray_component_manager.op_state_model,
            "op_state",
            DevState.ON,
            time=1,
        )

    def test_subarray_cm_update_assigned_resources_status(
        self, subarray_component_manager
    ):
        """
        Test the method called to update the status of the resources
        assigned to the subarray.
        """

        resource_value = """
        {
            "fsp_01":{
                "state": "ON",
                "health_state": "OK",
                "function_mode": "CORR"
                },
            "fsp_02":{
                "state": "ON",
                "health_state": "DEGRADED",
                "function_mode": "CORR"
                },
            "fsp_03":{
                "state": "ON",
                "health_state": "OK",
                "function_mode": "CORR"
                }
            }
            """

        dict_to_update = (
            subarray_component_manager.update_assigned_resources_status(
                "fsp", resource_value
            )
        )
        assert dict_to_update["assignedFspState"] == ["ON", "ON", "ON"]
        assert dict_to_update["assignedFspHealthState"] == [
            "OK",
            "DEGRADED",
            "OK",
        ]
        assert dict_to_update["assignedFspFunctionMode"] == [
            "CORR",
            "CORR",
            "CORR",
        ]
        assert (
            subarray_component_manager.assigned_fsp_health_state
            == dict_to_update["assignedFspHealthState"]
        )
        assert (
            subarray_component_manager.assigned_fsp_state
            == dict_to_update["assignedFspState"]
        )

        resource_value = """
            {
            "vcc_001": {"state": "ON", "health_state": "OK"},
            "vcc_002": {"state": "ON", "health_state": "DEGRADED"},
            "vcc_011": {"state": "ON", "health_state": "OK"}

        }"""
        dict_to_update = (
            subarray_component_manager.update_assigned_resources_status(
                "vcc", resource_value
            )
        )
        assert dict_to_update["assignedVccState"] == ["ON", "ON", "ON"]
        assert dict_to_update["assignedVccHealthState"] == [
            "OK",
            "DEGRADED",
            "OK",
        ]

    def test_subarray_cm_update_assigned_resources_list(
        self, subarray_component_manager
    ):
        """
        Test the method called to update the list of resources' FQDN assigned
        to the subarray.
        """

        vcc_json = """{
                "1": ["mid-cbf/vcc/001","mid-cbf/vcc/003"],
                "2": ["mid-cbf/vcc/004"],
                "3": ["mid-cbf/vcc/002"]
                }
                """
        assigned_vcc = subarray_component_manager.update_assigned_resources(
            "vcc", vcc_json
        )
        assert assigned_vcc["assignedResources"] == [
            "mid-cbf/vcc/001",
            "mid-cbf/vcc/003",
        ]
        fsp_list = [["mid-cbf/fsp/01", "mid-cbf/fsp/02"], ["", ""]]
        assigned_fsp = subarray_component_manager.update_assigned_resources(
            "fsp", fsp_list
        )
        assert assigned_fsp["assignedResources"] == [
            "mid-cbf/fsp/01",
            "mid-cbf/fsp/02",
            "mid-cbf/vcc/001",
            "mid-cbf/vcc/003",
        ]

    def test_subarray_cm_update_assigned_resources_failure(
        self, subarray_component_manager
    ):
        """
        Test the method called to update the list of resources' FQDN assigned
        to the subarray.
        """

        vcc_json = """{}"""
        assigned_vcc = subarray_component_manager.update_assigned_resources(
            "vcc", vcc_json
        )
        assert not assigned_vcc["assignedResources"]
        fsp_list = [["", ""], ["mid-cbf/fsp/01", "mid-cbf/fsp/02"]]
        assigned_fsp = subarray_component_manager.update_assigned_resources(
            "fsp", fsp_list
        )
        assert not assigned_fsp["assignedResources"]

    def test_subarray_configure_v4_0(
        self,
        subarray_component_manager,
        subarray_cm_connected,
        callbacks,
    ):
        """
        Test CSP Subarray Configure command when the script specifies
        the configuration version 4.0.

        Expected behavior:
        - the CSP Subarray observing state moves to READY
        - The task status is COMPLETED
        """
        assert subarray_component_manager.is_communicating
        TestMidSubarrayManager.set_subsystems_and_go_to_state(
            subarray_component_manager, DevState.ON
        )
        TestMidSubarrayManager.set_subsystems_and_go_to_obsstate(
            subarray_component_manager, ObsState.IDLE
        )

        config_input = TestMidSubarrayManager.configuration_input(
            filename="Configure_CBF_ver4.json"
        )
        subarray_component_manager.configure(
            functools.partial(
                callbacks["configure_task"], generate_command_id("Configure")
            ),
            **config_input,
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.QUEUED
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.COMPLETED, lookahead=2
        )
        probe_poller(
            subarray_component_manager.obs_state_model,
            "obs_state",
            ObsState.READY,
            time=1,
        )

        probe_poller(
            subarray_component_manager,
            "obs_modes",
            [ObsMode.IMAGING],
            time=1,
        )
