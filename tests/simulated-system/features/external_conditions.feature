Feature: All external conditions of CSP


Scenario: CspController is initialized while CbfController is missing in tangodb
    Given All subsystems are fresh initialized
    And CspController isCommunicating is True
    And CbfController is removed from tangodb
    And CspController is initialized

    Then CspController cspCbfState is DISABLE

    And CspController state is FAULT

    And CspController cspCbfHealthState is UNKNOWN

    And CspController HealthState is FAILED

    And CspController cspCbfAdminMode is OFFLINE

    And CspController AdminMode is ENGINEERING


Scenario: CspController is initialized while PssController is missing in tangodb
    Given All subsystems are fresh initialized
    And CspController isCommunicating is True
    And PssController is removed from tangodb
    And CspController is initialized

    Then CspController cspPssState is DISABLE

    # Should be same as Cbf State, which should be in STANDBY.
    And CspController state is STANDBY

    And CspController cspPssHealthState is UNKNOWN

    # Should be same as Cbf HealthState, which should be OK
    And CspController HealthState is OK

    And CspController cspPssAdminMode is OFFLINE

    And CspController AdminMode is ENGINEERING


Scenario: CspController is initialized while PstController is missing in tangodb
    Given All subsystems are fresh initialized
    And CspController isCommunicating is True
    And PstController is removed from tangodb
    And CspController is initialized

    Then CspController cspPstState is DISABLE

    # Should be same as Cbf State, which should be in STANDBY.
    And CspController state is STANDBY

    And CspController cspPstHealthState is UNKNOWN

    # Should be same as Cbf HealthState, which should be OK
    And CspController HealthState is OK

    And CspController cspPstAdminMode is OFFLINE

    And CspController AdminMode is ENGINEERING


Scenario: CspSubarray01 is initialized while CbfSubarray01 is missing in tangodb
    Given All subsystems are fresh initialized
    And CspController isCommunicating is True
    And CbfSubarray01 is removed from tangodb
    And CspSubarray01 is initialized

    Then CspSubarray01 cbfSubarrayState is DISABLE

    And CspSubarray01 state is FAULT

    And CspSubarray01 cbfSubarrayHealthState is UNKNOWN

    And CspSubarray01 HealthState is FAILED

    And CspSubarray01 cbfSubarrayAdminMode is OFFLINE

    And CspSubarray01 AdminMode is ENGINEERING

    And CspSubarray01 cbfSubarrayObsState is EMPTY

    And CspSubarray01 obsState is EMPTY


Scenario: CspSubarray01 is initialized while PssSubarray01 is missing in tangodb
    Given All subsystems are fresh initialized
    And CspController isCommunicating is True
    And PssSubarray01 is removed from tangodb
    And CspSubarray01 is initialized

    Then CspSubarray01 pssSubarrayState is DISABLE

    # Should be same as Cbf subarray State, which should be in OFF.
    And CspSubarray01 state is OFF

    And CspSubarray01 HealthState is OK

    And CspSubarray01 pssSubarrayHealthState is UNKNOWN

    And CspSubarray01 pssSubarrayAdminMode is OFFLINE

    And CspSubarray01 AdminMode is ENGINEERING

    And CspSubarray01 pssSubarrayObsState is EMPTY

    # Should be same as Cbf subarray obsState, which should be in EMPTY
    And CspSubarray01 obsState is EMPTY


Scenario: CspController is restarted when the system is in ON
    Given All subsystems are fresh initialized
    And CspController isCommunicating is True
    When On Command is issued on CspController with argument []
    And CspController commandResult is ('on', '0')
	And CspController longRunningCommandAttributes is (0, 'COMPLETED')
    Then CbfController state is ON
    And CbfSubarray01 state is ON
    And CbfSubarray02 state is ON
    And CbfSubarray03 state is ON

    And PssController state is ON
    And PssSubarray01 state is ON
    And PssSubarray02 state is ON
    And PssSubarray03 state is ON

    # And PstController state is ON
    # implementation is not able to turn on PstBeam
    # And PstBeam01 state is ON
    # And PstBeam02 state is ON

    And CspController state is ON
    And CspSubarray01 state is ON
    And CspSubarray02 state is ON
    And CspSubarray03 state is ON

    When CspController is restarted
    Then CspController state is ON
