Feature: Control that all the attribute of fake subsystems are in the proper state at initialization

  @pipeline
  Scenario: Fake Subsystems' controllers are properly initialized
    Given All subsystems are fresh initialized
    And CspController isCommunicating is True
    Then CbfController state is OFF
    And CbfController HealthState is OK
    And CbfController AdminMode is ONLINE
    And CbfController SimulationMode is TRUE

    And CbfSubarray01 state is OFF
    And CbfSubarray01 HealthState is OK
    And CbfSubarray01 AdminMode is ONLINE
    And CbfSubarray01 ObsState is EMPTY
    And CbfSubarray01 SimulationMode is TRUE


    And CbfSubarray02 state is OFF
    And CbfSubarray02 HealthState is OK
    And CbfSubarray02 AdminMode is ONLINE
    And CbfSubarray02 ObsState is EMPTY
    And CbfSubarray02 SimulationMode is TRUE


    And CbfSubarray03 state is OFF
    And CbfSubarray03 HealthState is OK
    And CbfSubarray03 AdminMode is ONLINE
    And CbfSubarray03 ObsState is EMPTY
    And CbfSubarray03 SimulationMode is TRUE


    # And PssController state is STANDBY
    And PssController state is OFF
    And PssController HealthState is OK
    And PssController AdminMode is ONLINE
    And PssController SimulationMode is TRUE


    And PssSubarray01 state is OFF
    And PssSubarray01 HealthState is OK
    And PssSubarray01 AdminMode is ONLINE
    And PssSubarray01 ObsState is EMPTY
    And PssSubarray01 SimulationMode is TRUE

    And PssSubarray02 state is OFF
    And PssSubarray02 HealthState is OK
    And PssSubarray02 AdminMode is ONLINE
    And PssSubarray03 ObsState is EMPTY
    And PssSubarray03 SimulationMode is TRUE

    And PssSubarray03 state is OFF
    And PssSubarray03 HealthState is OK
    And PssSubarray03 AdminMode is ONLINE
    And PssSubarray03 ObsState is EMPTY
    And PssSubarray03 SimulationMode is TRUE

    # And PstBeam01 state is OFF
    # And PstBeam01 HealthState is OK
    # And PstBeam01 AdminMode is ONLINE
    # And PstBeam01 ObsState is IDLE
    # And PstBeam01 SimulationMode is TRUE

    # And PstBeam02 state is OFF
    # And PstBeam02 HealthState is OK
    # And PstBeam02 AdminMode is ONLINE
    # And PstBeam02 ObsState is IDLE
    # And PstBeam02 SimulationMode is TRUE
