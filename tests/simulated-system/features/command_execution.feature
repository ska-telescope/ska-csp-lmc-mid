Feature: Tests for anomalous conditions in command execution

  @pipeline
  Scenario: Off Command is allowed when CspController is ON

    Given All subsystems are fresh initialized
    And CspController isCommunicating is True
    And On Command is issued on CspController with argument []
    And CspController commandResult is ('on', '0')
    And CspController state is ON
    And PssController state is ON
    And CspSubarray01 state is ON
    And CspSubarray02 state is ON
    And CspSubarray03 state is ON

    When Off Command is issued on CspController with argument []

    Then CspController state is OFF
    And PssController state is OFF
    #The Off command is not propagated to the subarrays
    And CspSubarray01 state is ON
    And CspSubarray02 state is ON
    And CspSubarray03 state is ON
    
  
  Scenario: ConfigureScan is not allowed when CspSubarray is ON/EMPTY

    Given All subsystems are fresh initialized
    And CspController isCommunicating is True
    And On Command is issued on CspSubarray01 (no argument)
    And CspSubarray01 commandResult is ('on', '0')
    And CspSubarray01 state is ON
    And CbfSubarray01 state is ON
    And PssSubarray01 state is ON
    And CspSubarray01 ObsState is EMPTY
    And CbfSubarray01 ObsState is EMPTY
    And PssSubarray01 ObsState is EMPTY

    When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF.json)

    Then CspSubarray01 state is ON
    And CbfSubarray01 state is ON
    And PssSubarray01 state is ON
    And CspSubarray01 ObsState is EMPTY
    And CbfSubarray01 ObsState is EMPTY
    And PssSubarray01 ObsState is EMPTY

  
  Scenario: AssignResources is not accepted on PssSubarray that is OFF

    Given All subsystems are fresh initialized
    And CspController isCommunicating is True
    And On Command is issued on CspSubarray01 (no argument)
    And CspSubarray01 commandResult is ('on', '0')
    And Off Command is issued on PssSubarray01 (no argument)
    And CbfSubarray01 state is ON
    And PssSubarray01 state is OFF
    And CspSubarray01 state is ON
    And CbfSubarray01 ObsState is EMPTY
    And PssSubarray01 ObsState is EMPTY
    And CspSubarray01 ObsState is EMPTY

    When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)

    Then CbfSubarray01 state is ON
    And PssSubarray01 state is OFF
    And CspSubarray01 state is ON
    And CbfSubarray01 ObsState is IDLE
    And PssSubarray01 ObsState is EMPTY

    And CspSubarray01 ObsState is IDLE

