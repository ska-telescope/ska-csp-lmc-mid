Feature: All the happy paths in CSP runtime


Scenario: CSP is turned ON
    Given All subsystems are fresh initialized
    And CspController isCommunicating is True
    When On Command is issued on CspController with argument [] 
    And CspController longRunningCommandAttributes is (0, 'COMPLETED')

    Then CbfController state is ON
    And CbfSubarray01 state is ON
    And CbfSubarray02 state is ON
    And CbfSubarray03 state is ON

    And PssController state is ON
    And PssSubarray01 state is ON
    And PssSubarray02 state is ON
    And PssSubarray03 state is ON


    And PstBeam01 state is ON
    And PstBeam02 state is ON

    And CspController state is ON
    And CspSubarray01 state is ON
    And CspSubarray02 state is ON
    And CspSubarray03 state is ON

Scenario: CSP assign resources (Dish 1 and 2)
    Given All subsystems are fresh initialized
    And CspController isCommunicating is True
    And On Command is issued on CspSubarray01 (no argument)
    And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
    And CspSubarray01 state is ON
    And CbfSubarray01 state is ON
    And PssSubarray01 state is ON
    And CspSubarray01 ObsState is EMPTY
    And CbfSubarray01 ObsState is EMPTY
    And PssSubarray01 ObsState is EMPTY

    When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)
    And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')

    Then CbfSubarray01 ObsState is IDLE
    And PssSubarray01 ObsState is IDLE
    And CspSubarray01 ObsState is IDLE
   