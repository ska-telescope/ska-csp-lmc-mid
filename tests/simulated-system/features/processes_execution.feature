Feature: Tests for anomalous conditions in process execution

  
  Scenario: CbfSubarray01 goes in ObsState=FAULT during scanning

    Given CspSubarray01 is SCANNING
    When CbfSubarray01 ObsState goes in FAULT

    # Expected behavior:
    # Then CspSubarray01 ObsState is FAULT
    # And PssSubarray01 ObsState is SCANNING

    # Actual Behavior:
    Then CspSubarray01 ObsState is FAULT
    And PssSubarray01 ObsState is SCANNING

  
  Scenario: PssSubarray01 goes in ObsState=FAULT during scanning

    #Given CspSubarray01 is SCANNING
    #When PssSubarray01 ObsState goes in FAULT
    #Then CspSubarray01 ObsState is SCANNING
    #And CbfSubarray01 ObsState is SCANNING


  # Following test is not possible to be executed since current implementation is not able to turn on PstBeam
  # Scenario: Pst Beam goes in ObsState=FAULT during scanning

    # Given CspSubarray01 is SCANNING
    # When PstBeam01 ObsState goes in FAULT
    # And PstBeam02 ObsState goes in FAULT
    # Then CspSubarray01 state is SCANNING
