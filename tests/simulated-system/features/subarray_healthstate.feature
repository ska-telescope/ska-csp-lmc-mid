
Feature: Csp-Lmc Mid Subarray reports and aggregates healthState
      As the Csp-Lmc Subarray for Mid telescope
      I want to collect healthState of connected devices and resources
      aggregate it into healthState of a Csp-Lmc Mid subarray
      and report it to TMC

    @pipeline @healthstate @XTP-41356
    Scenario: Mid Csp Subarray aggregates HealthState
        Given All subsystems are fresh initialized
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 state is ON

        When CbfSubarray01 HealthState are sent straight to <cbf_health>
        And PssSubarray01 HealthState are sent straight to <pss_health> 

        Then CspSubarray01 HealthState is <csp_health>
        Examples:
        | cbf_health  | pss_health    | csp_health    |
        | OK          | OK            | OK            |
        | DEGRADED    | OK            | DEGRADED      |
        | OK          | DEGRADED      | DEGRADED      |
        | DEGRADED    | DEGRADED      | OK            |    
        | FAILED      | OK            | FAILED        |        
        | OK          | FAILED        | DEGRADED      |
        | FAILED      | FAILED        | OK            |
        | OK          | UNKNOWN       | DEGRADED      |
        | UNKNOWN     | OK            | UNKNOWN       |                        
        | UNKNOWN     | UNKNOWN       | OK            | 
        | UNKNOWN     | FAILED        | UNKNOWN       |
        | FAILED      | UNKNOWN       | FAILED        |
        | FAILED      | DEGRADED      | FAILED        |
        | DEGRADED    | FAILED        | DEGRADED      |
        | UNKNOWN     | DEGRADED      | UNKNOWN       |
        | DEGRADED    | UNKNOWN       | DEGRADED      |        

   