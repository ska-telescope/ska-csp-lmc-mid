Feature: Csp-Lmc Mid test all commands

# ------------- Test all commands -------------------
    @pipeline @stress @debug
    Scenario: Test all commands on Subarray01
	#test is currently scanning only with CBF
        Given All subsystems are fresh initialized
        When On Command is issued on CspController with argument []
        Then CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController CspPssState is ON
        And CspController CspCbfState is ON
        And CspController state is ON
        And CspSubarray01 pssSubarrayState is ON
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 state is ON
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 pssSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY

        When AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is EMPTY
        And CspSubarray01 AssignedReceptors is ("SKA001", "SKA036")
        #And CspSubarray01 AssignedTimingBeamIDs is [1, 2]  (not in use)

        When Configure Command is issued on CspSubarray01 (argument from file: Configure_CBF.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 pssSubarrayObsState is EMPTY
        And CspSubarray01 obsState is READY

        When Scan Command is issued on CspSubarray01 (argument from file: Scan.json)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is SCANNING
        And CspSubarray01 pssSubarrayObsState is EMPTY
        And CspSubarray01 obsState is SCANNING

        When Endscan Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')
        And CspSubarray01 cbfSubarrayObsState is READY
        And CspSubarray01 pssSubarrayObsState is EMPTY
        And CspSubarray01 obsState is READY

        When GoToIdle Command is issued on CspSubarray01 (no argument)
        #Then CspSubarray01 commandResult is ('gotoidle', '0')
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is EMPTY
        And CspSubarray01 obsState is IDLE
        # And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')

        When ReleaseAllResources Command is issued on CspSubarray01 (no argument)
        Then CspSubarray01 commandResult is ('releaseallresources', '0')
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 pssSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 AssignedTimingBeamIDs is []
        # And CspSubarray01 longRunningCommandAttributes is (0, 'COMPLETED')

        # When Standby Command is issued on CspController with argument []
        # Then CspController CspPssState is STANDBY
        # And CspController CspCbfState is STANDBY
        # And CspController state is STANDBY
        # And CspSubarray01 pssSubarrayState is OFF
        # And CspSubarray01 cbfSubarrayState is OFF
        # And CspSubarray01 state is OFF
        # And CspSubarray01 cbfSubarrayObsState is EMPTY
        # And CspSubarray01 pssSubarrayObsState is EMPTY
        # And CspSubarray01 obsState is EMPTY
        # And CspController commandResult is ('standby', '0')

        When Off Command is issued on CspController with argument []
        And CspController longRunningCommandAttributes is (0, 'COMPLETED')
        And CspController CspPssState is OFF
        And CspController CspCbfState is OFF
        And CspController state is OFF
        # And CspSubarray01 pssSubarrayState is OFF
        # And CspSubarray01 cbfSubarrayState is OFF
        # And CspSubarray01 state is OFF
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 pssSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY
       

