Feature: Unhappy paths in CSP runtime

    Scenario: CSP Controller stays in STANDBY when turning ON if CBF raises exception
        Given All subsystems are fresh initialized
        
        When CbfController raise an exception      
        And On Command is issued on CspController with argument []

        Then CspController longRunningCommandStatus is 'IN_PROGRESS'
        And CspController longRunningCommandStatus is 'COMPLETED'
        And CspController longRunningCommandResult is '3'
        And CspController commandResult is ('on', '3')

        And CbfController state is STANDBY
        And CbfSubarray01 state is OFF
        And CbfSubarray02 state is OFF
        And CbfSubarray03 state is OFF

        And PssController state is STANDBY
        And PssSubarray01 state is OFF
        And PssSubarray02 state is OFF
        And PssSubarray03 state is OFF

        # And PstController state is STANDBY
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is OFF
        # And PstBeam02 state is OFF
        
        And CspController state is STANDBY
        And CspSubarray01 state is OFF
        And CspSubarray02 state is OFF
        And CspSubarray03 state is OFF

    Scenario: CSP Controller stays in STANDBY when turning ON if PSS raises exception
        Given All subsystems are fresh initialized
        
        When PssController raise an exception      
        And On Command is issued on CspController with argument []

        Then CspController longRunningCommandStatus is 'IN_PROGRESS'
        And CspController longRunningCommandStatus is 'COMPLETED'
        And CspController longRunningCommandResult is '3'
        And CspController commandResult is ('on', '3')

        And CbfController state is ON
        And CbfSubarray01 state is OFF
        And CbfSubarray02 state is OFF
        And CbfSubarray03 state is OFF

        And PssController state is STANDBY
        And PssSubarray01 state is OFF
        And PssSubarray02 state is OFF
        And PssSubarray03 state is OFF

        # And PstController state is STANDBY
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is ON
        # And PstBeam02 state is ON
        
        And CspController state is ON
        And CspSubarray01 state is OFF
        And CspSubarray02 state is OFF
        And CspSubarray03 state is OFF

    @skip
    Scenario: CSP Controller stays in STANDBY when turning ON if PST raises exception
        Given All subsystems are fresh initialized

        When PstController raise an exception
        And On Command is issued on CspController with argument []

        Then CspController longRunningCommandStatus is 'IN_PROGRESS'
        And CspController longRunningCommandStatus is 'COMPLETED'
        And CspController longRunningCommandResult is '3'
        And CspController commandResult is ('on', '3')

        And CbfController state is ON
        And CbfSubarray01 state is OFF
        And CbfSubarray02 state is OFF
        And CbfSubarray03 state is OFF

        And PssController state is ON
        And PssSubarray01 state is OFF
        And PssSubarray02 state is OFF
        And PssSubarray03 state is OFF

        And PstController state is STANDBY
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is ON
        # And PstBeam02 state is ON

        And CspController state is ON
        And CspSubarray01 state is OFF
        And CspSubarray02 state is OFF
        And CspSubarray03 state is OFF
    
    Scenario: CSP Controller stays in STANDBY when turning OFF if CBF raises exception
        Given All subsystems are fresh initialized
 
        When CbfController raise an exception
        And Off Command is issued on CspController with argument []        
        
        Then CbfController state is STANDBY 
        And CbfSubarray01 state is OFF
        And CbfSubarray02 state is OFF
        And CbfSubarray03 state is OFF    

        And PssController state is STANDBY
        And PssSubarray01 state is OFF
        And PssSubarray02 state is OFF
        And PssSubarray03 state is OFF

        # And PstController state is STANDBY
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is OFF
        # And PstBeam02 state is OFF
        
        And CspController state is STANDBY
        And CspSubarray01 state is OFF
        And CspSubarray02 state is OFF
        And CspSubarray03 state is OFF
        And CspController longRunningCommandStatus is 'COMPLETED'
        And CspController longRunningCommandResult is '3'
        And CspController commandResult is ('off', '3')

    
    Scenario: CSP Controller stays in STANDBY when turning OFF if PSS raises exception
        Given All subsystems are fresh initialized
        
        When PssController raise an exception
        And Off Command is issued on CspController with argument []
        
        Then CbfController state is STANDBY 
        And CbfSubarray01 state is OFF
        And CbfSubarray02 state is OFF
        And CbfSubarray03 state is OFF    

        And PssController state is STANDBY
        And PssSubarray01 state is OFF
        And PssSubarray02 state is OFF
        And PssSubarray03 state is OFF

        # And PstController state is STANDBY
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is ON
        # And PstBeam02 state is ON
        
        And CspController state is STANDBY
        And CspSubarray01 state is OFF
        And CspSubarray02 state is OFF
        And CspSubarray03 state is OFF
        And CspController longRunningCommandStatus is 'COMPLETED'
        And CspController longRunningCommandResult is '3'
        And CspController commandResult is ('off', '3')

    
    @skip
    Scenario: CSP Controller stays in STANDBY when turning OFF if PST raises exception
        Given All subsystems are fresh initialized
        
        When PstController raise an exception
        And Off Command is issued on CspController with argument []
        
        Then CbfController state is STANDBY 
        And CbfSubarray01 state is OFF
        And CbfSubarray02 state is OFF
        And CbfSubarray03 state is OFF    

        And PssController state is STANDBY
        And PssSubarray01 state is OFF
        And PssSubarray02 state is OFF
        And PssSubarray03 state is OFF

        And PstController state is STANDBY
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is ON
        # And PstBeam02 state is ON
        
        And CspController state is STANDBY
        And CspSubarray01 state is OFF
        And CspSubarray02 state is OFF
        And CspSubarray03 state is OFF
        And CspController longRunningCommandStatus is 'COMPLETED'
        And CspController longRunningCommandResult is '3'   
        And CspController commandResult is ('off', '3')
    
    
    Scenario: CSP Controller stays in ON when turning STANDBY if CBF raises exception
        Given All subsystems are fresh initialized
        And On Command is issued on CspController with argument []
        And CspController commandResult is ('on', '0')

        When CbfController raise an exception
        And Standby Command is issued on CspController with argument []

        Then CspController longRunningCommandStatus is 'IN_PROGRESS'
        And CspController longRunningCommandStatus is 'COMPLETED'
        And CspController longRunningCommandResult is '3'
        And CspController commandResult is ('standby', '3')  

        And CbfController state is ON 
        And CbfSubarray01 state is ON
        And CbfSubarray02 state is ON
        And CbfSubarray03 state is ON    

        And PssController state is ON
        And PssSubarray01 state is ON
        And PssSubarray02 state is ON
        And PssSubarray03 state is ON

        # And PstController state is ON
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is ON
        # And PstBeam02 state is ON
        
        And CspController state is ON
        And CspSubarray01 state is ON
        And CspSubarray02 state is ON
        And CspSubarray03 state is ON

    
    Scenario: CSP Controller stays in ON when turning STANDBY if PSS raises exception
        Given All subsystems are fresh initialized
        And On Command is issued on CspController with argument []
        And CspController commandResult is ('on', '0')

        When PssController raise an exception
        And Standby Command is issued on CspController with argument []

        Then CspController longRunningCommandStatus is 'IN_PROGRESS'
        And CspController longRunningCommandStatus is 'COMPLETED'
        And CspController longRunningCommandResult is '3'
        And CspController commandResult is ('standby', '3')  

        Then CbfController state is ON 
        And CbfSubarray01 state is ON
        And CbfSubarray02 state is ON
        And CbfSubarray03 state is ON    

        And PssController state is ON
        And PssSubarray01 state is ON
        And PssSubarray02 state is ON
        And PssSubarray03 state is ON

        # And PstController state is ON
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is ON
        # And PstBeam02 state is ON
        
        And CspController state is ON
        And CspSubarray01 state is ON
        And CspSubarray02 state is ON
        And CspSubarray03 state is ON

    @skip
    Scenario: CSP Controller stays in ON when turning STANDBY if PST raises exception
        Given All subsystems are fresh initialized
        And On Command is issued on CspController with argument []
        And CspController commandResult is ('on', '0')

        When PstController raise an exception
        And Standby Command is issued on CspController with argument []

        Then CspController longRunningCommandStatus is 'IN_PROGRESS'
        And CspController longRunningCommandStatus is 'COMPLETED'
        And CspController longRunningCommandResult is '3'
        And CspController commandResult is ('standby', '3')  

        Then CbfController state is ON 
        And CbfSubarray01 state is ON
        And CbfSubarray02 state is ON
        And CbfSubarray03 state is ON    

        And PssController state is ON
        And PssSubarray01 state is ON
        And PssSubarray02 state is ON
        And PssSubarray03 state is ON

        # And PstController state is ON
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is ON
        # And PstBeam02 state is ON
        
        And CspController state is ON
        And CspSubarray01 state is ON
        And CspSubarray02 state is ON
        And CspSubarray03 state is ON

    Scenario: CSP Controller stays in STANDBY when turning ON if CBF raises timeout
        Given All subsystems are fresh initialized
        
        When CbfController raise a timeout
        And On Command is issued on CspController with argument []
        
        Then CbfController state is STANDBY 
        And CbfSubarray01 state is OFF
        And CbfSubarray02 state is OFF
        And CbfSubarray03 state is OFF    

        And PssController state is STANDBY
        And PssSubarray01 state is OFF
        And PssSubarray02 state is OFF
        And PssSubarray03 state is OFF

        # And PstController state is STANDBY
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is OFF
        # And PstBeam02 state is OFF
        
        And CspController state is STANDBY
        And CspSubarray01 state is OFF
        And CspSubarray02 state is OFF
        And CspSubarray03 state is OFF

        And CspController longRunningCommandStatus is 'COMPLETED'
        And CspController longRunningCommandResult is '3'
        And CspController commandResult is ('on', '3')

    Scenario: CSP Controller stays in STANDBY when turning ON if PSS raises timeout
        Given All subsystems are fresh initialized
        
        When PssController raise a timeout
        And On Command is issued on CspController with argument []
        
        Then CbfController state is STANDBY 
        And CbfSubarray01 state is OFF
        And CbfSubarray02 state is OFF
        And CbfSubarray03 state is OFF    

        And PssController state is STANDBY
        And PssSubarray01 state is OFF
        And PssSubarray02 state is OFF
        And PssSubarray03 state is OFF

        # And PstController state is STANDBY
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is ON
        # And PstBeam02 state is ON
        
        And CspController state is STANDBY
        And CspSubarray01 state is OFF
        And CspSubarray02 state is OFF
        And CspSubarray03 state is OFF
        And CspController longRunningCommandStatus is 'COMPLETED'
        And CspController longRunningCommandResult is '3'
        And CspController commandResult is ('on', '3')

    @skip
    Scenario: CSP Controller stays in STANDBY when turning ON if PST raises timeout
        Given All subsystems are fresh initialized
        
        When PstController raise a timeout
        And On Command is issued on CspController with argument []
        
        Then CbfController state is STANDBY 
        And CbfSubarray01 state is OFF
        And CbfSubarray02 state is OFF
        And CbfSubarray03 state is OFF    

        And PssController state is STANDBY
        And PssSubarray01 state is OFF
        And PssSubarray02 state is OFF
        And PssSubarray03 state is OFF

        And PstController state is STANDBY
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is ON
        # And PstBeam02 state is ON
        
        And CspController state is STANDBY
        And CspSubarray01 state is OFF
        And CspSubarray02 state is OFF
        And CspSubarray03 state is OFF
        And CspController longRunningCommandStatus is 'COMPLETED'
        And CspController longRunningCommandResult is '3'        
        And CspController commandResult is ('on', '3')

    
    Scenario: CSP Controller stays in STANDBY when turning OFF if CBF raises timeout
        Given All subsystems are fresh initialized
 
        When CbfController raise a timeout
        And Off Command is issued on CspController with argument []        
        
        Then CbfController state is STANDBY 
        And CbfSubarray01 state is OFF
        And CbfSubarray02 state is OFF
        And CbfSubarray03 state is OFF    

        And PssController state is STANDBY
        And PssSubarray01 state is OFF
        And PssSubarray02 state is OFF
        And PssSubarray03 state is OFF

        # And PstController state is STANDBY
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is OFF
        # And PstBeam02 state is OFF
        
        And CspController state is STANDBY
        And CspSubarray01 state is OFF
        And CspSubarray02 state is OFF
        And CspSubarray03 state is OFF
        And CspController longRunningCommandStatus is 'COMPLETED'
        And CspController longRunningCommandResult is '3'
        And CspController commandResult is ('off', '3')

    
    Scenario: CSP Controller stays in STANDBY when turning OFF if PSS raises timeout
        Given All subsystems are fresh initialized
        
        When PssController raise a timeout
        And Off Command is issued on CspController with argument []
        
        Then CbfController state is STANDBY 
        And CbfSubarray01 state is OFF
        And CbfSubarray02 state is OFF
        And CbfSubarray03 state is OFF    

        And PssController state is STANDBY
        And PssSubarray01 state is OFF
        And PssSubarray02 state is OFF
        And PssSubarray03 state is OFF

        # And PstController state is STANDBY
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is ON
        # And PstBeam02 state is ON
        
        And CspController state is STANDBY
        And CspSubarray01 state is OFF
        And CspSubarray02 state is OFF
        And CspSubarray03 state is OFF
        And CspController longRunningCommandStatus is 'COMPLETED'
        And CspController longRunningCommandResult is '3'
        And CspController commandResult is ('off', '3')

    
    @skip
    Scenario: CSP Controller stays in STANDBY when turning OFF if PST raises timeout
        Given All subsystems are fresh initialized
        
        When PstController raise a timeout
        And Off Command is issued on CspController with argument []
        
        Then CbfController state is STANDBY 
        And CbfSubarray01 state is OFF
        And CbfSubarray02 state is OFF
        And CbfSubarray03 state is OFF    

        And PssController state is STANDBY
        And PssSubarray01 state is OFF
        And PssSubarray02 state is OFF
        And PssSubarray03 state is OFF

        And PstController state is STANDBY
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is ON
        # And PstBeam02 state is ON
        
        And CspController state is STANDBY
        And CspSubarray01 state is OFF
        And CspSubarray02 state is OFF
        And CspSubarray03 state is OFF
        And CspController longRunningCommandStatus is 'COMPLETED'
        And CspController longRunningCommandResult is '3'   
        And CspController commandResult is ('off', '3')
     
    Scenario: CSP Controller stays in ON when turning STANDBY if CBF raises timeout
        Given All subsystems are fresh initialized
        And On Command is issued on CspController with argument []
        And CspController commandResult is ('on', '0')

        When CbfController raise a timeout
        And Standby Command is issued on CspController with argument []
        And CspController longRunningCommandStatus is 'COMPLETED'
        And CspController longRunningCommandResult is '3'
        And CspController commandResult is ('standby', '3')  
        
        Then CbfController state is ON 
        And CbfSubarray01 state is OFF
        And CbfSubarray02 state is OFF
        And CbfSubarray03 state is OFF   

        And PssController state is ON
        And PssSubarray01 state is OFF
        And PssSubarray02 state is OFF
        And PssSubarray03 state is OFF

        # And PstController state is ON
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is ON
        # And PstBeam02 state is ON
        
        And CspController state is ON
        And CspSubarray01 state is OFF
        And CspSubarray02 state is OFF
        And CspSubarray03 state is OFF
        
    Scenario: CSP Controller transitions to STANDBY when turning STANDBY if PSS raises timeout
        Given All subsystems are fresh initialized
        And On Command is issued on CspController with argument []
        And CspController commandResult is ('on', '0')

        When PssController raise a timeout
        And Standby Command is issued on CspController with argument []
        And CspController longRunningCommandStatus is 'COMPLETED'
        And CspController longRunningCommandResult is '3'
        And CspController commandResult is ('standby', '3')
        
        Then CbfController state is STANDBY
        And CbfSubarray01 state is OFF
        And CbfSubarray02 state is OFF
        And CbfSubarray03 state is OFF    

        And PssController state is ON
        And PssSubarray01 state is OFF
        And PssSubarray02 state is OFF
        And PssSubarray03 state is OFF

        And CspController state is STANDBY
        And CspSubarray01 state is OFF
        And CspSubarray02 state is OFF
        And CspSubarray03 state is OFF

            
    Scenario: CSP Subarray stays in ON when turning OFF if CBF raises exception
        Given Subarray01 is fresh initialized
        And CspSubarray01 isCommunicating is True
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 commandResult is ('on', '0') 
        
        When CbfSubarray01 raise an exception
        And Off Command is issued on CspSubarray01 (no argument)

        Then CbfSubarray01 state is ON   

        And PssSubarray01 state is ON
        
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is ON
        # And PstBeam02 state is ON
        
        And CspSubarray01 state is ON
        And CspSubarray01 longRunningCommandResult is '3'
        And CspSubarray01 commandResult is ('off', '3')        
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'

    
    Scenario: CSP Subarray stays in ON when turning OFF if PSS raises exception
        Given Subarray01 is fresh initialized
        And CspSubarray01 isCommunicating is True
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 commandResult is ('on', '0') 
        
        When PssSubarray01 raise an exception
        And Off Command is issued on CspSubarray01 (no argument)

        Then CbfSubarray01 state is ON   

        And PssSubarray01 state is ON
        
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is ON
        # And PstBeam02 state is ON
        
        And CspSubarray01 state is ON
        And CspSubarray01 longRunningCommandResult is '3'
        And CspSubarray01 commandResult is ('off', '3')        
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'        

    
    Scenario: CSP Subarray stays in OFF when turning ON if CBF raises exception
        Given Subarray01 is fresh initialized
        And CspSubarray01 isCommunicating is True 
        
        When CbfSubarray01 raise an exception
        And On Command is issued on CspSubarray01 (no argument)

        Then CbfSubarray01 state is OFF

        And PssSubarray01 state is OFF
        
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is OFF
        # And PstBeam02 state is OFF
        
        And CspSubarray01 state is OFF
        And CspSubarray01 longRunningCommandResult is '3'
        And CspSubarray01 commandResult is ('on', '3')        
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'

    
    Scenario: CSP Subarray stays in OFF when turning ON if PSS raises exception
        Given Subarray01 is fresh initialized
        And CspSubarray01 isCommunicating is True

        When PssSubarray01 raise an exception
        And On Command is issued on CspSubarray01 (no argument)

        Then CbfSubarray01 state is OFF

        And PssSubarray01 state is OFF
        
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is OFF
        # And PstBeam02 state is OFF
        
        And CspSubarray01 state is OFF
        And CspSubarray01 longRunningCommandResult is '3'
        And CspSubarray01 commandResult is ('on', '3')        
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED' 

    
    Scenario: CSP Subarray stays in ON when turning OFF if CBF raises timeout
        Given Subarray01 is fresh initialized
        And CspSubarray01 isCommunicating is True
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 commandResult is ('on', '0') 
        
        When CbfSubarray01 raise a timeout
        And Off Command is issued on CspSubarray01 (no argument)

        Then CbfSubarray01 state is ON   

        And PssSubarray01 state is ON
        
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is ON
        # And PstBeam02 state is ON
        
        And CspSubarray01 state is ON
        And CspSubarray01 longRunningCommandResult is '3'
        And CspSubarray01 commandResult is ('off', '3')        
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'

    
    Scenario: CSP Subarray stays in ON when turning OFF if PSS raises timeout
        Given Subarray01 is fresh initialized
        And CspSubarray01 isCommunicating is True
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 commandResult is ('on', '0') 
        
        When PssSubarray01 raise a timeout
        And Off Command is issued on CspSubarray01 (no argument)

        Then CbfSubarray01 state is ON   

        And PssSubarray01 state is ON
        
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is ON
        # And PstBeam02 state is ON
        
        And CspSubarray01 state is ON
        And CspSubarray01 longRunningCommandResult is '3'
        And CspSubarray01 commandResult is ('off', '3')        
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'        

    
    Scenario: CSP Subarray stays in OFF when turning ON if CBF raises timeout
        Given Subarray01 is fresh initialized
        And CspSubarray01 isCommunicating is True 
        
        When CbfSubarray01 raise a timeout
        And On Command is issued on CspSubarray01 (no argument)

        Then CbfSubarray01 state is OFF

        And PssSubarray01 state is OFF
        
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is OFF
        # And PstBeam02 state is OFF
        
        And CspSubarray01 state is OFF
        And CspSubarray01 longRunningCommandResult is '3'
        And CspSubarray01 commandResult is ('on', '3')        
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'

    
    Scenario: CSP Subarray stays in OFF when turning ON if PSS raises timeout
        Given Subarray01 is fresh initialized
        And CspSubarray01 isCommunicating is True

        When PssSubarray01 raise a timeout
        And On Command is issued on CspSubarray01 (no argument)

        Then CbfSubarray01 state is OFF

        And PssSubarray01 state is OFF
        
        # current implementation is not able to turn on PstBeam
        # And PstBeam01 state is OFF
        # And PstBeam02 state is OFF
        
        And CspSubarray01 state is OFF
        And CspSubarray01 longRunningCommandResult is '3'
        And CspSubarray01 commandResult is ('on', '3')        
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'         


# ------------- Assign Resources Command -------------------

    @assign
    Scenario: CSPSubarray01 is assigning resources but CBF raise an exception

        Given Subarray01 is fresh initialized
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 commandResult is ('on', '0')
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 pssSubarrayState is ON
        And CbfSubarray01 ObsState are sent straight to EMPTY
        And PssSubarray01 ObsState are sent straight to EMPTY
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 pssSubarrayObsState is EMPTY
        And CspSubarray01 obsState is EMPTY

        When CbfSubarray01 raise an exception
        And AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)
     
        Then CbfSubarray01 obsState is EMPTY
        And PssSubarray01 obsState is IDLE
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 pssSubarrayObsState is IDLE    
        And CspSubarray01 commandResult is ('assignresources', '3')
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'
        And CspSubarray01 longRunningCommandResult is '3'  

    @assign
    Scenario: CSPSubarray01 is assigning resources but CBF raise a timeout

        Given Subarray01 is fresh initialized
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 commandResult is ('on', '0')
        And CspSubarray01 cbfSubarrayState is ON
        And CspSubarray01 pssSubarrayState is ON
        And CbfSubarray01 ObsState are sent straight to EMPTY
        And PssSubarray01 ObsState are sent straight to EMPTY
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 pssSubarrayObsState is EMPTY

        When CbfSubarray01 raise a timeout
        And AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)

        Then CspSubarray01 commandResult is ('assignresources', '3')
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'
        And CspSubarray01 longRunningCommandResult is '3'       
        And CbfSubarray01 obsState is EMPTY
        And PssSubarray01 obsState is IDLE
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 pssSubarrayObsState is IDLE   

    @assign
    Scenario: CSPSubarray01 is assigning resources but PSS raise an exception

        Given Subarray01 is fresh initialized
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 commandResult is ('on', '0')
        And CbfSubarray01 ObsState are sent straight to EMPTY
        And PssSubarray01 ObsState are sent straight to EMPTY
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 pssSubarrayObsState is EMPTY

        When PssSubarray01 raise an exception
        And AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)

        Then CspSubarray01 commandResult is ('assignresources', '3')
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'
        And CspSubarray01 longRunningCommandResult is '3'            
        And CbfSubarray01 obsState is IDLE
        And PssSubarray01 obsState is EMPTY
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is EMPTY

    @assign
    Scenario: CSPSubarray01 is assigning resources but PSS raise a timeout

        Given Subarray01 is fresh initialized
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 commandResult is ('on', '0')
        And CbfSubarray01 ObsState are sent straight to EMPTY
        And PssSubarray01 ObsState are sent straight to EMPTY
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 pssSubarrayObsState is EMPTY

        When PssSubarray01 raise a timeout
        And AssignResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)

        Then CspSubarray01 commandResult is ('assignresources', '3')
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'
        And CspSubarray01 longRunningCommandResult is '3'            
        And CbfSubarray01 obsState is IDLE
        And PssSubarray01 obsState is EMPTY
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is EMPTY


# ----------------- Release Resources Command -------------------

    @release
    Scenario: CSPSubarray01 is releasing resources but CBF raise an exception

        Given Subarray01 is fresh initialized
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 commandResult is ('on', '0')
        And CbfSubarray01 ObsState are sent straight to IDLE
        And PssSubarray01 ObsState are sent straight to IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is IDLE

        When CbfSubarray01 raise an exception
        And ReleaseResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)

        Then CspSubarray01 commandResult is ('releaseresources', '3')        
        And CbfSubarray01 obsState is IDLE
        And PssSubarray01 obsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is IDLE    
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'
        And CspSubarray01 longRunningCommandResult is '3'          

    @release
    Scenario: CSPSubarray01 is releasing resources but CBF raise a timeout

        Given Subarray01 is fresh initialized
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 commandResult is ('on', '0')
        And CbfSubarray01 ObsState are sent straight to IDLE
        And PssSubarray01 ObsState are sent straight to IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is IDLE

        When CbfSubarray01 raise a timeout
        And ReleaseResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)

              
        Then CbfSubarray01 obsState is IDLE
        And PssSubarray01 obsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is IDLE 
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'
        And CspSubarray01 longRunningCommandResult is '3'           
        And CspSubarray01 commandResult is ('releaseresources', '3') 
        

    @release
    Scenario: CSPSubarray01 is releasing resources but PSS raise an exception

        Given Subarray01 is fresh initialized
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 commandResult is ('on', '0')
        And CbfSubarray01 ObsState are sent straight to IDLE
        And PssSubarray01 ObsState are sent straight to IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is IDLE

        When PssSubarray01 raise an exception
        And ReleaseResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)

        Then CbfSubarray01 obsState is IDLE
        And PssSubarray01 obsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is IDLE  
        And CspSubarray01 commandResult is ('releaseresources', '3')
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'
        And CspSubarray01 longRunningCommandResult is '3'                   

    @release
    Scenario: CSPSubarray01 is releasing resources but PSS raise a timeout

        Given Subarray01 is fresh initialized
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 commandResult is ('on', '0')
        And CbfSubarray01 ObsState are sent straight to IDLE
        And PssSubarray01 ObsState are sent straight to IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is IDLE

        When PssSubarray01 raise a timeout
        And ReleaseResources Command is issued on CspSubarray01 (argument from file: AssignResources_CBF.json)

        
        Then CbfSubarray01 obsState is IDLE
        Then PssSubarray01 obsState is IDLE
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is IDLE 
        And CspSubarray01 commandResult is ('releaseresources', '3')
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'
        And CspSubarray01 longRunningCommandResult is '3'                  


# ------------- Release All Resources Command -------------------        

    @release
    Scenario: CSPSubarray01 is releasing all resources but CBF raise an exception

        Given Subarray01 is fresh initialized
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 commandResult is ('on', '0')
        And CbfSubarray01 ObsState are sent straight to IDLE
        And PssSubarray01 ObsState are sent straight to IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is IDLE

        When CbfSubarray01 raise an exception
        And ReleaseAllResources Command is issued on CspSubarray01 (no argument)

        Then CspSubarray01 commandResult is ('releaseallresources', '3')
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'
        And CspSubarray01 longRunningCommandResult is '3'          
        And CbfSubarray01 obsState is IDLE
        And PssSubarray01 obsState is EMPTY
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is EMPTY        

    @release
    Scenario: CSPSubarray01 is releasing all resources but CBF raise a timeout
        
        Given Subarray01 is fresh initialized
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 commandResult is ('on', '0')
        And CbfSubarray01 ObsState are sent straight to IDLE
        And PssSubarray01 ObsState are sent straight to IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is IDLE

        When CbfSubarray01 raise a timeout
        And ReleaseAllResources Command is issued on CspSubarray01 (no argument)

        Then CspSubarray01 commandResult is ('releaseallresources', '3')
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'
        And CspSubarray01 longRunningCommandResult is '3'          
        And CbfSubarray01 obsState is IDLE
        And PssSubarray01 obsState is EMPTY
        And CspSubarray01 obsState is IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is EMPTY 

    @release
    Scenario: CSPSubarray01 is releasing all resources but PSS raise an exception

        Given Subarray01 is fresh initialized
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 commandResult is ('on', '0')
        And CbfSubarray01 ObsState are sent straight to IDLE
        And PssSubarray01 ObsState are sent straight to IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is IDLE

        When PssSubarray01 raise an exception
        And ReleaseAllResources Command is issued on CspSubarray01 (no argument)

        Then CspSubarray01 commandResult is ('releaseallresources', '3')
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'
        And CspSubarray01 longRunningCommandResult is '3'          
        And CbfSubarray01 obsState is EMPTY
        And PssSubarray01 obsState is IDLE
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 pssSubarrayObsState is IDLE 
    

    @release
    Scenario: CSPSubarray01 is releasing all resources but PSS raise a timeout

        Given Subarray01 is fresh initialized
        And On Command is issued on CspSubarray01 (no argument)
        And CspSubarray01 commandResult is ('on', '0')
        And CbfSubarray01 ObsState are sent straight to IDLE
        And PssSubarray01 ObsState are sent straight to IDLE
        And CspSubarray01 cbfSubarrayObsState is IDLE
        And CspSubarray01 pssSubarrayObsState is IDLE

        When PssSubarray01 raise a timeout
        And ReleaseAllResources Command is issued on CspSubarray01 (no argument)

        Then CspSubarray01 commandResult is ('releaseallresources', '3')
        And CspSubarray01 longRunningCommandStatus is 'COMPLETED'
        And CspSubarray01 longRunningCommandResult is '3'          
        And CbfSubarray01 obsState is EMPTY
        And PssSubarray01 obsState is IDLE
        And CspSubarray01 obsState is EMPTY
        And CspSubarray01 cbfSubarrayObsState is EMPTY
        And CspSubarray01 pssSubarrayObsState is IDLE
