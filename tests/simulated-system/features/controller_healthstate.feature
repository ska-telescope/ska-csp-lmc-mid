
Feature: Csp-Lmc Mid Controller reports and aggregates healthState
      As the Csp-Lmc Controller for Mid telescope
      I want to collect healthState of connected devices and resources
      aggregate it into healthState of a Csp-Lmc Mid Controller
      and report it to TMC

    @pipeline @healthstate @XTP-41359
    Scenario: Mid Csp Controller aggregates HealthState
        Given All subsystems are fresh initialized
        And CspController isCommunicating is True
        And On Command is issued on CspController with argument []
        And CspController state is ON

        When CbfController HealthState are sent straight to <cbf_health>
        And CbfSubarray01 HealthState are sent straight to <cbf_sub01_health>
        And CbfSubarray02 HealthState are sent straight to <cbf_sub02_health>
        And CbfSubarray03 HealthState are sent straight to <cbf_sub03_health>        
        And PssController HealthState are sent straight to <pss_health>
        And PssSubarray01 HealthState are sent straight to <pss_sub01_health>
        And PssSubarray02 HealthState are sent straight to <pss_sub02_health>
        And PssSubarray03 HealthState are sent straight to <pss_sub03_health>          
        
        Then CspController HealthState is <csp_health>
        Examples:
        | cbf_health  | pss_health    | cbf_sub01_health | cbf_sub02_health | cbf_sub03_health | pss_sub01_health | pss_sub02_health | pss_sub03_health | csp_health |
        | OK          | OK            | OK               | OK               |OK                | OK               | OK               |OK                | OK         |  
        | FAILED      | OK            | OK               | OK               |OK                | OK               | OK               |OK                | FAILED     |
        | DEGRADED    | OK            | OK               | OK               |OK                | OK               | OK               |OK                | DEGRADED   |
        | UNKNOWN     | OK            | OK               | OK               |OK                | OK               | OK               |OK                | UNKNOWN    | #tbd           
        | OK          | FAILED        | OK               | OK               |OK                | OK               | OK               |OK                | DEGRADED   |
        | OK          | DEGRADED      | OK               | OK               |OK                | OK               | OK               |OK                | DEGRADED   |
        | OK          | UNKNOWN       | OK               | OK               |OK                | OK               | OK               |OK                | DEGRADED   |    
        | OK          | OK            | FAILED           | OK               |OK                | OK               | OK               |OK                | DEGRADED   | 
        | OK          | OK            | DEGRADED         | OK               |OK                | OK               | OK               |OK                | DEGRADED   |   
        | OK          | OK            | UNKNOWN          | OK               |OK                | OK               | OK               |OK                | DEGRADED   |      
        | OK          | OK            | OK               | OK               |OK                | FAILED           | OK               |OK                | DEGRADED   | 
        | OK          | OK            | OK               | OK               |OK                | DEGRADED         | OK               |OK                | DEGRADED   |   
        | OK          | OK            | OK               | OK               |OK                | UNKNOWN          | OK               |OK                | DEGRADED   |                                               
        