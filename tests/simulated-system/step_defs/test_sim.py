import logging

import pytest
from pytest_bdd import given, parsers, scenarios, when
from ska_csp_lmc_common.testing.test_bdd_utils import (
    TestDevice,
    fresh_new_subsystems,
)

module_logger = logging.getLogger(__name__)

#  -------------- SCENARIOS --------------

scenarios("../features/command_execution.feature")
scenarios("../features/external_conditions.feature")  # to remain skipped
scenarios("../features/happy_paths.feature")
scenarios("../features/unhappy_paths.feature")
scenarios("../features/initial_attributes.feature")
scenarios("../features/processes_execution.feature")
scenarios("../features/all_commands.feature")  # delete dummy test
scenarios("../features/subarray_healthstate.feature")
scenarios("../features/controller_healthstate.feature")

# -------------- MARKERS --------------


def pytest_configure(config):
    config.addinivalue_line("markers", "csp_k8s")
    config.addinivalue_line("markers", "bdd")
    config.addinivalue_line("markers", "controller")
    config.addinivalue_line("markers", "thisone")
    config.addinivalue_line("markers", "subarray")
    config.addinivalue_line("markers", "pipeline")
    config.addinivalue_line("markers", "all")
    config.addinivalue_line("markers", "nightly")
    config.addinivalue_line("markers", "stress")
    config.addinivalue_line("markers", "healthstate")


# -------------- FIXTURES --------------

temp_dev_info = None

repository = "mid"
test_context = "sim"

list_of_controller_names = [
    "MidCbfCtrlSimulator",
    "MidPssCtrlSimulator",
    "MidCspController",
]

list_of_subarray_names = [
    "MidCbfSubarraySimulator01",
    "MidCbfSubarraySimulator02",
    "MidCbfSubarraySimulator03",
    "MidPssSubarraySimulator01",
    "MidPssSubarraySimulator02",
    "MidPssSubarraySimulator03",
    "MidCspSubarray01",
    "MidCspSubarray02",
    "MidCspSubarray03",
]

# currently no Mid PST beam is deployed
list_of_beam_names = []

list_of_vcc_names = []

list_of_capability_names = []


@pytest.fixture(scope="session")
def all_controllers():
    controllers = dict()
    for name in list_of_controller_names:
        module_logger.info(f"Creating proxy for {name}")
        idx = (
            name.replace("Simulator", "")
            .replace("Mid", "")
            .replace("Ctrl", "Controller")
        )
        controllers[idx] = TestDevice(name, repository, test_context)
    return controllers


@pytest.fixture(scope="session")
def all_subarrays():
    subarrays = dict()
    for name in list_of_subarray_names:
        module_logger.info(f"Creating proxy for {name}")
        idx = name.replace("Simulator", "").replace("Mid", "")
        subarrays[idx] = TestDevice(name, repository, test_context)
    return subarrays


@pytest.fixture(scope="session")
def all_beams():
    beams = dict()
    for name in list_of_beam_names:
        module_logger.info(f"Creating proxy for {name}")
        idx = name.replace("SimulatorDevice", "")
        beams[idx] = TestDevice(name, repository, test_context)
    return beams


@pytest.fixture(scope="session")
def all_vccs():
    vccs = dict()
    return vccs


@pytest.fixture(scope="session")
def all_capabilities():
    capabilities = dict()
    return capabilities


@given("CspSubarray01 is SCANNING")
def subarray_in_scanning(all_controllers, all_subarrays, all_beams):
    """Send the CP Subarray in SCANNING.

    This is performed executing the configuration commands one after the other.
    """
    fresh_new_subsystems(all_controllers, all_subarrays, all_beams)
    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}

    csp_ctrl = all_subsystems["CspController"]
    csp_sub01 = all_subsystems["CspSubarray01"]

    csp_ctrl.send_command_with_argument("On", "[]")
    csp_ctrl.get_attribute("commandResult", "('on', '0')")
    csp_ctrl.get_attribute(
        "state",
        "ON",
    )
    csp_sub01.get_attribute(
        "state",
        "ON",
    )
    csp_sub01.get_attribute("ObsState", "EMPTY")
    all_subsystems["CbfSubarray01"].get_attribute("state", "ON")
    all_subsystems["CbfSubarray01"].get_attribute("ObsState", "EMPTY")
    all_subsystems["PssSubarray01"].get_attribute("state", "ON")
    all_subsystems["PssSubarray01"].get_attribute("ObsState", "EMPTY")

    csp_sub01.send_command_with_argument_from_file(
        "AssignResources",
        "AssignResources_CBF.json",
    )

    csp_sub01.get_attribute("ObsState", "IDLE")
    all_subsystems["CbfSubarray01"].get_attribute("ObsState", "IDLE")
    all_subsystems["PssSubarray01"].get_attribute("ObsState", "IDLE")
    csp_sub01.get_attribute("commandResult", "('assign', '0')")

    csp_sub01.send_command_with_argument_from_file(
        "Configure",
        "test_Configure_CBF_PSS.json",
    )

    csp_sub01.get_attribute("commandResult", "('configure', '0')")

    csp_sub01.get_attribute("ObsState", "READY")
    all_subsystems["CbfSubarray01"].get_attribute("ObsState", "READY")
    all_subsystems["PssSubarray01"].get_attribute("ObsState", "READY")

    csp_sub01.send_command_with_argument_from_file(
        "Scan",
        "Scan.json",
    )

    csp_sub01.get_attribute("ObsState", "SCANNING")
    all_subsystems["CbfSubarray01"].get_attribute("ObsState", "SCANNING")
    all_subsystems["PssSubarray01"].get_attribute("ObsState", "SCANNING")


@when(parsers.parse("{device_name} ObsState goes in FAULT"))
def go_to_obsstate_fault(
    device_name, all_controllers, all_subarrays, all_beams
):

    all_subsystems = {**all_controllers, **all_subarrays, **all_beams}
    device = all_subsystems[device_name]
    device.straight_to("ObsState", "FAULT")
    device.get_attribute("obsState", "FAULT")
