###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

Latest pre-release
------------------
- Updated the Helm charts to deploy PSS.LMC ver 0.2.0-rc1. 

1.0.0-rc.3
----------
- Quick fix to enable parametric configuration for number of Subarrays and PST Beams in controller

1.0.0-rc.2
----------
- Added possibility to deploy PST for On/Off operation
- Added parametric configuration for number of Subarrays and PST Beams
- Update ska-csp-lmc-common to v1.0.1 (fix SKB-734, SKB-736, SKB-739)
- Remove support for sdb cbf emulator (mid-cbf-emulated chart)

1.0.0-rc.1
----------
- Rework commands (fix SKB-637, SKB-501, SKB-629, SKB-505, SKB-689)
  - update abort
  - update command timeout
  - implement command map as class
  - update initialisation process and init command
  - include capabilities in the initialisation process
  - update standby
  - update on/off
  - update scan
  - update gotoidle/reset/restart
  - update loadDishCfg
  - update assign/release/releaseAll resources
  - clean code
  - fix restart command release pst beam (fix SKB-708)
  - update documentation
  - fix assigned beams and obsmode messages
- Update ska-csp-lmc-common v1.0.0

0.24.0
------
- Use common package 0.27.2 with fix for bug skb-591.
- Modify CSP.LMC Controller to only forward On/Off commands to CBF controller and skip subarrays (always On) 
- Update communication startup procedure to write simulationMode to CBF before adminMode
- Update ska-mid-cbf-mcs to v 1.0.0
- Use common package version 0.27.0 supporting TelModel 1.19.2
- Update the json parser to support the 'midcbf' section in configuration schema.
- update ska-csp-simulators 
  - version (0.7.1)
  - registry: artifact.skao.int
- update ska-csp-lmc-common to v 0.27.1 (fix SKB-493)

0.23.1
------
- Use common package version 0.25.1 (fix SKB-472)
- Add tests to verify behaviour of obsMode when reconfiguring

0.23.0
------
- Add CspMidJsonValidator class to perform semantic checks on JSON config file
- Use common package version 0.25.0.
  - Override the obsMode attribute and change the type to array of obsMode values
- Specialization of the method that evaluates the obsMode by retrieving information from the Configuration script.
- Update to ska-mid-cbf-mcs to version 0.15.2

0.22.0
------
- Use common package version 0.24.0
  - installing SKA Base Classes 1.0.0
  - update tel model to v1.18.1
- Updated tests to use ENGINEERING adminMode instead of MAINTENANCE
- Use CSP simulators helm chart v. 0.4.0
- Update documentation
- Update taranta charts

0.21.0
------
- Use common package version 0.22.1 supporting JSON strict validation (strictness=2) in 
  CspJsonValidator.
- Add telescope model and subsystem version pages in the documentation
- Add integration tests to evaluate HealthState aggregation using simulators
- Update csp lmc to use yaml file to configure its devices

0.20.1
------
- fix documentation RTD

0.20.0
------
- Add CHANGELOG to documentation RTD
- Add test for verify versionId value
- Updated to SKA BC 0.20.2 to fix the deadlock issue
- Updated to pytango 9.5.0
- Updated Dockerfile images to pytango-builder:9.5.0 and pytango-runtime:9.5.0
- Fix ska-csp-lmc-mid HELM chart to use image in harbor.skao.int/staging.
- add EDA configuration under resources/eda
- update taranta charts

0.19.0
------
- Use common package v0.21.0 (use of telmodel 1.14.0 in v0.20.1)
- Update controller and subarray attributes documentation with examples
- update RTD building procedure according to STS-553
- update RTD documentation
  - ska-tango-util 0.4.10
  - ska-tango-base 0.4.9
  - ska-tango-taranta 2.7.3
  - ska-tango-taranta-auth 0.2.1
  - ska-tango-taranta-dashboard 1.5.0
  - ska-tango-taranta-dashboard-pvc 1.5.0
- remove old simulators charts (sim-pss, sim-pst, sim-cbf) and use of the new ska-csp-simulators
- enable tests with new simulators
- enable scanID attribute subscription
- add tests to verify healthstate aggregation

0.18.2
------
- Use common package v0.19.2 (fix abort during resource assignment).
- Report dishVccCfg and sourceDishVccCfg attributes as empty before ONLINE.

0.18.1
------
- Use common package v.0.19.1.
- Report the updated value for the configurationID attribute.

0.18.0
------
- Use common package v.0.19.0.
- Implemented LoadDishCfg command and dishVccCfg and sourceDishVccCfg attributes  
- Update to ska-mid-cbf-mcs 0.12.15
- Add support for Jupyter
- Added BDD tests to check the correct detection of archive events on 
- Subscription to attributes related to LoadDishCfg command (commented until mid cbf set them to push event).
- Add push event on change to attributes related to LoadDishCfg command
- Implemented an alarm flag in Mid CSP.LMC Controller related to LoadDishCfg command failure
- Added the helm chart to deploy an Alarm Handler in CSP.LMC namespace. Provided tests to
  verify its configuration and operation.

0.17.2
------
- Update to ska-mid-cbf-mcs 0.12.2

0.17.1
------
- Update to ska-mid-cbf-mcs 0.12.0
- Implement call to InitSysParam command within On command procedure
- Update simulators to align with new command

0.17.0
------
- Updated to common 0.18.2 to use Telescope Model 1.9.1.
- Updated FSP capability to expose FSPs info aggregated by subarray membership (fspsSubarrayXX attributes)
- Updated VCC capability to expose VCCs info aggregated by subarray membership (vccsSubarrayXX attributes)
- Updated subarrays attributes related to assigned Fsps and Vccs subscribing Vcc and fsp capabilities aggregated attributes
- Updated Scan command to support scan_id as an integer
- Removed polling configuration for some of the attrubutes of the subarray device

0.16.5
------
- Updated to ska-csp-lmc-common 0.18.0
- Updated the Makefile, gitlab yaml and Helm charts file to enable the use of Tango operator in the deployment,
  except when simulator devices are used.
- Implemented a specific class (MidCapabilityConnectinManager) to handle the connection
  to the Capability devices, and subscription of the attributes. 
- The Subarray connects to the Capability devices and subscription to the
  attributes of interest (currently vccsFqdnAssigned and fspsFqdnAssigned).
- At disconnection (adminmode = OFFLINE) only the attributes of the sub-system devices
  are unsubscribed: the connection with the Capability devices (and subscription to the
  attributes) is  maintened.
- Implemented the attributes assignedVcc and assignedFsp in the Subarray device to report
  the list of the VCCs and FSPs IDs assigned to the subarray.
- Modify the value of the client side timeout when the On command is executed on the Mid CBF Controller.
  The set value is the one configured via the commandTimeout attribute.
  The client side timeout is reset to the default value (3 sec) when the command completes.

0.16.4
------
- Updated to ska-csp-lmc-common 0.17.7
- Updated the umbrella charts to use Mid CBF helm chart version 0.15.0
- Adapt FSP and VCC capability devices to agree with the new transitions of 
  VCCs and FSPs states reported by Mid CBF after Assign or Configure commands.
- Added new integration tests for Assign/Release commands to check different 
  dishes configurations.
- Modified the releaseresources method in the Mid Cbf subarray component to use
  the same JSON schema as the one used for AssignResources command.
  
0.16.3
------
- Command timeout default values set as Device Property of a device.
- Command timeout is configurable via TANGO attribute commandTimeout.
- Update assignedResources attribute with the list of the FQDNs of the assigned resources
- Added support to report subarray versionId and buildState info
- Update libraries:
  - ska-csp-lmc-common 0.17.6
  - pytango 9.4.2
  - ska-tango-base 0.19.1
- Update base containers:
  - ska-tango-images-pytango-builder:9.4.3
  - ska-tango-images-pytango-runtime:9.4.3
- Update charts:
  - ska-tango-util 0.4.7
  - ska-tango-base 0.4.7
  - ska-tango-images-tango-dsconfig 1.5.12
  - tango-itango 9.4.3
  - ska-tango-taranta 2.4.0
  - ska-tango-taranta-auth 0.1.7
  - ska-tango-tangogql 1.3.10
- Removed constraints to python package from '3.10.6' to '~3.10.6' 
- Update documentation
- Update .make subsystem to master
- Update sphinx packages, add them to pyproject.toml and use .readthedocs.yaml to build documentation

0.16.2
------
- Update to ska-csp-lmc-common v0.17.2 (initialization)
- Add option to deploy CBF subsystem only

0.16.1
------
- Update to ska-csp-lmc-common v0.17.0 (Scan command)
- Examples documentation includes long running command attributes use

0.16.0
------
- Update to ska-csp-lmc-common v0.16.0 (CspJsonvalidator)
- Update BC to v0.18.1

0.15.0
------
- Update ska-csp-lmc-common version to 0.15.0
- Update to BC v0.17.0
- Implement Abort command behavior for Subarrays
- Configure attributes that report subsystem states and modes to push archive and change events
- Add CSP.LMC devices and subsystems software version ID attributes

0.14.0
------
- Add Vcc capability device
- Update receptor id according to ADR32
- Update of poetry to version 1.3.2
- Update of pytango to version 9.3.6
- Update default global.minikube flag to false
- Update ska-csp-lmc-common to version 0.14.1

0.13.1
------
- Patch in scan command to support cbf-sdp emulator
- Use ska-csp-lmc-common v 0.12.2

0.13.0
------
- Intruduction of Capability Fsp Device
- Use ska-csp-lmc-common v 0.12.1 

0.12.0
------
- Use ska-csp-lmc-common v 0.12.0 (Changes in command execution)

0.11.8
------
- Use ska-csp-lmc-common v 0.11.14 (forwarding of AdminMode and events in Mocked connector for testing)

0.11.7
------
- Update simulators to enable them to simulate the timeout and exceptions on the resourcing commands.
- Use ska-csp-lmc-common package version = 0.11.13 with changes in code for simulators tests.

0.11.6
------
- Update simulators to enable them to simulate the timeout on the commands.
- Use ska-csp-lmc-common package version = 0.11.11 with changes in code for simulators tests.

0.11.5
------
- Changed is_allowed() method in Off command to allow the execution of this command from
  any State.
- Updated helm charts for Mid CSP and simulated devices to 0.11.5.
- Use ska-csp-lmc-common package version = 0.11.10 with changes in code for unit tests.

0.11.3
------
- Use ska-csp-lmc-common package version = 0.11.5 with changes in code for unit tests.
- Updated helm charts for Mid CSP and simulated devices to 0.11.3

0.11.2
------
- Use ska-csp-lmc-common package version = 0.11.3
- Update to the new CI/CD machinery. 
- Use of Poetry to manage packages and dependencies. 
- Large lint and reformat of the code. 

0.11.1
------
- Use ska-csp-lmc-common package version = 0.11.3

0.11.0
------
- Transition to SKA BC 0.11.3
- Use ska-csp-lmc-common package version = 0.11.2
- Implemented Mid CSP ComponentManager for Controller and Subarray devices
- Use Mid CBF helm chart 0.2.8
- Updated ska-mid-csp-lmc helm chart to use image ver 0.11.0
 
0.10.3
------
- use ska-csp-lmc-common 0.10.4 (fix bug CT-565)

**Release helm chart 0.1.12**

0.10.2
------
- update simulated subarrays: added functionalities to manage Configure and Scan

**Release helm chart 0.1.11**

0.10.1
------
- included simulated device for subsystems using TangoSimLib in the Python package

NOTE: chart was not released because of no changes in mid-csp-lmc device behaviour.

0.10.0
------
- use ska-csp-lmc-common 0.10.0
- refactoring for Controller and Subarray
- integration tests as bdd for Controller and Subarray

**Release helm chart 0.1.10**

0.8.2
-----
- Use ska-csp-lmc-common 0.8.2
- align JSON keyword re-naming as per ADR-35
- Update packages versions in requirements.txt.
- Updated Assign/Release resources to handle the new json strings.
  Updated tests and configuration files used for tests, as well.
- Moved mid-csp helm chart name and directory to ska-csp-lmc-mid to
  adhere to the ADR25. Modified --set options to point to the right chart
  variables to override with the tag and registry image of gitlab repo.

**Release helm chart 0.1.8**

- 2021/07/15: released helm chart 0.1.9: updated ska-tango-util conditions and chart template file

0.9.0
-----
- use csp-lmc-common 0.9.0

*use version 0.8.1 for integrated system*

0.8.1
-----
- set import in __init__.py
- use tango images from CAR (Central Artefact Repository)
- update tango-base and tango-util
- use csp-lmc-common 0.8.1

NOTE: all artefacts are still uploaded in the old repo engage-ska (waiting for System Team migration)

**Release helm chart 0.1.7**

0.8.0
-----
- created new repository on Gitlab (ska-csp-lmc-common) to separate the project from mid-csp and low-csp
- updated pipeline for the new repository
- updated documentation for the new repository
- use csp-lmc-common 0.8.0

**Release helm chart 0.1.6**

0.7.5
-----
- use csp-lmc-common 0.7.5
- updated Dockerfile to use ska-python-buildenv/runtime images 9.3.3.1
- updated mid-csp chart data configuration: removed polling on State attribute.
- skipped integration tests for reinitialization (issue with the use of the DevRestart
  administrative command and events)

**Release helm chart 0.1.5**

0.7.4
-----
- use csp-lmc-common 0.7.4
- update integration test for transaction id and off command when empty

0.7.3
-----
- use csp-lmc-common 0.7.3 

**Release helm chart 0.1.4**

0.7.2
-----
- use csp-lmc-common 0.7.2
- updated integration tests for init command
- removed tango forwarded-attributes

0.7.1
-----
- use csp-lmc-common 0.7.1
- updated integration tests

0.7.0
-----
- use csp-lmc-common 0.7.0
- align Mid CSP.LMC to the SKA Base Classes API
  implementing Assign/ReleaseResources commands.
- added transaction ID for ReleaseResources command.
- updated tests to handle the new commands.
- updated helm chart to version 0.1.2 (mid-csp-lmc:0.7.0 docker image)
- updated mid-csp-umbrella helm chart to rely on mid-csp version 0.1.2 

0.6.10
------
- use csp-lmc-common 0.6.12
- support to ADR-18/22
- use ska-telescope-model 0.2.0

0.6.9
-----
- use csp-lmc-common 0.6.9
- fix bug in receptor class

0.6.8
-----
- use csp-lmc-common version 0.6.8.
- removed import of CspSubarrayStateModel

0.6.7
-----
- use csp-lmc-common version 0.6.7

0.6.6
-----
- use csp-lmc-common version 0.6.6
- remove polling on VCC state attribute in MID.CBF DB configuration
  otherwise the VCC State read during configuration sometimes does not
  reflect the real VCC state.
- Tests for restart

0.6.5
-----
- use csp-lmc-common version 0.6.5
- added test for Abort.

0.6.4
-----
- use csp-lmc-common version 0.6.4
- Receptor class uses the assignedReceptors attribute (exported by the MidCspSubarray) 
  to get the number of assigned receptors. No more use of receptorMembership: this value sometimes
  has some delays due to the use of events to build it

0.6.3
-----
- use ska-python-buildenv and runtime 9.3.2
- use csp-lmc-common version 0.6.3
- fix a bug in receptors regarding the receptorId
- devlepoed unit tests with mock devices 

0.6.2
-----
- use csp-lmc-common version 0.6.2

0.6.1
-----
- use csp-lmc-common version 0.6.1

0.6.0
-----
- start implementing lmcbaseclasses 0.6.0

0.5.9
-----
- use csp-lmc-common version 0.5.11
- removed the cbfOutputLink forwarded attribute from the TANGO DB configuration file

0.5.8
-----
- use csp-lmc-common package 0.5.9
- implemented the ADR-4 and ADR-10 decisions:

  * the json file contains the unique configuration ID
    and the list of the outputlinks, channelAverageMap and
    received addresses for CBF
  * the Scan command specifies as argument the scan Id

0.5.7
-----
- use csp-lmc-common package 0.5.8
- updated decorator arguments for Configure method.
- decreased polling period (from 1 to 0.1 sec) during
  command execution monitoring. 

0.5.6
-----
- update to csp-lmc-common 0.5.7
- removed the problem with forwarded attributes of array of strings.
  When the root attribute of a forwarded attribute is an array of 
  strings the devices crashes.

0.5.5
-----
- update to csp-lmc-common 0.5.6

0.5.4
-----
- update to csp-lmc-common 0.5.5

0.5.3
-----
- Use lmcbaseclasses = 0.5.0
- Moved ConfigureScan command to Configure.
- Moved EndSB() method to GoToIdle().
- Install in editable mode the csp-lmc-common package to install also
  lmcbaseclasses package and its dependencies.
- Modified .gitlab-ci.yml file to execute linting and generate metrics.
- Still not resolved the issue to combine coverage outputs of 
  the different repository projects (csp-lmc-common and csp-lmc-mid).
- Moved logger.warn (deprecated) to logger.warning

0.4.0
-----
- Use lmcbaseclasses = 0.4.1
- ska-python-buildenv 9.3.1
- ska-python-runtime 9.3.1
- Use csp-lmc-common = 0.4.0

0.3.0
-----
- Use lmcbaseclasses version >=0.2.0
- Use csp-lmc-common >= 0.3.0
