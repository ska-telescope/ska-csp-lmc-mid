The Mid.CSP Sub-array TANGO Device class is required as a common interface feature, as described in the
document “SKA1 Control System Guidelines”. 

Its purpose is to provide a point of access for configuration, execution and monitoring of signal processing functionality 
for each sub‐array independently.

Overview of the functionality
--------------------------------

1. Add/release resources (receptors, beams) to/from the sub‐array.
   TM accesses directly the Mid.CSP Sub-array in order to add/release resources to/from sub‐arrays.
   The TM requests are validated by the Mid.CSP sub-array relying on the information maintained by the Mid.CSP Controller and
   Capability.
2. Set the frequency Band.
3. Configure the sub‐array Processing Modes using command *Configure()*. This includes configuration
   of the Capabilities used by the sub‐array.
4. Start/stop scan execution.
5. Set ‘engineering’ parameters for the sub‐arrays and Capabilities.

Receptors assignment
--------------------
The assignment of receptors to a MID sub-array is performed in advance of a scan configuration.

Up to 197 receptors can be assigned to one sub-array.

Receptors assignment to a sub-array is exclusive: one receptor can be assigned only to one sub-array.

During assignment, the Mid.CSP sub-array performs both formal and more deep checks.

Formal control:
- The assigned receptor list is empty. In this case, a warning is logged.
- The receptor names are compliant to ADR-32. In this case, a warning is logged for each receptor not compliant and the receptor is excluded from the assigned list.
- The assigned receptor list contains duplicated receptors. In this case, the duplicated receptor is removed from the list and a warning is logged.

Deep control:
- The requested receptors are in the list of the deployed ones.
- the requested receptors are already associated to another sub-array. In this case a warning message is logged.

In order for a Mid.CSP sub-array to accept a scan configuration, at least one receptor must be assigned to the sub-array.

Tied-array beams
----------------
Depending on the selected processing mode, Search Beams, Timing Beams and Vlbi Beams must be assigned to a MID sub-array in advance of a scan.

TM may specify the number of beams to be used or, alternatively, identify the Capability instances to be used
via their TANGO Fully Qualified Domain Name (FQDN).


Scan configuration
------------------
TM provides a complete scan configuration to a sub-array via an ASCII JSON encoded string.

When a complete and coherent scan configuration is received and the sub-array configuration 
(or re-configuration) completed,  the sub-array it's ready to observe.

Once configured, Mid.CSP keeps the sub‐array and scan configuration until one of the following occurs:

- Mid.CSP receives a command to release resources (receptors, FSPs, Search Beams and/or
  Timing Beams) used by the sub‐array.
- Mid.CSP receives a command to transition to low‐power state (state=STANDBY); in which case the
  active scans end and resources are released.
- Mid.CSP receives a command to shut‐down (OFF)
- Mid.CSP monitor and control function fails so that the configuration is lost.


Inherent Capabilities
^^^^^^^^^^^^^^^^^^^^^
The following inherent Capabilities that correspond to Processing Modes, and are
configured via scan configuration:

1. Correlation
2. PSS
3. PST
4. VLBI (beamforming and correlation in support of VLBI)
5. Transient Data Capture.

When a sub‐array does not perform correlation, its Capability Correlation is OFF; the same applies for all Processing Modes.

Control and Monitoring
----------------------
Each Mid.CSP Sub-array maintains and report the status and state transitions for the 
Mid.CSP sub-array as a whole and for the individual assigned resources.

In addition to pre-configured status reporting, a Mid.CSP Sub-array makes provision for the TM and any authorized client, 
to obtain the value of any sub-array attribute.

Mid.CSP Sub-array TANGO Device name
------------------------------------
The Mid.CSP Sub-array TANGO Device name is defined in the document “SKA1 TANGO Naming Conventions”::

        mid-csp/subarray/XY

where XY is a two digit number in range [01,..,16].

Mid.CSP Sub-array operational state
------------------------------------
Mid.CSP Sub-array intelligently rolls‐up the operational state of all components used by the sub‐array and reports the overall 
operational state for the sub‐array.

The Mid.CSP Sub-array supports the following sub-set of the TANGO Device states:

* UNKNOWN:  Mid.CSP sub-array is unresponsive, e.g. due to communication loss.
* OFF: The sub-array is not enabled to perform signal processing. The sub‐array is ‘empty’.
* INIT: Initialization of the monitor and control network,equipment and functionality is being performed.
* DISABLE: Mid.CSP sub-array is administratively disabled, basic monitor and control functionality is available but signal
  processing functionality is not available.
* ON:  The sub-array is enabled to perform signal processing. The sub‐array observing state is EMPTY if receptors have not 
  been assigned to the sub‐array, yet.
* ALARM:  Quality Factor for at least one attribute crossed the ALARM threshold. Part of functionality may be unavailable.
* FAULT: Unrecoverable fault has been detected. The sub‐array is not available for use and maintainer/operator intervention 
  might be required.

Mid.CSP Sub-array observing state
---------------------------------
The sub‐array Observing State indicates status related to scan configuration and
execution.

The Mid.CSP Sub-array observing state adheres to the State Machine defined by ADR-8.

Mid.CSP Sub-array TANGO Device Commands
-----------------------------------------
Mid.CSP Sub-array implements the standard set of commands as specified in:

* Standard set of TANGO Device commands as defined in TANGO User Manual
* Standard set of SKA TANGO Device commands
* Command specific to Mid.CSP Sub-array as described in API section.

Mid.CSP makes provision for TM to request state transitions for individual sub‐systems and/or Capabilities.

Mid.CSP Sub-array TANGO Device Attributes
------------------------------------------
Mid.CSP Sub-array implements the standard set of attributes as specified in:

* Standard set of TANGO Device attributes as defined in TANGO User Manual
* The standard set of SKA TANGO Device attributes as defined for the SKA Sub-array TANGO Device.
* Attributes to Mid.CSP Sub-array as described in API section.

Virtually all parameters provided in scan configuration are exposed as attributes either by
Mid.CSP Sub-array or by individual Capabilities.
