The Mid CSP.LMC Capabilities provide a layer of abstraction to allow the TM to set, control and monitor
signal processing, and other sub-system functions, without being aware of the details of the Mid CSP
implementation.

Each Mid CSP.LMC Capability represents a subset of the Mid.CSP functionality and uses a subset of the Mid.CSP hardware and
software resources. 


- VCCs (Very Coarse Channelizer): 197
- FSPs (Frequency Slice Processor): 27
- PSS (Pulsar Search Beams): 1500. 
- PST (Pulsar Timing Beams): 16



The Capabilities implemented by the Mid.CSP can be classified in two groups, as follows:

* The Capabilities inherent to the sub-arrays or other Capabilities; such Capabilities are created during
  initialization and are permanently assigned to a particular sub-array or Capability.  
  A Capability can be enabled/disabled, but cannot be removed and/or assigned to another subarray.
* The Capabilities that are function of resources: these are created during initialization and assigned to a pool of unused resources. 
  Individual instances of these resources may be assigned to subarrays, as commanded by the TM. In some cases the same instance may
  be used in a shared manner by more than one subarray.

Each Mid.CSP Subarray has the following inherent Capabilities that correspond to Processing Modes, and are
configured via scan configuration:

1. Correlation,
2. PSS,
3. PST,
4. VLBI (beamforming and correlation in support of VLBI), and
5. Transient Data Capture.

The Capabilities listed above are configured and controlled via a subarray; when a subarray does not perform
correlation, its Capability Correlation is OFF; the same applies for all Processing Modes.

The top-level Mid.CSP Capabilities associated to schedulable resources are reported in the table below:


+----------------------+------------------------------+-----------------------------+
| Capability Name      | Resources                    | Notes                       |
+----------------------+------------------------------+-----------------------------+
| Frequency Observing  | 197 VCCs                     | Support frequency bands:    |
| Band                 |                              |                             | 
|                      | (Very Coarse                 | - Band 1                    |
|                      | Channeliser)                 | - Band 2                    |
|                      |                              | - Band 3                    |
|                      |                              | - Band 4                    |
|                      |                              | - Band 5a                   |
|                      |                              | - Band 5b                   |
|                      |                              |                             |
+----------------------+------------------------------+-----------------------------+
| FSP Processing Modes | 27 FSPs                      | Supported processing modes: |
|                      |                              |                             |
|                      | (Frequency Slice Processor)  | - correlation               |
|                      | These resources are shareable| - pulsar search beamforming |
|                      | among the subarrays          | - pulsar timing beamforming |
+----------------------+------------------------------+-----------------------------+
| PSS Processing Mode  | 1500 Search Beams            | Collects the HW, SW and     |
|                      |                              | processing modes of PSS:    |
|                      |                              | beams:                      | 
|                      |                              |                             |
|                      |                              | This capability is a func   |
|                      |                              | of the following resources: |
|                      |                              |                             |
|                      |                              | - receptors                 |  
|                      |                              | - CBF beamformers           |  
|                      |                              | - PSS pipelines             |  
|                      |                              | - SDP links                 |
+----------------------+------------------------------+-----------------------------+
| PST Processing Mode  | 16 Timing Beams              | Collects the HW, SW and     |
|                      |                              | processing modes of PST:    |
|                      |                              | beams:                      | 
|                      |                              |                             |
|                      |                              | This capability is a func   |
|                      |                              | of the following resources: |
|                      |                              |                             |
|                      |                              | - receptors                 |  
|                      |                              | - CBF beamformers           |  
|                      |                              | - PST processors            |  
|                      |                              | - SDP links                 |
+----------------------+------------------------------+-----------------------------+

The previous list does not include VLBI. 
VLBI beamforming is configured per subarray per FSP, any further beam processing is performed by non-SKA1
equipment; there are no VLBI related Mid.CSP resources that have to be managed, other than the FSPs.

