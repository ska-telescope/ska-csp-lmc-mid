###################
Software Components
###################

The top-level software components provided by Mid.CSP LMC API are:

* :ref:`Mid.CSP LMC Controller <Mid.CSP-Controller>`
* :ref:`Mid.CSP LMC Subarray <Mid.CSP-Subarray>`
* :ref:`Mid.CSP Alarm Handler <Mid.CSP-AlarmHandler>`
* Mid.CSP Logger
* Mid.CSP TANGO Facility Database
* VCC Capability (receptors)
* :ref:`FSP Processing Modes Capability <Mid.CSP LMC Capabilities>`
* :ref:`Search Beam Capability <CSP-Capability>`
* :ref:`Timing Beam Capability <CSP-Capability>`
* :ref:`VLBI Beam Capability <CSP-Capability>`

Components listed above are implemented as TANGO devices, i.e. classes that implement standard TANGO API.
The CSP.LMC TANGO devices are based on the standard SKA1 TANGO Element Devices provided via the *SKA Base Classes*.

.. _Mid.CSP-Controller:

**********************
Mid.CSP LMC Controller
**********************

.. include:: controller/controller_device.rst.inc

.. _CSP-Capability:

************************
Mid.CSP LMC Capabilities
************************

.. include:: capability/capability.rst


.. _Mid.CSP-Subarray:

********************
Mid.CSP LMC Subarray
********************

.. include:: subarray/subarray_device.rst.inc


.. _Mid.CSP-AlarmHandler:

************************
Mid.CSP LMC AlarmHandler
************************

.. include:: alarm/alarm_handler.rst.inc
