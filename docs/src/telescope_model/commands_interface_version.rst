###################################
Telescope model interfaces version
###################################

The Telescope model version used is the *1.19.2*

The table below contains the supported commands CSP interface version.
The interface version received within a command json file argument is propagated to CSP LMC subsystems.

**********************************
Assign resources command interface
**********************************

.. list-table::
   :widths: 20 60
   :header-rows: 1

   * - CSP
     - Note
   * - 3.0
     - 
   * - 2.3
     - 
   * - 2.2
     - 

*****************************
Configure command interface
*****************************

.. list-table::
   :widths: 20 60
   :header-rows: 1

   * - CSP
     - Note
   * - 4.1
     - with ska-mid-cbf-mcs v1.0.0
   * - 4.0
     - with ska-mid-cbf-mcs v1.0.0
   * - 3.0
     - with ska-mid-cbf-mcs v1.0.0
   * - 2.6
     - with ska-mid-cbf-mcs less than v1.0.0
   * - 2.5
     - 
   * - 2.4
     - 
   * - 2.3
     - 
   * - 2.2
     - 
   * - 2.1
     - 
   * - 2.0
     - 
   * - 1.0
     - deprecated

************************
Scan command interface
************************

.. list-table::
   :widths: 20 60
   :header-rows: 1

   * - CSP
     - Note
   * - 2.3
     - 
   * - 2.2
     - 

**************************
Endscan command interface
**************************

.. list-table::
   :widths: 20 60
   :header-rows: 1

   * - CSP
     - Note
   * - 2.3
     - 
   * - 2.2
     - 

**************************************
Release resources command interface
**************************************

.. list-table::
   :widths: 20 60
   :header-rows: 1

   * - CSP
     - Note
   * - 3.0
     - 
   * - 2.6
     - 
   * - 2.2
     - 
