=============
Project's API
=============

Below are presented the API for Classes specialized for Mid Telescope.

All the functionalities common to Mid and Low Telescope are developed in the project 
`ska-csp-lmc-common <https://developer.skatelescope.org/projects/ska-csp-lmc-common/en/latest/api/index.html>`_

**************************
Mid.CSP LMC Devices API
**************************

.. toctree::
       
   MidCspController<device/controller/controller_api>
   MidCspSubarray<device/subarray/subarray_api>
   MidCspFspProcessingModeCapability<device/fsp_capability_api>


***********************
Mid.CSP LMC modules API
***********************
.. toctree::
   :maxdepth: 2

   Manager<manager/index>
   Mid.CSP Sub-systems <component/index>
