####################
Controller Commands
####################

The following table contains information about the Mid CSP LMC commands of the controller Tango device.
A more extensive documentation about command parameters can be found in the `Telescope Model - Mid.CSP schemas <https://developer.skao.int/projects/ska-telmodel/en/latest/schemas/csp/ska-mid-csp.html>`_

.. list-table::
   :widths: 15 25 15 10 5 10
   :header-rows: 1

   * - Command name
     - Description
     - Parameters
     - Example
     - Sequence diagram
     - Note
   * - *On*
     - The command has an argument list as input that contains the devices FQDN that have to be transition to ON.
     - List that contains the devices FQDN. when the list is empty, the commmand is propagated to all the subsystem devices
     - :ref:`On example <Power-on (off) the Mid.CSP>`
     - `On diagram <https://confluence.skatelescope.org/display/SE/On>`_
     - 
   * - *Off*
     - The Off command disables any signal processing capability of a device,
     - List that contains the devices FQDN. when the list is empty, the commmand is propagated to all the subsystem devices
     - :ref:`Off example <Power-on (off) the Mid.CSP>`
     - `Off diagram <https://confluence.skatelescope.org/display/SE/Off>`_
     - 
   * - *Standby*
     - Transition the CSP to a low power consumption mode. The CSP LMC controller is responsible to propagate the transition to the subsystems
     - List that contains the devices FQDN. when the list is empty, the commmand is propagated to all the subsystem devices
     - :ref:`Standby example <Power-on (off) the Mid.CSP>`
     - `Standby diagram <https://confluence.skatelescope.org/display/SE/Standby>`_
     - The Standby command is rejected since the CSP subsystems (Mid CBF, PST) do not support the command
   * - *loadDishCfg*
     - This command inizializes the Mid.CBF system parameters
     - :ref:`loadDishCfg input <Json Command Input>`
     - :ref:`loadDishCfg example <Configure DishID-VCC map>`
     - `loadDishCfg diagram <https://confluence.skatelescope.org/display/SE/LoadDishCfg>`_
     - If the command fails or is is not issued, the CBF cannot transition to On state
   * - *Reset*
     - Recover from a FAULT State. The controller will be move in STANDBY obsState
     - No input
     - :ref:`Reset example <csp-lmc-reset>`
     - `Reset diagram <https://confluence.skatelescope.org/display/SE/Reset>`_
     - The command is propagated to subarrays
