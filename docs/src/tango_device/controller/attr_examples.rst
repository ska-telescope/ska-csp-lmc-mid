:orphan:

##########################################
Mid CSP.LMC Controller Attributes Examples
##########################################

* buildState

.. _mid-controller-buildstate:

::

    'ska-csp-lmc-mid, 0.17.2, SKA MID CSP.LMC'

* longrunningcommandresult

.. _mid-controller-longrunningcommandresult:

::

    ('1708442895.8833039_185769114989020_On', '[0, "on completed 1/1"]')

* longrunningcommandstatus

.. _mid-controller-longrunningcommandstatus:

::

    ('1708442895.8833039_185769114989020_On', 'COMPLETED')

* maxcapabilities

.. _mid-controller-maxcapabilities:

::

    ('SearchBeams:1500', 'TimingBeams:16', 'VCC:4', 'VlbiBeams:20')

* jsoncomponentyersions

.. _mid-controller-jsoncomponentyersions:

::

    '{"mid_csp_cbf/sub_elt/controller": "0.11.4", "mid-csp/subarray/01”: "0.18.2", "mid-csp/subarray/02": "0.18.2", "mid-csp/subarray/03": "0.18.2"}'

* availablecapabilities

.. _mid-controller-availablecapabilities:

::

    ('SearchBeams:1500', 'TimingBeams:16', 'VCC:4', 'VlbiBeams:20')
