#################################
Mid CSP.LMC Controller Attributes
#################################

The following table contains information about the Mid CSP LMC attributes of the controller Tango device. 

.. list-table::
   :widths: 15 25 15 5 5 5 35
   :header-rows: 1

   * - Name
     - Description
     - Type
     - R/W
     - Change event
     - Archived
     - Example
   * - State
     - State
     - DevState
     - R
     - yes
     - yes
     - tango._tango.DevState.ON
   * - Status
     - Status
     - DevString
     - R
     - yes
     - yes
     - 'The device is in ON state.'
   * - adminMode
     - The admin mode reported for this device. It may interpret the current device condition and condition of all managed devices to set this. Most possibly an aggregate attribute.
     - Enum (AdminMode)
     - R/W
     - yes
     - yes
     - adminMode.ONLINE: 0
   * - healthState
     - Read the Health State of the device. It interprets the current device condition and condition of all managed devices to set this. Most possibly an aggregate attribute.
     - Enum (HealthState)
     - R
     - yes
     - yes
     - healthState.OK: 0
   * - versionId
     - Read the Version Id of the device.
     - String
     - R
     - no
     - no
     - '0.17.2'
   * - buildState
     - Build state of the device.
     - String
     - R
     - no
     - no
     - :ref:`example <mid-controller-buildstate>`
   * - controlMode
     - Read the Control Mode of the device. The control mode of the device are REMOTE, LOCAL Tango Device accepts only from a ‘local’ client and ignores commands and queries received from TM or any other ‘remote’ clients. The Local clients has to release LOCAL control before REMOTE clients can take control again.
     - Enum (ControlMode)
     - R/W
     - yes
     - yes
     - controlMode.REMOTE: 0
   * - loggingLevel
     - Read the logging level of the device. Initialises to LoggingLevelDefault on startup.See :py:class:`~ska_control_model.LoggingLevel`.
     - Enum (LoggingLevel)
     - R/W
     - 
     -
     - loggingLevel.INFO: 4
   * - simulationMode
     - Read the Simulation Mode of the device. Some devices may implement both modes, while others will have simulators that set simulationMode to True while the real devices always set simulationMode to False.
     - Enum (ControlMode)
     - R/W
     - yes
     - yes
     - simulationMode.TRUE: 1
   * - testMode
     - Read the Test Mode of the device. Either no test mode or an indication of the test mode.
     - Enum (TestMode)
     - R/W
     - yes
     - yes
     - testMode.NONE: 0
   * - loggingTargets
     - Read the additional logging targets of the device. Note that this excludes the handlers provided by the ska_ser_logging library defaults - initialises to LoggingTargetsDefault on startup. 
     - String
     - R/W
     -
     -
     - ('tango::logger',)
   * - longRunningCommandIDsInQueue
     - Read the IDs of the long running commands in the queue. Every client that executes a command will receive a command ID as response. Keep track of IDs in the queue. Pop off from front as they complete.
     - list of Strings
     - R
     - yes
     - yes
     - 
   * - longRunningCommandProgress
     - Read the progress of the currently executing long running command. ID, progress of the currently executing command. Clients can subscribe to on_change event and wait for the ID they are interested in.
     - list of Strings
     - R
     - yes
     - yes
     - 
   * - longRunningCommandResult
     - Read the result of the completed long running command. Reports unique_id, json-encoded result. Clients can subscribe to on_change event and wait for the ID they are interested in.
     - Tuple[str, str]
     - R
     - yes
     - yes
     - :ref:`example <mid-controller-longrunningcommandresult>`
   * - longRunningCommandStatus
     - Read the status of the currently executing long runnin ID, status pair of the currently executing command. Clients can subscribe to on_change event and wait for the ID they are interested in.
     - list of Strings
     - R
     - yes
     - yes
     - :ref:`example <mid-controller-longrunningcommandstatus>`
   * - longRunningCommandsInQueue
     - Read the long running commands in the queue. Keep track of which commands are in the queue. Pop off from front as they complete. 
     - list of Strings
     - R
     - yes
     - yes
     - 
   * - elementAlarmAddress
     - FQDN of Element Alarm Handlers
     - String
     - R
     - no
     - no
     - NOTE: not available
   * - elementDatabaseAddress
     - FQDN of Element Database device
     - String
     - R
     - no
     - no
     - NOTE: not available
   * - elementLoggerAddress
     - Read FQDN of Element Logger device.
     - String
     - R
     - no
     - no
     - NOTE: not available
   * - elementTelStateAddress
     - FQDN of Element TelState device
     - String
     - R
     - no
     - no
     - NOTE: not available
   * - maxCapabilities
     - Maximum number of instances of each capability type.
     - list of Strings
     - R
     - no
     - no
     - :ref:`example <mid-controller-maxcapabilities>`
   * - cspCbfState
     - The CBF sub-system Device State.
     - Enum (DevState)
     - R
     - yes
     - yes
     - tango._tango.DevState(0)
   * - cspPssState
     - The PSS sub-sstem Device State
     - Enum (DevState)
     - R
     - yes
     - yes
     - tango._tango.DevState(12)
   * - cspPstBeamsState
     - Report the state of the deployed PST Beams as a JSON formatted string
     - DevString of DevState
     - R
     - yes
     - yes
     - '{}'
   * - cspCbfAdminMode
     - The CBF subsystem adminMode
     - DevEnum (AdminMode)
     - R/W
     - yes 
     - yes
     - cspCbfAdminMode.ONLINE: 0
   * - cspPssAdminMode
     - The PSS sub-system adminMode
     - DevEnum (AdminMode)
     - R/W
     - yes 
     - yes
     - cspPssAdminMode.NOT-FITTED: 3
   * - cspPstBeamsAdminMode
     - Report the administration mode of the PST Beams as JSON formatted string
     - DevString of AdminMode
     - R/W
     - yes
     - yes
     - '{}'
   * - cspCbfHealthState
     - The CBF sub-system healthState.
     - DevEnum (HealthState)
     - R
     - yes
     - yes
     - cspCbfHealthState.OK: 0
   * - cspPssHealthState
     - The PSS sub-system healthState
     - DevEnum (HealthState)
     - R
     - yes
     - yes
     - cspPssHealthState.UNKNOWN: 3
   * - cspPstBeamsHealthState
     - Health state of the PST Beams as a JSON formatted string
     - DevEnum (HealthState)
     - R
     - yes
     - yes
     - '{}'
   * - cbfSimulationMode
     - Read the Simulation Mode of the cbf device. Some devices may implement both modes, while others will have simulators that set simulationMode to True while the real devices always set simulationMode to False.
     - DevBoolean
     - R/W
     - yes
     - yes
     - True
   * - cbfControllerAddress
     - TANGO FQDN of the CBF Controller device
     - DevString
     - R
     - no
     - no
     - 'mid_csp_cbf/sub_elt/controller'
   * - cbfVersion
     - SW version of the CBF Controller
     - String
     - R
     - no
     - no
     - '0.11.4'
   * - commandTimeout
     - Duration of command timeout in seconds. On set: Configure the timeout applied to each command. The specified value shall be \> 0. Negative values raise a *TypeError* exception. This is the expected behavior because the attribute has been defined as DevUShort. A zero value raises a *ValueError* exception.
     - DevUShort (0,100]
     - R/W
     - yes
     - yes
     - 60
   * - isCommunicating
     - Whether the CSP Controller TANGO Device is communicating with the System Under Control (SUT). The SUT is composed of: the Controller devices of the CSP sub-systems, all the deployed PST Beams of the PST sub-systems, the deployed CSPSubarray devices. This flag is reported as False when the CSP.LMC Controller is not able to communicate with the corresponding CBF device.
     - Bool
     - R
     - no
     - no
     - True
   * - jsonComponentVersions
     - Return a JSON formatted string with the SW versions of the CSP.LMC Controller subordinate devices. 
     - String
     - R
     - no
     - no
     - :ref:`example <mid-controller-jsoncomponentyersions>`
   * - numOfDevCompletedTask
     - Number of devices that completed the task.
     - DevUShort
     - R
     - no
     - no
     - NOTE: not available
   * - testAlarm
     - flag to test alarm
     - DevBoolean
     - R/W
     - no
     - no
     - True
   * - offCmdDurationExpected
     - Expected duration (sec) of the Off command. Set/Report the duration expected (in sec.) for the Off command execution.
     - DevUShort
     - R/W
     - no 
     - yes
     - NOTE: deprecated
   * - offCmdDurationMeasured
     - Measured duration (sec) of the Off command execution. Report the measured duration (in sec.) of the Off command execution 
     - DevUShort
     - R
     - no 
     - no
     - NOTE: not available
   * - offCmdFailure
     - Off execution failure flag. Alarm flag set when the Off command fails with error(s).
     - DevBoolean
     - R
     - no
     - no
     - NOTE: deprecated
   * - offCmdTimeoutExpired
     - PSS command timeout flag. Signal the occurence of a timeout during the execution off commands on PSS Sub-system.
     - DevBoolean
     - R
     - no
     - no
     - NOTE: deprecated
   * - offCommandProgress
     - Progress percentage for the Off command. Percentage progress implemented for commands that result in state/mode transitions for a large number of components and/or are executed in stages (e.g power up, power down)
     - DevUShort [0,100]
     - R
     - no
     - no
     - NOTE: deprecated
   * - offFailureMessage
     - Off execution failure message. Alarm message when the Off command fails with error(s).
     - DevString
     - R
     - no
     - no
     - NOTE: deprecated
   * - onCmdDurationExpected
     - Expected duration (sec) of the On command. Set/Report the duration expected (in sec.) for the Oncommand execution
     - DevUShort [0,100]  
     - R/W
     - no
     - yes
     - NOTE: deprecated
   * - onCmdDurationMeasured
     - Measured duration (sec) of the On command execution. Report the measured duration (in sec.) of the On command execution
     - DevUShort [0,100]
     - R/W
     - no
     - no
     - NOTE: not available
   * - onCmdFailure
     - CBF command failure flag. Alarm flag set when the On command fails with error(s).
     - DevBoolean
     - R
     - no
     - no
     - NOTE: deprecated
   * - onCmdTimeoutExpired
     - CBF command timeout flag. Signal the occurence of a timeout during the execution of commands on CBF sub-system.
     - DevBoolean
     - R
     - no
     - no
     - NOTE: deprecated
   * - onCommandProgress
     - Progress percentage for the On command. Percentage progress implemented for commands that result in state/modetransitions for a large number of components and/or are executed in stages (e.g power up, power down)
     - DevUShort [0,100] 
     - R
     - no
     - no
     - NOTE: deprecated
   * - onFailureMessage
     - On execution failure message. Alarm message when the On command fails with error(s). 
     - DevString
     - R
     - no
     - no
     - NOTE: deprecated
   * - pssControllerAddress
     - TANGO FQDN of the PSS sub-system Controller.
     - DevString
     - R
     - no
     - no
     - ''
   * - pssVersion
     - The SW version of the PSS Controller.
     - String
     - R
     - no
     - no
     - NOTE: not available
   * - standbyCmdDurationExpected
     - Expected duration (sec) of the Standby command. Set/Report the duration expected (in sec.) for the Standby command execution. 
     - DevUShort [0,100] 
     - R/W
     - no
     - yes
     - NOTE: deprecated
   * - standbyCmdDurationMeasured
     - Measured duration (sec) of the Standby command execution. Report the measured duration (in sec.) of the Standby command execution.
     - DevUShort [0,100] 
     - R
     - no
     - no
     - NOTE: not available
   * - standbyFailureMessage
     - Standby execution failure message. Alarm message when the Standby command fails with error(s).
     - DevString
     - R
     - no
     - no
     - NOTE: deprecated
   * - standbyCmdTimeoutExpired
     - PST command timeout flag. Signal the occurence of a timeout during the execution of commands on PST sub-system.
     - DevBoolean
     - R
     - no
     - no
     - NOTE: deprecated
   * - standbyCommandProgress
     - Progress percentage for the Standby command. Percentage progress implemented for commands that result in state/mode transitions for a large number of components and/or are executed in stages (e.g power up, power down).
     - DevUShort [0,100]
     - R
     - no
     - no
     - NOTE: deprecated
   * - standbyCmdFailure
     - Standby execution failure message. Alarm flag set when the Standby command fails with error(s).
     - DevBoolean
     - R
     - no
     - no
     - NOTE: deprecated
   * - commandResult
     - A tuple with the name and the result code of latest command execution: (commandName, resultCode)
     - String
     - R  
     - yes
     - yes
     - ('on', '0')
   * - pstBeamsAddresses
     - TANGO FQDNs of the PST Beams
     - list of Strings, max_dim=16
     - R
     - no
     - no
     - ('',)
   * - pstVersion
     - Return the SW of the PST Beams
     - list of Strings, max_dim=16
     - R
     - no
     - no
     - ()
   * - searchBeamsAddresses
     - The SearchBeam Capabilities FQDNs
     - list of DevStrings, max_dim=1500
     - R  
     - no
     - no
     - NOTE: not defined
   * - timingBeamsAddresses
     - The list of Timing Beam Capabilities FQDNs
     - list of DevStrings, max_dim=16
     - R  
     - no
     - no
     - NOTE: not available
   * - vlbiBeamsAddresses
     - The list of VlbiBeam Capabilities FQDNs
     - list of DevStrings, max_dim=20
     - R  
     - no
     - no
     - NOTE: not available
   * - commandResultName
     - The first entry of commandResult attribute, i.e. the name of latest command executed
     - DevString
     - R
     - no
     - no
     - 'on'
   * - commandResultCode
     - The second entry of commandResult attribute, i.e. the resultCode of latest command executed
     - DevString
     - R
     - no
     - no
     - '0'
   * - availableCapabilities
     - A list of available number of instances of each capability type
     - list of DevStrings, max_dim=20
     - R  
     - no
     - no
     - :ref:`example <mid-controller-availablecapabilities>`
   * - cspFspCapabilityAddress
     - The FQDN of the CSP.LMC FSP Capability Device
     - DevString
     - R
     - no
     - no
     - 'mid-csp/capability-fsp/0'
   * - cspVccCapabilityAddress
     - The FQDN of the CSP.LMC VCC Capability Device.
     - DevString
     - R
     - no
     - no
     - 'mid-csp/capability-vcc/0'
   * - receptorMembership
     - The receptors affiliation to MID CSP sub-arrays.
     - list of DevStrings, max_dim=197
     - R  
     - no
     - no
     - NOTE: not available
   * - receptorsList
     - The list of all receptors
     - list of DevStrings, max_dim=197
     - R  
     - no
     - no
     - NOTE: not available
   * - unassignedReceptorIDs
     - The list of available receptors IDs.
     - list of DevStrings, max_dim=197
     - R  
     - no
     - no
     - NOTE: not available
   * - vccAddresses
     - The list of VCC Capabilities FQDNs
     - list of DevString (max_dim=197)
     - R
     - no
     - no
     - NOTE: not available
   * - fspAddresses
     - The list of FSP Capabilities FQDNs
     - list of DevString (max_dim=27)
     - R
     - no
     - no
     - NOTE: not available
   * - dishVccConfig
     - The Dish ID - VCC ID mapping in a json string
     - DevString
     - R
     - yes
     - yes
     - :ref:`example <loadDishCfg-json-map>`
   * - sourceDishVccConfig
     - The CAR source of DishID-VCC ID mapping in a json string
     - DevString
     - R
     - yes
     - yes
     - :ref:`example <loadDishCfg-car-uri>`
   * - alarmDishIdCfg
     - Alarm raised when the DishVccConfig fails
     - DevBoolean
     - R/W
     - no
     - no
     - True