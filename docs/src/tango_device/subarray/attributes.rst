###############################
Mid CSP.LMC Subarray Attributes
###############################

The following table contains information about the Mid CSP LMC attributes of the subarray Tango device.

.. list-table::
   :widths: 15 25 15 2 2 2 30
   :header-rows: 1

   * - Name
     - Description
     - Type
     - R/W 
     - Change event
     - Archived
     - Example
   * - State
     - State
     - DevState
     - R
     - yes
     - yes
     - tango._tango.DevState.ON
   * - Status
     - Status
     - DevString
     - R
     - yes
     - yes
     - 'The device is in DISABLE state.'
   * - adminMode
     - The admin mode reported for this device. It may interpret the current device condition and condition of all managed devices to set this. Most possibly an aggregate attribute.
     - Enum
       (AdminMode)
     - R/W
     - yes
     - yes
     - adminMode.ONLINE: 0
   * - healthState
     - Read the Health State of the device. It interprets the current device condition and condition of all managed devices to set this. Most possibly an aggregate attribute.
     - Enum (HealthState)
     - R
     - yes
     - yes
     - healthState.OK: 0
   * - versionId
     - Read the Version Id of the device.
     - String
     - R
     - no
     - no
     - '0.17.2'
   * - buildState
     - Build state of the device.
     - String
     - R
     - no
     - no
     - :ref:`example <mid-subarray-buildstate>`
   * - controlMode
     - Read the Control Mode of the device. The control mode of the device are REMOTE, LOCAL Tango Device accepts only from a ‘local’ client and ignores commands and queries received from TM or any other ‘remote’ clients. The Local clients has to release LOCAL control before REMOTE clients can take control again.
     - Enum (ControlMode)
     - R/W
     - yes
     - yes
     - controlMode.REMOTE: 0
   * - loggingLevel
     - Read the logging level of the device. Initialises to LoggingLevelDefault on startup.See :py:class:`~ska_control_model.LoggingLevel`.
     - Enum (LoggingLevel)
     - R/W
     - yes
     - yes
     - loggingLevel.INFO: 4
   * - simulationMode
     - Read the Simulation Mode of the device. Some devices may implement both modes, while others will have simulators that set simulationMode to True while the real devices always set simulationMode to False.
     - Enum (ControlMode)
     - R/W
     - yes
     - yes
     - simulationMode.FALSE: 0
   * - testMode
     - Read the Test Mode of the device. Either no test mode or an indication of the test mode.
     - Enum (TestMode)
     - R/W
     - yes
     - yes
     - testMode.NONE: 0
   * - loggingTargets
     - Read the additional logging targets of the device. Note that this excludes the handlers provided by the ska_ser_logging library defaults - initialises to LoggingTargetsDefault on startup. 
     - String
     - R/W
     -
     -
     - ('tango::logger',)
   * - longRunningCommandIDsInQueue
     - Read the IDs of the long running commands in the queue. Every client that executes a command will receive a command ID as response. Keep track of IDs in the queue. Pop off from front as they complete.
     - list of Strings
     - R
     - yes
     - yes
     - 
   * - longRunningCommandProgress
     - Read the progress of the currently executing long running command. ID, progress of the currently executing command. Clients can subscribe to on_change event and wait for the ID they are interested in.
     - list of Strings
     - R
     - yes
     - yes
     - 
   * - longRunningCommandResult
     - Read the result of the completed long running command. Reports unique_id, json-encoded result. Clients can subscribe to on_change event and wait for the ID they are interested in.
     - Tuple[str, str]
     - R
     - yes
     - yes
     - :ref:`example <mid-subarray-longRunningCommandResult>`
   * - longRunningCommandStatus
     - Read the status of the currently executing long runnin ID, status pair of the currently executing command. Clients can subscribe to on_change event and wait for the ID they are interested in.
     - list of Strings
     - R
     - yes
     - yes
     - :ref:`example <mid-subarray-longrunningcommandstatus>`
   * - longRunningCommandsInQueue
     - Read the long running commands in the queue. Keep track of which commands are in the queue. Pop off from front as they complete. 
     - list of Strings
     - R
     - yes
     - yes
     - 
   * - commandTimeout
     - Duration of command timeout in seconds. On set: Configure the timeout applied to each command. The specified value shall be \> 0. Negative values raise a *TypeError* exception. This is the expected behavior because the attribute has been defined as DevUShort. A zero value raises a *ValueError* exception.
     - DevUShort (0,100]  
     - R/W
     - yes
     - yes
     - 60
   * - isCommunicating
     - Whether the CSP Controller TANGO Device is communicating with the System Under Control (SUT). The SUT is composed of: the Controller devices of the CSP sub-systems, all the deployed PST Beams of the PST sub-systems, the deployed CSPSubarray devices. This flag is reported as False when the CSP.LMC Controller is not able to communicate with the corresponding CBF device.
     - Bool
     - R
     - no
     - no
     - True
   * - numOfDevCompletedTask
     - Number of devices that completed the task.
     - DevUShort
     - R
     - no
     - no
     - 0
   * - commandResult
     - A tuple with the name and the result code of latest command execution: (commandName, resultCode)
     - String
     - R  
     - yes
     - yes
     - ('assignresources', '0')
   * - commandResultName
     - The first entry of commandResult attribute, i.e. the name of latest command executed
     - DevString
     - R
     - no
     - no
     - NOTE: not available
   * - commandResultCode
     - The second entry of commandResult attribute, i.e. the resultCode of latest command executed
     - DevString
     - R
     - no
     - no
     - NOTE: not available
   * - activationTime
     - Read the time of activation in seconds since Unix epoch.
     - double
     - R
     - no
     - no
     - 0.0
   * - assignedResources
     - Read the resources assigned to the device. The list of resources assigned to the subarray.
     - Tuple("str"), max_dim = 512
     - R
     - no
     - no
     - :ref:`example <mid-subarray-assignedResources>`
   * - configuredCapabilities
     - Read capabilities configured in the Subarray. A list of capability types with no. of instances in use on this subarray; e.g. Correlators:512, PssBeams:4 PstBeams:4, VlbiBeams:0.
     - Tuple("str"), max_dim = 10
     - R
     - no
     - no
     - :ref:`example <mid-subarray-configuredCapabilities>`
   * - configurationProgress
     - Read the percentage configuration progress of the device.
     - uint16, max value = 100, min value =0
     - R
     - yes
     - yes
     - NOTE: deprecated
     
   * - obsMode
     - Read the Observation Mode of the device.
     - (ObsMode,)
     - R
     - yes
     - yes
     - (ObsMode.IMAGING,)
   * - obsState
     - Read the Observation State of the device.
     - ObsState
     - R
     - yes
     - yes
     - obsState.IDLE: 2
   * - configurationDelayExpected
     - Read the expected Configuration Delay in seconds.
     - uint16
     - R 
     - yes
     - yes
     - NOTE: deprecated
   * - addResourcesCmdProgress
     - The progress percentage for the Add resources command
     - DevUShort
     - R
     - polling, polling period=1500, abs_change=5
     - no
     - NOTE: deprecated
   * - addSearchBeamsDurationExpected
     - The duration expected (in sec) for the AddSearchBeams command
     - DevUShort
     - R/W
     - no
     - no
     - NOTE: not available
   * - addSearchBeamsDurationMeasured
     - The duration measured (in sec) for the AddSearchBeams command.
     - DevUShort
     - R
     - no
     - no
     - NOTE: not available
   * - addTimingBeamsDurationExpected
     - The duration expected (in sec) for the AddTimingBeams command
     - DevUShort
     - R/W  
     - no
     - no
     - NOTE: deprecated
   * - addTimingBeamsDurationMeasured
     - The duration measured (in sec) for the AddTimingBeams command
     - DevUShort
     - R
     - no
     - no
     - NOTE: not available
   * - assignedTimingBeams
     - String with the list of TimingBeam IDs assigned to the CSP sub-array
     - DevString
     - R/W
     - yes
     - yes
     - NOTE: not available
   * - assignresourcesCmdProgress
     - The progress percentage for the Assign Resources command
     - DevUShort
     - R
     - polling, polling period=1500, abs_change=5
     - no
     - NOTE: deprecated
   * - assignresourcesDurationExpected
     - The duration expected (in sec) for the Assign Resources command
     - DevUShort
     - R/W
     - no
     - no
     - NOTE: deprecated
   * - assignresourcesDurationMeasured
     - The duration measured (in sec) for the Assign Resources command
     - DevUShort
     - R
     - no
     - no
     - NOTE: not available
   * - cbfSubarrayAddress
     - The CBF sub-element sub-array FQDN
     - DevString
     - R
     - no
     - no
     - 'mid_csp_cbf/sub_elt/subarray_01'
   * - cbfSubarrayAdminMode
     - CBF Subarray Admin Mode
     - DevEnum (AdminMode)
     - R/W
     - yes
     - yes
     - cbfSubarrayAdminMode.ON-LINE: 0
   * - cbfSubarrayHealthState
     - CBF Subarray Health State
     - DevEnum (HealthState)
     - R
     - yes
     - yes
     - cbfSubarrayHealthState.OK: 0
   * - cbfSubarrayObsState
     - The CBF subarray observing state.
     - ObsState
     - R
     - yes
     - yes
     - cbfSubarrayObsState.IDLE: 2
   * - cbfSubarrayState
     - State of the Cbf Subarray
     - DevState
     - R
     - yes
     - yes
     - tango._tango.DevState(0)
   * - cmdFailureMessage
     - The failure message on command execution
     - DevString
     - R
     - no
     - no
     - NOTE: not available
   * - configurationID
     - The configuration identifier (string)
     - DevString
     - R
     - no
     - no
     - ''
   * - endScanCmdProgress
     - EndScan command progress percentage	
     - DevUShort
     - R
     - no
     - polling, polling period=1500, abs_change=5
     - NOTE: deprecated
   * - endScanDurationExpected
     - The duration expected (in sec) for the EndScan command.
     - DevUShort
     - R
     - no
     - no
     - NOTE: deprecated
   * - endScanDurationMeasured
     - The duration measured (in sec) for the EndScan command
     - DevUShort
     - R
     - no
     - no
     - NOTE: not available
   * - failureRaisedFlag
     - The failure flag for a CspSubarray command
     - DevBoolean
     - R
     - no
     - polling, polling period=1000
     - False
   * - goToIdleCmdProgress
     - The progress percentage for the GoToIdle command.
     - DevUShort
     - R
     - no
     - polling, polling period=5000, abs_change=5
     - NOTE: deprecated
   * - goToIdleDurationExpected
     - The duration expected (in sec) for the GoToIdle command
     - DevUShort
     - R
     - no
     - no
     - NOTE: deprecated
   * - goToIdleDurationMeasured
     - The duration measured (in sec) for the GoToIdle command
     - DevUShort
     - R
     - no
     - no
     - NOTE: not available
   * - isCmdInProgress
     - Is command in progress
     - DevBoolean
     - R
     - no
     - no
     - False
   * - procModeCorrelationAddr
     - The CSP sub-array Correlation Inherent Capability FQDN
     - DevString
     - R
     - no
     - no
     - NOTE: not available
   * - procModePssAddr
     - The CSP sub-array PSS Inherent Capability FQDN
     - DevString
     - R
     - no
     - no
     - NOTE: not available
   * - procModePstAddr
     - The CSP sub-array PST Inherent Capability FQDN
     - DevString
     - R
     - no
     - no
     - NOTE: not available
   * - procModeVlbiAddr
     - The CSP sub-array VLBI Inherent Capability FQDN.
     - DevString
     - R
     - no
     - no
     - NOTE: not available
   * - pssSubarrayAddress
     - The PSS sub-element sub-array FQDN.
     - DevString
     - R
     - no
     - no
     - 'mid-pss/subarray/01'
   * - pssSubarrayAdminMode
     - PSS Subarray Admin Mode
     - DevEnum (AdminMode)
     - R/W
     - yes
     - yes
     - pssSubarrayAdminMode.NOT-FITTED: 3
   * - pssSubarrayHealthState
     - PSS Subarray Health State
     - DevEnum (HealthState)
     - R
     - yes
     - yes
     - pssSubarrayHealthState.UNKNOWN: 3
   * - pssSubarrayObsState
     - The PSS subarray observing state
     - ObsState
     - R
     - yes
     - yes
     - pssSubarrayObsState.EMPTY: 0
   * - pssSubarrayState
     - The PSS subarray state
     - DevState
     - R
     - yes
     - yes
     - tango._tango.DevState(12)
   * - pstBeamsAdminMode
     - Report the administration mode of the beams assigned to the subarray as JSON formatted string
     - DevString
     - R
     - yes
     - yes
     - '{}'
   * - pstBeamsHealthState
     - Report the health state of the pst beams assigned to the subarray as a JSON formatted string
     - DevString
     - R
     - yes
     - yes
     - '{}'
   * - pstBeamsObsState
     - Report the observing state of the pst beams assigned to the subarray as a JSON formatted string
     - DevString
     - R
     - yes
     - yes
     - '{}'
   * - pstBeamsState
     - Report the state of the pst beams assigned to the subarray as a JSON formatted string
     - DevString
     - R
     - yes
     - yes
     - '{}'
   * - pstOutputLink
     - The output link for PST products
     - DevString
     - R
     - no
     - no
     - NOTE: not available
   * - testAlarm
     - flag to test alarm
     - DevBoolean
     - R/W
     - no
     - no
     - false
   * - timeoutExpiredFlag
     - The timeout flag for a CspSubarray command.
     - DevBoolean
     - R
     - polling, polling period=1000
     - no
     - False
   * - failureRaisedFlag
     - The failure flag for a CspSubarray command
     - DevBoolean
     - R
     - no
     - polling, polling period=1000
     - False
   * - remSearchBeamsDurationExpected
     - The duration expected (in sec) for the RemoveSearchBeams command
     - DevUShort
     - R/W
     - no
     - no
     - NOTE: deprecated
   * - remSearchBeamsDurationMeasured
     - The duration measured (in sec) for the RemoveSearchBeams command
     - DevUShort
     - R/W
     - no
     - no
     - NOTE: not available
   * - remTimingBeamsDurationExpected
     - RemoveTimingBeams command duration expected
     - DevUShort
     - R/W
     - no
     - no
     - NOTE: deprecated
   * - remTimingBeamsDurationMeasured
     - The duration measured (in sec) for the RemoveTimingBeams command
     - DevUShort
     - R/W
     - no
     - no
     - NOTE: not available
   * - removeResourcesCmdProgress
     - The progress percentage for the Remove Resources command
     - DevUShort
     - R
     - polling, polling period=1500, abs_change=5
     - no
     - NOTE: deprecated
   * - reservedSearchBeamNum
     - Number of SearchBeam IDs reserved for the CSP sub-array
     - DevUShort
     - R
     - no
     - no
     - NoTE: not available
   * - scanCmdProgress
     - The progress percentage for the Scan command
     - DevUShort
     - R
     - polling, polling period=1500, abs_change=5
     - no
     - NOTE: deprecated
   * - scanID
     - Scan ID
     - DevULong64
     - R/W
     - yes
     - yes
     - 2
   * - timeoutExpiredFlag
     - The timeout flag for a CspSubarray command.
     - DevBoolean
     - R
     - polling, polling period=1000
     - no
     - False
   * - validScanConfiguration
     - Store the last valid scan configuration
     - DevString
     - R
     - no
     - no
     - :ref:`example <mid-subarray-validScanConfiguration>`
   * - assignedSearchBeamIDs
     - List of assigned Search Beams
     - tuple (DevUShort), max_dim = 1500
     - R
     - no
     - no
     - NOTE: not available
   * - assignedSearchBeamsAdminMode
     - AdminMode of the assigned SearchBeams
     - Tuple (DevUShort), max_dim = 1500
     - R
     - no
     - no
     - NOTE: not available
   * - assignedSearchBeamsHealthState
     - HealthState of the assigned SearchBeams
     - Tuple (DevUShort), max_dim = 1500
     - R
     - no
     - no
     - NOTE: not available
   * - assignedSearchBeamsObsState
     - ObsState of the assigned SearchBeams
     - Tuple (DevUShort), max_dim = 1500
     - R
     - no
     - no
     - NOTE: not available
   * - assignedSearchBeamsState
     - State of the assigned SearchBeams
     - Tuple (DevUShort), max_dim = 1500
     - R
     - no
     - no
     - NOTE: not available
   * - assignedTimingBeamIDs
     - List of TimingBeam IDs assigned to the CSP sub-array
     - Tuple (DevUShort), max_dim = 16
     - R/W
     - yes
     - yes
     - []
   * - assignedTimingBeamsAdminMode
     - AdminMode of the assigned TimingBeams
     - Tuple (DevUShort), max_dim = 16
     - R
     - no
     - no
     - [0]
   * - assignedTimingBeamsHealthState
     - HealthState of the assigned TimingBeams
     - Tuple (DevUShort), max_dim = 16
     - R
     - no
     - no
     - []
   * - assignedTimingBeamsObsState
     - ObsState of the assigned TimingBeams
     - Tuple (DevUShort), max_dim = 16
     - R
     - no
     - no
     - []
   * - assignedTimingBeamsState
     - State of the assigned TimingBeams
     - Tuple (DevState), max_dim = 16
     - R
     - no
     - no
     - NOTE: not available
   * - assignedVlbiBeamIDs
     - List of VlbiBeam IDs assigned to the CSP sub-array
     - Tuple (DevState), max_dim = 20
     - R
     - no
     - no
     - NOTE: not available
   * - assignedVlbiBeamsAdminMode
     - AdminMode of the assigned VlbiBeams
     - Tuple (DevState), max_dim = 20
     - R
     - no
     - no
     - NOTE: not available
   * - assignedVlbiBeamsHealthState
     - HealthState of the assigned VlbiBeams
     - Tuple (DevState), max_dim = 20
     - R
     - no
     - no
     - NOTE: not available
   * - assignedVlbiBeamsObsState
     - ObsState of the assigned VlbiBeams
     - Tuple (DevState), max_dim = 20
     - R
     - no
     - no
     - NOTE: not available
   * - assignedVlbiBeamsState
     - State of the assigned VlbiBeams
     - Tuple (DevState), max_dim = 20
     - R
     - no
     - no
     - NOTE: not available
   * - listOfDevCompletedTask
     - List of devices that completed the task
     - Tuple (DevString), max_dim = 100
     - R
     - no
     - no
     - ()
   * - pstBeamsAddress
     - PST sub-element PstBeam TANGO device FQDNs.
     - Tuple (DevString), max_dim = 16
     - R
     - no
     - no
     - ('mid-pst/beam/01', 'mid-pst/beam/02')
   * - reservedSearchBeamIDs
     - List of SearchBeam IDs reserved for the CSP sub-array
     - Tuple (DevUShort), max_dim = 1500
     - R
     - no
     - no
     - NOTE: not available
   * - addReceptorsCmdProgress
     - The progress percentage for the AddReceptors command.
     - DevUShort
     - R
     - polling, polling period=1500
     - no
     - NOTE: deprecated
   * - assignedVcc
     - List of assigned VCCs
     - Tuple (DevUShort), max_dim = 197
     - R
     - no
     - no
     - [1, 2]
   * - assignedVccHealthState
     - The health state of the assigned VCCs.
     - Tuple (DevString), max_dim = 197
     - R
     - no
     - no
     - ('OK', 'OK')
   * - assignedVccState
     - The State of the assigned VCCs.
     - Tuple (DevString), max_dim = 197
     - R
     - no
     - no
     - ('ON', 'ON')
   * - assignedFsp
     - List of assigned FSPs
     - Tuple (DevUShort), max_dim = 27
     - R
     - no
     - no
     - []
   * - assignedFspHealthState
     - The health state of the assigned FSPs
     - Tuple (DevString), max_dim = 27
     - R
     - no
     - no
     - ()
   * - assignedFspState
     - The State of the assigned FSPs.
     - Tuple (DevString), max_dim = 27
     - R
     - no
     - no
     - ()
   * - assignedFspFunctionMode
     - The Function Mode of the assigned FSPs.
     - Tuple (DevString), max_dim = 27
     - R
     - no
     - no
     - ()
   * - assignedReceptors
     - The list of receptors assigned to the subarray
     - Tuple (DevString), max_dim = 27
     - R
     - no
     - no
     - ('SKA001', 'SKA036')
   * - removeReceptorsCmdProgress
     - The progress percentage for the RemoveReceptors command
     - DevUShort
     - R
     - polling, polling period=1500
     - no
     - NOTE: deprecated
   * - dishVccConfig
     - The Dish ID - VCC ID mapping in a json string
     - DevString
     - R
     - yes
     - yes
     - :ref:`example <mid-subarray-dishVccConfig>`
