:orphan:

########################################
Mid CSP.LMC Subarray Attributes Examples
########################################

* buildState

.. _mid-subarray-buildstate:

::

    'ska-csp-lmc-mid, 0.17.2, SKA MID CSP.LMC'

* longrunningcommandresult

.. _mid-subarray-longrunningcommandresult:

::

    ('1708358750.7680037_31446241612572_On', '[0, "on completed  1/1"]')

* longrunningcommandstatus

.. _mid-subarray-longrunningcommandstatus:

::

    ('1708597146.4755158_254155063643066_AssignResources', 'COMPLETED')

* assignedResources

.. _mid-subarray-assignedResources:

::

    ('mid_csp_cbf/vcc/001', 'mid_csp_cbf/vcc/002')

* configuredCapabilities

.. _mid-subarray-configuredCapabilities:

::

    ('Correlators:512, PssBeams:4 PstBeams:4, VlbiBeams:0')

* validScanConfiguration

.. _mid-subarray-validScanConfiguration:

::

    '{
        "interface": "https://schema.skao.int/ska-csp-scan/2.2",
        "scan_id": 11
    }'

* dishVccConfig

.. _mid-subarray-dishVccConfig:

::

    '{
        "interface": "https://schema.skao.int/ska-mid-cbf-initsysparam/1.0",
        "dish_parameters": {
            "SKA001": {
                "vcc": 1,
                "k"  : 11
            },
            "SKA036": {
                "vcc": 2,
                "k"  : 101
            },
            "SKA063": {
                "vcc": 3,
                "k"  : 1127
            },
            "SKA100": {
                "vcc": 4,
                "k"  : 620
            }
        }
    }'
