##################
Subarray Commands
##################

The following table contains information about the Mid CSP LMC commands of the subarray Tango device.
A more extensive documentation about command parameters can be found in the `Telescope Model - Mid.CSP schemas <https://developer.skao.int/projects/ska-telmodel/en/latest/schemas/csp/ska-mid-csp.html>`_

.. list-table::
   :widths: 15 25 15 10 5 10
   :header-rows: 1

   * - Command name
     - Description
     - Parameters
     - Example
     - Sequence diagram
     - Note
   * - *AssignResources*
     - | Propagates the resources to be assigned to CBF
     - :ref:`Assign resources input <Assign Resources>`
     - :ref:`Assign resources example <Assign resources to a Mid CSP.LMC Subarray>`
     - `Assign resources diagram <https://confluence.skatelescope.org/display/SE/AssignResources>`_
     - 
   * - *Configure*
     - | Propagates the configuration information to PST subsystem
       | Reads address information from PST
       | Add PST address information to CBF json string
       | Propagate the updated configuration information to CBF
     - :ref:`Configure input <Configure>`
     - :ref:`Configure example <Configure, issue and end a scan>`
     - `Configure diagram <https://confluence.skatelescope.org/display/SE/Configure>`_
     - 
   * - *Scan*
     - | Propagates the scanning information to subsystems
     - :ref:`Scan input <Scan>`
     - :ref:`Scan example <Configure, issue and end a scan>`
     - `Scan diagram <https://confluence.skatelescope.org/display/SE/Scan>`_
     - 
   * - *ReleaseResources*
     - | Releases the allocated resources defined in the input string
     - :ref:`Release resources input <Release Resources>`
     - :ref:`Release resources example <Go To Idle and Release Resources>`
     - 
     - 
   * - *ReleaseAllResources*
     - | Releases all the allocated resources
     - No input
     - :ref:`Release resources example <Go To Idle and Release Resources>`
     - 
     - 
   * - *Gotoidle/End*
     - | Moves CSP LMC and subsystems from obsState EMPTY to IDLE
     - No input
     - :ref:`Go to idle example <Go To Idle and Release Resources>`
     - 
     - 
   * - *Endscan*
     - | Interrupts a scan session
     - No input
     - :ref:`EndScan example <Configure, issue and end a scan>`
     - `EndScan diagram <https://confluence.skatelescope.org/display/SE/End+Scan>`_
     - 
   * - *Abort*
     - | Interrupts the commands queue 
       | Performs clean-up of command queues
       | Propagates the abort commadn to subsystems 
     - No input
     - :ref:`Abort example <Configure, issue and end a scan>`
     - `Abort diagram <https://confluence.skatelescope.org/pages/viewpage.action?pageId=227170479>`_
     - 
   * - *Off*
     - | The Off command disables any signal processing capability of a device
     - List that contains the device FQDN.
     - :ref:`Off example <Power-on (off) the Mid.CSP>`
     - `Off diagram <https://confluence.skatelescope.org/display/SE/Off>`_
     - 
   * - *Restart*
     - | Recovers from a FAULT obsState. The subarray will be moved in EMPTY obsState
     - No input
     - :ref:`Restart example <csp-lmc-troubleshooting>`
     - `Restart diagram <https://confluence.skatelescope.org/display/SE/Restart>`_
     - This command will release also all the resources, bringing the subarray into an EMPTY *obsState*. 
   * - *Reset*
     - | Recovers from a FAULT State. The subarray will be moved in OFF/EMPTY state
     - No input
     - :ref:`Reset example <csp-lmc-reset>`
     - `Reset diagram <https://confluence.skatelescope.org/display/SE/Reset>`_
     - The command is propagated to subarrays
