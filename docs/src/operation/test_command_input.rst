##################
Json Command Input
##################

The following Json string are used as command input in our integration tests to vallidate the Mid CSP.LMC behavior.

.. _loaddishcfg-json-map:

LoadDishCfg JSON MAP
####################

Json string contains the DishId-VCC map content::

    {
        "interface": "https://schema.skao.int/ska-mid-cbf-initsysparam/1.0",
        "dish_parameters": {
            "SKA001": {
                "vcc": 1,
                "k"  : 11
            },
            "SKA036": {
                "vcc": 2,
                "k"  : 101
            },
            "SKA063": {
                "vcc": 3,
                "k"  : 1127
            },
            "SKA100": {
                "vcc": 4,
                "k"  : 620
            }
        }
    }

.. _loadDishCfg-car-uri:

LoadDishCfg CAR URI
###################

Json string contains the DishId-VCC map CAR URI::

    {
        "interface": "https://schema.skao.int/ska-mid-cbf-initsysparam/1.0",
        "tm_data_sources": ["car://gitlab.com/ska-telescope/ska-telmodel-data?ska-sdp-tmlite-repository-1.0.0#tmdata"],
        "tm_data_filepath": "instrument/ska1_mid_psi/ska-mid-cbf-system-parameters.json"
    }


Assign Resources
################

::

    {
        "subarray_id": 1,
        "dish":{ 
            "receptor_ids":["SKA001", "SKA036", "SKA063", "SKA100"]
        }
    }


Release Resources
#################

::

    {
        "subarray_id": 1,
        "dish":{ 
            "receptor_ids":["SKA001"]
        },
    }


Configure
#########
    
::

  {
    "interface": "https://schema.skao.int/ska-csp-configurescan/3.0",
    "common": {
        "config_id": "sbi-mvp01-20200325-00001-science_A",
        "frequency_band": "1",
        "subarray_id": 1,
        "eb_id": "eb-m001-20230712-56789"
    },
    "cbf": {
        "fsp": [{
            "fsp_id": 1,
            "function_mode": "CORR",
            "frequency_slice_id": 1,
            "integration_factor": 1,
            "channel_averaging_map": [
                [0, 2],
                [744, 0]
            ],
            "channel_offset": 0,
            "output_link_map": [
                [0, 1]
            ]
        }, {
            "fsp_id": 2,
            "function_mode": "CORR",
            "frequency_slice_id": 2,
            "integration_factor": 1,
            "channel_averaging_map": [
                [0, 2],
                [744, 0]
            ],
            "channel_offset": 740,
            "output_link_map": [
                [0, 1]
            ]
        }],
        "vlbi": {},
        "delay_model_subscription_point": "ska_mid/tm_leaf_node/csp_subarray_01/delayModel"
    },
    "transaction_id": "txn-local-20240911-688170695"
  }


Scan
####

::

    {
        "interface": "https://schema.skao.int/ska-csp-scan/2.2", 
        "scan_id": 11
    }
