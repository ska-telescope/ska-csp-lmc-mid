.. Mid.CSP LMC Prototype documentation master file, created by
   sphinx-quickstart on Thu Jun 13 16:15:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SKA Mid CSP.LMC
===============

.. Architecture ===========================================================

.. toctree::
   :maxdepth: 1
   :caption: Architecture

   architecture/architecture.rst

.. TANGO device: property, commands, attributes ===========================

.. toctree::
   :maxdepth: 2
   :caption: Tango devices

   Controller <tango_device/controller/index>
   Subarray <tango_device/subarray/index>
   Subsystems version <tango_device/subsystems_version.rst>

.. OPERATION ==============================================================

.. toctree::
   :maxdepth: 1
   :caption: Operation

   CSP LMC Tango clients examples <operation/example>
   Commands argument examples <operation/test_command_input>

.. TROUBLESHOOTING =========================================================

.. toctree::
   :maxdepth: 1
   :caption: Troubleshooting

   Troubleshooting <troubleshooting/troubleshooting>

.. Components =============================================================

.. toctree::
   :maxdepth: 1
   :caption: Components

   components/mid_csp_lmc.rst

.. API ====================================================================

.. toctree::
   :maxdepth: 1
   :caption: Tango API

   api/index.rst

.. Telescope model ====================================================================

.. toctree::
   :maxdepth: 1
   :caption: Telescope model

   Telescope model <telescope_model/commands_interface_version.rst>

.. CHANGELOG ==============================================================

.. toctree::
   :maxdepth: 1
   :caption: Releases

   CHANGELOG.rst

.. README =================================================================

.. toctree::
   :maxdepth: 1
   :caption: Deployment

   Gitlab README<../../README>