#
# Project makefile for Mid CSP.LMC project. #

PROJECT = ska-csp-lmc-mid

# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= mid-csp

# RELEASE_NAME is the release that all Kubernetes resources will be labelled
# with
RELEASE_NAME ?= test  #TODO: check

# HELM_CHART is the umbrella chart name
HELM_CHART ?= mid-csp-umbrella
UMBRELLA_CHART_PATH ?= charts/$(HELM_CHART)/

# Fixed variables
# Timeout for gitlab-runner when run locally
TIMEOUT = 86400
# Helm version
HELM_VERSION = v3.3.1
# kubectl version
KUBERNETES_VERSION = v1.19.2

CI_PROJECT_DIR ?= .

KUBE_CONFIG_BASE64 ?=  ## base64 encoded kubectl credentials for KUBECONFIG
KUBECONFIG ?= /etc/deploy/config ## KUBECONFIG location

XAUTHORITY ?= $(HOME)/.Xauthority
THIS_HOST := $(shell ip a 2> /dev/null | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
DISPLAY ?= $(THIS_HOST):0
JIVE ?= false# Enable jive
TARANTA ?= false# Enable Taranta
MINIKUBE ?= true ## Minikube or not
EXPOSE_All_DS ?= true ## Expose All Tango Services to the external network (enable Loadbalancer service)
SKA_TANGO_OPERATOR ?= true

NOTEBOOK_IGNORE_FILES = not notebook.ipynb

CI_JOB_ID ?= local##pipeline job id

# ----- OCI BUILD ---------

#use the artefact prefix for local build
ifeq ($(CI_JOB_ID),local)
CAR_OCI_USE_HARBOR=false 
endif

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
# include OCI Images support 
include .make/oci.mk 

# include k8s support 
include .make/k8s.mk 

# include Helm Chart support 
include .make/helm.mk 

# Include Python support 
include .make/python.mk 

# include raw support 
include .make/raw.mk 

# include core make support 
include .make/base.mk

# include your own private variables for custom deployment configuration 
-include PrivateRules.mak 

# include xray support 
include .make/xray.mk

# Chart for testing
K8S_CHART = $(HELM_CHART)
K8S_CHARTS = $(K8S_CHART)

# NOTE: this is overwritten in pipeline and it can be used to create different 
# conditions for local use

TANGO_HOST ?= tango-databaseds:10000## TANGO_HOST connection to the Tango DS
TANGO_SERVER_PORT ?= 45450## TANGO_SERVER_PORT - fixed listening port for local server
CLUSTER_DOMAIN ?= cluster.local## Domain used for naming Tango Device Servers
K8S_TEST_RUNNER = test-runner-$(CI_JOB_ID)##name of the pod running the k8s-test

OCI_IMAGES = ska-csp-lmc-mid

ITANGO_ENABLED ?= true## ITango enabled in ska-tango-base

# -------- UNIT TESTING ---------

MARK_UN ?= unit
PYTHON_TEST_FILE ?= tests/unit

ADDITIONAL_AFTER_PYTEST ?= -n 4
PYTHON_VARS_AFTER_PYTEST = -k $(MARK_UN) --forked \
                        --disable-pytest-warnings $(ADDITIONAL_AFTER_PYTEST) -vv

# ---------- LINTING -------------

PYTHON_LINT_TARGET = src/ tests/

# C0103(invalid-name)
# R0401(cyclic-import),R0801(duplicate-code) not enough to disable in file
PYTHON_SWITCHES_FOR_PYLINT =--disable=C0103,R0801,R0401

# ------- K8S DEPLOYMENT  --------

HELM_CHARTS_TO_PUBLISH = ska-csp-lmc-mid
HELM_CHARTS ?= $(HELM_CHARTS_TO_PUBLISH)

PYTHON_BUILD_TYPE = non_tag_setup

VALUES_FILE ?= charts/mid-csp-umbrella/values-default.yaml
CUSTOM_VALUES =

ifneq ($(VALUES_FILE),)
CUSTOM_VALUES:=--values $(VALUES_FILE)
else
endif

K8S_TEST_IMAGE_TO_TEST = artefact.skao.int/ska-csp-lmc-mid:$(VERSION)

ifneq ($(CI_REGISTRY),)
K8S_TEST_TANGO_IMAGE_PARAMS = --set ska-csp-lmc-mid.midcsplmc.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
	--set ska-csp-lmc-mid.midcsplmc.image.registry=$(CI_REGISTRY)/ska-telescope/ska-csp-lmc-mid
K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/ska-csp-lmc-mid/ska-csp-lmc-mid:$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
else
K8S_TEST_TANGO_IMAGE_PARAMS = --set ska-csp-lmc-mid.midcsplmc.image.tag=$(VERSION)
endif

TARANTA_PARAMS = --set ska-tango-taranta.enabled=$(TARANTA) \
				 --set ska-tango-taranta-auth.enabled=$(TARANTA) \
				 --set ska-dashboard-repo.enabled=$(TARANTA)

ifneq ($(MINIKUBE),)
ifneq ($(MINIKUBE),true)
TARANTA_PARAMS = --set ska-tango-taranta.enabled=$(TARANTA) \
				 --set ska-tango-taranta-auth.enabled=false \
				 --set ska-dashboard-repo.enabled=false
endif
endif

USE_GITLAB_IMAGE ?= false
COMMIT_ID ?=
DEV_IMAGE_TAG ?= 

# If the flag USE_GITLAB_IMAGE is set to true, it set the registry for CSP.LMC to be the one in gitlab
# setting the COMMIT_ID will use a specific commit image, otherwise the latest
ifeq ($(USE_GITLAB_IMAGE), true)
ifneq ($(COMMIT_ID),)
DEV_IMAGE_TAG =-dev.c$(COMMIT_ID)
endif 
REGISTRY=registry.gitlab.com/ska-telescope/ska-csp-lmc-mid
# Strip 'dirty' if present
CLEAN_VERSION := $(if $(findstring dirty,$(VERSION)),$(patsubst %-dirty,%,$(VERSION)),$(VERSION))
K8S_EXTRA_PARAMS = --set ska-csp-lmc-mid.midcsplmc.image.registry=$(REGISTRY) \
	--set ska-csp-lmc-mid.midcsplmc.image.tag=$(CLEAN_VERSION)$(DEV_IMAGE_TAG)
K8S_TEST_IMAGE_TO_TEST=$(REGISTRY)/ska-csp-lmc-mid:$(CLEAN_VERSION)$(DEV_IMAGE_TAG)
endif

K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
	--set global.exposeAllDS=$(EXPOSE_All_DS) \
	--set global.tango_host=$(TANGO_HOST) \
	--set global.cluster_domain=$(CLUSTER_DOMAIN) \
	--set global.device_server_port=$(TANGO_SERVER_PORT) \
	--set global.operator=$(SKA_TANGO_OPERATOR) \
	--set ska-tango-base.display=$(DISPLAY) \
	--set ska-tango-base.xauthority=$(XAUTHORITY) \
	--set ska-tango-base.jive.enabled=$(JIVE) \
	--set ska-tango-base.itango.enabled=$(ITANGO_ENABLED) \
	$(TARANTA_PARAMS) \
	${K8S_TEST_TANGO_IMAGE_PARAMS} \
	$(K8S_EXTRA_PARAMS) \
	$(CUSTOM_VALUES)

# -- K8S TESTING  -

TEST_FOLDER ?= integration
COUNT ?= 1

XRAY_TEST_RESULT_FILE ?= build/reports/cucumber.json
XRAY_EXECUTION_CONFIG_FILE ?= tests/xray-config.json

ifeq ($(MARK), all)
 SEL_TEST=-m pipeline
else ifeq ($(MARK), nightly)
 SEL_TEST=
else 
 SEL_TEST=-m $(MARK)
endif 

# these variables are set only if the entry for make is k8s-test
ifeq ($(strip $(firstword $(MAKECMDGOALS))),k8s-test)
# need to set the PYTHONPATH since the ska-cicd-makefile default definition 
# of it is not OK for the alpine images
PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=/app/src:/usr/local/lib/python3.10/site-packages TANGO_HOST=$(TANGO_HOST)
PYTHON_VARS_AFTER_PYTEST := --cucumberjson=build/reports/cucumber.json \
            --json-report --json-report-file=build/reports/report.json \
             --disable-pytest-warnings --count=$(COUNT) --timeout=600 --forked --true-context --log-cli-level=INFO $(SEL_TEST) -k $(TEST_FOLDER)
endif

k8s-pre-template-chart: k8s-pre-install-chart

requirements: ## Install Dependencies
	poetry install

.PHONY: all test up down help k8s show lint logs describe mkcerts localip namespace delete_namespace ingress_check kubeconfig kubectl_dependencies helm_dependencies rk8s_test k8s_test rlint

clean: #remove cache files after test
	@ sudo rm -rf $$(find . -name __pycache__) build .pytest_cache
	
# BRANCH variable is calculated for pipeline. It can be injected from common pipeline or set according to the branch on which the pipeline is running
BRANCH = $(shell if [[ ! -z $$BRANCH_FROM_COMMON ]]; then \
					echo $$BRANCH_FROM_COMMON; \
				elif [[ ! -z $$CI_COMMIT_BRANCH ]]; then \
					echo $$CI_COMMIT_BRANCH; \
				elif [[ ! -z $$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME ]]; then \
					echo $$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME; \
				fi)

# CHECK variable is is empty if the "BRANCH" does not exist on common
CHECK = $(shell (curl -s --head https://gitlab.com/ska-telescope/ska-csp-lmc-common/-/tree/$(BRANCH) | head -n 1 | grep "HTTP/2 [23].."))

# Installing the ska-csp-lmc-common from a branch if on Gitlab python-test and python-linting jobs
# The gitlab job is already installing so we need to remove and install it from proper source
pre-gitlab-lint-and-test: 
	@BRANCH="$(BRANCH)"; \
	CHECK="$(CHECK)"; \
	if [[ ! -z $$BRANCH ]]; then \
		echo "We are on job: $$CI_JOB_NAME. Checking for branch: $$BRANCH"; \
		CHECK="$(CHECK)"; \
		if [[ ! -z $$CHECK ]]; then \
			echo "installing ska-csp-lmc-common from branch $(BRANCH)"; \
		else \
			echo "branch not found, installing from master"; \
			BRANCH=master; 	\
		fi; \
		poetry remove ska-csp-lmc-common; \
		poetry add git+https://gitlab.com/ska-telescope/ska-csp-lmc-common.git@$$BRANCH ; \
	else \
		echo "installing ska-csp-lmc-common as specified in pyproject.toml"; \
	fi 

# configure the PYTHON_VARS_BEFORE_PYTEST variable without specifying the TANGO_HOST
python-pre-test: PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=./src:/app/src:/app/src/ska_csp_lmc_mid
# override python.mk python-pre-test target
python-pre-test: pre-gitlab-lint-and-test
	@echo "python-pre-test: running with: $(PYTHON_VARS_BEFORE_PYTEST) $(PYTHON_RUNNER) pytest $(PYTHON_VARS_AFTER_PYTEST) \
	 --cov=src --cov-report=term-missing --cov-report xml:build/reports/code-coverage.xml --junitxml=build/reports/unit-tests.xml $(PYTHON_TEST_FILE)" ; \
	# \
	# Following lines are for installing the ska-csp-lmc-common from a branch. They are used only on Gitlab python-test branch \
	# The gitlab job is already installing so we need to remove and install it from proper source \
	# \

python-pre-lint: pre-gitlab-lint-and-test

# If in Gitlab, detect if a branch with the same name exist on common and then change the poetry files to install it
pre-gitlab-install-common: 
	@BRANCH="$(BRANCH)"; \
	if [[ ! -z $$BRANCH ]]; then \
		echo "Installing ska-csp-lmc-common from gitlab branch $$BRANCH"; \
		CHECK="$(CHECK)"; \
		if [[ -z $$CHECK ]] ; then \
			echo "branch not found, installing from master"; \
			BRANCH=master; \
		fi; \
		cp pyproject.toml pyproject.toml.tmp && \
		cp poetry.lock poetry.lock.tmp && \
		git config --global user.email "cream@inaf.it" ;\
		git config --global user.name "cream" ;\
		# \
		# Relevant lines are found in **pyproject.toml** and changed with the ones appropriated for installing from branch\
		NLINE=$$(grep -n 'ska-csp-lmc-common = ' pyproject.toml | cut -f1 -d:) ;\
		sed ''"$$NLINE"'d' pyproject.toml > temp ;\
		sed ''"$$NLINE"' i ska-csp-lmc-common = {git = "https://gitlab.com/ska-telescope/ska-csp-lmc-common", rev ="'"$$BRANCH"'"}' temp > pyproject.toml ;\
		# Removing temporary files \
		rm temp # temp1 temp2; \
		rm poetry.lock; \
		poetry lock --no-update; \
		git add . ; \
		git commit -m 'dummy commit for not having dirty release'; \
	else \
		echo "installing ska-csp-lmc-common as specified in pyproject.toml"; \
	fi 

# jobs to build an image with a local ska-csp-lmc-common in parent directory
pre-local-install-common: 
	@echo "Building the image installing local ska-csp-lmc-common" ; \
	# \
	# Check if the common directory is not being copied yet; \
	if [[ "$$(ls | grep ska-csp-lmc-common)" ]]; then \
		make post-local-install-common; \
	fi; \
	cp -r ../ska-csp-lmc-common . && \
	cp pyproject.toml pyproject.toml.tmp && \
	cp poetry.lock poetry.lock.tmp && \
	# \
	# Relevant lines are found in poetry.lock and changed with the ones appropriated for local installing \
	# if it exists, remove the 'develop = ' string in common package (since it could be false)\
	sed -i '/ska-csp-lmc-common/,/\[package.dependencies\]/{/develop =.*/d;}' poetry.lock && \
	# find line number of the last element of 'files' in ska-csp-lmc-common package ']' \
	NLINE_t1=$$(sed -n '/ska-csp-lmc-common/,/\[package.dependencies\]/{/\]/=;}' poetry.lock | head -n1) && \
	# add entry 'develop = true' at the desired line \
	sed -i ''"$${NLINE_t1}"'a develop = true' poetry.lock && \
	# \
	# remove replace common package source \
	# find line of common package source \
	NLINE_pkgs=$$(sed -n '/ska-csp-lmc-common/,/\[package.source\]/{/\[package.source\]/=;}' poetry.lock | head -n1) && \
	# find line of the beginning of the package after the common package \
	NLINE_pkg=$$(sed -n '/ska-csp-lmc-common/,/\[\[package\]\]/{/\[\[package\]\]/=;}' poetry.lock | head -n1) && \
	# remove pipeline package source \
	sed -i ''"$${NLINE_pkgs}"','"$${NLINE_pkg}"'d' poetry.lock && \
	# add local package source \
	sed -i ''"$${NLINE_pkgs}"'i [package.source] \ntype="directory" \nurl="ska-csp-lmc-common"\n\n\[\[package\]\] ' poetry.lock && \
	# \
	# remove entries about git repo in order to use the local version \
	sed -i '/ska_csp_lmc_common-/d' poetry.lock && \
	# Relevant lines are found in pyproject.toml and changed with the ones appropriated for local installing \
	NLINE3=$$(grep -n 'ska-csp-lmc-common = ' pyproject.toml | cut -f1 -d:) && \
	sed ''"$$NLINE3"'d' pyproject.toml > temp4 && sed ''"$$NLINE3"' i ska-csp-lmc-common = {path = "./ska-csp-lmc-common", develop = true}' temp4 > pyproject.toml; \
	# \
	# An alternative could be update the lock. This is cleaner but takes more time (40s) \
	#poetry lock --no-update; \
	# \
	# Removing temporary files \
	rm temp4; \

# this job restore poetry files with the one backed up and remove common repository
post-local-install-common: clean
	@ echo "restoring poetry files" ; \
	if [ -f pyproject.toml.tmp ]; then mv -f pyproject.toml.tmp pyproject.toml; fi; \
	if [ -f poetry.lock.tmp ]; then mv -f poetry.lock.tmp poetry.lock; fi; \
	rm -rf ska-csp-lmc-common; \
	# \
	# The following lines create an image tagged without dirty, needed in the local deployment \
	IMAGE=$(K8S_TEST_IMAGE_TO_TEST); \
	if [[ $$IMAGE == *"dirty"* ]]; then \
		IMAGE_NO_DIRTY=$$(echo $$IMAGE | sed s/"-dirty"//); \
		docker tag $$IMAGE $$IMAGE_NO_DIRTY; \
	fi

PYTANGO_BUILDER_VERSION = 9.5.0 
CONTAINER_FOR_TESTING ?= artefact.skao.int/ska-tango-images-pytango-builder:$(PYTANGO_BUILDER_VERSION)

# This will run a container with local common installed, where to run unit tests or linting
dev-container: pre-local-install-common
	@echo "using $(CONTAINER_FOR_TESTING)"; \
	if [[ "$$(docker ps -aq -f name=container-for-test-mid)" ]]; then \
		docker rm container-for-test-mid; \
	fi; \
	# create dev container and initialize it (install poetry) in background \
	docker run --rm -d -v "$$(pwd)":/app --name container-for-test-mid $(CONTAINER_FOR_TESTING) /bin/bash -c "sleep infinity" ; \
	docker exec container-for-test-mid /bin/bash -c "poetry config virtualenvs.create false && poetry install" && \
	docker exec -it container-for-test-mid /bin/bash && \
	# stop container\
	docker stop container-for-test-mid; \
	make post-local-install-common; \

# Build the image using the local package for common
local-oci-build: pre-local-install-common oci-build post-local-install-common

# This allows the build in pipeline to use the branch package of common, if required
oci-pre-build-all: pre-gitlab-install-common
