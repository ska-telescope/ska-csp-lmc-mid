"""MidComponentFactory.

The Observing Device and Controller Component Factories to specialize Mid CSP
sub-systems components classes
"""

from __future__ import annotations

import logging

from ska_csp_lmc_common.controller.controller_factory import (
    ControllerComponentFactory,
)
from ska_csp_lmc_common.subarray.csp_subarray import CspSubarrayComponent
from ska_csp_lmc_common.subarray.pst_beam import PstBeamComponent
from ska_csp_lmc_common.subarray.subarray_factory import (
    ObservingComponentFactory,
)

from ska_csp_lmc_mid.controller import (
    MidCbfControllerComponent,
    MidPssControllerComponent,
)
from ska_csp_lmc_mid.subarray import (
    MidCbfSubarrayComponent,
    MidPssSubarrayComponent,
)

# pylint: disable=super-init-not-called
# pylint: disable=too-few-public-methods

module_logger = logging.getLogger(__name__)


class MidObservingComponentFactory(ObservingComponentFactory):
    """Specialize Mid CSP sub-systems components classes for observing
    devices."""

    def __init__(
        self: MidObservingComponentFactory, csp_ctrl_fqdn: str, logger=None
    ) -> None:
        self._creators = {
            "cbf": MidCbfSubarrayComponent,
            "pss": MidPssSubarrayComponent,
            "pst": PstBeamComponent,
        }
        self.csp_ctrl_fqdn = csp_ctrl_fqdn
        self.logger = logger or module_logger
        self.logger.debug(
            f"MidObservingComponentFactory csp_fqdn {csp_ctrl_fqdn}"
        )

    def get_component(self, fqdn, identifier):
        """Override the base function to inject the FQDN of the CSP Controller
        into a Sub-system component. The CSP Controller address can be used to
        access to the Capabilities information.

        :param fqdn: The CSP sub-system component address
        :type fqdn: string
        :param identifier: The CSP Sub-system identifier label: cbf-
            subarray, pst-beam etc,
        :type identifier: string

        :return: The Sub-system component class
        :rtype: :py:class:`ObservingComponent`
        """
        creator = super().get_component(fqdn, identifier)
        setattr(creator, "csp_ctrl_fqdn", self.csp_ctrl_fqdn)
        return creator


class MidControllerComponentFactory(ControllerComponentFactory):
    """Specialize Mid CSP sub-systems components classes for controller
    devices."""

    def __init__(self: MidControllerComponentFactory, logger=None) -> None:
        self._creators = {
            "cbf": MidCbfControllerComponent,
            "pss": MidPssControllerComponent,
            "pst": PstBeamComponent,
            "subarray": CspSubarrayComponent,
        }
        self.logger = logger or module_logger
        self.logger.debug("MidControllerComponentFactory")
