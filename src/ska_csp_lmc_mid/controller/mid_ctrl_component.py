"""Specialize the Subsystem Controller Component classes for the Mid.CSP
LMC."""
from __future__ import annotations

import logging
from typing import Dict, List, Optional

from ska_control_model import HealthState
from ska_csp_lmc_common.connector import Connector
from ska_csp_lmc_common.controller.cbf_controller import CbfControllerComponent
from ska_csp_lmc_common.controller.pss_controller import PssControllerComponent
from ska_csp_lmc_common.subarray.pst_beam import PstBeamComponent
from tango import DevState, EventData

from ska_csp_lmc_mid.mid_receptor_util import index_to_receptor_id

# pylint: disable=use-dict-literal
# pylint: disable=broad-except
# pylint: disable=use-list-literal
# pylint: disable=logging-fstring-interpolation
# pylint: disable=invalid-name


class MidCbfControllerComponent(CbfControllerComponent):
    """Specialize the CBF Controller Component class for the Mid.CSP LMC.

    This class works as a cache and adaptor towards the real Mid device.
    """

    def __init__(
        self: MidCbfControllerComponent,
        fqdn: str,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """Initialize the MidCbfControllerComponent.

        :param fqdn: The Mid.CBF controller TANGO Device Fully Qualified
            Domain Name.
        :param logger: The device or python logger if default is None.
        """
        super().__init__(fqdn, logger=logger)

        self._vcc_to_receptor_map = {}
        self._receptor_to_vcc_map = {}
        self._linked_receptors = []
        # attributes to be subscribed in the CBF controller
        self.attrs_for_change_events = [
            # "sourceSysParam",
            # "sysParam",
            # "reportVccState",
            # "reportVccHealthState",
        ]
        self.map_cbf_csp_attr_name = {
            "sourceSysParam": "sourceDishVccConfig",
            "sysParam": "dishVccConfig",
        }

    @property
    def vcc_state(
        self: MidCbfControllerComponent,
    ) -> List[DevState]:
        """Return the operational state (State) of the VCCs organized as a list
        where the element index corresponds to the (vcc_id -1).

        Example::

            vcc_state = [ON, ON, FAULT, UNKNOWN]

            VCC 1 -> ON
            VCC 2 -> ON
            VCC 3 -> FAULT
            VCC 4 -> UNKNOWN

        :return: a list with the VCCs' healthState.
        """
        return self.read_attr("reportVccState")

    @property
    def vcc_health(
        self: MidCbfControllerComponent,
    ) -> List[HealthState]:
        """Return the healthState of the VCCs organized as a list where the
        element index corresponds to the (vcc_id -1).

        Example::

            vcc_health = [OK, OK, FAILED, UNKNOWN]

            VCC 1 -> OK
            VCC 2 -> OK
            VCC 3 -> FAILED
            VCC 4 -> UNKNOWN

        :return: a list with the VCCs' healthState.
        """
        return self.read_attr("reportVccHealthState")

    @property
    def vcc_affiliation(
        self: MidCbfControllerComponent,
    ) -> List[int]:
        """Return the affiliation of the VCCs to the Mid.CSP subarrays,
        organized as a list where.

        the position of the element is the receptor_id - 1 and the value is
        the subarray ID owing that resource. 0 means the current resource is
        not assigned, yet

        Example::

            vcc_affiliation = [1,0,0,2]

            VCC 1 -> subarray 1
            VCC 2 -> not assigned
            VCC 3 -> not assigned
            VCC 4 -> subarray 2

        :return: A list with the receptors' affiliation to subarrays
        """
        return self.read_attr("reportVccSubarrayMembership")

    @property
    def vcc_to_receptor_map(self: MidCbfControllerComponent) -> Dict[str, str]:
        """Return the map vcc-id -receptor-id for all the usable (deployed)
        receptors as a dictionary.

        The format of the attribute is the following one::

            {vcc_id: receptor_id,...}

            Example:

            vcc_to_receptor_map = {"1":"SKA002", "2":"SKA004",
                                   "3":"SKA001", "4":"SKA003"
                                   }

        :return: the associations between vcc_id and receptors_id organized as
            a dictionary.
        """
        return self._vcc_to_receptor_map

    @property
    def vcc_to_receptor(
        self: MidCbfControllerComponent,
    ) -> List[str]:
        """Return the value of the Mid.CBF Controller *vccToReceptor* attribute
        storing a list with the map of all (vcc_id:receptor_index) associations
        (ordered by vcc id). The receptor index is the (array index + 1) of a
        full list of 197 receptors.
        The format of the attribute is the following one:

            ['vcc_id:receptor_index',...]

            Example:

            vcc_to_receptor = ['1:2', '2:1', '3:3', '4:75']

        :return: the list with the associations (vcc_id:receptor_index) for
            each deployed VCC.
        """
        return self.read_attr("vccToReceptor")

    @property
    def receptor_to_vcc(
        self: MidCbfControllerComponent,
    ) -> List[str]:
        """Return the value of the Mid.CBF Controller *receptorToVcc* attribute
        storing a list with the map of all (receptor_id:vcc_id) associations.
        (ordered by receptor id). The receptor index is the (array index + 1)
        of a full list of 197 receptors.
        The format of the attribute is the following one::

            ['receptor_id:vcc_id',...]

            Example:

            receptor_to_vcc = ['1:2', '2:1', '3:3', '4:75']

        :return: the list with the associations (receptor_index:vcc_id) for
            each deployed VCC.
        """
        return self.read_attr("receptorToVcc")

    @property
    def list_of_receptors(self: MidCbfControllerComponent) -> List[str]:
        """Return the ordered list of all the usable (deployed) receptors IDs.

        Example::

            If there are only 4 VCC devices deployed:

            list_of_receptors = ['SKA001','SKA022','SKA103','MKT002']
        """
        _receptor_list = list(self._receptor_to_vcc_map.keys())
        self.logger.debug("LIST_OF_RECEPTORS:" f"{_receptor_list}")
        return _receptor_list

    @property
    def linked_receptors(self: MidCbfControllerComponent) -> Dict[str, str]:
        """
        Return information about the receptors IDs with a valid connection to
        the VCC.
        Valid receptor ids are in [SKA001,..,SKA133] and [MKT000,MKT63] range.
        A receptor_id = DIDINV means the link connection between the receptor
        and the VCC is off.
        NOTE: Right now, the receptor ID contains always a value, since it is
        the theoretical connection coming from the TM. In order to be able to
        check the receptor ID vs VCC link (receptor_id != "DIDINV"), it should
        be needed to verify the VCC capability attributes.

        note::

            Example:

                list_of_receptors = ["SKA001","SKA002","SKA003","SKA004"]
                vcc_state = [ON, UNKNOWN, ON, ON]
                vcc_to_receptor_map = {'1':"SKA002", '2':"SKA004",
                                    '3':"SKA001", '4':"SKA003"
                                    }
                linked_receptors = ['1':"SKA002", '3':"SKA001"]

        :return: the map of the valid (vcc_id, receptor_id) associations.
        """
        self._linked_receptors = {}

        if not self._vcc_to_receptor_map:
            self.get_vcc_receptor_map()
        for pos, (vcc_id, receptor_id) in enumerate(
            self._vcc_to_receptor_map.items()
        ):
            # for vcc_id, receptor_id in self._vcc_to_receptor_map.items():
            self.logger.debug(f"vcc_id {vcc_id} receptor_id {receptor_id}")
            # vcc_health = self.vcc_health
            vcc_state = self.vcc_state
            self.logger.debug(f"vcc_state: {vcc_state[pos]}")
            # self.logger.info(f"1 vcc_health: {vcc_health}")
            # if (vcc_health[vcc_id - 1] == HealthState.OK)
            # and receptor_id > 0:
            # This line should be changed with the one before. But to do
            # this, polling on the healthState and State of the VCC should
            # be disabled otherwise CSP receives old values.
            # These attributes should push events from the device (not
            # polling).
            if (
                # vcc_state[vcc_id - 1] in [DevState.ON, DevState.OFF]
                vcc_state[pos]
                in [DevState.ON, DevState.OFF]
            ) and receptor_id != "DIDINV":
                self._linked_receptors[vcc_id] = receptor_id

        self.logger.debug(f"linked receptors : {self._linked_receptors}")
        return self._linked_receptors

    @property
    def unassigned_receptors(
        self: MidCbfControllerComponent,
    ) -> List[str]:
        """Return the not ordered list of availbale receptors IDs.
        The list includes all the receptors that are not assigned to any
        subarray and, from the side of CSP, are considered "full working".
        This means:

            * a valid link connection receptor-VCC\n
            * the connected VCC healthState OK

        Example::

            linked_receptors = ["1":"SKA002", "3":"SKA001"]
            vcc_affiliation = [1, 0, 0, 3]
            unassigned_receptors = ["SKA001"]

        TODO: Check which is the criteria to establish when a VCC is available
            (working): check the healthState (OK) and the state (ON)?

        :return: The list of the unassigned (available) receptors IDs on
            success, otherwise an empty list.
        :raise: ValueError if an error is caught during CBF attributes reading.
        """

        unassigned_receptors = []
        self.logger.debug("Read unassigned receptors")
        try:
            vcc_affiliation = self.vcc_affiliation
            self.logger.debug(f"vcc_affiliation: {vcc_affiliation}")
            for pos, (vcc_id, receptor_id) in enumerate(
                self.linked_receptors.items()
            ):
                self.logger.debug(f"vcc_id {vcc_id} receptor_id {receptor_id}")
                if vcc_affiliation[pos] != 0:
                    continue
                unassigned_receptors.append(receptor_id)
        except Exception as err:
            self.logger.error(
                f"Error accessing VCC/Receptors information : {err}"
            )
        return unassigned_receptors

    @property
    def unassigned_vcc(
        self: MidCbfControllerComponent,
    ) -> List[str]:
        """Return the list of availbale VCC IDs.

        :return: The list of the unassigned (available) VCC IDs on success,
            otherwise an empty list.
        """
        unassigned_vcc = []
        self.logger.debug("Read unassigned vcc")
        try:
            vcc_affiliation = self.vcc_affiliation
            self.logger.debug(f"vcc_affiliation: {vcc_affiliation}")
            for pos, (vcc_id, _) in enumerate(
                self._vcc_to_receptor_map.items()
            ):
                if vcc_affiliation[pos] != 0:
                    continue
                unassigned_vcc.append(vcc_id)
        except Exception as err:
            self.logger.error(
                f"Error accessing VCC/Receptors information : {err}"
            )
        return unassigned_vcc

    @property
    def receptors_affiliation(
        self: MidCbfControllerComponent,
    ) -> List[int]:
        """Build the list reporting the receptors affiliation to subarray, if
        any. The element index corresponds to (receptor_id -1) and the value
        stored is the number of the subarray to which the receptor belongs.

        Example::

            vcc_affiliation = [subid=1, subid=2,  ... ]
            vcctoReceptor_map = [vccId1:SKA035, vccId2:SKA054]
            receptor_list = [SKA001, SKA035,SKA054,...]
            receptors_affiliation = [0,1,2,0]

            receptors_affiliation = [1,0,0,2]

            receptor 1 -> subarray 1
            receptor 2 -> not assigned
            receptor 3 -> not assigned
            receptor 4 -> subarray 2

        :return: A list with the receptors' affiliation to subarrays

        :return: The receptors affiliation to subarray_state on success,
            otherwise an empty list.
        """
        receptors_membership = []
        if not self._vcc_to_receptor_map:
            self.get_vcc_receptor_map()
        try:
            vcc_affiliation = self.vcc_affiliation
            receptor_list = list(self._receptor_to_vcc_map)

            receptors_membership = [0] * len(self.vcc_affiliation)
            for vcc_idx, (_, receptor_id) in enumerate(
                self._vcc_to_receptor_map.items()
            ):
                # detect position of receptor in ordered list
                receptor_idx = receptor_list.index(receptor_id)
                receptors_membership[receptor_idx] = vcc_affiliation[vcc_idx]
                # receptors_membership[receptor_id] = vcc_affiliation[vcc_id]
            self.logger.debug(f"RECEPTOR_MEMBERSHIP: {receptors_membership}")
        except Exception as e:
            self.logger.error(
                f"Unable to access Vcc/Receptors information: {e}"
            )
        return receptors_membership

    @property
    def dish_vcc_config(
        self: MidCbfControllerComponent,
    ) -> str:
        """Return the information about mapping between DishID and VCCs.

        :return: The DishID-VCC mapping
        """
        return self.read_attr("sysParam")

    @property
    def source_dish_vcc_config(
        self: MidCbfControllerComponent,
    ) -> str:
        """Return the information about mapping between DishID and VCCs.

        :return: The DishID-VCC mapping source
        """
        return self.read_attr("sourceSysParam")

    #################
    # Public methods
    #################

    def get_vcc_receptor_map(self: MidCbfControllerComponent):
        """Build the map between VCC-id and Receptor-id and viceversa.
        The funciton stores them in instance attributes.
        """
        self.logger.debug("Read the VCC to Receptors map")

        try:
            self._get_vcc_to_receptor_map()
            self._get_receptor_to_vcc_map()

        except ValueError:
            self.logger.debug(
                "Failure in accessing CBF proxy to get VCC-Receptor map"
            )
        except Exception as error:
            # this message is part of a test. Please do not change it
            self.logger.error(f"Failure: {error}")

    def _get_vcc_to_receptor_map(self: MidCbfControllerComponent):
        """Get VCC-id vs Receptor-index map from CBF and convert it to
        a dictionary.
        """
        # vcc_to_receptor is read from CBF property
        vcc_to_receptor = self.vcc_to_receptor

        self.logger.debug(f"VCC_TO_RECEPTOR: {vcc_to_receptor}")
        # convert list of string (i.e ['1:2', '2:5' ...]) to dictionary
        vcc_to_receptor_map = {}
        for pair in vcc_to_receptor:
            key, value = pair.split(":")
            vcc_to_receptor_map[key] = value
        self.logger.debug(f"VCC_TO_RECEPTOR_MAP: {self._vcc_to_receptor_map}")
        # create dictionary in ordered according to vcc id
        # [1':'SKA001', '2':'MKT022'..]
        for vcc_id, receptor_index in vcc_to_receptor_map.items():
            receptor_id = index_to_receptor_id(receptor_index)
            self._vcc_to_receptor_map[vcc_id] = receptor_id

    def _get_receptor_to_vcc_map(self: MidCbfControllerComponent):
        """Get Receptor-index vs VCC-id map from CBF and convert it to
        a dictionary.
        """
        # receptor_to_vcc is read from CBF property
        receptor_to_vcc = self.receptor_to_vcc
        # convert list of string (i.e ['2:1', '5:2' ...]) to dictionary
        receptor_to_vcc_map = {}
        for pair in receptor_to_vcc:
            key, value = pair.split(":")
            receptor_to_vcc_map[key] = value
        # receptor_to_vcc_map = dict(
        #    [ID for ID in pair.split(":")] for pair in receptor_to_vcc
        # )
        # create dictionary in ordered according to receptor id
        # ['SKA001':'2', .. ,'MKT022':'1' ..]
        for receptor_index, vcc_id in receptor_to_vcc_map.items():
            receptor_id = index_to_receptor_id(receptor_index)
            self._receptor_to_vcc_map[receptor_id] = vcc_id

    def connect(self: MidCbfControllerComponent) -> Connector:
        """Establish a connection with the Mid CBF Controller. On successfull
        connection , the VCCs/receptors mapping is retrieved.

        :return: An instance of the Connector class on success, otherwise None.
        """
        connector = None
        try:
            connector = super().connect()
            if connector:
                self.logger.info(connector)
                # ask for the VCCs/receptors mapping
                self.get_vcc_receptor_map()
        except ValueError as err:
            self._vcc_to_receptor_map = {}
            self._receptor_to_vcc_map = {}
            self.logger.error(f"Error in {self.name} connect: {err}")
        return connector

    def _push_event(
        self: MidCbfControllerComponent,
        recv_event: EventData,
        device_attr_name=None,
    ) -> None:
        """Extend the component event callback to specifie the correct
        attribute name in case there is a mismach between the CBF component
        and CPS tango device"""

        token_position = recv_event.attr_name.rfind("/")
        # !!!!
        # NOTE: need to save the position of attr_name to avoid
        # conflicts between python-format and python-lint
        # !!!!
        first_char_position = token_position + 1
        attr_name = recv_event.attr_name[first_char_position:]
        if attr_name in self.map_cbf_csp_attr_name:
            attr_name = self.map_cbf_csp_attr_name[attr_name]
        super()._push_event(recv_event, attr_name)


class MidPssControllerComponent(PssControllerComponent):
    """Specialization of the PssController component class for the Mid.CSP."""

    def __init__(
        self: MidPssControllerComponent,
        fqdn: str,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """Initialize the MidPssControllerComponent.

        :param fqdn: The Mid.CBF controller TANGO Device Fully Qualified
            Domain Name.
        :param logger: The device or python logger if default is None.
        """
        super().__init__(fqdn, logger=logger)

    @property
    def list_of_beams(self):
        """Return the list of PSS beams IDs."""
        return [1, 2, 3, 4]


class MidPstBeamComponent(PstBeamComponent):
    """Specialization of the PstController component class for the Mid.CSP."""

    def __init__(
        self: MidPstBeamComponent,
        fqdn: str,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """Initialize the MidCbfControllerComponent.

        :param fqdn: The Mid.PST controller TANGO Device Fully Qualified
            Domain Name.
        :param logger: The device or python logger if default is None.
        """
        super().__init__(fqdn, logger=logger)

    @property
    def list_of_beams(self) -> List[int]:
        """Return the list of PST beams IDs.

        :return: a list containing the PST beams IDs
        """
        return [1, 2, 3, 4]
