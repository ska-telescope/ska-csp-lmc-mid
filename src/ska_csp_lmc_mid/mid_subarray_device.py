# -*- coding: utf-8 -*-
#
# This file is part of the MidCspSubarray project
#
# INAF, SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
"""MidCspSubarray.

The base class for MID CspSubarray. Functionality to monitor assigned CSP.LMC
Capabilities, as well as inherent Capabilities, are implemented in separate
TANGO Devices.
"""

# Python standard library
from __future__ import annotations

from typing import Any, List, Tuple

# import CSP.LMC Common package
from ska_csp_lmc_common import CspSubarray
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)

# from ska_tango_base.commands import CommandReturnT, FastCommand, ResultCode
from tango import DevState
from tango.server import attribute, run

from ska_csp_lmc_mid.manager import MidCspSubarrayComponentManager
from ska_csp_lmc_mid.subarray.mid_json_schema_validator import (
    CspMidJsonValidator,
)

# pylint: disable=unnecessary-pass
# pylint: disable=broad-except
# pylint: disable=invalid-name
# pylint: disable=logging-fstring-interpolation


__all__ = ["MidCspSubarray", "main"]


class MidCspSubarray(CspSubarray):
    """The base class for MID CspSubarray.

    Functionality to monitor assigned CSP.LMC Capabilities, as well as inherent
    Capabilities, are implemented in separate TANGO Devices.
    """

    def _get_schema_validator(
        self: MidCspSubarray,
    ) -> CspMidJsonValidator:
        """Retrieve the class that handles the validation of the input
        JSON files sent as argument of AssignResources, Configure and Scan."""
        return CspMidJsonValidator

    # ----------------
    # Class Properties
    # ----------------

    # -----------------
    # Device Properties
    # -----------------

    # ---------------
    # Public methods
    # ---------------

    def set_component_manager(
        self: MidCspSubarray, cm_configuration: ComponentManagerConfiguration
    ) -> MidCspSubarrayComponentManager:
        """Set the CM for the Mid CSP Subarray device.

        :param cm_configuration: A class with all the device properties
            accessible as attributes

        :returns: The Mid.CSP SubarrayComponentManager
        """
        return MidCspSubarrayComponentManager(
            self.health_model,
            self.op_state_model,
            self.obs_state_model,
            cm_configuration,
            self.update_property,
            self.logger,
        )

    def _init_state_model(self: MidCspSubarray) -> None:
        """Override method in order to set mid attributes for change events."""
        super()._init_state_model()

        list_of_change_only_attributes = [
            "dishVccConfig",
        ]
        for attr in list_of_change_only_attributes:
            self.set_change_event(attr, True, False)

    def update_property(
        self: MidCspSubarray, property_name: str, property_value: Any
    ) -> None:
        """
        Specialization of the CspSubarray method.
        This method is registered within the Component Manager of the Mid
        Subarray device and is invoked by the ComponentManager to update
        the value of a device attribute and push an event on it.
        The attribute has to be properly configured to push events from the
        device.

        :param property_name: the TANGO attribute name
        :param property_value: the attribute value
        """
        dict_to_update = {property_name: property_value}
        try:
            if property_name in (
                {
                    f"vccssubarray{self.component_manager.sub_id:02d}",
                    f"fspssubarray{self.component_manager.sub_id:02d}",
                }
            ):
                dict_to_update = (
                    self.component_manager.update_assigned_resources_status(
                        property_name[:3], property_value
                    )
                )

            elif property_name in {"vccsfqdnassigned", "fspsfqdnassigned"}:

                dict_to_update = (
                    self.component_manager.update_assigned_resources(
                        property_name[:3], property_value
                    )
                )
            for _property_name, _property_value in dict_to_update.items():
                self.logger.info(
                    f"Updating {property_name} to {property_value}"
                )
                super().update_property(_property_name, _property_value)
        except ValueError as err:
            self.logger.warning(err)

    # ----------
    # Attributes
    # ----------

    commandResultName = attribute(
        dtype="DevString",
        label="commandResult name",
        doc=(
            "The first entry of commandResult attribute, i.e. the name of"
            " latest command executed. For Taranta visualization"
        ),
    )

    commandResultCode = attribute(
        dtype="DevString",
        label="commandResult resultCode",
        doc=(
            "The second entry of commandResult attribute, i.e. the resultCode"
            " of latest command executed. For Taranta visualization"
        ),
    )

    assignedFsp = attribute(
        dtype=("DevUShort",),
        max_dim_x=27,
        label="List of assigned FSPs",
        doc="List of assigned FSPs.",
    )

    assignedVcc = attribute(
        dtype=("DevUShort",),
        max_dim_x=197,
        label="List of assigned VCCs",
        doc="List of assigned VCCs.",
    )

    addReceptorsCmdProgress = attribute(
        dtype="DevUShort",
        label="AddReceptors command progress percentage",
        polling_period=1500,
        doc="The progress percentage for the AddReceptors command.",
    )

    removeReceptorsCmdProgress = attribute(
        dtype="DevUShort",
        label="RemoveReceptors command progress percentage",
        polling_period=1500,
        doc="The progress percentage for the RemoveReceptors command.",
    )

    assignedVccState = attribute(
        dtype=("DevString",),
        max_dim_x=197,
        label="assignedVccState",
        doc="The State of the assigned VCCs.",
    )

    assignedVccHealthState = attribute(
        dtype=("DevString",),
        max_dim_x=197,
        label="assignedVccHealthState",
        doc="The health state of the assigned VCCs.",
    )

    assignedFspState = attribute(
        dtype=("DevString",),
        max_dim_x=27,
        label="assignedFSPState",
        doc="The State of the assigned FSPs.",
    )

    assignedFspHealthState = attribute(
        dtype=("DevString",),
        max_dim_x=27,
        label="assignedFSPHealthState",
        doc="The health state of the assigned FSPs.",
    )

    assignedFspFunctionMode = attribute(
        dtype=("DevString",),
        max_dim_x=27,
        label="assignedFSPFunctionMOde",
        doc="The Function Mode of the assigned FSPs.",
    )

    assignedReceptors = attribute(
        dtype=("DevString",),
        max_dim_x=197,
        label="assignedReceptors",
        doc="The list of receptors assigned to the subarray.",
    )

    capabilityConnected = attribute(
        dtype="DevBoolean",
        doc=(
            "Whether the device is communicating with the component under"
            " control"
        ),
    )

    dishVccConfig = attribute(
        dtype="DevString",
        max_dim_x=197,
        label="dishVccConfig",
        doc="The Dish ID - VCC ID mapping in a json string",
    )

    # ------------------
    # Attributes methods
    # ------------------

    def read_commandResultName(self: MidCspSubarray) -> str:
        """Return the name of the last executed CSP task.

        :return: The name of the CSP task.
        """
        try:
            return self.component_manager.command_result[0]
        except Exception:
            return ""

    def read_commandResultCode(self: MidCspSubarray) -> str:
        """Return the *ResultCode* of the last executed CSP task.

        :return: The result code (see :py:class:`ResultCode`) of the last
            executed task.
        """
        try:
            return self.component_manager.command_result[1]
        except Exception:
            return ""

    def read_addReceptorsCmdProgress(self: MidCspSubarray) -> int:
        """Return the addReceptorsCmdProgress attribute.

        :return: The progress percentage for the AddReceptors command.

        NOTE: not implemented
        """
        # //  MidCspSubarray.addReceptorsCmdProgress_read

    def read_removeReceptorsCmdProgress(self: MidCspSubarray) -> int:
        """Return the removeReceptorsCmdProgress attribute.

        :return: The progress percentage for the RemoveReceptors command.

        NOTE: not implemented
        """
        # //  MidCspSubarray.removeReceptorsCmdProgress_read

    def read_assignedFsp(self: MidCspSubarray) -> Tuple[int]:
        """Return the assignedFsp attribute.

        :return: List of assigned FSPs.

        NOTE: not implemented
        """
        return [
            int(fsp_fqdn[-2:])
            for fsp_fqdn in self.component_manager.assigned_fsp
        ]

    def read_assignedVcc(self: MidCspSubarray) -> List[int]:
        # NOTE: CBF has not this attribute. it should be reconstructed
        # (from receptor_id)
        """
        *Attribute method*

        :returns:
            The list of VCC IDs assigned to the subarray.
        """
        return [
            int(vcc_fqdn[-3:])
            for vcc_fqdn in self.component_manager.assigned_vcc
        ]

    def read_assignedVccState(self: MidCspSubarray) -> List[DevState]:
        """Return the assignedVccState attribute.

        :return: The State of the assigned VCCs.
        """
        # return self.component_manager.online_components[
        #     self.CbfSubarray
        # ].read_attr("vccState")
        return self.component_manager.assigned_vcc_state

    def read_assignedVccHealthState(self: MidCspSubarray) -> List[str]:
        """Return the assignedVccHealthState attribute.

        :return: The health state of the assigned VCCs.
        """
        return self.component_manager.assigned_vcc_health_state
        # return self.component_manager.online_components[
        #     self.CbfSubarray
        # ].read_attr("vccHealthState")

    def read_assignedFspState(self: MidCspSubarray) -> List[DevState]:
        """Return the assignedFspState attribute.

        :return: The State of the assigned FSPs.
        """
        return self.component_manager.assigned_fsp_state
        # NOTE: not implemented
        # """
        # return self.component_manager.online_components[
        #     self.CbfSubarray
        # ].read_attr("fspState")

    def read_assignedFspHealthState(self: MidCspSubarray) -> List[str]:
        """Return the assignedFspHealthState attribute.

        :return: The health state of the assigned FSPs.
        """
        return self.component_manager.assigned_fsp_health_state
        # return self.component_manager.online_components[
        #     self.CbfSubarray
        # ].read_attr("fspHealthState")

    def read_assignedFspFunctionMode(self: MidCspSubarray) -> List[str]:
        """Return the assignedFspHealthState attribute.

        :return: The health state of the assigned FSPs.
        """
        return self.component_manager.assigned_fsp_function_mode

    def read_assignedReceptors(self: MidCspSubarray) -> List[str]:
        """Return the assignedReceptors attribute.

        :return: The list of receptors assigned to the subarray.
        """
        return self.component_manager.online_components[
            self.CbfSubarray
        ].assigned_receptors

    def read_capabilityConnected(self: MidCspSubarray) -> bool:
        """Whether the TANGO device is communicating with the Capability
        Devices.

        :return: Whether the device is communicating with the component under
            control.
        """
        return self.component_manager.capability_connected

    def read_dishVccConfig(self) -> DevState:
        """Return the dishVccConfig attribute.

        :return: The corresponding Cbf Subarray sysParam attribute
        :raise: ValueError exception if attribute reading from the device
            fails
        :raise: KeyError exception.
        """
        try:
            dish_vcc_config = self.component_manager.online_components[
                self.CbfSubarray
            ].dish_vcc_config
        except KeyError:
            dish_vcc_config = ""
        return dish_vcc_config


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # pylint: disable=missing-function-docstring
    return run((MidCspSubarray,), args=args, **kwargs)


if __name__ == "__main__":
    main()
