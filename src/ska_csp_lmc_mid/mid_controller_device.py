# -*- coding: utf-8 -*-
#
# This file is part of the MidCspController project
#
# INAF-SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""MidCspController class.

The base class for MID CspController.

Functionality to monitor CSP.LMC Capabilities are
implemented in separate TANGO Devices.
"""

# Python standard library
from __future__ import annotations

from typing import List

# import CSP.LMC Common package
from ska_csp_lmc_common import CspController
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)

# Additional import
from ska_tango_base import utils
from ska_tango_base.commands import ResultCode, SubmittedSlowCommand
from tango import DebugIt, DevState
from tango.server import attribute, command, device_property, run

from ska_csp_lmc_mid.manager import MidCspControllerComponentManager

DevVarLongStringArrayType = tuple[list[ResultCode], list[str]]

# pylint: disable=unnecessary-pass
# pylint: disable=broad-except
# pylint: disable=fixme
# pylint: disable=logging-fstring-interpolation
# pylint: disable=invalid-name


class MidCspController(CspController):
    """The base class for MID CspMAster. Functionality to monitor CSP.LMC
    Capabilities are implemented in separate TANGO Devices.

    **Device Properties:**

        CspVccCapability
            - TANGO Device to monitor the Mid.CSP VCCs Capabilities devices.
            - Type:'DevString'Properties

        CspFspCapability
            - TANGO Device to monitor the Mid.CSP FSPs Capabilities devices.
            - Type:'DevString'
    """

    # -----------------
    # Device Properties
    # -----------------

    CspVccCapability = device_property(
        dtype="DevString",
    )

    CspFspCapability = device_property(
        dtype="DevString",
    )

    # ---------------
    # Public methods
    # ---------------

    def set_component_manager(
        self: MidCspController, cm_configuration: ComponentManagerConfiguration
    ) -> MidCspControllerComponentManager:
        """Set the Component Manager for the Mid CSP Controller device.

        :param cm_configuration: A class with all the device properties
            accessible as attributes

        :returns: The Mid.Csp ControllerComponentManager
        """
        return MidCspControllerComponentManager(
            self.op_state_model,
            self.health_model,
            cm_configuration,
            self.update_property,
            self.logger,
        )

    def init_command_objects(self: MidCspController) -> None:
        """Set up the command objects."""
        super().init_command_objects()

        for command_name, method_name in [
            ("LoadDishCfg", "loaddishcfg"),
        ]:
            self.register_command_object(
                command_name,
                SubmittedSlowCommand(
                    command_name,
                    self._command_tracker,
                    self.component_manager,
                    method_name,
                    callback=None,
                    logger=self.logger,
                ),
            )
            self.logger.debug("registered a command LoadDishCfg")

    def _init_state_model(self: MidCspController) -> None:
        """Override method in order to set mid attributes for change events."""
        super()._init_state_model()

        list_of_change_only_attributes = [
            "alarmDishIdCfg",
            "dishVccConfig",
            "sourceDishVccConfig",
        ]
        for attr in list_of_change_only_attributes:
            self.set_change_event(attr, True, False)

    def is_LoadDishCfg_allowed(self: MidCspController) -> bool:
        """
        Return whether the `LoadDishCfg` command may be called in the
        current device state.

        :param raise_if_disallowed: whether to raise an error or
            simply return False if the command is disallowed.
            Currently not used.

        :return: whether the command may be called in the current device
            state
        """
        return self.get_state() in [
            DevState.OFF,
        ]

    @command(
        dtype_in="DevString",
        doc_in=(
            "The input argument can be either a json containing the DishID-VCC"
            " Mapping or a json with the URI where this mapping can be"
            " obtained from."
        ),
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def LoadDishCfg(
        self: MidCspController, argin: str
    ) -> DevVarLongStringArrayType:
        """
        Load DishID to VCC Mapping.

        To modify behaviour for this command, modify the do() method of
        the command class.

        :param argin: mapping

        :return: A tuple containing a result code and a string message. If the
            result code indicates that the command was accepted, the message is
            the unique ID of the task that will execute the command. If the
            result code indicates that the command was not excepted, the
            message explains why.
        """
        handler = self.get_command_object("LoadDishCfg")
        (result_code, message) = handler(argin)
        return ([result_code], [message])

    # ----------
    # Attributes
    # ----------

    commandResultName = attribute(
        dtype="DevString",
        label="commandResult name",
        doc=(
            "The first entry of commandResult attribute, i.e. the name of"
            " latest command executed. For Taranta visualization"
        ),
    )

    commandResultCode = attribute(
        dtype="DevString",
        label="commandResult resultCode",
        doc=(
            "The second entry of commandResult attribute, i.e. the resultCode"
            " of latest command executed. For Taranta visualization"
        ),
    )

    availableCapabilities = attribute(
        dtype=("DevString",),
        max_dim_x=20,
        doc=(
            "A list of available number of instances of each capability type,"
            " e.g. `CORRELATOR:512`, `PSS-BEAMS:4`."
        ),
    )

    # Note:
    #     This attribute is defined in SKAMaster Class from which CspMaster
    #     class inherits. To override the attribute *read* method, the
    #     *availableCapabilities* attribute is added again ("overload" button
    #     enabled in POGO).

    receptorsList = attribute(
        dtype=("DevString",),
        max_dim_x=197,
        label="Receptors List",
        doc="The list of all receptors",
    )

    receptorMembership = attribute(
        dtype=("DevUShort",),
        max_dim_x=197,
        label="Receptor Membership",
        doc="The receptors affiliation to MID CSP sub-arrays.",
    )

    unassignedReceptorIDs = attribute(
        dtype=("DevString",),
        max_dim_x=197,
        label="Unassigned receptors IDs",
        doc="The list of available receptors IDs.",
    )

    cspVccCapabilityAddress = attribute(
        dtype="DevString",
        label="CSP VCC Capability device address",
        doc="The FQDN of the CSP VCC Capability",
    )

    cspFspCapabilityAddress = attribute(
        dtype="DevString",
        label="CSP FSP Capability device address",
        doc="The FQDN of the CSP FSP Capability",
    )

    vccAddresses = attribute(
        dtype=("DevString",),
        max_dim_x=197,
        label="CBF VCC Capabilities Addresses",
        doc="The list of VCC Capabilities FQDNs",
    )

    fspAddresses = attribute(
        dtype=("DevString",),
        max_dim_x=27,
        label="CBF FSP Capabilities Addresses",
        doc="The list of FSP Capabilities FQDNs",
    )

    dishVccConfig = attribute(
        dtype="DevString",
        max_dim_x=197,
        label="dishVccConfig",
        doc="The Dish ID - VCC ID mapping in a json string",
    )

    sourceDishVccConfig = attribute(
        dtype="DevString",
        max_dim_x=197,
        label="sourceDishVccConfig",
        doc="The source of Dish ID - VCC ID mapping in a json string",
    )

    # ------------------
    # Attributes methods
    # ------------------

    def read_commandResultName(self: MidCspController) -> str:
        """Return the name of the last executed CSP task.

        :return: The name of the CSP task.
        """
        try:
            return self._csp_command_result[0]
        except Exception:
            return ""

    def read_commandResultCode(self: MidCspController) -> str:
        """Return the *ResultCode* of the last executed CSP task.

        :return: The result code (see :py:class:`ResultCode`) of the last
            executed task.
        """
        try:
            return self._csp_command_result[1]
        except Exception:
            return ""

    def read_availableCapabilities(
        self: MidCspController,
    ) -> List[str]:
        """Return the list of available capabilities.

        :return: For each Mid.CSP capability type, reports the number of
            available resources.
        """
        try:
            # self._available_capabilities is dictionary:
            # key: capability name
            # value: capability available instances
            self.logger.debug(
                "available_capabilities:"
                f" {self._available_capabilities} "
                f"{type(self._available_capabilities)}"
            )
            # read the available receptors = available VCC
            cbf_ctrl = self.component_manager.online_components[self.CspCbf]
            unassigned_vcc = cbf_ctrl.unassigned_vcc
            self.logger.debug(
                f"unassigned_vcc: {unassigned_vcc} {type(unassigned_vcc)}"
            )
            # TODO: Update also the other capabilities
            if unassigned_vcc:
                self._available_capabilities["VCC"] = str(len(unassigned_vcc))
        except Exception as e:
            self.logger.error(
                f"Can't update availableCapabilities values: {e}"
            )
        return utils.convert_dict_to_list(self._available_capabilities)

    def read_receptorsList(
        self: MidCspController,
    ) -> List[str]:
        """Return the list with all the (deployed) receptors IDs.

        :return: The list of receptors Ids.
        """
        try:
            return self.component_manager.online_components[
                self.CspCbf
            ].list_of_receptors
        except KeyError:
            return [""]

    def read_receptorMembership(
        self: MidCspController,
    ) -> List[int]:
        """Return the information about the receptors/VCCs affiliation to
        Mid.CSP subarrays. The value stored in the (receptor_id -1) element
        corresponds to the subarray Id owing the VCC associated to the
        receptor. This value is in the range [0, 16]:  0 means the current
        resource is not assigned to any subarray.

        On failure an empty list is returned (VERIFY!!!)

        :return: The subarray affiliation of the receptors.
        """
        try:
            return self.component_manager.online_components[
                self.CspCbf
            ].receptors_affiliation
        except KeyError:
            return []

    def read_unassignedReceptorIDs(
        self: MidCspController,
    ) -> List[str]:

        """Return the list of available receptors IDs. The list includes all
        the receptors that are not assigned to any subarray and, are "full
        working". This means:

        * a valid link connection receptor-VCC
        * the connected VCC healthState OK

        On failure an empty list is returned.

        :return: The list of the available receptors IDs.
        """
        try:
            return self.component_manager.online_components[
                self.CspCbf
            ].unassigned_receptors
        except KeyError:
            return []

    def read_cspVccCapabilityAddress(self: MidCspController) -> List[str]:
        """
        Return the CSP VCC Capability device address.
        """
        attr_value = ""
        if self.CspVccCapability:
            attr_value = self.CspVccCapability
        return attr_value

    def read_cspFspCapabilityAddress(self: MidCspController) -> List[str]:
        """
        Return the CSP FSP Capability device address.
        """
        attr_value = ""
        if self.CspFspCapability:
            attr_value = self.CspFspCapability
        return attr_value

    def read_vccAddresses(self: MidCspController) -> List[str]:
        """ReturnCspFspCapabilityVcc the list with the FQDNs of
        the VCCs TANGO devices.

        :return: the list of VCC Capabilities FQDNs
        """
        # To implement
        return []

    def read_fspAddresses(self: MidCspController) -> List[str]:
        """Return the list with the FQDNs of the FSPs TANGO devices.

        :return: The list of FSP Capabilities FQDNs
        """
        # To implement
        return []

    def read_dishVccConfig(self) -> DevState:
        """Return the dishVccConfig attribute.

        :return: The corresponding Cbf Controller sysParam attribute
        :raise: ValueError exception if attribute reading from the device
            fails
        :raise: KeyError exception.
        """
        try:
            dish_vcc_config = self.component_manager.online_components[
                self.CspCbf
            ].dish_vcc_config
        except KeyError:
            dish_vcc_config = ""
        return dish_vcc_config

    def read_sourceDishVccConfig(self) -> DevState:
        """Return the dishVccConfig attribute.

        :return: The corresponding Cbf Controller sysParam attribute
        :raise: ValueError exception if attribute reading from the device
            fails
        :raise: KeyError exception.
        """
        try:
            source_dish_vcc_config = self.component_manager.online_components[
                self.CspCbf
            ].source_dish_vcc_config
        except KeyError:
            source_dish_vcc_config = ""
        return source_dish_vcc_config

    @attribute(
        dtype="DevBoolean",
        label="alarmDishIdCfg",
        doc="Alarm raised in case of failure of the command LoadDishCfg",
    )
    def alarmDishIdCfg(self: MidCspController) -> bool:
        """Return whether the alarm flag related to LoadDishCfg is set"""
        return self.component_manager.alarm_dish_id_cfg

    # --------
    # Commands
    # --------


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # pylint: disable=missing-function-docstring
    return run((MidCspController,), args=args, **kwargs)


if __name__ == "__main__":
    main()
