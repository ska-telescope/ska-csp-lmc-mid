"""Mid CSP constant"""
from enum import Enum


class FspModes(Enum):
    """Define FSP processing mode"""

    IDLE = 0
    CORR = 1
    PSS_BF = 2
    PST_BF = 3
    VLBI = 4


def convert_band_to_string(value):
    """Convert VCC frequency_band attribute from int to string label"""
    name = ["1", "2", "3", "4", "5a", "5b"]

    return name[value]


DISH_MKT_RANGE = [0, 63]
DISH_SKA_RANGE = [1, 133]
DISH_TYPE = ["MKT", "SKA"]
DISH_TYPE_LEN = 3
DISH_RANGE_LEN = 3
