# -*- coding: utf-8 -*-
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
"""Mid CSP Capability VCC"""
from __future__ import annotations

import logging
import queue
from typing import Any, List, Tuple

# Tango imports
import tango
from ska_control_model import AdminMode, HealthState, ResultCode
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.model.op_state_model import OpStateModel
from ska_tango_base.base import SKABaseDevice
from tango import AttrWriteType, DevState
from tango.server import attribute, device_property, run

from ska_csp_lmc_mid.manager.mid_vcc_capability_component_manager import (
    MidVccCapabilityComponentManager,
)

module_logger = logging.getLogger(__name__)

# pylint: disable=logging-format-interpolation
# pylint: disable=logging-fstring-interpolation
# pylint: disable=attribute-defined-outside-init
# pylint: disable=protected-access
# pylint: disable=broad-exception-caught

# To be removed when integrated with component manager
# pylint: disable=no-member


class MidCspCapabilityVcc(SKABaseDevice):
    """
    Mid CSP Capability VCC device
    Aggregates VCC information and presents to higher level devices
    """

    # pylint: disable=too-many-public-methods
    @attribute(dtype="str")
    def buildState(self: MidCspCapabilityVcc) -> str:
        """
        Read the Build State of the device.

        :return: the build state of the device
        """
        return self.component_manager._csp_build_state

    @attribute(dtype="str")
    def versionId(self: MidCspCapabilityVcc) -> str:
        """
        Return the SW version of the current device.

        The SW version is equal to the release number
        of the CSP Helm chart and also of the containerized
        image.

        :return: the version id of the device
        """
        return self.component_manager._csp_version

    class InitCommand(SKABaseDevice.InitCommand):
        """Class to handle the CSP.LMC Controller Init command."""

        def do(
            self: MidCspCapabilityVcc.InitCommand,
            *args: Any,
            **kwargs: Any,
        ) -> Tuple[ResultCode, str]:
            """Invoke the *init* command on the Controller ComponentManager.

            :return: a tuple with the command result code and an informative
                message.
            """
            # NOTE: in InitCommand parent class, the healthState of the device
            # is set to OK. To make the HealthStateModel work fine, the initial
            # value of the healthState must be set to OK, too.
            super().do()
            # device._health_state = HealthState.UNKNOWN
            # This is a temporary line to make the component_manager able to
            # manage old initialization
            self._device.component_manager.init()
            self._device._admin_mode = AdminMode.ONLINE
            return ResultCode.OK, "Mid VccCapability init command finished"

    def init_device(self: MidCspCapabilityVcc):
        """
        Initialise the tango device after startup.
        """
        self._omni_queue = queue.Queue()
        self._health_state = HealthState.UNKNOWN
        self.component_manager = None
        self.subarray_deployed = 16
        super().init_device()

    def initialize_dynamic_attributes(self: MidCspCapabilityVcc):
        """This method will be called automatically by the device_factory
        for each device.
        Within this method  all the dynamic attributes are created.
        This method is called once, when the device starts

        """
        for sub_id in range(1, self.subarray_deployed + 1):
            attr_name = f"vccsSubarray{sub_id:02d}"
            attr = attribute(
                name=attr_name,
                dtype=str,
                label=f"json with VCCs info for subarray{sub_id: 02d}",
                doc=(
                    "Json containing the aggregated data for all VCCs of "
                    f"subarray{sub_id: 02d}"
                ),
                access=AttrWriteType.READ,
                fget=self._vcc_subarray_read,
                fset=None,
            )
            self.add_attribute(attr)
            self.set_change_event(attr_name, True)

    def delete_device(self: MidCspCapabilityVcc) -> None:
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        if not self.component_manager:
            return
        if self.component_manager.evt_manager:
            self.component_manager.evt_manager.stop()
            self.component_manager.evt_manager.unsubscribe_all_events()

    def _init_state_model(self: MidCspCapabilityVcc) -> None:
        """Override the health and operational State models:

        current CSP.LMC implementation does no longer rely on the SKA State
        models and associated State Machine library.
        """
        super()._init_state_model()
        # OpStateModel is not needed for the capability, but has to be there.
        # Othewise the BC rises an exception
        try:
            self.op_state_model = OpStateModel(
                DevState.INIT, self.update_device_attribute, self.logger
            )
        except Exception as e:
            self.logger.error(f"Debug Init Device: {e}")

        list_of_attributes = [
            "vccState",
            "vccHealthState",
            "vccAdminMode",
            "isCommunicating",
            "vccsFqdnAssigned",
            "vccSubarrayMembership",
            "vccSimulationMode",
            "vccBandActive",
            "vccReceptorId",
        ]

        for attr in list_of_attributes:
            self.set_change_event(attr, True)
            # self.set_archive_event(attr, True)

    def set_component_manager(
        self: MidCspCapabilityVcc,
        cm_configuration: ComponentManagerConfiguration,
    ) -> MidVccCapabilityComponentManager:
        """Configure the ComponentManager for the CSP Capability VCC device.

        :param cm_configuration: A class with all the device properties
            accessible as attributes

        :return: The CSP Capability VCC ComponentManager
        """
        return MidVccCapabilityComponentManager(
            cm_configuration,
            update_device_property_cbk=self.update_device_attribute,
            logger=self.logger,
        )

    def create_component_manager(
        self: MidCspCapabilityVcc,
    ) -> MidVccCapabilityComponentManager:
        """Override the base method.

        :returns: The CSP ControllerComponentManager
        """
        cm_configuration = ComponentManagerConfiguration(
            self.get_name(), self.logger
        )
        cm_configuration.add_attributes()
        return self.set_component_manager(cm_configuration)

    def update_device_attribute(
        self: MidCspCapabilityVcc, attr_name: str, attr_value: Any
    ) -> None:
        """General method invoked by the ComponentManager to push an event
        on a device attribute properly configured to push events from the
        device.

        :param attr_name: the TANGO attribute name
        :param attr_value: the attribute value
        """
        if attr_name == "state":
            if attr_value != self.get_state():
                self.set_state(attr_value)
                self.push_change_event("state", attr_value)
        elif attr_name == "healthstate":
            if attr_value != self._health_state:
                self._health_state = attr_value
                self.push_change_event("healthstate", attr_value)
        else:
            self.push_change_event(attr_name, attr_value)

    def _vcc_subarray_read(self, attr: tango.server.attribute):
        """Read method associated to the dynamic attributes.

        Return a JSON string with the information about all the
        VCCs assigned to the subarray whose number is specified by
        the attribute name (last two ciphres)
        """
        try:
            attr_name = attr.get_name()
            sub_id = int(attr_name[-2:])
            attr.set_value(
                self.component_manager.get_vcc_data_of_subarray(sub_id)
            )
        except AttributeError as error:
            self.logger.warning("Attribute error: %s", error)

    # -----------------
    # Device Properties
    # -----------------

    ConnectionTimeout = device_property(dtype="DevUShort", default_value=60)

    PingConnectionTime = device_property(dtype="DevUShort", default_value=5)

    CspCbf = device_property(
        dtype="DevString",
    )

    # ----------
    # Attributes
    # ----------
    @attribute(
        dtype="uint16",
        label="vccsDeployed",
        doc="Number of VCCs deployed",
    )
    def vccsDeployed(self: MidCspCapabilityVcc) -> int:
        """Read number of deployed VCCs"""
        return self.component_manager.devices_deployed

    @attribute(
        dtype=("str",),
        max_dim_x=197,
        label="vccFqdn",
        doc="List of FQDNs of all VCC devices",
    )
    def vccFqdn(self: MidCspCapabilityVcc) -> List[str]:
        """Read list of fqdn for VCCs"""
        return self.component_manager.devices_fqdn

    @attribute(
        dtype=("str",),
        max_dim_x=197,
        label="vccState",
        doc="List of operational state for all VCCs.",
    )
    def vccState(self: MidCspCapabilityVcc) -> List[DevState]:
        """Read list of state for VCCs"""
        return self.component_manager.devices_state

    @attribute(
        dtype=("str",),
        max_dim_x=197,
        label="vccHealthState",
        doc="List of HealthState for all VCCs",
    )
    def vccHealthState(self: MidCspCapabilityVcc) -> List[str]:
        """Read list of health state for VCCs"""
        return self.component_manager.devices_health_state

    @attribute(
        dtype=("str",),
        max_dim_x=197,
        label="vccAdminMode",
        doc="List of administration mode for all VCCs.",
    )
    def vccAdminMode(self: MidCspCapabilityVcc) -> List[str]:
        """Read list of admin mode for VCCs"""
        return self.component_manager.devices_admin_mode

    @attribute(
        dtype=("str",),
        max_dim_x=197,
        label="vccSimulationMode",
        doc="List of simulation mode for all VCCs.",
    )
    def vccSimulationMode(self: MidCspCapabilityVcc) -> List[str]:
        """Read list of simulation mode for VCCs"""
        return self.component_manager.devices_simulation_mode

    @attribute(
        dtype=("str",),
        max_dim_x=197,
        label="vccReceptorId",
        doc="Receptor ID",
    )
    def vccReceptorId(self: MidCspCapabilityVcc) -> List[str]:
        """Read VCC receptor id"""
        return self.component_manager.devices_receptor_id

    @attribute(
        dtype=("int",),
        max_dim_x=197,
        label="vccSubarrayMembership",
        doc="Subarray membership",
    )
    def vccSubarrayMembership(self: MidCspCapabilityVcc) -> List[int]:
        """Read VCC subarray membership"""
        return self.component_manager.devices_subarray_membership

    @attribute(
        dtype=("str",),
        max_dim_x=197,
        label="vccBandActive",
        doc="List of the active band lable for all the VCCs."
        "Following the list of the available string: "
        "['1', '2', '3', '4', '5a', '5b']",
    )
    def vccBandActive(self: MidCspCapabilityVcc) -> List[str]:
        """Read VCC band active"""
        return self.component_manager.devices_band_active

    @attribute(
        dtype="str",
        label="vccJson",
        doc="Json containing the aggregated data for all VCCs",
    )
    def vccJson(self: MidCspCapabilityVcc) -> str:
        """Read json which contain raw VCC information."""
        # See example below:
        # {
        #   "vccs_deployed": 2,
        #   "vcc_fqdn_member_order" : [1,2,3,4],
        #   "vcc": [
        #     {
        #       "vcc_id": 0,
        #       "receptor_id": 'SKA001',
        #       "fqdn": "xxx.yyy.zzz",
        #       "state": "ON",
        #       "band_available": [2,],
        #       "band_active": "2",
        #       "health_state": "OK",
        #       "admin_mode": "ONLINE",
        #       "simulation_mode": "TRUE",
        #       "subarray_membership": 1
        #     },
        #     {
        #       "vcc_id": 1,
        #       "receptor_id": 'SKA022',
        #       "fqdn": "xxx.yyy.zzz",
        #       "state": "ON",
        #       "band_available": [2,],
        #       "band_active": "2",
        #       "health_state": "OK",
        #       "admin_mode": "ONLINE",
        #       "simulation_mode": "TRUE",
        #       "subarray_membership": 1
        #     },
        #   ]
        # }
        return self.component_manager.devices_json

    @attribute(
        dtype="DevBoolean",
        label="isCommunicating",
        doc="Whether the device is communicating with the component "
        "under control",
    )
    def isCommunicating(self: MidCspCapabilityVcc) -> bool:
        """Whether the TANGO device is communicating with the controlled
        component."""
        return self.component_manager.is_communicating

    @attribute(
        dtype=AdminMode,
        memorized=True,
        hw_memorized=True,
        label="AdminMode",
        doc=(
            "The admin mode reported for this device. It may interpret "
            "the current device condition and condition of all managed "
            "devices to set this. Most possibly an aggregate attribute."
        ),
    )
    def adminMode(self: MidCspCapabilityVcc) -> AdminMode:
        """
        Read the Admin Mode of the device.

        It may interpret the current device condition and condition of all
        managed devices to set this. Most possibly an aggregate attribute.

        :return: Admin Mode of the device
        """
        return self.component_manager._admin_mode

    @adminMode.write  # type: ignore[no-redef]
    def adminMode(self: MidCspCapabilityVcc, value: AdminMode) -> None:
        # pylint: disable=arguments-differ
        # The argument are the same of SKABaseDevice,
        # but pylint think we are inheriting by the attribute definition,
        # that has the same name method and one argument.
        # This is due to the pytango syntax.
        """
        Set the Admin Mode of the device.

        :param value: Admin Mode of the device.

        :raises ValueError: for unknown adminMode
        """
        if value == AdminMode.ONLINE:
            self.component_manager._admin_mode = value
            self.component_manager.start_communicating()
        else:
            raise ValueError("This device does not allow setting AdminMode")

    @attribute(
        dtype=(str,),
        max_dim_x=197,
        label="vccsSwVersion",
        doc="online vccs software version attribute list",
    )
    def vccsSwVersion(self: MidCspCapabilityVcc) -> List[str]:
        """Return the online vccs software version attribute list.

        :return: the online vccs software version attribute list.
        """
        return self.component_manager.devices_sw_version

    @attribute(
        dtype=(str,),
        max_dim_x=197,
        label="vccsHwVersion",
        doc="online vccs hardware version attribute list",
    )
    def vccsHwVersion(self: MidCspCapabilityVcc) -> List[str]:
        """Return the online vccs hardware version attribute list.

        :return: the online vccs hardware version attribute list.
        """
        return self.component_manager.devices_hw_version

    @attribute(
        dtype=(str,),
        max_dim_x=197,
        label="vccsFwVersion",
        doc="online vccs firmare version attribute list",
    )
    def vccsFwVersion(self: MidCspCapabilityVcc) -> List[str]:
        """Return the online vccs firmare version attribute list.

        :return: the online vccs firmware version attribute list.
        """
        return self.component_manager.devices_fw_version

    @attribute(
        dtype=(str,),
        max_dim_x=197,
        label="vccFqdnMemberOrder",
        doc="List of FQDN member order",
    )
    def vccFqdnMemberOrder(self: MidCspCapabilityVcc) -> List[str]:
        """
        Return the VCC fqdn order.It is the map order for all
        property lists.
        """
        return self.component_manager.devices_fqdn_member_order

    @attribute(
        dtype=str,
        label="vccsFqdnAssigned",
        doc="List of the assigned VCC FQDNs for all subarrays",
    )
    def vccsFqdnAssigned(self: MidCspCapabilityVcc) -> str:
        """
        Return a string with the assigned VCC fqdn for each
        subarray.
        """
        return self.component_manager.devices_fqdn_assigned


def main(args=None, **kwargs):
    """
    Launch an SKABaseDevice device.

    :param args: positional args to tango.server.run
    :param kwargs: named args to tango.server.run
    """
    return run((MidCspCapabilityVcc,), args=args, **kwargs)


if __name__ == "__main__":
    main()
