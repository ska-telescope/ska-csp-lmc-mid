# -*- coding: utf-8 -*-
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
"""Mid CSP Capability FSP"""
from __future__ import annotations

import logging
import queue
from typing import Any, List, Tuple

import tango
from ska_control_model import AdminMode, HealthState, ResultCode
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.model.op_state_model import OpStateModel
from ska_tango_base.base import SKABaseDevice

# Tango imports
from tango import AttrWriteType, DevState
from tango.server import attribute, device_property, run

from ska_csp_lmc_mid.manager.mid_fsp_capability_component_manager import (
    MidFspCapabilityComponentManager,
)

module_logger = logging.getLogger(__name__)

# pylint: disable=logging-format-interpolation
# pylint: disable=logging-fstring-interpolation
# pylint: disable=attribute-defined-outside-init
# pylint: disable=protected-access
# pylint: disable=broad-except


# To be removed when integrated with component manager
# pylint: disable=no-member
class MidCspCapabilityFsp(SKABaseDevice):
    """
    Mid CSP Capability FSP device
    Aggregates FSP information and presents to higher level devices
    """

    # pylint: disable=too-many-public-methods
    @attribute(dtype="str")
    def buildState(self: MidCspCapabilityFsp) -> str:
        """
        Read the Build State of the device.

        :return: the build state of the device
        """
        return self.component_manager._csp_build_state

    @attribute(dtype="str")
    def versionId(self: MidCspCapabilityFsp) -> str:
        """
        Return the SW version of the current device.

        The SW version is equal to the release number
        of the CSP Helm chart and also of the containerized
        image.

        :return: the version id of the device
        """
        return self.component_manager._csp_version

    class InitCommand(SKABaseDevice.InitCommand):
        """Class to handle the CSP.LMC Controller Init command."""

        def do(
            self: MidCspCapabilityFsp.InitCommand,
            *args: Any,
            **kwargs: Any,
        ) -> Tuple[ResultCode, str]:
            """Invoke the *init* command on the Controller ComponentManager.

            :return: a tuple with the command result code and an informative
                message.
            """
            # NOTE: in InitCommand parent class, the healthState of the device
            # is set to OK. To make the HealthStateModel work fine, the initial
            # value of the healthState must be set to OK, too.
            super().do()
            # device._health_state = HealthState.UNKNOWN
            # This is a temporary line to make the component_manager able to
            # manage old initialization
            self._device.component_manager.init()
            self._device._admin_mode = AdminMode.ONLINE
            return ResultCode.OK, "Mid FspCapability init command finished"

    def init_device(self: MidCspCapabilityFsp):
        """
        Initialise the tango device after startup.
        """
        self._omni_queue = queue.Queue()
        self._health_state = HealthState.UNKNOWN
        self.component_manager = None
        self.subarray_deployed = 16
        super().init_device()

    def initialize_dynamic_attributes(self: MidCspCapabilityFsp):
        """This method will be called automatically by the device_factory
        for each device.
        Within this method  all the dynamic attributes are created.
        This method is called once, when the device starts

        """
        for sub_id in range(1, self.subarray_deployed + 1):
            attr_name = f"fspsSubarray{sub_id:02d}"
            attr = attribute(
                name=attr_name,
                dtype=str,
                label=f"json with FSPs info for subarray{sub_id: 02d}",
                doc=(
                    f"Json containing the aggregated data for all FSPs of "
                    f"subarray{sub_id: 02d}"
                ),
                access=AttrWriteType.READ,
                fget=self._fsp_subarray_read,
                fset=None,
            )
            self.add_attribute(attr)
            self.set_change_event(attr_name, True)

    def delete_device(self: MidCspCapabilityFsp) -> None:
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        if not self.component_manager:
            return
        if self.component_manager.evt_manager:
            self.component_manager.evt_manager.stop()
            self.component_manager.evt_manager.unsubscribe_all_events()

    def _init_state_model(self: MidCspCapabilityFsp) -> None:
        """Override the health and operational State models:

        current CSP.LMC implementation does no longer rely on the SKA State
        models and associated State Machine library.
        """
        super()._init_state_model()
        # OpStateModel is not needed for the capability, but has to be there.
        # Othewise the BC rises an exception
        try:
            self.op_state_model = OpStateModel(
                DevState.INIT, self.update_device_attribute, self.logger
            )
        except Exception as e:
            self.logger.error(f"Debug Init Device: {e}")

        list_of_attributes = [
            "fspState",
            "fspHealthState",
            "fspAdminMode",
            "isCommunicating",
            "fspsFqdnAssigned",
            "fspsSubarrayMembership",
            "fspFunctionMode",
        ]

        for attr in list_of_attributes:
            self.set_change_event(attr, True)
            # self.set_archive_event(attr, True)

    def set_component_manager(
        self: MidCspCapabilityFsp,
        cm_configuration: ComponentManagerConfiguration,
    ) -> MidFspCapabilityComponentManager:
        """Configure the ComponentManager for the CSP Capability FSP device.

        :param cm_configuration: A class with all the device properties
            accessible as attributes

        :return: The CSP Capability FSP ComponentManager
        """
        return MidFspCapabilityComponentManager(
            cm_configuration,
            update_device_property_cbk=self.update_device_attribute,
            logger=self.logger,
        )

    def create_component_manager(
        self: MidCspCapabilityFsp,
    ) -> MidFspCapabilityComponentManager:
        """Override the base method.

        :returns: The CSP ControllerComponentManager
        """
        cm_configuration = ComponentManagerConfiguration(
            self.get_name(), self.logger
        )
        cm_configuration.add_attributes()
        return self.set_component_manager(cm_configuration)

    def update_device_attribute(
        self: MidCspCapabilityFsp, attr_name: str, attr_value: Any
    ) -> None:
        """General method invoked by the ComponentManager to push an event on a
        device attribute properly configured to push events from the device.

        :param attr_name: the TANGO attribute name
        :param attr_value: the attribute value
        """
        if attr_name == "state":
            if attr_value != self.get_state():
                self.set_state(attr_value)
                self.push_change_event("state", attr_value)
        elif attr_name == "healthstate":
            if attr_value != self._health_state:
                self._health_state = attr_value
                self.push_change_event("healthstate", attr_value)
        else:
            self.push_change_event(attr_name, attr_value)

    def _fsp_subarray_read(self, attr: tango.server.attribute):
        """Read method associated to the dynamic attributes.

        Return a JSON string with the information about all the
        VCCs assigned to the subarray whose number is specified by
        the attribute name (last two ciphres)
        """
        try:
            attr_name = attr.get_name()
            sub_id = int(attr_name[-2:])
            attr.set_value(
                self.component_manager.get_fsp_data_subarray(sub_id)
            )
        except AttributeError as error:
            self.logger.warning("Attribute error: %s", error)

    # -----------------
    # Device Properties
    # -----------------

    ConnectionTimeout = device_property(dtype="DevUShort", default_value=60)

    PingConnectionTime = device_property(dtype="DevUShort", default_value=5)

    CspCbf = device_property(
        dtype="DevString",
    )

    # ----------
    # Attributes
    # ----------
    @attribute(
        dtype="uint16", label="fspsDeployed", doc="Number of FSPs deployed"
    )
    def fspsDeployed(self: MidCspCapabilityFsp) -> int:
        """Read number of deployed FSPs"""
        return self.component_manager.devices_deployed

    @attribute(
        dtype=("str",),
        max_dim_x=27,
        label="fspFqdn",
        doc="List of FQDNs of all FSP devices",
    )
    def fspFqdn(self: MidCspCapabilityFsp) -> List[str]:
        """Read list of fqdn for FSPs"""
        return self.component_manager.devices_fqdn

    @attribute(
        dtype=("str",),
        max_dim_x=27,
        label="fspFunctionMode",
        doc="List of function mode for all FSPs",
    )
    def fspFunctionMode(self: MidCspCapabilityFsp) -> List[str]:
        """Read list of function mode for FSPs"""
        return self.component_manager.devices_function_mode

    @attribute(
        dtype=("str",),
        max_dim_x=27,
        label="fspState",
        doc="List of operational state for all FSPs.",
    )
    def fspState(self: MidCspCapabilityFsp) -> List[DevState]:
        """Read list of state for FSPs"""
        return self.component_manager.devices_state

    @attribute(
        dtype=("str",),
        max_dim_x=27,
        label="fspHealthState",
        doc="List of HealthState for all FSPs",
    )
    def fspHealthState(self: MidCspCapabilityFsp) -> List[str]:
        """Read list of health state for FSPs"""
        return self.component_manager.devices_health_state

    @attribute(
        dtype=("str",),
        max_dim_x=27,
        label="fspAdminMode",
        doc="List of administration mode for all FSPs.",
    )
    def fspAdminMode(self: MidCspCapabilityFsp) -> List[str]:
        """Read list of admin mode for FSPs"""
        return self.component_manager.devices_admin_mode

    @attribute(
        dtype=("uint16",),
        max_dim_x=27,
        label="fspAvailable",
        doc="List of non-programmed FSP IDs with State.ON",
    )
    def fspAvailable(self: MidCspCapabilityFsp) -> List[int]:
        """Read list of available FSPs IDs,
        meaning function_mode=idle and state=ON
        """
        return self.component_manager.devices_available

    @attribute(
        dtype=("uint16",),
        max_dim_x=27,
        label="fspUnavailable",
        doc="List of FSP IDs with state != State.ON",
    )
    def fspUnavailable(self: MidCspCapabilityFsp) -> List[int]:
        """Read list of unavailable FSPs IDs, meaning state!=ON"""
        return self.component_manager.devices_unavailable

    @attribute(
        dtype=("uint16",),
        max_dim_x=27,
        label="fspPss",
        doc="List of FSP IDs programmed in PSS Functionmode",
    )
    def fspPss(self: MidCspCapabilityFsp) -> List[int]:
        """Read list of FSPs IDs which are in function_mode=PSS"""
        return self.component_manager.devices_pss

    @attribute(
        dtype=("uint16",),
        max_dim_x=27,
        label="fspPst",
        doc="List of FSP IDs programmed in PST Functionmode",
    )
    def fspPst(self: MidCspCapabilityFsp) -> List[int]:
        """Read list of FSPs IDs which are in function_mode=PST"""
        return self.component_manager.devices_pst

    @attribute(
        dtype=("uint16",),
        max_dim_x=27,
        label="fspCorrelation",
        doc="List of FSP IDs programmed in correlation function mode",
    )
    def fspCorrelation(self: MidCspCapabilityFsp) -> List[int]:
        """Read list of FSPs IDs which are in function_mode=Correlation"""
        return self.component_manager.devices_correlation

    @attribute(
        dtype=("str",),
        label="fspsSubarrayMembership",
        max_dim_x=16,
        doc="FSPs subarray membership",
    )
    def fspsSubarrayMembership(self: MidCspCapabilityFsp) -> List[str]:
        """Return the  FSPs subarray membership as a List of strings.
        Each string contains the IDs of the subarrays to which a FSP belongs
        to.
        """
        return self.component_manager.devices_subarray_membership

    @attribute(
        dtype=(("str",),),
        label="fspsFqdnAssigned",
        max_dim_x=4,
        max_dim_y=16,
        doc="FQDNs of the assigned FSPs",
    )
    def fspsFqdnAssigned(self: MidCspCapabilityFsp) -> str:
        """Return the  FQDNs of the FSPs assigned to subarrays.

        The value is a matrix of dim 16*4 where in each row is reported the
        list of the FQDNs of the FSPs belonging to the subarray whose
        id = nrow * 1
        """
        return self.component_manager.devices_fqdn_assigned

    @attribute(
        dtype="str",
        label="fspJson",
        doc="Json containing the aggregated data for all FSPs",
    )
    def fspJson(self) -> str:
        """Read json which contain raw FSP information."""
        # See example below:
        # {
        #   "fsps_deployed": 2,
        #   "fsp": [
        #     {
        #       "fsp_id": 0,
        #       "fqdn": "xxx.yyy.zzz",
        #       "function_mode": "Idle",
        #       "state": "ON",
        #       "health_state": "OK",
        #       "admin_mode": "ONLINE",
        #       "subarray_membership": [3, 5, 8]
        #     },
        #     {
        #       "fsp_id": 1,
        #       "fqdn": "xxx.yyy.zzz",
        #       "function_mode": "Idle",
        #       "state": "ON",
        #       "health_state": "OK",
        #       "admin_mode": "ONLINE",
        #       "subarray_membership": [3, 5, 8]
        #     },
        #   ]
        # }

        return self.component_manager.devices_json

    @attribute(
        dtype="DevBoolean",
        max_dim_x=27,
        label="isCommunicating",
        doc="Whether the device is communicating with the component "
        "under control",
    )
    def isCommunicating(self: MidCspCapabilityFsp) -> bool:
        """Whether the TANGO device is communicating with the controlled
        component."""
        return self.component_manager.is_communicating

    @attribute(
        dtype=AdminMode,
        memorized=True,
        hw_memorized=True,
        label="AdminMode",
        doc=(
            "The admin mode reported for this device. It may interpret "
            "the current device condition and condition of all managed "
            "devices to set this. Most possibly an aggregate attribute."
        ),
    )
    def adminMode(self: MidCspCapabilityFsp) -> AdminMode:
        """
        Read the Admin Mode of the device.

        It may interpret the current device condition and condition of all
        managed devices to set this. Most possibly an aggregate attribute.

        :return: Admin Mode of the device
        """
        return self.component_manager._admin_mode

    @adminMode.write  # type: ignore[no-redef]
    def adminMode(self: MidCspCapabilityFsp, value: AdminMode) -> None:
        # pylint: disable=arguments-differ
        # The argument are the same of SKABaseDevice,
        # but pylint think we are inheriting by the attribute definition,
        # that has the same name method and one argument.
        # This is due to the pytango syntax.
        """
        Set the Admin Mode of the device.

        :param value: Admin Mode of the device.

        :raises ValueError: for unknown adminMode
        """
        if value == AdminMode.ONLINE:
            self.component_manager._admin_mode = value
            self.component_manager.start_communicating()
        else:
            raise ValueError("This device does not allow setting AdminMode")

    @attribute(
        dtype=(str,),
        max_dim_x=27,
        label="fspsSwVersion",
        doc="online fsps software version attribute list",
    )
    def fspsSwVersion(self: MidCspCapabilityFsp) -> List[str]:
        """Return the online fsps software Version attribute list.

        :return: the online fsps software Version attribute list.
        """
        return self.component_manager.devices_sw_version

    @attribute(
        dtype=(str,),
        max_dim_x=27,
        label="fspsHwVersion",
        doc="online fsps software hardware attribute list",
    )
    def fspsHwVersion(self: MidCspCapabilityFsp) -> List[str]:
        """Return the online fsps hardware Version attribute list.

        :return: the online fsps hardware Version attribute list.
        """
        return self.component_manager.devices_hw_version

    @attribute(
        dtype=(str,),
        max_dim_x=27,
        label="fspsFwVersion",
        doc="online fsps software firmare attribute list",
    )
    def fspsFwVersion(self: MidCspCapabilityFsp) -> List[str]:
        """Return the online fsps firmare Version attribute list.

        :return: the online fsps firmware Version attribute list.
        """
        return self.component_manager.devices_fw_version


def main(args=None, **kwargs):
    """
    Launch an SKABaseDevice device.

    :param args: positional args to tango.server.run
    :param kwargs: named args to tango.server.run
    """
    return run((MidCspCapabilityFsp,), args=args, **kwargs)


if __name__ == "__main__":
    main()
