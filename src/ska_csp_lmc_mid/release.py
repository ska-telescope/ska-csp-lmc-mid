# -*- coding: utf-8 -*-
#
# This file is part of the CentralNode project
#
#
#
# Distributed under the terms of the BSD-3-Clause license.
# See LICENSE.txt for more info.

"""Release information for Python Package."""

# pylint: disable=redefined-builtin
# pylint: disable=invalid-name

name = """ska-csp-lmc-mid"""
version = "1.0.0-rc.3"
version_info = version.split(".")
description = """SKA MID CSP.LMC"""
author = "INAF-OAA"
author_email = "elisabetta.giani@inaf.it", "gianluca.marotta@inaf.it"
license = """BSD-3-Clause"""
url = """https://gitlab.com/ska-telescope/ska-csp-lmc-mid.git"""
copyright = """INAF, SKA Telescope"""
