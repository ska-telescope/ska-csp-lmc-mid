"""FSP Capability Device Component Manager Class."""

from __future__ import annotations  # allow forward references in type hints

import json
import logging
from typing import Callable, List, Optional

from ska_csp_lmc_common.capability import CapabilityComponentManager
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)

from ska_csp_lmc_mid import release

# from ska_csp_lmc_mid.constant import FspModes
from ska_csp_lmc_mid.manager.mid_fsp_data_handler import FspDataHandler

# pylint: disable=unused-argument
# pylint: disable=too-many-arguments
# pylint: disable=broad-except
# pylint: disable=logging-fstring-interpolation
# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-public-methods

module_logger = logging.getLogger(__name__)

"""
Json with the FSP information

"fsps_deployed": 4,
  "fsp": [
    {
      "fsp_id": 1,
      "fqdn": "xxx.yyy.zzz",
      "function_mode": "IDLE",
      "state": "ON",
      "health_state": "OK",
      "admin_mode": "ONLINE",
      "subarry_membership": ""
    },
    {
      "fsp_id": 2,
      "fqdn": "xxx.yyy.zzz",
      "function_mode": "CORR",
      "state": "ON",
      "health_state": "OK",
      "admin_mode": "ONLINE",
      "subarry_membership": "1,3,8"
    },
    {
      "fsp_id": 3,
      "fqdn": "xxx.yyy.zzz",
      "function_mode": "IDLE",
      "state": "ON",
      "health_state": "OK",
      "admin_mode": "ONLINE",
      "subarry_membership": ""
    },
    {
      "fsp_id": 4,
      "fqdn": "xxx.yyy.zzz",
      "function_mode": "CORR",
      "state": "ON",
      "health_state": "OK",
      "admin_mode": "ONLINE",
      "subarry_membership": "3,5,8"
    }
  ]
"""


class MidFspCapabilityComponentManager(CapabilityComponentManager):
    """Class for Mid Csp Fsp Capability Component Manager."""

    def __init__(
        self: MidFspCapabilityComponentManager,
        properties: ComponentManagerConfiguration,
        update_device_property_cbk: Optional[Callable] = None,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """Initialize the FspCapability Component Manager Class

        :param properties: A class instance whose properties are the
            device properties.
        :param max_workers: the number of worker threads used by the
            ComponentManager ThreadPool
        :param update_device_property_cbk: The device method invoked
            to update the attributes. Defaults to None.
        :param logger: The device or python logger if default is None.
        """
        super().__init__(
            properties=properties,
            update_device_property_cbk=update_device_property_cbk,
            logger=logger,
        )

        self.device_properties = properties
        self._init_version_info(release)

    @property
    def devices_function_mode(
        self: MidFspCapabilityComponentManager,
    ) -> List[str]:
        """
        Return the list of FSPs function modes.
        """
        return self._data_handler.get_device_dict_entry("function_mode")
        # return self._data_handler.list_of_function_mode

    @property
    def devices_available(self: MidFspCapabilityComponentManager) -> List[int]:
        """
        Return the list of available FSPs.
        A FSP is available when its state is ON and the functionMode is IDLE.
        """
        return self._data_handler.fsp_in_function_mode("IDLE")

    @property
    def devices_correlation(
        self: MidFspCapabilityComponentManager,
    ) -> List[int]:
        """
        Return the list of FSPs programmed for correlation.
        """
        return self._data_handler.fsp_in_function_mode("CORR")

    @property
    def devices_pss(self: MidFspCapabilityComponentManager) -> List[int]:
        """
        Return the list of FSPs programmed for pss beam-forming.
        """
        return self._data_handler.fsp_in_function_mode("PSS_BF")

    @property
    def devices_pst(self: MidFspCapabilityComponentManager) -> List[int]:
        """
        Return the list of FSPs programmed for pst beam-forming.
        """
        return self._data_handler.fsp_in_function_mode("PST_BF")

    @property
    def devices_unavailable(
        self: MidFspCapabilityComponentManager,
    ) -> List[int]:
        """
        Return the list of FSPs not available.
        A FSP is not available when its state is not ON.
        """
        return self._data_handler.unavailable

    @property
    def devices_subarray_membership(
        self: MidFspCapabilityComponentManager,
    ) -> list[str]:
        """
        Return the FSPs subarray membership.

        :return: a list of strings. Each string reports the subarray IDs to
                which a FSP belongs to.

        note:
            Example:

            fsp_num  = list_idx  + 1

            ["1,2", "1,3,4", "", ""]

            FSP 1 is assigned to subarrays 1 and 2
            FSP 2 is assigned to subarrays 1,3,4
            The other FSPs are not assigned at all.
        """
        # return name of the band
        # return self._data_handler.get_fsp_values("subarray_membership")
        return self._data_handler.get_device_dict_entry("subarray_membership")

    @property
    def devices_fqdn_assigned(
        self: MidFspCapabilityComponentManager,
    ) -> List[List[str]]:
        """
        Return the nested list of the FSPs' fqdn assigned to all subarrays.
        note:
            Examples:
            fsps_fqdn_assigned =
            [
                ['mid_csp_cbf/fsp/01', '','',''],
                ['', 'mid_csp_cbf/fsp/03','',''],
                ['', '','',''],
                ['mid_csp_cbf/fsp/02','mid_csp_cbf/fsp/02','',''],
                ....
            ]
            fsps_fqdn_assigned[0] = FQDNs of the FSPs assigned to subarray1
            fsps_fqdn_assigned[1] = FQDNs of the FSPs assigned to subarray2
        """

        return self._data_handler.subarray_membership_fqdn.tolist()

    @property
    def devices_deployed(self: CapabilityComponentManager) -> int:
        """
        Return the number of devices deployed in the system.
        """
        return self._data_handler.json_dict["fsps_deployed"]

    @property
    def subarray_fsp_map(self: MidFspCapabilityComponentManager) -> List[str]:
        """
        Return the list of FSPs state.
        """
        return self._data_handler.assigned_fsps_to_subarray_map

    #
    # Protected Methods
    #
    def _create_init_callbacks(self: MidFspCapabilityComponentManager):
        """Method not used by this device"""
        self.logger.info("Method not used")

    def _create_stop_callbacks(self: MidFspCapabilityComponentManager):
        """Method not used by this device"""

    def _instantiate_data_handler(
        self: MidFspCapabilityComponentManager,
        logger: Optional[logging.Logger] = None,
    ):
        """Instantiate the specific data handler that is meant to
        update the capability attributes"""
        return FspDataHandler(self._update_device_property_cbk, self.logger)

    def _populate_capability_specifics(
        self: MidFspCapabilityComponentManager,
    ) -> None:
        """Specify the specific variables of the Capability"""

        self.master_device_name = "cbf-controller"
        self.property_master_fqdn = "CspCbf"
        self.master_attribute_name = "FSP"
        self.subsystem_name = "fsp-"

        self.attributes_to_be_subscribed.extend(
            [
                # "simulationmode",
                "functionmode",
                "subarraymembership",
                # "receptorID": 0,
            ]
        )

    def _retrieve_devices_fqdn(self: MidFspCapabilityComponentManager) -> None:
        """
        Retrieve the FQDNs of the subsystem devices to be connected
        By default, the fqdn list can be retrieved directly from
        `master device`. Otherwise the method has to be specialized

        return: the list of the subsistems FQDN deployed
        """
        devices_fqdn = []
        try:
            self._connect_to_master_device(
                self.property_master_fqdn, self.master_device_name
            )
            self.logger.info(
                f"Connection to {self.master_device_name} succeeded"
            )
            fsp_properties = self.master_dev.proxy.get_property(
                self.master_attribute_name
            )
            devices_fqdn = fsp_properties["FSP"]
            self.logger.info(
                "List of deployed subsystems retrieved: " f"{devices_fqdn}"
            )
        except Exception as exc:
            raise ValueError(
                f"Connection to {self.master_device_name} failed"
            ) from exc
        return devices_fqdn

    def _read_device_id(
        self: MidFspCapabilityComponentManager, device_online
    ) -> None:
        """Retrieve the ID of an online device.

        :param device_online: one of the online pst component
        """
        # for fqdn in device_online:
        #     fsp_id = int(fqdn[-2:])
        #     self._device_id.append(str(fsp_id))

    def _read_device_version(
        self: MidFspCapabilityComponentManager, device_online
    ) -> None:
        """Read the Software, firmware and hardware version attributes
          from an online FSP.

        :param device_online: one of the online FSP
        """
        sw_version = "NA"
        hw_version = "NA"
        fw_version = "NA"
        try:
            sw_version = device_online.read_attr("versionId")
            # sw_version = device_online.proxy.versionId
        except Exception:
            self.logger.warning(
                f"FSP {device_online} has not the version attribute"
            )
        try:
            # fw_version = device_online.proxy.fw_version
            fw_version = device_online.read_attr("fwVersion")
        except Exception:
            self.logger.warning(
                f"FSP {device_online} has not the fw_version attribute"
            )
        try:
            # fw_version = device_online.proxy.fw_version
            hw_version = device_online.read_attr("hwVersion")
        except Exception:
            self.logger.warning(
                f"FSP {device_online} has not the hw_version attribute"
            )
        self._sw_version.append(sw_version)
        self._hw_version.append(hw_version)
        self._fw_version.append(fw_version)

    def off(
        self: MidFspCapabilityComponentManager,
        task_callback: Optional[Callable] = None,
    ):
        """Method not used by this device"""
        self.logger.info("Method not used")

    def on(
        self: MidFspCapabilityComponentManager,
        task_callback: Optional[Callable] = None,
    ):
        """Method not used by this device"""
        self.logger.info("Method not used")

    def standby(
        self: MidFspCapabilityComponentManager,
        task_callback: Optional[Callable] = None,
    ):
        """Method not used by this device"""
        self.logger.info("Method not used")

    def stop_communicating(
        self: MidFspCapabilityComponentManager,
        task_callback: Optional[Callable] = None,
    ):
        """Method not used by this device"""
        self.logger.info("Method not used")

    def abort_commands(
        self: MidFspCapabilityComponentManager,
        task_callback: Optional[Callable] = None,
    ):
        """Method not used by this device"""
        self.logger.info("Method not used")

    def get_fsp_data_subarray(
        self: MidFspCapabilityComponentManager, subarray_num
    ) -> str:
        """
        Return string attributes from FSPs assigned to requested
        subarray.
        .. note::

            The dictionary is in the following format:
                {'fsp_01':{"state": DevState.OFF,
                        "health_state": 'UNKNOWN',
                        "function_mode": 'IDLE'}
                'fsp_02':{"state": DevState.OFF,
                        "health_state": 'UNKNOWN',
                        "function_mode": 'IDLE'}}
        """
        try:
            subarray_data = self._data_handler.assigned_fsps_to_subarray_map[
                subarray_num
            ]
            return json.dumps(subarray_data)
        except KeyError as err:
            self.logger.warning(
                f"Error in getting fsp data of subarray {subarray_num}: {err}"
            )
            return "{}"
        except TypeError as e:
            self.logger.error(f"Error in serialize subarray_data JSON: {e}")
            return ""
