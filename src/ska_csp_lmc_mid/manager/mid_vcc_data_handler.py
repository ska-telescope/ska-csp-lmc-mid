"""VCC Capability data handler Class."""
from __future__ import annotations  # allow forward references in type hints

import json
import logging
import threading
from typing import Any, Callable, Dict, List, Optional

from ska_control_model import AdminMode, HealthState, SimulationMode
from tango import DevState

from ska_csp_lmc_mid.constant import convert_band_to_string

module_logger = logging.getLogger(__name__)

# pylint: disable=broad-except
# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-nested-blocks
# pylint: disable=logging-fstring-interpolation


class VccDataHandler:
    """
    Class handling the update and access to the dictionary with the
    overall VCCs status.
    """

    def __init__(
        self: VccDataHandler,
        task_callback: Optional[Callable] = None,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """
        Class to handle the VCCs information.

        :param task_callback: the method invoked to update a device
            attribute
        :param logger: The device or python logger if default is None.
        """
        self._update_device_property = task_callback
        self._json_dict = {}
        self._vcc_fqdn_assigned = {}
        self.assigned_vccs_to_subarray_map = {}
        self.list_of_state = []
        self.list_of_health_state = []
        self.list_of_admin_mode = []
        self.list_of_simulation_mode = []
        self.logger = logger or module_logger
        self._data_lock = threading.Lock()
        self.attrname_to_enum_class_map = {
            "healthstate": HealthState,
            "adminmode": AdminMode,
            "simulationmode": SimulationMode,
        }
        self.attrname_to_key_map = {
            "state": "state",
            "healthstate": "health_state",
            "adminmode": "admin_mode",
            "subarraymembership": "subarray_membership",
            "simulationmode": "simulation_mode",
            "frequencyband": "band_active",
            "receptorid": "receptor_id",
        }

    @property
    def json_dict(self: VccDataHandler) -> Dict:
        """
        Return the internal dictionary.
        """
        return self._json_dict

    @property
    def fqdn(self: VccDataHandler) -> List[str]:
        """
        Return a list with the VCCs FQDNs.
        """
        return self._get_device_dict_entry("fqdn")

    @property
    def vccs_fqdn_assigned(self: VccDataHandler) -> str:
        """
        Return the list with the VCCs FQDNs assigned to
        all subarrays.
        """
        return self._vcc_fqdn_assigned

    @property
    def vcc_fqdn_member_order(
        self: VccDataHandler,
    ):
        """Return the ordered list of the VCCs fqdn member (=vcc id)"""
        return self._json_dict["vcc_fqdn_member_order"]

    #
    # Class public methods
    #
    def init(self: VccDataHandler, list_of_vcc_devices: List[str]) -> None:
        """
        Initialize the internal VCC json dictionary.
        """
        self._json_dict["vccs_deployed"] = len(list_of_vcc_devices)
        self._json_dict["vcc"] = []

        vcc_fqdn_member_order = []
        # re-order list of device (list_of_vcc_devices) according
        # to fqdn member (growing)

        list_of_vcc_devices = sorted(list_of_vcc_devices)
        vcc_fqdn_member = 0
        for vcc_fqdn in list_of_vcc_devices:
            vcc_fqdn_member = int(vcc_fqdn[-3:])
            # create map for array lists
            vcc_fqdn_member_order.append(vcc_fqdn_member)
            vcc_dict = {
                "vcc_id": vcc_fqdn_member,
                "band_active": "",
                "receptor_id": "DIDINV",
                "band_available": [""],
                "fqdn": vcc_fqdn,
                "state": DevState.UNKNOWN,
                "health_state": HealthState.UNKNOWN.value,
                "admin_mode": AdminMode.OFFLINE.value,
                "simulation_mode": SimulationMode.FALSE.value,
                "subarray_membership": 0,
            }
            self._json_dict["vcc"].append(vcc_dict)

        self._json_dict["vcc_fqdn_member_order"] = vcc_fqdn_member_order

    def get_device_dict_entry(
        self: VccDataHandler, dict_key: str
    ) -> List[str]:
        """
        Return the information stored in the dictionary for the
        given key.
        The access to the internal dictionary is regulated by a lock.

        :param dict_key: the dictionary key whose value is requested.

        :return: a list with the requested information
        """

        with self._data_lock:
            return self._get_device_dict_entry(dict_key)

    def _get_device_dict_entry(
        self: VccDataHandler, dict_key: str
    ) -> List[str]:
        """
        Return the information stored in the dictionary for the
        given key.
        The access to the internal dictionary is regulated by a lock.

        :param dict_key: the dictionary key whose value is requested.

        :return: a list with the requested information
        """
        list_of_values = []
        if self._json_dict:
            for item in self._json_dict["vcc"]:
                try:
                    list_of_values.append(item[dict_key])
                except KeyError as e:
                    self.logger.warning(f"Invalid vcc dictionary key: {e}")
        return list_of_values

    def update_vcc_dict_entry(
        self: VccDataHandler,
        attr_name: str,
        entry_value: Any,
        vcc_pos: int = None,
    ) -> None:
        """Update the content of the internal dictionary.
        The access to the internal dictionary is controlled by a lock

        :param attr_name: the key name of the entry to update into the
        dictionary
        :param entry_value: the value to store
        :param vcc_pos: if not None, the index of the vcc record into the
        dictionary list
        """
        with self._data_lock:
            self._update_vcc_dict_entry(attr_name, entry_value, vcc_pos)

    def _update_vcc_dict_entry(
        self: VccDataHandler,
        attr_name: str,
        entry_value: Any,
        vcc_pos: int = None,
    ):
        """
        Update the content of the internal dictionary without locking.

        :param attr_name: the name of the attribute to update into the
            internal dictionary
        :param entry_value: the value to store
        :param vcc_pos: if not None, the index of the vcc record into
        the dictionary list
        """
        try:
            entry_key = self.attrname_to_key_map[attr_name]
            if vcc_pos is not None:
                # get the key dictionary associated with the attribute
                attr_name = [
                    _attr_name
                    for _attr_name, value in self.attrname_to_key_map.items()
                    if value == entry_key
                ]
                attr_name = attr_name[0]
                if attr_name in self.attrname_to_enum_class_map:
                    entry_value = self.attrname_to_enum_class_map[attr_name](
                        entry_value
                    ).name
                self._json_dict["vcc"][vcc_pos][entry_key] = entry_value
            else:
                self._json_dict[entry_key] = entry_value
        except KeyError as err:
            self.logger.warning(f"Failed to update vcc entry: {err}")

    def update(
        self: VccDataHandler, device_fqdn: str, attr_name: str, attr_value: Any
    ) -> None:
        """Method invoked when an event is received.

        Update the VCC internal json structure and invoke the device
        update method for the following attributes of the device:
        - vccDeployed
        - vccAdminMode
        - vccHealthState
        - vccState
        - vccSimulationMode

        :param device_fqdn: the VCC device FQDN
        :param attr_name: the name of the attribute with event
        :param attr_value: the value the attribute with event
        """

        with self._data_lock:
            # get the vcc position inside the internal dict
            vcc_idx = self.fqdn.index(device_fqdn)
            # get the vcc record
            item = self._json_dict["vcc"][vcc_idx]
            # update array element according to fqdn
            if device_fqdn == item["fqdn"]:
                vcc_id = item["vcc_id"]
                if attr_name == "state":
                    attr_value = str(attr_value)
                self._update_vcc_dict_entry(attr_name, attr_value, vcc_idx)
                if attr_name in {
                    "state",
                    "healthstate",
                    # "adminmode",
                    "simulationmode",
                }:

                    self._update_subarray_map_attributes(
                        vcc_id, attr_name, attr_value
                    )
                elif attr_name == "subarraymembership":
                    self._update_subarray_map(vcc_id, attr_value)
                    self._update_vcc_fqdn_assigned(attr_value, device_fqdn)
                elif attr_name == "frequencyband":
                    item["band_active"] = convert_band_to_string(attr_value)
                elif attr_name == "dishid":
                    item["receptor_id"] = attr_value
                elif attr_name == "adminmode":
                    # if the attribute is a defined enum, try to recover the
                    # label
                    attr_value = self.attrname_to_enum_class_map[attr_name](
                        attr_value
                    ).name
                    item["admin_mode"] = attr_value
                else:
                    raise ValueError("Items not updated!")

                func_name = f"_update_{attr_name.lower()}"
                try:
                    f = self.__class__.__dict__[func_name]
                    f(self)
                except (KeyError, AttributeError) as e:
                    self.logger.error(
                        f"Failure in invoking callback method: {e}"
                    )
            else:
                self.logger.warning(
                    f"Error in updating {attr_name}"
                    f" on device {device_fqdn}:"
                    f" entry mismatch received: {device_fqdn}"
                    f' extracted: {item["fqdn"]}'
                )

    #
    # Protected Methods
    #

    def _update_vcc_fqdn_assigned(
        self: VccDataHandler, subarray_id: int, device_fqdn: str
    ) -> None:
        """
        Update the internal list with the FQDNs of assigned VCCs.

        :param subarray_id: the subarray_id
        :device_fqdn: the VCC fqdn to add/remove
        """
        if subarray_id:
            try:
                _ = self._vcc_fqdn_assigned[subarray_id]
            except KeyError:
                self._vcc_fqdn_assigned[subarray_id] = []
            if device_fqdn not in self._vcc_fqdn_assigned[subarray_id]:
                self._vcc_fqdn_assigned[subarray_id].append(device_fqdn)
        else:
            for sub_id, fqdn_list in self._vcc_fqdn_assigned.items():
                if device_fqdn in fqdn_list:
                    self.logger.info(f"Removing {device_fqdn} from {sub_id}")
                    fqdn_list.pop(fqdn_list.index(device_fqdn))
                    self._vcc_fqdn_assigned[sub_id] = fqdn_list
        if self._update_device_property:
            self._update_device_property(
                "vccsFqdnAssigned", json.dumps(self._vcc_fqdn_assigned)
            )

    def _update_state(self: VccDataHandler) -> None:
        """
        Update the list with the VCCs state.

        This method is invoked when an event on the State
        attribute is received.
        """
        list_of_state = self._get_device_dict_entry("state")
        if list_of_state != self.list_of_state:
            self.list_of_state = list_of_state
            if self._update_device_property:
                self._update_device_property("vccState", list_of_state)

    def _update_healthstate(self: VccDataHandler) -> None:
        """
        Update the list with the VCCs health state.

        This method is invoked when an event on the healthState
        attribute is received.
        """
        list_of_health = self._get_device_dict_entry("health_state")
        if list_of_health != self.list_of_health_state:
            self.list_of_health_state = list_of_health
            if self._update_device_property:
                self._update_device_property("vccHealthState", list_of_health)

    def _update_adminmode(self: VccDataHandler) -> None:
        """
        Update the list with the VCCs admin mode.

        This method is invoked when an event on the adminMode
        attribute is received.
        """
        list_of_admin_mode = self._get_device_dict_entry("admin_mode")
        if list_of_admin_mode != self.list_of_admin_mode:
            self.list_of_admin_mode = list_of_admin_mode
            if self._update_device_property:
                self._update_device_property(
                    "vccAdminMode", list_of_admin_mode
                )

    def _update_simulationmode(self: VccDataHandler) -> None:
        """
        Update the list with the VCCs simulation mode.

        This method is invoked when an event on the simulationMode
        attribute is received.
        """
        list_of_simulation_mode = self._get_device_dict_entry(
            "simulation_mode"
        )
        if list_of_simulation_mode != self.list_of_simulation_mode:
            self.list_of_simulation_mode = list_of_simulation_mode
            if self._update_device_property:
                self._update_device_property(
                    "vccSimulationMode", list_of_simulation_mode
                )

    def _update_frequencyband(self: VccDataHandler) -> None:
        """
        Update the list with the VCCs frequency bands.

        This method is invoked when an event on the frequencyBand
        attribute is received.
        """
        list_of_band_active = self._get_device_dict_entry("band_active")
        if self._update_device_property:
            self._update_device_property("vccBandActive", list_of_band_active)

    def _update_receptorid(self: VccDataHandler) -> None:
        """
        Update the list with the VCCs receptor id.

        This method is invoked when an event on the State
        attribute is received.
        """
        list_of_receptor_id = self._get_device_dict_entry("receptor_id")
        if self._update_device_property:
            self._update_device_property("vccReceptorId", list_of_receptor_id)

    def _update_subarraymembership(self: VccDataHandler) -> None:
        """
        Update the list with the VCCs sub-array membership
        """
        list_of_subarray_membership = self._get_device_dict_entry(
            "subarray_membership"
        )
        if self._update_device_property:
            self._update_device_property(
                "vccSubarrayMembership", list_of_subarray_membership
            )

    def _update_subarray_map_attributes(
        self: VccDataHandler, vcc_id: int, attr_name: str, value: Any
    ) -> None:
        """Update the assigned_vccs_to_subarray_map attribute with the
        input value

        :param vcc_id: id of the VCC whose data have to be updated.
        :param attr_name: the name of the attribute whose value has
            to be updated inside the map.
        :param value: the value of the attribute that has to be updated
        """
        # retrieve the key dictionary associated to the attribute
        attr_key = self.attrname_to_key_map[attr_name]
        # if the attribute is a defined enum, try to recover the label
        if attr_name in self.attrname_to_enum_class_map:
            value = self.attrname_to_enum_class_map[attr_name](value).name
        try:
            device_id = f"vcc_{vcc_id:03d}"
            for (
                sub_id,
                device_dict,
            ) in self.assigned_vccs_to_subarray_map.items():
                if device_id in device_dict.keys():
                    self.assigned_vccs_to_subarray_map[sub_id][device_id][
                        attr_key
                    ] = value
                    if self._update_device_property:
                        self._update_device_property(
                            f"vccsSubarray{sub_id}",
                            json.dumps(
                                self.assigned_vccs_to_subarray_map[sub_id]
                            ),
                        )
                    break
        except Exception as err:
            self.logger.error(
                f" Error updating the subarray map attributes:" f"{err}"
            )

    def _update_subarray_map(
        self: VccDataHandler, vcc_id: int, subarray_id: int
    ) -> None:
        """
        update the VCC-json information devided for each subarray
        for a better usability of data.

        :param vcc_id: id of the VCC whose data have to be updated.
        :param subarray_id: the subarray whose data has to be updated
        """
        property_name = None
        property_value = ""
        subarray_map = self.assigned_vccs_to_subarray_map
        # extract the dictionary data for the specified device
        for device_item in self._json_dict["vcc"]:
            if device_item["vcc_id"] == vcc_id:
                try:
                    device_name = f"vcc_{vcc_id:03d}"
                    # update the subarray vcc-data dictionary
                    vcc_data_dict = {
                        device_name: {
                            "state": device_item["state"],
                            "health_state": device_item["health_state"],
                            "receptor_id": device_item["receptor_id"],
                            "admin_mode": device_item["admin_mode"],
                        }
                    }
                    if subarray_id > 0:
                        if subarray_id not in subarray_map:
                            subarray_map[subarray_id] = {}
                        subarray_map[subarray_id].update(vcc_data_dict)
                        property_name = f"vccsSubarray{subarray_id:02d}"
                        property_value = subarray_map[subarray_id]
                    else:
                        # remove the record of vcc_id from the subarray
                        # it belongs to
                        for sub_id, vcc_record in subarray_map.items():
                            if device_name in vcc_record:
                                subarray_map[sub_id].pop(device_name)
                                property_name = f"vccsSubarray{sub_id:02d}"
                                property_value = subarray_map[sub_id]
                    self.assigned_vccs_to_subarray_map = subarray_map
                    if self._update_device_property:
                        self._update_device_property(
                            property_name, json.dumps(property_value)
                        )
                except KeyError as e:
                    self.logger.warning("Invalid vcc dictionary key: %s", e)
                break
