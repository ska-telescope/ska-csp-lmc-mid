""" Class to handling connection to Mid CSP Capability devices"""
from __future__ import annotations

import threading
from typing import Any, List, Tuple

from ska_control_model import ResultCode, TaskStatus
from ska_csp_lmc_common.commands import LeafTask, Task, TaskExecutionType
from ska_csp_lmc_common.component import Component
from ska_csp_lmc_common.connector import Connector
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from tango import DevError

# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-arguments


class MidCapabilityConnectionManager:
    """Monitor class for Mid Capability"""

    def __init__(
        self: MidCapabilityConnectionManager,
        properties: ComponentManagerConfiguration,
        task_callback,
        logger,
    ):
        self._subscribe_callback = task_callback
        self.sub_id = -1
        self._is_communicating = False
        self.logger = logger
        self._device_properties = properties
        self.fsp_capability_fqdn = ""
        self.vcc_capability_fqdn = ""
        self._access_lock = threading.Lock()
        self._capabilities = {}

    @property
    def is_communicating(self: MidCapabilityConnectionManager) -> bool:
        """Return whether communication with the component is established.

        :return: whether there is currently a connection to the component.
        """
        with self._access_lock:
            return self._is_communicating

    @is_communicating.setter
    def is_communicating(self: MidCapabilityConnectionManager, value):
        with self._access_lock:
            self._is_communicating = value

    def set_sub_id(self: MidCapabilityConnectionManager, value):
        """Set the subarray id attribute."""
        self.sub_id = value

    def _retrieve_capability_fqdn(
        self: MidCapabilityConnectionManager,
    ) -> Tuple[str, str]:
        """
        Retrieve the FQDNs of the Capability devices from the `CspController`.

        :return: A tuple with the FQDNs for VCC and FSP capability devices.

        :raises: ValueError if the FQDNs cannot be retrieved.
        """
        try:
            csp_controller_fqdn = self._device_properties.CspController
            csp_connector = Connector(csp_controller_fqdn, logger=self.logger)

            vcc_capability_fqdn = csp_connector.get_attribute(
                "cspVccCapabilityAddress"
            ).value
            fsp_capability_fqdn = csp_connector.get_attribute(
                "cspFspCapabilityAddress"
            ).value

            return vcc_capability_fqdn, fsp_capability_fqdn
        except Exception as e:
            raise ValueError(f"Error retrieving capability FQDNs: {e}") from e

    def _add_capability_components(
        self: MidCapabilityConnectionManager,
        capability_fqdn_tuple: Tuple[str, str],
    ) -> Tuple[Component, Component]:
        """
        Create CSP VCC and FSP capability proxy components if not already
        defined.

        :param capability_fqdn_tuple: A tuple containing FQDNs for VCC and FSP
            capabilities.

        :return: Tuple of VCC and FSP capability components.
        :raises: ValueError if errro in creating components.
        """
        try:
            vcc_capability = Component(
                fqdn=capability_fqdn_tuple[0],
                name="vcc-capability",
                logger=self.logger,
            )

            fsp_capability = Component(
                fqdn=capability_fqdn_tuple[1],
                name="fsp-capability",
                weight=0,
                logger=self.logger,
            )
            return vcc_capability, fsp_capability
        except Exception as e:
            raise ValueError(
                f"Error creating capability components: {e}"
            ) from e

    # pylint: disable=unused-argument
    def connection_callback(
        self: MidCapabilityConnectionManager,
        status: TaskStatus | None = None,
        progress: int | None = None,
        result: Any | None = None,
        error: tuple[DevError] | None = None,
        **kwargs: Any,
    ) -> None:
        """
        Callback invoked at connection completion.

        If the connection to the Mid CSP.LMC Capability devices completes
        with success, some attributes used by the CSP subarray to monitor
        the assigned resources and FSP information, are subscribed.
        """
        if status == TaskStatus.IN_PROGRESS:
            return
        if status == TaskStatus.COMPLETED:
            if result[0] == ResultCode.OK:
                self._subscribe_callback(
                    self._capabilities[self.vcc_capability_fqdn],
                    ["vccsFqdnAssigned", f"vccsSubarray{self.sub_id:02d}"],
                )
                self._subscribe_callback(
                    self._capabilities[self.fsp_capability_fqdn],
                    ["fspsFqdnAssigned", f"fspsSubarray{self.sub_id:02d}"],
                )
                self.is_communicating = True
            else:
                self.logger.info("Connection to capabilities devices failed!")

    def create_capability_task(
        self: MidCapabilityConnectionManager, cm
    ) -> Task:
        """
        Create the task to connect to the Mid CSP Capability devices.

        :param cm: The CSP device ComponentManager.

        :return: A Task object for initialization, None otherwise.
        :raises: ValueError if the task cannot be created.
        """
        capability_task = None
        try:
            capability_fqdn_tuple = self._retrieve_capability_fqdn()
            (
                self.vcc_capability_fqdn,
                self.fsp_capability_fqdn,
            ) = capability_fqdn_tuple
            capability_components = self._add_capability_components(
                capability_fqdn_tuple
            )
            self._capabilities = dict(
                zip(capability_fqdn_tuple, capability_components)
            )
            leaf_tasks = self._create_leaf_tasks(capability_components, cm)

            if leaf_tasks:
                capability_task = Task(
                    name="Connect-capabilities",
                    subtasks=leaf_tasks,
                    task_type=TaskExecutionType.PARALLEL,
                    logger=self.logger,
                )
        except (ValueError, KeyError) as e:
            self.logger.warning(
                f"Error creating the task to connect to Capabilities: {e}"
            )
            warn_msg = (
                "Failure in creating the task to connect to "
                "the Mid CSP Capability devices."
                f" Got error: {e}"
            )
            raise ValueError(warn_msg) from e
        return capability_task

    def _create_leaf_tasks(
        self: MidCapabilityConnectionManager, components: List[Component], cm
    ) -> List[LeafTask]:
        """
        Create leaf tasks for connecting to individual components.

        :param components: List of capability components.
        :param cm: The CSP device ComponentManager.

        :return: List of LeafTask objects.
        """
        leaf_tasks = []
        for component in components:
            if not component.proxy:
                leaf_tasks.append(
                    LeafTask(
                        name=f"connect-{component.name}",
                        target=cm,
                        command_name="_connect_device",
                        task_type=TaskExecutionType.INTERNAL,
                        argin=component,
                        logger=self.logger,
                    )
                )
        return leaf_tasks
