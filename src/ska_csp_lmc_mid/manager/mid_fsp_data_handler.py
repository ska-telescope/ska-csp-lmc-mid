"""FSP Capability Info Manager Class."""

from __future__ import annotations  # allow forward references in type hints

import json
import logging
import threading
from typing import Any, Callable, Dict, List, Optional

import numpy as np
from numpy import ndarray
from ska_control_model import AdminMode, HealthState
from tango import DevState

from ska_csp_lmc_mid.constant import FspModes

module_logger = logging.getLogger(__name__)

# pylint: disable=too-many-instance-attributes
# pylint: disable=broad-except
# pylint: disable=too-many-nested-blocks


class FspDataHandler:
    """
    Class handling the update and access to the dictionary with the
    overall FSPs status.
    """

    def __init__(
        self: FspDataHandler,
        task_callback: Optional[Callable] = None,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """
        Class to handle the FSPs information.

        :param task_callback: the method invoked to update a device
            attribute
        :param logger: The device or python logger if default is None.
        """
        self._update_device_property = task_callback
        self._json_dict = {}
        self.list_of_state = []
        self.list_of_health_state = []
        self.list_of_admin_mode = []
        self.list_of_function_mode = []
        self.assigned_fsps_to_subarray_map = {}
        self._data_lock = threading.Lock()
        # pylint: disable=fixme
        # TODO: get the number of deployed subarrays and FSPs to fix
        # the matrix dimension.For the moment we use 16 subarrays and 4 FSPs.
        self.deployed_subarray = 16
        self.subarray_membership_fqdn = np.empty(
            (self.deployed_subarray, 4), dtype="U20"
        )  # matrix of string

        # self._submit_lock = threading.Lock()
        self.logger = logger or module_logger

        self.attrname_to_enum_class_map = {
            "healthstate": HealthState,
            "adminmode": AdminMode,
            # "simulationmode": SimulationMode,
            "functionmode": FspModes,
        }
        self.attrname_to_key_map = {
            "state": "state",
            "healthstate": "health_state",
            "adminmode": "admin_mode",
            "functionmode": "function_mode",
            "subarraymembership": "subarray_membership",
            # "simulationmode": "simulation_mode",
        }

    @property
    def json_dict(self: FspDataHandler) -> Dict:
        """
        Return the internal dictionary.
        """

        return self._json_dict

    @property
    def unavailable(self: FspDataHandler) -> List[int]:
        """
        Return the list of the FSPs that are not available.

        An FSP is currently considered unaviable when its state is not
        ON, OFF or ALARM. In ALARM the state is ON but one ore more
        attributes of the devices are ouside alarm ranges.
        """
        fsp_state = self._get_device_dict_entry("state")
        list_availability = [
            index + 1
            for index, state in enumerate(fsp_state)
            if state not in ["ON", "OFF", "ALARM"]
        ]
        return list_availability

    @property
    def fqdn(self: FspDataHandler) -> List[str]:
        """
        Return a list with the FSPs FQDNs.
        """
        return self._get_device_dict_entry("fqdn")

    #
    # Class public methods
    #
    def init(self: FspDataHandler, list_of_fsp_devices):
        """
        Initialize the internal FSP json dictionary.
        """
        self._json_dict["fsps_deployed"] = len(list_of_fsp_devices)
        self._json_dict["fsp"] = []
        for fsp_fqdn in list_of_fsp_devices:
            fsp_fqdn_member = int(fsp_fqdn[-2:])
            fsp_dict = {
                "fsp_id": fsp_fqdn_member,
                "fqdn": fsp_fqdn,
                "state": DevState.UNKNOWN,
                "health_state": HealthState.UNKNOWN.value,
                "admin_mode": AdminMode.OFFLINE.value,
                "function_mode": FspModes.IDLE.value,
                "subarray_membership": "",
            }
            self._json_dict["fsp"].append(fsp_dict)

    def get_device_dict_entry(self: FspDataHandler, dict_key):
        """
        Return the information stored in the dictionary for the
        given key.
        The access to the internal dictionary is regulated by a lock.

        :param dict_key: the dictionary key whose value is requested.
        """

        with self._data_lock:
            return self._get_device_dict_entry(dict_key)

    def _get_device_dict_entry(self: FspDataHandler, dict_key):
        """
        Return the information stored in the dictionary for the
        given key.
        The access to the internal dictionary is regulated by a lock.

        :param dict_key: the dictionary key whose value is requested.
        """
        list_of_values = []
        if self._json_dict:
            for item in self._json_dict["fsp"]:
                try:
                    list_of_values.append(item[dict_key])
                except KeyError as e:
                    self.logger.warning(f"Invalid fsp dictionary key: {e}")
        return list_of_values

    def update_fsp_dict_entry(
        self, attr_name, entry_value, fsp_pos: Any = None
    ):
        """Update the content of the internal dictionary.
        The access to the internal dictionary is controlled by a lock

        :param entry_key: the key name of the entry to update into the
        dictionary
        :param entry_value: the value to store
        :param fsp_pos: if not None, the index of the fsp record into the
        dictionary list
        """
        with self._data_lock:
            self._update_fsp_dict_entry(attr_name, entry_value, fsp_pos)

    def _update_fsp_dict_entry(
        self, attr_name, entry_value, fsp_pos: Any = None
    ):
        """
        Update the content of the internal dictionary without locking.

        :param entry_key: the key name of the entry to update into the
        dictionary
        :param entry_value: the value to store
        :param fsp_pos: if not None, the index of the fsp record into
        the dictionary list
        """
        try:
            entry_key = self.attrname_to_key_map[attr_name]
            if fsp_pos is not None:

                # get the key dictionary associated with the attribute
                attr_name = [
                    _attr_name
                    for _attr_name, value in self.attrname_to_key_map.items()
                    if value == entry_key
                ]
                attr_name = attr_name[0]
                if attr_name in self.attrname_to_enum_class_map:
                    entry_value = self.attrname_to_enum_class_map[attr_name](
                        entry_value
                    ).name
                self._json_dict["fsp"][fsp_pos][entry_key] = entry_value
            else:
                self._json_dict[entry_key] = entry_value
        except KeyError as err:
            self.logger.warning(f"Failed to update fsp entry: {err}")

    def update(
        self: FspDataHandler, device_fqdn: str, attr_name: str, attr_value: Any
    ) -> None:
        """Method invoked when an event is received.

        Update the FSP internal json structure and invoke the device
        update method for the following attributes of the device:
        - fspDeployed
        - fspFunctionMode
        - fspAdminMode
        - fspHealthState
        - fspState

        :param device_fqdn: the FSP device FQDN
        :param attr_name: the name of the attribute with event
        :param attr_value: the value the attribute with event
        """
        # fsp_num = int(device_fqdn[-2:])
        with self._data_lock:
            # get the fsp position inside the internal dict
            fsp_idx = self.fqdn.index(device_fqdn)
            # get the fsp record
            item = self._json_dict["fsp"][fsp_idx]
            # update array element according to fqdn
            if device_fqdn == item["fqdn"]:
                fsp_id = item["fsp_id"]

                if attr_name == "state":
                    attr_value = str(attr_value)

                if attr_name in {
                    "state",
                    "healthstate",
                    "adminmode",
                    # "simulationmode",
                    "functionmode",
                }:
                    self._update_fsp_dict_entry(attr_name, attr_value, fsp_idx)
                    self._update_fsp_data_in_subarray_map(
                        fsp_id, attr_name, attr_value
                    )
                elif attr_name == "subarraymembership":
                    current_subarray_membership = item["subarray_membership"]
                    new_subarray_membership = self._calculate_fsp_membership(
                        attr_value
                    )
                    self._update_subarray_map(
                        fsp_id,
                        new_subarray_membership,
                        current_subarray_membership,
                    )
                    item["subarray_membership"] = new_subarray_membership
                else:
                    raise ValueError("Items not updated!")

                func_name = f"_update_{attr_name.lower()}"
                try:
                    f = self.__class__.__dict__[func_name]
                    f(self)
                except KeyError as e:
                    self.logger.error(
                        ("Failure in invoking callback method: %s", e)
                    )
            else:
                self.logger.warning(
                    "Error in updating %s. Entry mismatch received: %s"
                    " extracted: %s",
                    attr_name,
                    device_fqdn,
                    item["fqdn"],
                )

    def fsp_in_function_mode(self: FspDataHandler, function_mode) -> List[int]:
        """
        Return the list of the FSP id that are in the requested
        function mode.

        :param function_mode: the requested functionMode (label)
        """
        list_of_fsp = []
        for item in self._json_dict["fsp"]:
            if item["function_mode"] == function_mode:
                fsp_state = item["state"]
                if fsp_state in ["ON", "OFF", "ALARM"]:
                    list_of_fsp.append(item["fsp_id"])
        # order the number
        return list(set(list_of_fsp))

    #
    # Class protected methods
    #
    def _calculate_fsp_membership(
        self: FspDataHandler, attr_value: ndarray
    ) -> None:
        """Convert the input attribute value in string format. i.e. "1,3,5"

        :param attr_value: subarray membership value written as list

        :return fsp_subarray_membership: attr_value written as string format.
        i.e. '2,3,4'
        """
        fsp_subarray_membership = ""

        if attr_value is not None and (
            isinstance(attr_value, ndarray) and attr_value.ndim
        ):
            attr_value_list = attr_value.tolist()
            fsp_subarray_membership = ",".join(str(e) for e in attr_value_list)
        return fsp_subarray_membership

    def _update_fsp_fqdn_assigned(
        self: FspDataHandler,
        sub_id_add: List[int],
        sub_id_remove: List[int],
        device_fqdn: str,
    ) -> None:
        """
        Update the internal list with the FQDNs of assigned FSPs.

        :param sub_id_add: the value of the subarray membership
        written as ndarray [1,2] from which add the fsp fqdn
        :param sub_id_remove: the value of the subarray membership
        written as ndarray [1,2] from which remove the fsp fqdn
        :param device_fqdn: the FSP fqdn that have to be assigned
        to subarrays
        """
        fsp_idx = self.fqdn.index(device_fqdn)
        sub_membership = self.subarray_membership_fqdn
        for i in sub_id_add:
            sub_membership[i - 1][fsp_idx] = device_fqdn
        for i in sub_id_remove:
            # fsp has no more affiliation with this subarray
            sub_membership[i - 1][fsp_idx] = ""
        self.subarray_membership_fqdn = sub_membership

    def _update_functionmode(self: FspDataHandler) -> None:
        """
        Update the list with the FSPs functionMode.

        This method is invoked when an event on the functionMode
        attribute is received.
        """
        list_of_function_mode = self._get_device_dict_entry("function_mode")
        if list_of_function_mode != self.list_of_function_mode:
            self.list_of_function_mode = list_of_function_mode
            if self._update_device_property:
                self._update_device_property(
                    "fspFunctionMode", list_of_function_mode
                )

    def _update_state(self: FspDataHandler) -> None:
        """
        Update the list with the FSPs state.

        This method is invoked when an event on the State
        attribute is received.
        """
        list_of_state = self._get_device_dict_entry("state")
        if list_of_state != self.list_of_state:
            self.list_of_state = list_of_state
            if self._update_device_property:
                self._update_device_property("fspState", list_of_state)

    def _update_healthstate(self: FspDataHandler) -> None:
        """
        Update the list with the FSPs health state.

        This method is invoked when an event on the healthState
        attribute is received.
        """
        list_of_health = self._get_device_dict_entry("health_state")
        if list_of_health != self.list_of_health_state:
            self.list_of_health_state = list_of_health
            if self._update_device_property:
                self._update_device_property("fspHealthState", list_of_health)

    def _update_adminmode(self: FspDataHandler) -> None:
        """
        Update the list with the FSPs admin mode.

        This method is invoked when an event on the adminMode
        attribute is received.
        """
        list_of_admin_mode = self._get_device_dict_entry("admin_mode")
        if list_of_admin_mode != self.list_of_admin_mode:
            self.list_of_admin_mode = list_of_admin_mode
            if self._update_device_property:
                self._update_device_property(
                    "fspAdminMode", list_of_admin_mode
                )
                self._update_functionmode()

    def _update_subarraymembership(self: FspDataHandler) -> None:
        """
        Update the list with the FPs sub-array membership
        """
        list_of_subarraymembership = self._get_device_dict_entry(
            "subarray_membership"
        )
        if self._update_device_property:
            self._update_device_property(
                "fspsSubarrayMembership", list_of_subarraymembership
            )
            self._update_device_property(
                "fspsFqdnAssigned", self.subarray_membership_fqdn.tolist()
            )

    def _update_fsp_data_in_subarray_map(
        self: FspDataHandler, fsp_id: int, attr_name: str, value: Any
    ):
        """Update the assigned_fsps_to_subarray_map attribute with the
        input value

        :param fsp_id: the FSP id whose data has been updated.
        :param attr_name: tha attribute name whose value has to be update
            inside the map
        :param value: the updated attribute value.
        """
        # retrieve the key dictionary associated to the attribute
        attr_key = self.attrname_to_key_map[attr_name]
        # if the attribute is a defined enum, try to recover the label
        if attr_name in self.attrname_to_enum_class_map:
            value = self.attrname_to_enum_class_map[attr_name](value).name
        try:
            device_name = f"fsp_{fsp_id:02d}"
            for (
                sub_id,
                device_dict,
            ) in self.assigned_fsps_to_subarray_map.items():
                if device_name in device_dict.keys():
                    self.assigned_fsps_to_subarray_map[sub_id][device_name][
                        attr_key
                    ] = value
                # push the change event of fspsSubarrayXY attribute
                if self._update_device_property:
                    self._update_device_property(
                        f"fspsSubarray{sub_id:02d}",
                        json.dumps(self.assigned_fsps_to_subarray_map[sub_id]),
                    )
        except Exception as err:
            self.logger.error(
                f" Error updating data for {device_name} in subarray map:"
                f"{err}"
            )

    def _update_subarray_map(
        self: FspDataHandler,
        fsp_id: int,
        new_membership: str,
        current_membership: str,
    ):
        """
        Update the internal JSON map with FSPs status organized on subarray
        basis.

        Example:

        {1, {
            'fsp_01': {
                'state':'ON',
                'health_state': 'DEGRADED',
                'function_mode: 'CORR'
                },
            'fsp_03': {
                'state':'ON',
                'health_state': 'OK',
                'function_mode: 'CORR'
                },
            }
            },
         2, {
            'fsp_01': {
                'state':'ON',
                'health_state': 'DEGRADED',
                'function_mode: 'CORR'
                },
            'fsp_04': {
                'state':'ON',
                'health_state': 'OK',
                'function_mode: 'CORR'
                },
            }
            }
        }

        :param fsp_id: the FSP id whose data has been updated.
        :param new_membership: the updated string with the comma separated list
            of subarrays to which the FSP belongs to. Es: "1,2,4"
        :param current_membership: the string with the comma separated list
            of subarrays to which the FSP belonged before the event. Es: "1,"
        """
        subarray_map = self.assigned_fsps_to_subarray_map
        for device_item in self._json_dict["fsp"]:
            if device_item["fsp_id"] == fsp_id:
                device_fqdn = device_item["fqdn"]
                device_name = f"fsp_{fsp_id:02d}"
                try:
                    (
                        sub_id_list_add,
                        sub_id_list_rem,
                    ) = self._calculate_membership_update(
                        new_membership, current_membership
                    )
                    if sub_id_list_add:
                        fsp_dict = {
                            device_name: {
                                "state": device_item["state"],
                                "admin_mode": device_item["admin_mode"],
                                "health_state": device_item["health_state"],
                                "function_mode": device_item["function_mode"],
                            }
                        }
                        for sub_id in sub_id_list_add:
                            if sub_id not in subarray_map:
                                subarray_map[sub_id] = {}
                            subarray_map[sub_id].update(fsp_dict)

                    for sub_id in sub_id_list_rem:
                        if device_name in subarray_map[sub_id].keys():
                            subarray_map[sub_id].pop(device_name)
                    self._update_fsp_fqdn_assigned(
                        sub_id_list_add, sub_id_list_rem, device_fqdn
                    )

                    self.assigned_fsps_to_subarray_map = subarray_map

                    for sub_id in sub_id_list_add + sub_id_list_rem:
                        if self._update_device_property:
                            dump = json.dumps(
                                self.assigned_fsps_to_subarray_map[sub_id]
                            )
                            self._update_device_property(
                                f"fspsSubarray{sub_id:02d}", dump
                            )
                except (KeyError, TypeError) as e:
                    self.logger.warning(f"Error in updating subarray map: {e}")

    def _calculate_membership_update(
        self: FspDataHandler, new_membership: str, current_membership: str
    ):
        """Given the new list of subarray membership of an FSP and the
        currently in use subarray mambership, the function calculates how the
        subarray membership for the considered device (FSP) is changed.

        :param new_membership: updated command separated string with the list
            of the subarrays that have been assigned to use an FSP
            (i.e. "1,2,3").
        :param current_membership: original comma separated string with the
            list of the subarrays that were assigned to use an FSP
            (i.e "2,4").

        :return: a 2-lists tuple with the subarray ids from which an FSP
            device should be removed or added. i.e add=[1, 3] rem=[4]

        """

        def from_strings_to_list(comma_string: str) -> List[int]:
            """
            Utility function to convert a string with a list in a list.

            :param comm_string: string with a comma separated list of entries

            :return: a list of integer
            """
            return [int(i) for i in comma_string.split(",") if i]

        new_membership_list = from_strings_to_list(new_membership)
        current_membership_list = from_strings_to_list(current_membership)
        membership_to_add = list(
            set(new_membership_list).difference(current_membership_list)
        )
        membership_to_remove = list(
            set(current_membership_list).difference(new_membership_list)
        )
        return membership_to_add, membership_to_remove
