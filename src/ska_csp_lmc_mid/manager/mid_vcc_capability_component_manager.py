"""VCC Capability Device Component Manager Class."""

from __future__ import annotations  # allow forward references in type hints

import json
import logging
from typing import Callable, List, Optional

# from ska_control_model import HealthState, ResultCode, TaskStatus
from ska_csp_lmc_common.capability import CapabilityComponentManager
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)

from ska_csp_lmc_mid import release
from ska_csp_lmc_mid.manager.mid_vcc_data_handler import VccDataHandler

# pylint: disable=unused-argument
# pylint: disable=too-many-arguments
# pylint: disable=too-many-public-methods
# pylint: disable=broad-except
# pylint: disable=logging-fstring-interpolation
# pylint: disable=too-many-instance-attributes


module_logger = logging.getLogger(__name__)

"""
Json with the VCC information

  "vccs_deployed": 4,
  "vcc_fqdn_member_order": [1,2,3,4]
  "vcc": [
    {
      "vcc_id": 1,
      "receptor_id": "SKA001",
      "fqdn": "xxx.yyy.zzz",
      "band_available": [2,]
      "band_active": "2",
      "state": "ON",
      "health_state": "OK",
      "admin_mode": "ONLINE",
      "simulation_mode": "TRUE",
      "subarray_membership": 1
    },
    {
      "vcc_id": 2
      "receptor_id": "SKA022",
      "fqdn": "xxx.yyy.zzz",
      "band_available": [2,]
      "band_active": "2",
      "state": "ON",
      "health_state": "OK",
      "admin_mode": "ONLINE",
      "simulation_mode": "TRUE",
      "subarray_membership": 1
    },
    {
      "vcc_id": 3,
      "receptor_id": "SKA103",
      "fqdn": "xxx.yyy.zzz",
      "band_available": [2,]
      "band_active": "2",
      "state": "ON",
      "health_state": "OK",
      "admin_mode": "ONLINE",
      "simulation_mode": "TRUE",
      "subarray_membership": 1
    },
    {
      "vcc_id": 4,
      "receptor_id": "SKA104",
      "fqdn": "xxx.yyy.zzz",
      "band_available": [2,]
      "band_active": "2",
      "state": "ON",
      "health_state": "OK",
      "admin_mode": "ONLINE",
      "simulation_mode": "TRUE",
      "subarray_membership": 1
    }
  ]
"""


class MidVccCapabilityComponentManager(CapabilityComponentManager):
    """Class for Mid Csp Vcc Capability Component Manager."""

    def __init__(
        self: MidVccCapabilityComponentManager,
        properties: ComponentManagerConfiguration,
        max_workers: Optional[int] = 5,
        update_device_property_cbk: Optional[Callable] = None,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """Initialize the VccCapability Component Manager Class

        :param properties: A class instance whose properties are the
            device properties.
        :param max_workers: the number of worker threads used by the
            ComponentManager ThreadPool
        :param update_device_property_cbk: The device method invoked
                to update the attributes. Defaults to None.
        :param logger: The device or python logger if default is None.
        """
        super().__init__(
            properties=properties,
            update_device_property_cbk=update_device_property_cbk,
            logger=logger,
        )
        self.list_of_receptor_id = []
        self.device_properties = properties
        self._init_version_info(release)

    @property
    def devices_simulation_mode(
        self: MidVccCapabilityComponentManager,
    ) -> List[str]:
        """
        Return the list of VCCs simulation mode.
        """
        return self._data_handler.get_device_dict_entry("simulation_mode")

    @property
    def devices_receptor_id(
        self: MidVccCapabilityComponentManager,
    ) -> list[str]:
        """
        Return the receptor id list.
        """
        return list(
            map(str, self._data_handler.get_device_dict_entry("receptor_id"))
        )

    @property
    def devices_band_active(
        self: MidVccCapabilityComponentManager,
    ) -> list[str]:
        """
        Return a list with active band of each VCC.
        """
        return self._data_handler.get_device_dict_entry("band_active")

    @property
    def devices_subarray_membership(
        self: MidVccCapabilityComponentManager,
    ) -> list[str]:
        """
        Return a list with subaray membership for each VCC.
        """
        return self._data_handler.get_device_dict_entry("subarray_membership")

    @property
    def devices_fqdn_assigned(self: MidVccCapabilityComponentManager) -> str:
        """
        Return a JSON formatted string with the vcc fqdn assigned to
        each subarray.

        vcc_fqdn_Assigned is a python dictionary. For example:
        {
            1: ['mid_csp_cbf/vcc/002', 'mid_csp_cbf/vcc/003'],
            2: [],
            3: [],
            4: ['mid_csp_cbf/vcc/001']
        }
        """
        return json.dumps(self._data_handler.vccs_fqdn_assigned)

    @property
    def devices_fqdn_member_order(
        self: MidVccCapabilityComponentManager,
    ) -> List[int]:
        """
        Return the VCC fqdn_member order (it is the map order for all
        property lists).
        """
        return self._data_handler.vcc_fqdn_member_order

    @property
    def devices_deployed(self: CapabilityComponentManager) -> int:
        """
        Return the number of devices deployed in the system.
        """
        return self._data_handler.json_dict["vccs_deployed"]

    @property
    def subarray_vcc_map(self: MidVccCapabilityComponentManager) -> List[str]:
        """
        Return the list of VCCs state.
        """
        return self._data_handler.assigned_vccs_to_subarray_map

    #
    # Protected Methods
    #

    def _create_init_callbacks(self: MidVccCapabilityComponentManager):
        """Method not used by this device"""
        self.logger.info("Method not used")

    def _create_stop_callbacks(self: MidVccCapabilityComponentManager):
        """Method not used by this device"""
        self.logger.info("Method not used")

    def _instantiate_data_handler(
        self: MidVccCapabilityComponentManager,
        logger: Optional[logging.Logger] = None,
    ):
        """Instantiate the specific data handler that is meant to
        update the capability attributes"""
        return VccDataHandler(self._update_device_property_cbk, self.logger)

    def _populate_capability_specifics(
        self: MidVccCapabilityComponentManager,
    ) -> None:
        """Specify the specific variables of the Capability"""

        self.master_device_name = "cbf-controller"
        self.property_master_fqdn = "CspCbf"
        self.master_attribute_name = "VCC"
        self.subsystem_name = "vcc-"

        self.attributes_to_be_subscribed.extend(
            [
                "subarrayMembership",
                "frequencyBand",
                "simulationmode",
                "dishID",
            ]
        )

    def _retrieve_devices_fqdn(self: MidVccCapabilityComponentManager) -> None:
        """
        Retrieve the FQDNs of the subsystem devices to be connected
        By default, the fqdn list can be retrieved directly from
        `master device`. Otherwise the method has to be specialized

        return: the list of the subsistems FQDN deployed
        """
        devices_fqdn = []
        try:
            self._connect_to_master_device(
                self.property_master_fqdn, self.master_device_name
            )
            self.logger.info(
                f"Connection to {self.master_device_name} succeeded"
            )
            vcc_properties = self.master_dev.proxy.get_property(
                self.master_attribute_name
            )
            devices_fqdn = vcc_properties["VCC"]

            self.logger.info(
                "List of deployed subsystems retrieved: " f"{devices_fqdn}"
            )
        except Exception as exc:
            raise ValueError(
                f"Connection to {self.master_device_name} failed"
            ) from exc
        return devices_fqdn

    def _read_device_id(
        self: MidVccCapabilityComponentManager, device_online
    ) -> None:
        """Retrieve the ID of an online device.

        :param device_online: one of the online pst component
        """
        # for fqdn in device_online:
        #     vcc_id = int(fqdn[-3:])
        #     self._device_id.append(str(vcc_id))
        #     try:
        #         vcc_idx = self.devices_fqdn.index(fqdn)
        #         self._data_handler.update_vcc_dict_entry(
        #             # "receptorid", vcc.proxy.receptorID, vcc_pos=vcc_idx
        #             "receptorid",
        #             device_online.read_attr("receptorID"),
        #             vcc_pos=vcc_idx,
        #         )
        #     except Exception:
        #         self.logger.warning(
        #             f"vcc {device_online} has not the receptorId attribute"
        #         )

    def _read_device_version(
        self: MidVccCapabilityComponentManager, device_online
    ) -> None:
        """Read the version attribute from an online VCC (base classes)

        :param device_online: one of the online VCC
        """
        sw_version = "NA"
        hw_version = "NA"
        fw_version = "NA"
        try:
            # sw_version = device_online.proxy.versionId
            sw_version = device_online.read_attr("versionId")
        except Exception:
            self.logger.warning(
                f"VCC {device_online} has not the versionId attribute"
            )
        try:
            # fw_version = device_online.proxy.fw_version
            fw_version = device_online.read_attr("fwVersion")
        except Exception:
            self.logger.warning(
                f"VCC {device_online} has not the fwVersion attribute"
            )
        try:
            # hw_version = device_online.proxy.hw_version
            hw_version = device_online.read_attr("hwVersion")
        except Exception:
            self.logger.warning(
                f"VCC {device_online} has not the hwVersion attribute"
            )

        self._sw_version.append(sw_version)
        self._fw_version.append(fw_version)
        self._hw_version.append(hw_version)

    def off(
        self: MidVccCapabilityComponentManager,
        task_callback: Optional[Callable] = None,
    ):
        """Method not used by this device"""
        self.logger.info("Method not used")

    def on(
        self: MidVccCapabilityComponentManager,
        task_callback: Optional[Callable] = None,
    ):
        """Method not used by this device"""
        self.logger.info("Method not used")

    def standby(
        self: MidVccCapabilityComponentManager,
        task_callback: Optional[Callable] = None,
    ):
        """Method not used by this device"""
        self.logger.info("Method not used")

    def stop_communicating(
        self: MidVccCapabilityComponentManager,
        task_callback: Optional[Callable] = None,
    ):
        """Method not used by this device"""
        self.logger.info("Method not used")

    def abort_commands(
        self: MidVccCapabilityComponentManager,
        task_callback: Optional[Callable] = None,
    ):
        """Method not used by this device"""
        self.logger.info("Method not used")

    def get_vcc_data_of_subarray(
        self: MidVccCapabilityComponentManager, subarray_num
    ) -> str:
        """
        Return the relevent data for all the VCCs assigned to requested
        subarray in string format.
        Note:  {'vcc_1':{"state": DevState.OFF,
                            "health_state": 'UNKNOWN'}
                'vcc_2':{"state": DevState.OFF,
                            "health_state": 'UNKNOWN'}
                }
        """
        try:
            subarray_data = self._data_handler.assigned_vccs_to_subarray_map[
                subarray_num
            ]
            return json.dumps(subarray_data)
        except KeyError as err:
            self.logger.warning(
                f"Error in getting vcc data for subarray:{err}"
            )
            return "{}"
        except TypeError as e:
            self.logger.error(f"Error in serialize subarray_data JSON: {e}")
            return "{}"
