"""Specialization Class for Mid Csp Component Manager."""

from __future__ import annotations

import json
from typing import Callable, Dict, List, Optional

from ska_control_model import ObsMode, ResultCode, TaskStatus
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.manager.subarray_component_manager import (
    CSPSubarrayComponentManager,
)
from ska_csp_lmc_common.subarray import (
    SubarrayHealthModel,
    SubarrayObsStateModel,
    SubarrayOpStateModel,
)
from ska_csp_lmc_common.utils.cspcommons import get_interface_version

from ska_csp_lmc_mid import release
from ska_csp_lmc_mid.commands.mid_command_map import (
    MidCommandMap,  # midSubarrayCommandMap
)
from ska_csp_lmc_mid.manager.mid_capability_connection import (
    MidCapabilityConnectionManager,
)
from ska_csp_lmc_mid.mid_component_factory import MidObservingComponentFactory
from ska_csp_lmc_mid.subarray.mid_json_config_parser import (
    MidJsonConfigurationParser,
)

# pylint: disable=too-many-arguments
# pylint: disable=abstract-method
# pylint: disable=too-many-instance-attributes
# pylint: disable=logging-fstring-interpolation


class MidCspSubarrayComponentManager(CSPSubarrayComponentManager):
    """Specialization Class for Mid Csp Component Manager."""

    def __init__(
        self: MidCspSubarrayComponentManager,
        health_model: SubarrayHealthModel,
        op_state_model: SubarrayOpStateModel,
        obs_state_model: SubarrayObsStateModel,
        properties: ComponentManagerConfiguration,
        update_device_property_cbk: Callable = None,
        logger=None,
    ) -> None:
        super().__init__(
            health_model,
            op_state_model,
            obs_state_model,
            properties,
            update_device_property_cbk=update_device_property_cbk,
            logger=logger,
        )
        self._assigned_vcc = []
        self._assigned_fsp = []
        self._capability_manager = None
        self.device_properties = properties
        self._assigned_vcc_state = []
        self._assigned_vcc_health_state = []
        self._assigned_fsp_state = []
        self._assigned_fsp_health_state = []
        self._assigned_fsp_function_mode = []
        self._dish_vcc_cfg = ""
        self._init_version_info(release)
        self._command_map = MidCommandMap().get_subarray_command_map()

    @property
    def assigned_vcc(self: MidCspSubarrayComponentManager) -> List[str]:
        """
        Return the list with the FQDNs of the VCCs assigned
        to the subarray.
        """
        return self._assigned_vcc

    @property
    def assigned_fsp(self: MidCspSubarrayComponentManager) -> List[str]:
        """
        Return the list with the FQDNs of the VCCs assigned
        to the subarray.
        """
        return self._assigned_fsp

    @property
    def capability_connected(self: MidCspSubarrayComponentManager) -> bool:
        """
        Wether the subarray is connected to the Capability devices
        """
        if self._capability_manager:
            return self._capability_manager.is_communicating
        return False

    @property
    def assigned_vcc_state(self: MidCspSubarrayComponentManager) -> List[int]:
        """
        Return the list with the assigned VCCs state
        to the subarray.
        """
        return self._assigned_vcc_state

    @property
    def assigned_vcc_health_state(
        self: MidCspSubarrayComponentManager,
    ) -> List[str]:
        """
        Return the list of the assigned VCCs health state
        to the subarray.
        """
        return self._assigned_vcc_health_state

    @property
    def assigned_fsp_state(self: MidCspSubarrayComponentManager) -> List(int):
        """
        Return the list with the assigned FSPs state
        to the subarray.
        """
        return self._assigned_fsp_state

    @property
    def assigned_fsp_health_state(
        self: MidCspSubarrayComponentManager,
    ) -> List[str]:
        """
        Return the list of the assigned FSPs health state
        to the subarray.
        """
        return self._assigned_fsp_health_state

    @property
    def assigned_fsp_function_mode(
        self: MidCspSubarrayComponentManager,
    ) -> List[int]:
        """
        Return the list of the assigned FSPs function mode
        to the subarray.
        """
        return self._assigned_fsp_function_mode

    @property
    def dish_vcc_cfg(
        self: MidCspSubarrayComponentManager,
    ) -> str:
        """
        Return the corresponding Cbf Subarray sysParam attribute
        """
        return self._dish_vcc_cfg

    def start_communicating(self: MidCspSubarrayComponentManager):
        """Overwrite the CSPSubarrayComponentManager function in order to
        add the capabilities subscriptions
        """
        super().start_communicating()

    def update_assigned_resources(
        self: MidCspSubarrayComponentManager,
        resource_type: str,
        resource_value: List[str],
    ) -> Dict:
        """
        Method to update the assignResources attribute of the subarray.

        This method is called by the update_property method of the subarray.
        The update_property method is invoked when an event is raised on an
        attribute.
        Each time the subarray affiliation of a VCC/FSP changes, an event
        is raised on the vccFqdnAssigned/fspsFqdnAssigned attribute of the
        VCC/FSP Capability device. These attributes are subscribed for changes
        by the subarray after the communication with its subordinate
        sub-systems has been completed.

        This method overrides the one defined in the parent class. In future
        when Capability devices are implemented also for PSS and PST beams,
        all the resources will be updated here.

        :param resource_type: the key name of resources type (VCC, FSS, etc)
        :param resource_value: the list of assigned resources' FQDNs.

        raise: ValueError if the resource type is not valid
        """
        if resource_type == "pst":
            pass
        elif resource_type == "fsp":
            self._assigned_fsp = self._update_fsp_resources_attribute(
                resource_value
            )
        elif resource_type == "vcc":
            self._assigned_vcc = self._update_vcc_resources_attribute(
                resource_value
            )
        else:
            raise ValueError(f"Invalid resources {resource_type}")
        self._assigned_resources = sorted(
            self._assigned_fsp + self._assigned_vcc + self._assigned_pst_beam
        )
        # build the property to update
        return {"assignedResources": self._assigned_resources}

    def update_assigned_resources_status(
        self: MidCspSubarrayComponentManager,
        resource_type: str,
        resource_value: str,
    ) -> Dict:
        """Update the status of the resources assigned to the subarray.
        This method reports the main resources attribute information as:
        state, health, etc.

        :param resource_type: type of the device (fsp/vcc) owner
        of the attribute that has to be updated.
        :param resource_value: value of the attribute that has
        to be updated.

        :return a dictionary with the attribute name (key) and the value to
        update
        """
        if resource_type == "fsp":
            dict_to_update = self._update_fsps_subarray(resource_value)
        elif resource_type == "vcc":
            dict_to_update = self._update_vccs_subarray(resource_value)
        else:
            raise ValueError(f"Invalid resources {resource_type}")

        return dict_to_update

    def force_health_state(
        self: MidCspSubarrayComponentManager, faulty_flag: bool
    ) -> None:
        """
        Force the halthstate of the device (Override the base method).
        Update the dishVccMap reading the content on the CBF subarray.

        :param faulty_flag: whether to force the healthState to FAILED or not.
        """
        super().force_health_state(faulty_flag)
        self._dish_vcc_cfg = self.online_components[
            self.CbfSubarrayFqdn
        ].dish_vcc_config
        if self._update_device_property_cbk:
            self._update_device_property_cbk(
                "dishVccConfig", self._dish_vcc_cfg
            )

    def update_obs_mode(
        self: MidCspSubarrayComponentManager, resources_to_send: Dict
    ):
        """Specialize method to set the obsMode based on Mid specific
        resources.

        param: resources_to_send Resources to be parsed for obsMode
        """

        def get_subarray_obs_mode_pre4(resources_to_send):
            """
            Evaluate the value of the obsmode starting from the configuration
            files with schemas prior to version 4.0.
            """
            if (
                any(
                    fsp["function_mode"] == "CORR"
                    for fsp in resources_to_send[self.CbfSubarrayFqdn]["cbf"][
                        "fsp"
                    ]
                )
            ) and ObsMode.IMAGING not in self._obs_modes:
                self.logger.info("Updating MID obs mode with IMAGING")
                self._obs_modes.append(ObsMode.IMAGING)
            if (
                any(
                    fsp["function_mode"] == "PSS-BF"
                    for fsp in resources_to_send[self.CbfSubarrayFqdn]["cbf"][
                        "fsp"
                    ]
                )
            ) and ObsMode.PULSAR_SEARCH not in self._obs_modes:
                self.logger.info("Updating MID obs mode with PULSAR_SEARCH")
                self._obs_modes.append(ObsMode.PULSAR_SEARCH)

        def get_subarray_obs_mode(resources_to_send):
            """
            Evaluate the value of the obsmode starting from the configuration
            files with schemas greater or equal to version 4.0.
            """
            if (
                "correlation"
                in resources_to_send[self.CbfSubarrayFqdn]["midcbf"]
                and ObsMode.IMAGING not in self._obs_modes
            ):
                self.logger.info("Updating MID obs mode with IMAGING")
                self._obs_modes.append(ObsMode.IMAGING)

        if resources_to_send and self.CbfSubarrayFqdn in resources_to_send:
            major, minor = get_interface_version(
                resources_to_send[self.CbfSubarrayFqdn]
            )
            if (major, minor) < (4, 0):
                get_subarray_obs_mode_pre4(resources_to_send)
            else:
                get_subarray_obs_mode(resources_to_send)
            self.logger.info(f"Obsmodes: {self._obs_modes}")
            super().update_obs_mode(resources_to_send)

    def _update_fsps_subarray(
        self: MidCspSubarrayComponentManager,
        value: str,
    ) -> None:
        """
        Method to update the status information of the FSPs
        assigned to the subarray.

        This method is called by the update_property method of the subarray.
        The update_property method is invoked when an event is raised on an
        attribute.

        :param value: string containing the FSPs assigned to the
        subarray and its parameters (state, health_state, function_mode).

        :return: a dictionary {attr_name:attr_value}
        """
        dict_to_update = {}
        try:
            value_json = json.loads(value)
            state = []
            health_state = []
            function_mode = []
            for data in value_json.values():
                state.append(data["state"])
                health_state.append(data["health_state"])
                function_mode.append(data["function_mode"])
            if self._assigned_fsp_state != state:
                self._assigned_fsp_state = state
                property_name = "assignedFspState"
                property_value = state
                dict_to_update[property_name] = property_value

            if self._assigned_fsp_health_state != health_state:
                self._assigned_fsp_health_state = health_state
                property_name = "assignedFspHealthState"
                property_value = health_state
                dict_to_update[property_name] = property_value

            if self._assigned_fsp_function_mode != function_mode:
                self._assigned_fsp_function_mode = function_mode
                property_name = "assignedFspFunctionMode"
                property_value = function_mode
                dict_to_update[property_name] = property_value
        except (ValueError, KeyError) as err:
            self.logger.error("Failure in getting FSPs information: %s", err)

        return dict_to_update

    def _update_vccs_subarray(
        self: MidCspSubarrayComponentManager,
        value: str,
    ) -> Dict:
        """
        Method to update the status of the VCCs assigned to the subarray.

        This method is called by the update_property method of the subarray.
        The update_property method is invoked when an event is raised on an
        attribute.

        :param value: JSON string containing the VCCs assigned to the
            subarray and its parameters (state, health_state, function_mode).

        :return: dictionary with the attribute name to update (dict key)
            and the new value (dict value)
        """
        dict_to_update = {}
        try:
            value_json = json.loads(value)
            state = []
            health_state = []
            for data in value_json.values():
                state.append(data["state"])
                health_state.append(data["health_state"])
            if self._assigned_vcc_state != state:
                self._assigned_vcc_state = state
                property_name = "assignedVccState"
                property_value = state
                dict_to_update[property_name] = property_value

            if self._assigned_vcc_health_state != health_state:
                self._assigned_vcc_health_state = health_state
                property_name = "assignedVccHealthState"
                property_value = health_state
                dict_to_update[property_name] = property_value

        except (ValueError, KeyError) as err:
            self.logger.error("Failure in getting VCC information: %s", err)
        return dict_to_update

    def _observing_factory(self: MidCspSubarrayComponentManager, logger):
        """
        Specialize the factory to create the Low CSP sub-system
        components.
        """
        return MidObservingComponentFactory(
            self.device_properties.CspController, logger
        )

    def _json_configuration_parser(
        self: MidCspSubarrayComponentManager, argin: dict
    ) -> MidJsonConfigurationParser:
        """
        Configure the class that handles the parsing of the input JSON files
        sent as argument of AssignResources, Configure and Scan.
        This class is specialized in Mid.CSP and Low.CSP.

        param: argin The input dictionary
        """
        return MidJsonConfigurationParser(self, argin, self.logger)

    def _update_vcc_resources_attribute(
        self: MidCspSubarrayComponentManager, vcc_assigned_json: str
    ) -> List[str]:
        """
        Read the currently assigned VCcs FQDN.
        Retrieve the information from the VCCCapability device and update the
        list of assigned resources.
        """
        assigned_vcc = []
        try:
            if vcc_assigned := json.loads(vcc_assigned_json):
                vcc_list = vcc_assigned[str(self.sub_id)]
                assigned_vcc = sorted(vcc_list)
        except (ValueError, KeyError) as err:
            self.logger.error("Failure in getting VCC information: %s", err)
        return assigned_vcc

    def _update_fsp_resources_attribute(
        self: MidCspSubarrayComponentManager, fsp_capability_fqdn_list
    ) -> List[str]:
        """
        Read the currently assigned FSPs FQDN.

        Retrieve the information from the FspCapability device and update the
        list of assigned resources on the subarray.
        """
        assigned_fsp = []
        try:
            if fsp_capability_fqdn_list:
                # skip empty entries
                fsp_assigned = [
                    fsp
                    for fsp in fsp_capability_fqdn_list[self.sub_id - 1]
                    if fsp
                ]
                assigned_fsp = sorted(fsp_assigned)
        except Exception as err:  # pylint: disable=broad-exception-caught
            self.logger.error(
                "Failure in getting FSP information: %s",
                err,
            )
        return assigned_fsp

    def _start_communicating_to_capabilities(
        self: MidCspSubarrayComponentManager,
        task_callback: Optional[Callable] = None,
    ):
        """
        Establish communication with the capability devices

        :param task_callback: the method to invoke when the task completes.
        """
        try:
            capability_task = self._create_capability_task()
            if capability_task:
                self._main_task_executor.submit_main_task(
                    capability_task,
                    self._capability_manager.connection_callback,
                )
        except Exception as err:  # pylint: disable=broad-exception-caught
            warning_msg = (
                f"Internal SW error executing task `Connect-capabilities`"
                f" on Mid CSP Subarray{self.sub_id}. Received exception: {err}"
            )
            self.logger.error(warning_msg)
            if task_callback:
                task_callback(
                    result=(ResultCode.FAILED, warning_msg),
                    status=TaskStatus.COMPLETED,
                )

    def _start_communicating(
        self: MidCspSubarrayComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> None:
        """
        Establish communication with the subordinate subsystems and then
        initiate the connection with the capability devices.

        :param task_callback: the method to invoke when the task completes.
            Since the this method is not invoked via an LRC command, the
            task_callback is in general None.
        """
        # the parent '_ start_communicating' method establisheds the connection
        # with the subordinate sub-systems
        super()._start_communicating()
        self._start_communicating_to_capabilities(task_callback)

    def _create_capability_task(  # pylint: disable=unused-argument
        self: MidCspSubarrayComponentManager,
    ):
        """
        Create the task to connect to the Mid CSP Capability devices.
        """
        self._capability_manager = MidCapabilityConnectionManager(
            self.device_properties,
            self.subscribe_attributes_for_change_events,
            self.logger,
        )

        self._capability_manager.set_sub_id(self.sub_id)
        capability_task = self._capability_manager.create_capability_task(self)
        return capability_task
