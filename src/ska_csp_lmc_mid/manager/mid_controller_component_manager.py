"""Specialization Class for Mid CSp Controller Component Manager."""

from __future__ import annotations  # allow forward references in type hints

import logging
import threading
import traceback
from typing import Any, Callable, Dict, List, Optional, Tuple

from ska_control_model import AdminMode, ResultCode, SimulationMode, TaskStatus
from ska_csp_lmc_common.commands.main_task_composer import MainTaskComposer
from ska_csp_lmc_common.commands.task import LeafTask, Task, TaskExecutionType
from ska_csp_lmc_common.component import Component
from ska_csp_lmc_common.controller import (
    ControllerHealthModel,
    ControllerOpStateModel,
)
from ska_csp_lmc_common.manager.controller_component_manager import (
    CSPControllerComponentManager,
)
from ska_csp_lmc_common.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_csp_lmc_common.utils.cspcommons import method_name
from ska_csp_lmc_common.utils.decorators import kwarged, reset_resources
from tango import DevError

from ska_csp_lmc_mid import release
from ska_csp_lmc_mid.commands.mid_command_map import MidCommandMap
from ska_csp_lmc_mid.mid_component_factory import MidControllerComponentFactory

# pylint: disable=abstract-method
# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals
# pylint: disable=broad-exception-caught
# pylint: disable=logging-fstring-interpolation


class MidCspControllerComponentManager(CSPControllerComponentManager):
    """Specialization Class for Mid CSp Controller Component Manager."""

    def __init__(
        self: MidCspControllerComponentManager,
        op_state_model: ControllerOpStateModel,
        health_model: ControllerHealthModel,
        properties: ComponentManagerConfiguration,
        update_device_property_cbk: Callable = None,
        logger: logging.Logger = None,
    ) -> None:
        super().__init__(
            op_state_model,
            health_model,
            properties,
            update_device_property_cbk=update_device_property_cbk,
            logger=logger,
        )
        self._init_version_info(release)
        self._alarm_dish_id_cfg = False
        self._programmed_cbf_simulation_mode = SimulationMode.TRUE
        self._command_map = MidCommandMap().get_controller_command_map()

    def _controller_factory(
        self: MidCspControllerComponentManager, logger: logging.Logger
    ) -> MidControllerComponentFactory:
        """Specialize the CSP Sub-systems component constructor for Mid
        Telescope.

        :param logger: The device or python logger if default is None.
        :type logger: :py:class:`logging.Logger`, or an object that
            implements the same interface
        """
        return MidControllerComponentFactory(logger)

    def _create_start_subtasks(
        self: MidCspControllerComponentManager, component: Component
    ) -> List[Task]:
        """
        Override the method in Common.

        Create the task to initialize the connection
        with one sub-system.

        :param component: the target of the task

        :return: the parent task.
        """
        leaf_tasks = []
        if not component.proxy:
            lft_0 = LeafTask(
                f"connect-{component.name}",
                self,
                "_connect_device",
                task_type=TaskExecutionType.INTERNAL,
                argin=component,
                logger=self.logger,
            )
            leaf_tasks.append(lft_0)
        # the simulationMode is written only on the Mid
        # CBF Controller
        if "cbf" in component.fqdn:
            lft_1 = LeafTask(
                f"set-simulationmode-{component.name}",
                self,
                "_set_simulationmode",
                task_type=TaskExecutionType.INTERNAL,
                argin=component,
                logger=self.logger,
            )
            leaf_tasks.append(lft_1)

        lft_2 = LeafTask(
            f"set-adminmode-{component.name}",
            self,
            "_set_adminmode",
            task_type=TaskExecutionType.INTERNAL,
            argin=component,
            logger=self.logger,
        )
        leaf_tasks.append(lft_2)

        task_serial = Task(
            f"Start-{component.name}",
            leaf_tasks,
            TaskExecutionType.SEQUENTIAL,
            logger=self.logger,
        )
        return task_serial

    def _set_simulationmode(
        self: MidCspControllerComponentManager, component: Component
    ) -> Tuple[TaskStatus, ResultCode]:
        """
        Write the value of the simulationMode on Mid CBF controller.

        :return: a tuple with the task status and the result code.
        """
        task_status = TaskStatus.COMPLETED
        result_code = ResultCode.FAILED
        if component.proxy:
            try:
                component.write_attr(
                    "simulationMode",
                    self._programmed_cbf_simulation_mode,
                )
                task_status = TaskStatus.COMPLETED
                result_code = ResultCode.OK
            except Exception as e:
                self.logger.error(
                    f"Exception occurred:\n {traceback.format_exc()}"
                )
                msg = (
                    "Error in writing the simulationMode "
                    f" on component {component.fqdn}"
                )
                raise ValueError(msg) from e
        return (task_status, result_code)

    # -----------------
    # Device Properties
    # -----------------

    @property
    def alarm_dish_id_cfg(self: MidCspControllerComponentManager) -> bool:
        """Return whether the loaddishcfg command failed.

        :return: Return an alarm flag that is rised whether the
        loaddishcfg command failed.
        """
        return self._alarm_dish_id_cfg

    # ---------------
    # Public methods
    # ---------------

    def loaddishcfg(
        self: MidCspControllerComponentManager,
        mapping_dict: Dict,
        task_callback: Optional[Callable[[], None]] = None,
    ) -> Tuple[TaskStatus, str]:
        """Run the command loaddishcfg on the Mid CBF Controller.

        :param mapping_dict: the input dish-vcc map.

        :return: a tuple with the result code and the message.
        """
        return self._task_submitter(
            task_name=method_name(),
            task_callback=task_callback,
            argin=mapping_dict,
        )

    def set_cbf_simulation_mode(
        self: CSPControllerComponentManager, value
    ) -> None:
        """Set the Simulation Mode of CBF Controller.

        :param value: desired Simulation Mode of the device.
        """
        try:
            if self._admin_mode in (AdminMode.ONLINE, AdminMode.ENGINEERING):
                self.online_components[self.CbfCtrlFqdn].write_attr(
                    "simulationMode", value
                )
                self._programmed_cbf_simulation_mode = value
            elif self._admin_mode == AdminMode.OFFLINE:
                self.logger.debug(
                    "Simulation mode set internally, will be written"
                    "to the device when communication is established."
                )
                self._programmed_cbf_simulation_mode = value
        except (KeyError, ValueError):
            self.logger.warning(
                "Failed writing the CBF controller simulation mode"
            )

    #
    # Protected methods
    #
    def _force_subarrays_health_state(
        self: MidCspControllerComponentManager, value: bool
    ):
        for fqdn, component in self.online_components.items():
            if fqdn in self.CspSubarrays:
                component.force_health_state(value)

    # def _is_loaddishcfg_failed(self: MidCspControllerComponentManager):
    def update_loaddishcfg(self: MidCspControllerComponentManager) -> None:
        """
        Verify whether the LoadDishCfg command has failed on the Mid CBF
        Controller. If so, the health state of the Mid CSP Subarrays is
        set to FAILED.

        :return: a tuple with the task status and the result, including the
            result code and message.
        """
        # retrieve the tracker of the main task to read the
        # current result of the command
        main_tracker = self._main_task_executor.main_task.tracker
        task_result = main_tracker.get_result()
        if task_result == ResultCode.FAILED:
            self._alarm_dish_id_cfg = True
            self._force_subarrays_health_state(True)
        elif task_result == ResultCode.OK:
            # when command ends with success
            # - the alarm flag is reset if already set from a previous
            #   command failure.
            # - the health faulty flag of each CSP Subarray is reset
            self._force_subarrays_health_state(False)
            if self._alarm_dish_id_cfg:
                self._alarm_dish_id_cfg = False

        if self._update_device_property_cbk:
            self._update_device_property_cbk(
                "alarmDishIdCfg", self._alarm_dish_id_cfg
            )
        # the internal task alway end with COMPLETED, OK?
        status = TaskStatus.COMPLETED
        result = ResultCode.OK

        return status, result

    @reset_resources
    def _loaddishcfg(  # pylint: disable=unused-argument
        self: MidCspControllerComponentManager,
        task_name: str,
        argin: dict,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Create and submit the main task to load the Dish-VCC map
        onto the Mid CBF Controller.

        :param task_name: the name of the task
        :param argin: the dictionary with the dish-vcc map.
        :param task_callback: method called to update the task result
            related attributes
        """

        @kwarged
        def loaddishcfg_completed(  # pylint: disable=unused-argument
            status: TaskStatus | None = None,
            progress: int | None = None,
            result: Any | None = None,
            error: tuple[DevError] | None = None,
            **kwargs: Any,
        ) -> None:
            """
            Method invoked at command completion.
            When the execution of the command fails on Mid
            CBF Controller, the task status is reported as
            FAILED.

            :param status: the task status
            :param progress: the progress of execution as percentage.
            :param result: tuple with the task result code and the message
            :param error: the exception, if any.

            """

            if status and status in [TaskStatus.COMPLETED, TaskStatus.FAILED]:
                # status, result = self._is_loaddishcfg_failed()
                try:
                    source_dish_vcc_config = self.online_components[
                        self.CbfCtrlFqdn
                    ].source_dish_vcc_config
                    dish_vcc_config = self.online_components[
                        self.CbfCtrlFqdn
                    ].dish_vcc_config
                    if self._update_device_property_cbk:
                        self._update_device_property_cbk(
                            "sourceDishVccConfig", source_dish_vcc_config
                        )
                        self._update_device_property_cbk(
                            "dishVccConfig", dish_vcc_config
                        )

                except ValueError as exp:
                    self.logger.error(
                        f"Error in reading dish-vcc information: {exp}"
                    )
                finally:
                    try:
                        self.update_loaddishcfg()
                    except Exception as gen_exp:
                        self.logger.error(f"Error : {gen_exp}")

            task_callback(
                status=status,
                result=result,
            )

        try:
            self.resources[self.CbfCtrlFqdn] = argin

            composer = MainTaskComposer(
                task_name,
                self._command_map,
                self,
                task_callback,
            )
            main_task = composer.compose()
            if main_task and main_task.tracker.total_subtasks:
                self._main_task_executor.submit_main_task(
                    main_task,
                    loaddishcfg_completed,
                )

        # pylint: disable-next=broad-except
        except Exception as err:
            warning_msg = (
                f"Internal SW error executing in task {task_name} on CSP"
                f" Controller. Received exception: {err}"
            )
            self.logger.error(warning_msg)
            self.op_state_model.component_fault(True)
            self.health_model.component_fault(True)
            task_callback(
                status=TaskStatus.FAILED,
                result=(ResultCode.FAILED, warning_msg),
            )
