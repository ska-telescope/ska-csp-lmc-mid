"""Mid CSP utility"""

from ska_csp_lmc_mid.constant import (
    DISH_MKT_RANGE,
    DISH_RANGE_LEN,
    DISH_SKA_RANGE,
    DISH_TYPE,
)


def index_to_receptor_id(receptor_index):
    """Convert the receptor index value received from CBf to receptor id
        ascii according to ADR32

    :param receptor_index: receptor index value

    :return: the receptor id as ascii
    """

    # SKA receptor index start from 1 while MKT_RANGE start from 0
    receptor_index_int = int(receptor_index)
    if receptor_index_int in range(
        (DISH_MKT_RANGE[0] + 1), (DISH_MKT_RANGE[1] + 2)
    ):
        receptor_id = DISH_TYPE[0]
        receptor_range = str(receptor_index_int - 1)
    else:
        receptor_index_int = receptor_index_int - (DISH_MKT_RANGE[1] + 1)
        if receptor_index_int in range(
            DISH_SKA_RANGE[0], DISH_SKA_RANGE[1] + 1
        ):
            receptor_id = DISH_TYPE[1]
            # SKA range start from 1
            receptor_range = str(receptor_index_int)
        else:
            receptor_id = "DIDINV"
    if receptor_id != "DIDINV":
        receptor_range = receptor_range.rjust(DISH_RANGE_LEN, "0")
        receptor_id = receptor_id + receptor_range
    return receptor_id
