"""Handling the parsing of the configuration strings.for AssignResources,
ReleaseResources and Configure in Mid Telescope."""

from __future__ import annotations

import logging
from typing import Dict, Optional

from ska_csp_lmc_common.subarray.json_configuration_parser import (
    JsonConfigurationParser,
)
from ska_csp_lmc_common.utils.cspcommons import get_interface_version

# pylint: disable=use-dict-literal
# pylint: disable=broad-except
# pylint: disable=arguments-renamed
# pylint: disable=logging-fstring-interpolation


class MidJsonConfigurationParser(JsonConfigurationParser):
    """creates the input for Subsystem's ComponentCommand."""

    def __init__(
        self: JsonConfigurationParser,
        component_manager,
        json_config: dict,
        logger: Optional[logging.Logger] = None,
    ):
        super().__init__(component_manager, json_config, logger)
        # starting from version 4.0, the section cbf has been
        # renamed midcbf
        self.major, self.minor = get_interface_version(self.config)
        if (self.major, self.minor) >= (4, 0):
            self.key_to_subsystem_map["cbf"] = "midcbf"

    def configure(self: MidJsonConfigurationParser) -> Dict:
        """Retrieve the json script with the configuration for each CSP
        subordinate sub-system. The configuration specific for a sub-system is
        stored in a python dictionary, addressable via the FQDN of the sub-
        system.

        Override the parent class method.

        :return: on success a dict mapping keys (sub-system FQDN) to
            the corresponding sub-system configuration fetched.
            On failure the returned dict is empty.
        """
        self.logger.debug("Mid json parser for configure resources")
        config_dict = {}
        try:
            if cbf_json_script := self.generate_subsystem_config_dict("cbf"):
                config_dict[self.cm.CbfSubarrayFqdn] = cbf_json_script
            if pss_json_script := self.generate_subsystem_config_dict("pss"):
                config_dict[self.cm.PssSubarrayFqdn] = pss_json_script
        except Exception as err:
            self.logger.error(f"Got error: {err}")
        return config_dict

    def get_id(self: MidJsonConfigurationParser) -> str:
        """Return the json configuration id.
        Override the method in the base class.

        NOTE: this function can be called only after validation
        Version >= 2 implement ADR35 naming convention for JSON keywords.

        NOTE: this function should be deprecated and it is no longer used
        in Low. Backward compatibility is no longer a feature needed in CSP.LMC

        :return: the value of configuration id on success, otherwise an empty
            string.
        """
        config_id = ""
        try:
            key_id = "config_id" if self.major >= 2 else "id"

            # config_id field is not mandatory in the JSON script.
            # Need to check if it's available.

            if self.major >= 1:
                common_sec = self.get_section("common")
                config_id = common_sec[key_id]
            else:
                config_id = self.config[key_id]
        except (KeyError, ValueError):
            self.logger.debug("Configuration ID not specified")
        self.logger.debug(f"Configuration ID: {config_id}")
        return config_id

    def assignresources(self: MidJsonConfigurationParser) -> Dict:
        """Retrieve the json script to forward to each CSP sub-system device.

        Override the parent class method.

        :return: a dictionary with the extracted resources for each sub-system
            on success, otherwise an empty one if the json is not valid.
            The dictionary is addressable via the sub-system fqdn.
        """
        resources = self.config
        resources_to_send = {}

        try:
            assert self.cm.CbfSubarrayFqdn
            sub_id = resources["subarray_id"]
            if sub_id != int(self.cm.sub_id):
                self.logger.error(
                    "Wrong value for the subarray ID: "
                    f"{sub_id}/{self.cm.sub_id}"
                )
            else:
                if "dish" in resources:
                    resources_to_send[self.cm.CbfSubarrayFqdn] = resources[
                        "dish"
                    ]["receptor_ids"]
                else:
                    self.logger.warning("No dish entry specified in the JSON")
                if self.cm.PssSubarrayFqdn and "pss" in resources:
                    resources_to_send[self.cm.PssSubarrayFqdn] = {}
                    resources_to_send[self.cm.PssSubarrayFqdn]["common"] = {
                        "subarray_id": sub_id
                    }
                    resources_to_send[self.cm.PssSubarrayFqdn][
                        "pss"
                    ] = resources["pss"]
                else:
                    self.logger.info(
                        "PssSubarray not deployed or no entry for Pss"
                    )

                if self.cm.PstBeamsFqdn and "pst" in resources:
                    beams_ids = resources["pst"]["beams_id"]
                    for fqdn in self.cm.PstBeamsFqdn:
                        if int(fqdn[-2:]) in beams_ids:
                            resources_to_send[fqdn] = {"subarray_id": sub_id}
                else:
                    self.logger.warning(
                        "No Pst Beam deployed or no entry for Pst beams"
                    )
        # pylint: disable-next=broad-except
        except (KeyError, AssertionError) as err:
            self.logger.error(
                "Parser for assignresources failed with error:"
                f"{err} in resources :{resources}"
            )
        return resources_to_send

    def releaseresources(self: MidJsonConfigurationParser) -> Dict:
        """Retrieve the json script to forward to each CSP sub-system device.

        Override the parent class method.

        :return: a dictionary with the extracted resources for each sub-system
            on success, otherwise an empty one if the json is not valid.
            The dictionary is addressable via the sub-system fqdn.
        """
        resources = self.config
        resources_to_remove = {}
        try:
            # 09/28/2023: Partial release of resources is not yet supported
            # by TM and there is no Telescope Model schema for this command.
            # !!!!!
            # For the moment we assume to receive a json file equal to
            # the one of the AssignResources.
            # !!!!!
            # common_dict = self.get_section("common")
            # sub_id = int(common_dict["subarray_id"])
            sub_id = int(resources["subarray_id"])
            if sub_id != int(self.cm.sub_id):
                self.logger.error(
                    "Wrong value for the subarray ID: "
                    f"{sub_id}/{self.cm.sub_id}"
                )
            else:
                if "dish" in resources:
                    resources_to_remove[
                        self.cm.device_properties.CbfSubarray
                    ] = resources["dish"]
                if "pss" in resources:
                    resources_to_remove[
                        self.cm.device_properties.PssSubarray
                    ] = resources["pss"]
                if "pst" in resources:
                    beams_ids = resources["pst"]
                    for fqdn in self.cm.PstBeamsFqdn:
                        if int(fqdn[-2:]) in beams_ids:
                            resources_to_remove[fqdn] = {"subarray_id": sub_id}

        # pylint: disable-next=broad-except
        except Exception:
            warning_msg = f"Mid ReleaseResources: wrong input args {resources}"
            self.logger.error(warning_msg)
        return resources_to_remove
