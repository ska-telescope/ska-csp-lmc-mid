"""Specialization of the PssSubarray component class for the Mid.CSP."""

from __future__ import annotations

from typing import Any

from ska_csp_lmc_common.subarray import PssSubarrayComponent

# pylint: disable=arguments-renamed


class MidPssSubarrayComponent(PssSubarrayComponent):
    """Specialization of the PssSubarray component class for the Mid.CSP."""

    def __init__(self: MidPssSubarrayComponent, fqdn: Any, logger=None):
        super().__init__(fqdn, logger=logger)

        self.csp_ctrl_proxy = None
