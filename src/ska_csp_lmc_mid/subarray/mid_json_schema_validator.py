"""
JSON Validator for MID domain
"""

from __future__ import annotations

import logging
from typing import Any

import jsonschema
from ska_csp_lmc_common.subarray.json_schema_validator import CspJsonValidator
from ska_csp_lmc_common.utils.cspcommons import get_interface_version

# pylint: disable=invalid-name
# pylint: disable=no-member
# pylint: disable=missing-function-docstring
# pylint: disable=unused-argument

module_logger = logging.getLogger(__name__)


class CspMidJsonValidator(
    CspJsonValidator
):  # pylint: disable=too-few-public-methods
    """
    An argument validator for MID JSON commands.

    It performs coherence checks in the MID domain.

    This class is a derivation from JsonValidator of ska_tango_base
    """

    # pylint: disable=super-init-not-called
    def __init__(
        self: CspMidJsonValidator,
        command_name: str,
        input_schema=None,
        attrs_dict: dict = None,
        logger: logging.Logger | None = None,
    ) -> None:
        """
        Initialise a new instance.

        :param command_name: name of the command to be validated.
        :param input_schema: an optional schema against which the JSON
            argument should be validated. If not provided, a warning is
            issued.
        :param attrs_dict: an optional dictionary, providing the key
            device attributes that need to be checked in the JSON file
        :param logger: a logger for this validator to use
        """
        # self._command_name = command_name
        # self.logger = logger
        # self._schema = input_schema or self.DEFAULT_SCHEMA
        # self._attrs_dict = attrs_dict
        super().__init__(command_name, input_schema, attrs_dict, logger)

    def validate(
        self: CspMidJsonValidator,
        *args: Any,
        **kwargs: Any,
    ) -> tuple[tuple[Any, ...], dict[str, Any]]:
        """
        Validate the command arguments.

        :param args: positional args to the command
        :param kwargs: keyword args to the command

        :returns: validated args and kwargs
        """

        # Perform common coherence check at first
        _, decoded_dict = super().validate(*args, **kwargs)

        if self._command_name.lower() == "assignresources":
            self._validate_mid_assignresources(decoded_dict, self._attrs_dict)
        elif self._command_name.lower() == "configure":
            self._validate_mid_configure(decoded_dict, self._attrs_dict)
        elif self._command_name.lower() == "releaseresources":
            self._validate_mid_releaseresources(decoded_dict, self._attrs_dict)

        if "interface" not in decoded_dict:
            warning_msg = (
                f"JSON argument to command {self._command_name} "
                "will only be validated as a dictionary."
            )
            jsonschema.validate(decoded_dict, self._schema)
            if self.logger:
                self.logger.warning(warning_msg)

        return (), decoded_dict

    def _validate_mid_assignresources(
        self: CspMidJsonValidator, decoded_dict: dict, attrs_dict: dict
    ):
        """
        Coherence checks for the assingresources command

        :param decoded_dict: json input file decoded as a dictionary
        """
        # check if the subarray id matches
        if attrs_dict is not None:
            sub_id = int(decoded_dict["subarray_id"])
            attr_sub_id = int(attrs_dict["subarray_id"])
            if sub_id != attr_sub_id:
                self._log_and_raise_dict_error(
                    ValueError,
                    f"Wrong value for the subarray ID: {sub_id}/{attr_sub_id}",
                )
        # Check if dish field is present
        self._validate_section(decoded_dict, "dish")

    def _validate_mid_configure(
        self: CspMidJsonValidator, decoded_dict: dict, attrs_dict: dict
    ):
        """
        Coherence checks for the configure command

        :param decoded_dict: json input file decoded as a dictionary
        """
        major, minor = get_interface_version(decoded_dict)
        cbf_section_name = "cbf"
        if (major, minor) >= (4, 0):
            cbf_section_name = "midcbf"
        self._validate_section(decoded_dict, cbf_section_name)

    def _validate_mid_releaseresources(
        self: CspMidJsonValidator, decoded_dict: dict, attrs_dict: dict
    ):
        """
        Coherence checks for the releaseresources command

        :param decoded_dict: json input file decoded as a dictionary
        """

    def _validate_section(self, decoded_dict: dict, section_name: str):
        """
        Helper method to check dish field.

        :param decoded_dict: json subarray subsection decoded as a dictionary
        :param section_name: name of the section to be retrieved
        """
        if section_name not in decoded_dict:
            self._log_and_raise_dict_error(
                KeyError, f"{section_name} field not found"
            )
