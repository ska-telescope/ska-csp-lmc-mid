"""Specialization of the CbfSubarray component class for the Mid.CSP."""

from __future__ import annotations

import logging
from typing import Any, Callable, List, Optional

import tango
from ska_csp_lmc_common.connector import Connector
from ska_csp_lmc_common.subarray import CbfSubarrayComponent
from tango import EventData

from ska_csp_lmc_mid.constant import (
    DISH_MKT_RANGE,
    DISH_RANGE_LEN,
    DISH_SKA_RANGE,
    DISH_TYPE,
    DISH_TYPE_LEN,
)

# pylint: disable=broad-except
# pylint: disable=arguments-renamed
# pylint: disable=no-member
# pylint: disable=invalid-name


class MidCbfSubarrayComponent(CbfSubarrayComponent):
    """Specialization of the CbfSubarray component class for the Mid.CSP."""

    def __init__(
        self: MidCbfSubarrayComponent,
        fqdn: str,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """Initialize the MidCbfSubarrayComponent.

        :param fqdn: The Mid.CBF subarray TANGO Device Fully Qualified Domain
            Name.
        :param logger: The device or python logger if default is None.
        """
        super().__init__(fqdn, logger=logger)
        self.csp_ctrl_connector = None
        # subscribe following attribute. It is commented since mid CBD has
        # not defined them as push attributes.
        self.attrs_for_change_events = [
            # 'receptors',
            # 'sysParam',
        ]
        self.map_cbf_csp_attr_name = {
            "sysParam": "dishVccConfig",
        }

    @property
    def assigned_receptors(
        self: MidCbfSubarrayComponent,
    ) -> List[str]:
        """Return the information about the receptors assigned to the subarray.

        :return: The list of receptors currently assigned to Mid Cbf
            subarray on success, otherwise an empty list.
        """
        return self.read_attr("receptors")

    @property
    def dish_vcc_config(
        self: MidCbfSubarrayComponent,
    ) -> str:
        """Return the information about mapping between DishID and VCCs.

        :return: The DishID-VCC mapping
        """
        return self.read_attr("sysParam")

    ###################
    # Protected methods
    ###################

    def _push_event(
        self: MidCbfSubarrayComponent,
        recv_event: EventData,
        device_attr_name=None,
    ) -> None:
        """Extend the component event callback to specifie the correct
        attribute name in case there is a mismach between the CBF component
        and CSP tango device"""

        token_position = recv_event.attr_name.rfind("/")
        # !!!!
        # NOTE: need to save the position of attr_name to avoid
        # conflicts between python-format and python-lint
        # !!!!
        first_char_position = token_position + 1
        attr_name = recv_event.attr_name[first_char_position:]
        if attr_name in self.map_cbf_csp_attr_name:
            attr_name = self.map_cbf_csp_attr_name[attr_name]
        super()._push_event(recv_event, attr_name)

    # ----------- Assign/Release receptors helper functions --------------#

    def _assign_receptors_validator(
        self: MidCbfSubarrayComponent, receptor_list: Any
    ) -> List[str]:
        """Helper method to validate the list of receptors received as argument
        of the *AssignResources command*, filtering out the receptors IDs that
        cannot be assigned to the subarray because already assigned or not
        available.

        The method gets the input list and checks the required receptors IDs
        against the list of available receptors. This list includes the
        receptors that:

            * have a valid network link with the corresponding VCC
            * the connected VCC is enabled and working.
            * is not assigned to another subarray.

        :param receptor_list: list of receptors IDs to be assigned to the
            Mid.CBF subarray, as specified by the input argument of the
            *AssignResources* method.

        :return: the list of valid receptors IDs to assign to the Mid.CBF
            Subarray on success, otherwise an empty list.
        """
        receptors_to_assign = []
        if not receptor_list:
            self.logger.warning("Receptor list is empty")
        else:
            # remove receptors id duplication from the input list
            # receptor_list = set(receptor_list)
            for item in receptor_list:
                if (
                    item not in receptors_to_assign
                    and self._receptor_id_validation(item)
                ):
                    validated_receptor_id = self._receptor_id_available(
                        receptor_id=item, check_activated=True
                    )

                    if validated_receptor_id:
                        receptors_to_assign.append(validated_receptor_id)
                    else:
                        self.logger.warning(
                            f"Receptor id: {item} not available"
                        )
                else:
                    self.logger.warning(
                        f"Receptor id {item} invalid or duplicated"
                    )
        return receptors_to_assign

    def _get_csp_ctrl_connector(self: MidCbfSubarrayComponent):
        """
        Method that create the CSP controller proxy if not already defined
        and return it.
        """
        if not self.csp_ctrl_connector:
            self.csp_ctrl_connector = Connector(
                self.csp_ctrl_fqdn, logger=self.logger
            )
        return self.csp_ctrl_connector

    def _receptor_id_available(
        self: MidCbfSubarrayComponent,
        receptor_id: str,
        check_activated: Optional[bool] = True,
    ) -> str:
        """
        Method that performs some the folliwing check:
            - the receptor id is stored in the available lists
            - the receptor is already assigned to a subarray

        :param receptor_id: Ascii receptor id (i.e. SKA001, MKT000)
        :param check_activated: optional flag. If True additional checks are
            performed on the receptor_id

        :return: valid ascii receptor id if valid. Else empty string.

        """
        returned_receptor_id = ""
        if check_activated:
            try:
                # Information about the available receptors are currently
                # retrieved from the Mid.CSP Controller.
                # As soon as the CSP Capabilities are implemented, the same
                # information are read from them.
                self.logger.debug(
                    "Mid CSP Controller fqdn:" f" {self.csp_ctrl_fqdn}"
                )
                csp_ctrl_connector = self._get_csp_ctrl_connector()

                # get the list of the receptors not assigned to any subarray
                available_receptors = csp_ctrl_connector.get_attribute(
                    "unassignedReceptorIDs"
                ).value
                self.logger.debug(
                    "Available_receptors: " f"{available_receptors}"
                )
                if not any(available_receptors):
                    self.logger.warning(
                        "No available receptor to add to subarray"
                        f" {self.capability_id}"
                    )
                # check if the required receptorID is available
                if receptor_id in available_receptors:
                    returned_receptor_id = receptor_id
                else:
                    self.logger.warning(
                        f"Receptor {receptor_id} already assigned "
                        "or not available"
                    )
            except tango.DevFailed as df:
                self.logger.error(
                    "Failure in getting receptors information:"
                    f" {str(df.args[0].reason)}"
                )
        else:
            returned_receptor_id = receptor_id
        return returned_receptor_id

    def _receptor_id_validation(
        self: MidCbfSubarrayComponent, receptor_id: str
    ) -> bool:
        """Verify if receptor name is compliant with ADR32
        Dish type: SKA dish [SKA] and MeerKAT dish [MKT].
        MeerKAT range 0 to 63
        SKA range 1 to 133
        i.e: MKT063, SKA001

        :param receptor_id: Ascii receptor id (i.e SKA001, MKT000)

        :return: True if receptor name is compliant to ADR32
        """
        return_value = False
        try:
            if isinstance(receptor_id, str) and len(receptor_id) == (
                DISH_RANGE_LEN + DISH_TYPE_LEN
            ):
                receptor_type = receptor_id[:DISH_TYPE_LEN]
                receptor_value = int(receptor_id[DISH_RANGE_LEN:])
                if receptor_type == DISH_TYPE[0] and receptor_value in range(
                    DISH_MKT_RANGE[0], (DISH_MKT_RANGE[1] + 1)
                ):
                    return_value = True
                elif receptor_type == DISH_TYPE[1] and receptor_value in range(
                    DISH_SKA_RANGE[0], (DISH_SKA_RANGE[1] + 1)
                ):
                    return_value = True
        except Exception as error:
            self.logger.error(f"Failure verifying the receptor name: {error}")
        return return_value

    def _remove_receptors_validator(
        self: MidCbfSubarrayComponent,
        receptor_list: List[
            str,
        ],
    ) -> List[str]:
        """Method to filter out the receptors IDs that do not belong to the
        subarray. Validate the list of receptors received as argument of the.

        *RemoveResources command*, filtering out the receptors IDs that do not
        belong to the subarray.

        :param receptor_list: list of receptors IDs to remove from the CBF
            subarray.

        :return: the list of valid receptors IDs to remove from the MID CBF
            Subarray on success, otherwise an empty list.
        """
        receptor_to_release = []
        self.logger.debug(
            "receptor_to_release- to"
            f" remove:{receptor_list} assigned:{self.assigned_receptors}"
        )
        try:
            receptor_to_release = list(
                set(receptor_list) & set(self.assigned_receptors)
            )
        except Exception as e:
            self.logger.error(e)
        return receptor_to_release

    def _validated_receptors(
        self: MidCbfSubarrayComponent,
        resources: dict,
        validator_function: Callable,
    ) -> List[str]:
        """Return the list of the validated receptors IDs that are passed as
        input argument to the Mid.CBF subarray Add/RemoveReceptors command.

        :param resources: the input dictionary with the list of receptors to
            add/remove to/from the subarray.
        :param validator_function: The callback invoked to validate the list
            of receptors.

        :return: the list with the receptors to add/remove.
        """
        valid_receptor_ids = []
        try:
            # get the list of the receptors to add/remove to/from the subarray
            # _receptors_list = list(map(int, resources["receptor_ids"]))
            _receptors_list = resources["receptor_ids"]
            self.logger.info(f"input receptors list: {_receptors_list}")
            # validate  the receptors IDs
            valid_receptor_ids = validator_function(_receptors_list)
            self.logger.info(f"validated_receptors: {valid_receptor_ids}")
        except Exception as e:
            self.logger.error(f"Error in validate_receptors_list: {e}")
        return valid_receptor_ids
